package com.xsis.winners.xbc184.controller;
	import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.CategoryModel;
import com.xsis.winners.xbc184.model.IdleNewsModel;
import com.xsis.winners.xbc184.service.CategoryService;
import com.xsis.winners.xbc184.service.IdleNewsService;

	@Controller
	public class IdleNewsController extends UserController {
		
		@Autowired
		private IdleNewsService idleNewsService;
		
		@Autowired
		private CategoryService categoryService;
		
	/*
	 * private String categoryIdleNews; private String tittleIdleNews; private
	 * String contentIdleNews;
	 */

		@RequestMapping(value="idleNews")
		public String idleNews() {
			String jsp = "idleNews/idleNews";
			return jsp;
		}
		
		@RequestMapping(value="idleNews/cruds/add")
		public String idleNewsAdd(Model model) {
			
			List<CategoryModel> categoryModelList = new ArrayList<CategoryModel>(); 
			categoryModelList = this.categoryService.search(false);
			model.addAttribute("categoryModelList", categoryModelList);
			String jsp = "idleNews/cruds/add";
			return jsp;
		}
		
		@RequestMapping(value="idleNews/cruds/create")
		public String idleNewsCreate(HttpServletRequest request) {
			
			//get nilai dari jsp
			Integer categoryId = Integer.valueOf(request.getParameter("idCategory"));
			String tittleIdleNews = request.getParameter("tittleIdleNews");
			String contentIdleNews = request.getParameter("contentIdleNews");
			
			//set nilai ke variabel modelnya
			IdleNewsModel idleNewsModel = new IdleNewsModel();
			idleNewsModel.setCategoryId(categoryId);
			idleNewsModel.setTittleIdleNews(tittleIdleNews);
			idleNewsModel.setContentIdleNews(contentIdleNews);
			
			//save audit trail untuk created
			Integer xIdCreatedBy = this.userSearch().getIdUser();
			idleNewsModel.setxIdCreatedBy(xIdCreatedBy);
			idleNewsModel.setxCreatedDate(new Date());
			idleNewsModel.setIsDelete(false);
			
			//save
			this.idleNewsService.create(idleNewsModel);
			
			String jsp = "idleNews/idleNews";
			return jsp;
		}
		
		@RequestMapping(value="idleNews/cruds/list")
		public String idleNewsList(Model model) {
			List<IdleNewsModel> idleNewsModelList = new ArrayList<IdleNewsModel>();
			idleNewsModelList = this.idleNewsService.search();
			model.addAttribute("idleNewsModelList", idleNewsModelList);
			
			String jsp = "idleNews/cruds/list";
			return jsp;
		}
		
		
		@RequestMapping(value = "idleNews/cruds/edit")
		public String idleNewsEdit(HttpServletRequest request, Model model) {
			Integer idIdleNews = Integer.valueOf(request.getParameter("idIdleNews"));
			
			IdleNewsModel idleNewsModel = new IdleNewsModel();
			idleNewsModel = this.idleNewsService.searchId(idIdleNews);
					
			model.addAttribute("idleNewsModel", idleNewsModel);
				
			String jsp = "idleNews/cruds/edit"; 
			return jsp;
		}
		
		@RequestMapping(value="idleNews/cruds/update")
		public String idleNewsUpdate(HttpServletRequest request) {
			
			//get niali dari variabel name di jsp
			Integer idIdleNews = Integer.valueOf(request.getParameter("idIdleNews"));
			String tittleIdleNewsType = request.getParameter("tittleIdleNewsType");
			String categoryIdleNews = request.getParameter("categoryIdleNews");
			String contentIdleNews = request.getParameter("contentIdleNews");
			
			//set niali ke variabel modelnya
			IdleNewsModel idleNewsModel = new IdleNewsModel();
			idleNewsModel = this.idleNewsService.searchId(idIdleNews);
			
			idleNewsModel.setTittleIdleNews(tittleIdleNewsType);
			idleNewsModel.setCategoryIdleNews(categoryIdleNews);
			idleNewsModel.setContentIdleNews(contentIdleNews);
			
		/*
		 * idleNewsModel.setCategoryidIdleNews(categoryIdleNews);
		 * idleNewsModel.setTittleIdleNews(tittleIdleNews);
		 */
			//audit trail
			Integer xIdModifiedBy = this.userSearch().getIdUser();
			idleNewsModel.setxIdModifiedBy(xIdModifiedBy);
			idleNewsModel.setxModifiedDate(new Date());
			//save
			this.idleNewsService.update(idleNewsModel);
			
			String jsp = "idleNews/idleNews";
			return jsp;
		}
		
		@RequestMapping(value="idleNews/cruds/search/name")
		public String idleNewsSearchName(HttpServletRequest request, Model model) {
			String nameIdleNews= request.getParameter("nameIdleNewsTypeKey");
			List<IdleNewsModel> idleNewsModelList = new ArrayList<IdleNewsModel>();
			idleNewsModelList = this.idleNewsService.searchNama(nameIdleNews);
			
			model.addAttribute("idleNewsModelList", idleNewsModelList);
			
			String jsp = "idleNews/cruds/list";
			return jsp;
		}
			
		@RequestMapping(value="idleNews/cruds/remove")
		public String idleNewsRemove(HttpServletRequest request, Model model) {
			Integer idIdleNews = Integer.valueOf(request.getParameter("idIdleNews"));
			
			IdleNewsModel idleNewsModel = new IdleNewsModel();
			idleNewsModel = this.idleNewsService.searchId(idIdleNews);
			model.addAttribute("idleNewsModel", idleNewsModel);
			
			String jsp = "idleNews/cruds/remove";
			return jsp;
		}
		
		@RequestMapping(value="idleNews/cruds/delete")
		public String idleNewsDelete (HttpServletRequest request) {
			//get nilai dari jsp
			Integer idIdleNews = Integer.valueOf(request.getParameter("idIdleNews"));
			
			IdleNewsModel idleNewsModel = new IdleNewsModel();
			idleNewsModel = this.idleNewsService.searchId(idIdleNews);
			idleNewsModel.setIsDelete(true); // 1 artinya telah dihapus temporary
			
			//audit trail
			Integer xIdDeletedBy = this.userSearch().getIdUser();
			idleNewsModel.setxIdDeletedBy(xIdDeletedBy);
			idleNewsModel.setxDeletedDate(new Date());
			
			//save
			this.idleNewsService.deleteTemporary(idleNewsModel);
			String jsp = "idleNews/idleNews";
			return jsp;
		}

		@RequestMapping(value="idleNews/cruds/publish")
		public String idleNewsPublish (HttpServletRequest request) {
			//get nilai dari jsp
//			Integer idIdleNews = Integer.valueOf(request.getParameter("idIdleNews"));
			
//			IdleNewsModel idleNewsModel = new IdleNewsModel();
//			idleNewsModel = this.idleNewsService.searchId(idIdleNews);
//			idleNewsModel.setIsDelete(true); // 1 artinya telah dihapus temporary
			
			//audit trail
//			Integer xIdDeletedBy = this.userSearch().getIdUser();
//			idleNewsModel.setxIdDeletedBy(xIdDeletedBy);
//			idleNewsModel.setxDeletedDate(new Date());
			
			//save
//			this.idleNewsService.deleteTemporary(idleNewsModel);
			String jsp = "idleNews/cruds/publish";
			return jsp;
		}
	
		@RequestMapping(value="idleNews/cruds/published")
		public String idleNewsPublised(HttpServletRequest request) {
			//get nilai dari jsp
//			Integer idIdleNews = Integer.valueOf(request.getParameter("idIdleNews"));
//			
//			IdleNewsModel idleNewsModel = new IdleNewsModel();
//			idleNewsModel = this.idleNewsService.searchId(idIdleNews);
//			idleNewsModel.setIsDelete(true); // 1 artinya telah dihapus temporary
//			
//			//audit trail
//			Integer xIdDeletedBy = this.userSearch().getIdUser();
//			idleNewsModel.setxIdDeletedBy(xIdDeletedBy);
//			idleNewsModel.setxDeletedDate(new Date());
//			
//			//save
//			this.idleNewsService.deleteTemporary(idleNewsModel);
			String jsp = "idleNews/idleNews";
			return jsp;
		}
}
