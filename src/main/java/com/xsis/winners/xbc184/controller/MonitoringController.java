package com.xsis.winners.xbc184.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.BiodataModel;
import com.xsis.winners.xbc184.model.MonitoringModel;
import com.xsis.winners.xbc184.service.BiodataService;
import com.xsis.winners.xbc184.service.MonitoringService;

@Controller
public class MonitoringController extends AuditLogController {
	
	@Autowired
	private MonitoringService monitoringService;
	
	@Autowired
	private BiodataService biodataService;

	@RequestMapping(value="monitoring")
	public String monitoring() {
		String jsp = "monitoring/monitoring";
		return jsp;
	}
	
	@RequestMapping(value="monitoring/cruds/list")
	public String monitoringList(Model model) {
		List<MonitoringModel> monitoringModelList = new ArrayList<MonitoringModel>();
		monitoringModelList = this.monitoringService.search();
		model.addAttribute("monitoringModelList", monitoringModelList);
		
		String jsp = "monitoring/cruds/list";
		return jsp;
	}
	
	@RequestMapping(value="monitoring/cruds/add")
	public String monitoringAdd(Model model) {
		
		String jsp = "monitoring/cruds/add";
		this.biodataList(model);
		return jsp;
	}
	
	@RequestMapping(value="monitoring/cruds/create")
	public String monitoringCreate(HttpServletRequest request) throws ParseException {
		//get nilai dari jsp
		Integer idBiodata = Integer.valueOf(request.getParameter("idBiodata"));
		Date idleDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("idleDate"));
//		String idleDateStr = request.getParameter("idleDate");
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); /* hh-mm-ss,000000 */
//		Date idleDate = new Date();
//		try {
//			idleDate = sdf.parse(idleDateStr);
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
		String lastProject = request.getParameter("lastProject");
		String idleReason = request.getParameter("idleReason");
		
		//set nilai ke variabel modelnya
		MonitoringModel monitoringModel = new MonitoringModel();
		monitoringModel.setIdleDate(idleDate);
		monitoringModel.setLastProject(lastProject);
		monitoringModel.setIdleReason(idleReason);
		
		//set join table
		monitoringModel.setIdBiodata(idBiodata);
		
		//save audit trail untuk created
		Integer xIdCreatedBy = this.userSearch().getIdUser();
		monitoringModel.setxIdCreatedBy(xIdCreatedBy);
		monitoringModel.setxCreatedDate(new Date());
		monitoringModel.setIsDelete(false);
		
		
		//save
		this.monitoringService.create(monitoringModel);
		this.saveLog(monitoringModel);
		
		String jsp = "monitoring/monitoring";
		return jsp;
	}
	
	@RequestMapping(value="monitoring/cruds/edit")
	public String monitoringEdit(HttpServletRequest request, Model model) {
		Integer idMonitoring = Integer.valueOf(request.getParameter("idMonitoring"));
		
		MonitoringModel monitoringModel = new MonitoringModel();
		monitoringModel = this.monitoringService.searchId(idMonitoring);
		
		model.addAttribute("monitoringModel", monitoringModel);
		model.addAttribute("idleDate", new SimpleDateFormat("yyyy-MM-dd").format(monitoringModel.getIdleDate()));
		
		//tampilkan biodata select option
		this.biodataList(model);
		
		String jsp = "monitoring/cruds/edit";
		return jsp;
	}
	
	@RequestMapping(value="monitoring/cruds/update")
	public String monitoringUpdate(HttpServletRequest request) throws ParseException {
		Integer idMonitoring = Integer.valueOf(request.getParameter("idMonitoring"));
		Integer idBiodata = Integer.valueOf(request.getParameter("idBiodata"));
		Date idleDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("idleDate"));
//		String idleDateStr = request.getParameter("idleDate");
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); /* hh-mm-ss,000000 */
//		Date idleDate = new Date();
//		try {
//			idleDate = sdf.parse(idleDateStr);
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
		String lastProject = request.getParameter("lastProject");
		String idleReason = request.getParameter("idleReason");
		
		MonitoringModel monitoringModelAfter = new MonitoringModel();
		monitoringModelAfter = this.monitoringService.searchId(idMonitoring);
		monitoringModelAfter.setIdBiodata(idBiodata);
		monitoringModelAfter.setIdleDate(idleDate);
		monitoringModelAfter.setLastProject(lastProject);
		monitoringModelAfter.setIdleReason(idleReason);
		
		MonitoringModel monitoringModelBefore = new MonitoringModel();
		monitoringModelBefore = this.monitoringService.searchId(idMonitoring);
		
		//audit trail
		Integer xIdModifiedBy = this.userSearch().getIdUser();
		monitoringModelAfter.setxIdModifiedBy(xIdModifiedBy);
		monitoringModelAfter.setxModifiedDate(new Date());
		
		//update
		this.monitoringService.update(monitoringModelAfter);
		this.updateLog(monitoringModelBefore, monitoringModelAfter);
		
		String jsp = "monitoring/monitoring";
		return jsp;
	}
	
	@RequestMapping(value="monitoring/cruds/search/name")
	public String monitoringSearchName(HttpServletRequest request, Model model) {
		String name = request.getParameter("nameKey");
		List<MonitoringModel> monitoringModelList = new ArrayList<MonitoringModel>();
		monitoringModelList = this.monitoringService.searchNamaBiodata(name);
		
		//System.out.println(monitoringModelList.size());
		
		model.addAttribute("monitoringModelList", monitoringModelList);
		
		String jsp = "monitoring/cruds/list";
		return jsp;
	}
	
	@RequestMapping(value="monitoring/cruds/placementAdd")
	public String monitoringPlacement(HttpServletRequest request, Model model) {
		Integer idMonitoring = Integer.valueOf(request.getParameter("idMonitoring"));
		
		MonitoringModel monitoringModel = new MonitoringModel();
		monitoringModel = this.monitoringService.searchId(idMonitoring);
		
		model.addAttribute("monitoringModel", monitoringModel);
		
		String jsp = "monitoring/cruds/placementAdd";
		return jsp;
	}
	
	@RequestMapping(value="monitoring/cruds/createPlacement")
	public String monitoringAddPlacement(HttpServletRequest request, Model model) throws ParseException {
		//get nilai dari jsp
		Integer idMonitoring = Integer.valueOf(request.getParameter("idMonitoring"));
		Date placementDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("placementDate"));
		Date idleDate = new Date();
//		String placementDateStr = request.getParameter("placementDate");
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); /* hh-mm-ss,000000 */
//		Date placementDate = new Date();
//		Date idleDate = new Date();
//		try {
//			placementDate = sdf.parse(placementDateStr);
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
		String placementAt = request.getParameter("placementAt");
		String notes = request.getParameter("notes");
		
		MonitoringModel monitoringModel = new MonitoringModel();
		monitoringModel = this.monitoringService.searchId(idMonitoring);
		
		//set
		monitoringModel.setPlacementDate(placementDate);
		monitoringModel.setPlacementAt(placementAt);
		monitoringModel.setNotes(notes);
		monitoringModel.setIdleDate(idleDate);
		
		
		//compare Date
		if (idleDate.before(placementDate)) {
			//save
			this.monitoringService.createPlacement(monitoringModel);
		} else {
			int dateResult = 0;
			model.addAttribute("dateResult", dateResult);
		}
		
		String jsp = "monitoring/monitoring";
		return jsp;
	}
	
	@RequestMapping(value="monitoring/cruds/placementEdit")
	public String monitoringPlacementEdit(HttpServletRequest request, Model model) {
		Integer idMonitoring = Integer.valueOf(request.getParameter("idMonitoring"));
		
		MonitoringModel monitoringModel = new MonitoringModel();
		monitoringModel = this.monitoringService.searchId(idMonitoring);
		
		model.addAttribute("monitoringModel", monitoringModel);
		//untuk date
		model.addAttribute("placementDate", new SimpleDateFormat("yyyy-MM-dd").format(monitoringModel.getPlacementDate()));
		
		String jsp = "monitoring/cruds/placementEdit";
		return jsp;
	}
	
	@RequestMapping(value="monitoring/cruds/updatePlacement")
	public String monitoringUpdatePlacement(HttpServletRequest request, Model model) throws ParseException {
		//get nilai dari jsp
		Integer idMonitoring = Integer.valueOf(request.getParameter("idMonitoring"));
		Date placementDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("placementDate"));
		Date idleDate = new Date();
//		String placementDateStr = request.getParameter("placementDate");
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); /* hh-mm-ss,000000 */
//		Date placementDate = new Date();
//		Date idleDate = new Date();
//		try {
//			placementDate = sdf.parse(placementDateStr);
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
		String placementAt = request.getParameter("placementAt");
		String notes = request.getParameter("notes");
		
		MonitoringModel monitoringModel = new MonitoringModel();
		monitoringModel = this.monitoringService.searchId(idMonitoring);
		
		//set
		monitoringModel.setPlacementDate(placementDate);
		monitoringModel.setPlacementAt(placementAt);
		monitoringModel.setNotes(notes);
		monitoringModel.setIdleDate(idleDate);
		
		//compare Date
		if (idleDate.before(placementDate)) {
			//save
			this.monitoringService.createPlacement(monitoringModel);
		} else {
			int dateResult = 0;
			model.addAttribute("dateResult", dateResult);
		}
		
		//save
		this.monitoringService.createPlacement(monitoringModel);
		
		String jsp = "monitoring/monitoring";
		return jsp;
	}
	
	@RequestMapping(value="monitoring/cruds/remove")
	public String monitoringRemove(HttpServletRequest request, Model model) {
		Integer idMonitoring = Integer.valueOf(request.getParameter("idMonitoring"));
		
		MonitoringModel monitoringModel = new MonitoringModel();
		monitoringModel = this.monitoringService.searchId(idMonitoring);
		model.addAttribute("monitoringModel", monitoringModel);
		
		String jsp = "monitoring/cruds/remove";
		return jsp;
	}
	
	@RequestMapping(value="monitoring/cruds/delete")
	public String monitoringDelete(HttpServletRequest request) {
		Integer idMonitoring = Integer.valueOf(request.getParameter("idMonitoring"));
		MonitoringModel monitoringModelAfter = new MonitoringModel();
		monitoringModelAfter = this.monitoringService.searchId(idMonitoring);
		monitoringModelAfter.setIsDelete(true);
		
		MonitoringModel monitoringModelBefore = new MonitoringModel();
		monitoringModelBefore = this.monitoringService.searchId(idMonitoring);
		
		//audit trail
		Integer xIdDeletedBy = this.userSearch().getIdUser();
		monitoringModelAfter.setxIdDeletedBy(xIdDeletedBy);
		monitoringModelAfter.setxDeletedDate(new Date());
		
		//delete temporary
		this.monitoringService.deleteTemporary(monitoringModelAfter);
		this.updateLog(monitoringModelBefore, monitoringModelAfter);
		
		String jsp = "monitoring/monitoring";
		return jsp;
	}
	
	public void biodataList(Model model) {
		List<BiodataModel> biodataModelList = new ArrayList<>();
		biodataModelList = this.biodataService.searchBiodataNotInMonitoring();
		model.addAttribute("biodataModelList", biodataModelList);
		
	}
	
//	public Integer cekIdBiodata(Integer idBiodata) {
//		List<MonitoringModel> monitoringModelList = new ArrayList<MonitoringModel>();
//		monitoringModelList = this.monitoringService.searchIdEqual(idBiodata);
//		
//		Integer jumlahDataBiodata=0;
//		
//		if(monitoringModelList==null) {
//			jumlahDataBiodata=0;
//		}else {
//			jumlahDataBiodata=monitoringModelList.size();
//		}
//		return jumlahDataBiodata;
//	}
	
//	public void cekDatePlacement(Date placementDate) {
//		List<MonitoringModel> monitoringModelList = new ArrayList<MonitoringModel>();
//	}
	
	/*
	 * public void biodataListName(HttpServletRequest request ,Model model) { String
	 * name = request.getParameter("nameKey"); List<BiodataModel> biodataModelList =
	 * new ArrayList<>(); biodataModelList = this.biodataService.searchName(name);
	 * model.addAttribute("biodataModelList", biodataModelList); }
	 */
	
}
