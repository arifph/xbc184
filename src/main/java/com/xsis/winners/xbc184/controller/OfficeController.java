package com.xsis.winners.xbc184.controller;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.OfficeModel;
import com.xsis.winners.xbc184.model.RoomModel;
import com.xsis.winners.xbc184.service.OfficeService;
import com.xsis.winners.xbc184.service.RoomService;

@Controller
public class OfficeController extends AuditLogController{

	@Autowired
	private OfficeService officeService;

	@Autowired
	private RoomService roomService;
	
	//untuk menyimpan value id
	private Integer idOffice;
	//private Integer idRoom;
	
	
	@RequestMapping(value = "office")
	public String office(HttpServletRequest request, Model model) {
		
		String jsp = "office/office";
		return jsp;
	}

	@RequestMapping(value = "office/cruds/add")
	public String officeAdd(Model model) {

		OfficeModel officeModel = new OfficeModel();
		officeModel.setName(" ");
		
		Integer idUser = this.userSearch().getIdUser();
		officeModel.setCreatedBy(idUser);
		officeModel.setCreatedOn(new Date());
		
		this.officeService.create(officeModel);
		Integer idOffice = officeModel.getId();
		
		model.addAttribute("idOffice", idOffice);
		this.idOffice = idOffice;
		
		this.saveLog(officeModel);

		
		
		String jsp = "office/cruds/add";
		return jsp;

	}

	@RequestMapping(value = "office/cruds/create")
	public String officeCreate(HttpServletRequest request) {
		Integer id = Integer.valueOf(request.getParameter("idOffice"));

		String name = request.getParameter("name");
		String phone = request.getParameter("phone");
		String email = request.getParameter("email");
		String address = request.getParameter("address");
		String notes = request.getParameter("notes");
		
		Integer idUser = this.userSearch().getIdUser();
		
		OfficeModel officeModelBefore = this.officeService.searchId(id);

		
		OfficeModel officeModel = new OfficeModel();
		officeModel = this.officeService.searchId(id);
		
		officeModel.setName(name);
		officeModel.setPhone(phone);
		officeModel.setEmail(email);
		officeModel.setAddress(address);
		officeModel.setNotes(notes);
		officeModel.setIsDelete(false);
		
		officeModel.setCreatedBy(officeModelBefore.getCreatedBy());
		officeModel.setCreatedOn(officeModelBefore.getCreatedOn());
		
		officeModel.setModifiedBy(idUser);
		officeModel.setModifiedOn(new Date());
		
		this.officeService.update(officeModel);
		
		//Audit Log
		this.updateLog(officeModelBefore, officeModel);
		
		String jsp = "office/office";
		return jsp;
	}

	@RequestMapping(value = "office/cruds/list")
	public String officeList(Model model) {
		
		
		List<OfficeModel> officeModelList = new ArrayList<OfficeModel>();
				
		officeModelList = this.officeService.search();
		model.addAttribute("officeModelList", officeModelList);

		String jsp = "office/cruds/list";
		return jsp;
	}
	
	//delete nama kosong
	@RequestMapping(value = "office/cruds/list-delete")
	public String officeListDelete(Model model) {
		
		
		List<OfficeModel> deleteOfficeModelList = new ArrayList<OfficeModel>();
		
		deleteOfficeModelList = this.officeService.searchNameSpace();
		for (int i = 0; i < deleteOfficeModelList.size(); i++) {
			this.officeService.delete(deleteOfficeModelList.get(i));
		}
		
		model.addAttribute("deleteOfficeModelList", deleteOfficeModelList);
		
		String jsp = "office/cruds/list";
		return jsp;
	}
	
	
	
	@RequestMapping(value = "office/cruds/edit")
	public String officeEdit(HttpServletRequest request, Model model) {

		Integer id = Integer.valueOf(request.getParameter("id"));
	
		idOffice = id;
		
		OfficeModel officeModel = new OfficeModel();
		officeModel = this.officeService.searchId(id);
		model.addAttribute("officeModel", officeModel);

		
		RoomModel roomModel = new RoomModel();
		roomModel = this.roomService.searchId(id);
		model.addAttribute("roomModel", roomModel);
		
		//this.roomList(model);

		String jsp = "office/cruds/edit";
		return jsp;
	}

	@RequestMapping(value = "office/cruds/update")
	public String officeUpdate(HttpServletRequest request) {
		// get nilai dari variabel jsp
		Integer id = Integer.valueOf(request.getParameter("id"));
		String name = request.getParameter("name");
		String phone = request.getParameter("phone");
		String email = request.getParameter("email");
		String address = request.getParameter("address");
		String notes = request.getParameter("notes");
		
		//tsany
		//Integer createdBy = Integer.valueOf(request.getParameter("createdBy"));
		//String date = request.getParameter("createdOn");
		
		//AUDIT LOG
		OfficeModel officeModelBefore = this.officeService.searchId(id);
		
//		OfficeModel officeModel = new OfficeModel();
//		officeModel = this.officeService.searchId(id);
//
//		officeModel.setName(name);
//		officeModel.setPhone(phone);
//		officeModel.setEmail(email);
//		officeModel.setAddress(address);
//		officeModel.setNotes(notes);
		
		//AUDIT LOG
		OfficeModel officeModelAfter = new OfficeModel();
		officeModelAfter.setId(id);
		officeModelAfter.setName(name);
		officeModelAfter.setPhone(phone);
		officeModelAfter.setEmail(email);
		officeModelAfter.setAddress(address);
		officeModelAfter.setNotes(notes);
		officeModelAfter.setIsDelete(false);
		
		officeModelAfter.setCreatedBy(officeModelBefore.getCreatedBy());
		officeModelAfter.setCreatedOn(officeModelBefore.getCreatedOn());
		 		
		Integer idUser = this.userSearch().getIdUser();
		officeModelAfter.setModifiedBy(idUser);
		officeModelAfter.setModifiedOn(new Date());
		
		this.officeService.update(officeModelAfter);
		//Audit Log
		this.updateLog(officeModelBefore, officeModelAfter);
		
		
		String jsp = "office/office";
		return jsp;
	}

	@RequestMapping(value = "office/cruds/remove")
	public String officeRemove(HttpServletRequest request, Model model) {

		Integer id = Integer.valueOf(request.getParameter("id"));
		
		
		OfficeModel officeModel = new OfficeModel();
		officeModel = this.officeService.searchId(id);

		model.addAttribute("officeModel", officeModel);

		String jsp = "office/cruds/remove";
		return jsp;
	}

	@RequestMapping(value = "office/cruds/delete")
	public String officeDelete(HttpServletRequest request) {
		Integer id = Integer.valueOf(request.getParameter("id"));

		OfficeModel officeModelBefore = new OfficeModel();
		officeModelBefore = this.officeService.searchId(id);

		OfficeModel officeModel = new OfficeModel();
		officeModel = this.officeService.searchId(id);

		officeModel.setIsDelete(true);
		
		Integer idUser = this.userSearch().getIdUser();
		officeModel.setDeletedBy(idUser);
		officeModel.setDeletedOn(new Date());
		
		
		this.officeService.deleteTemporary(officeModel);

		//Audit Log
		this.updateLog(officeModelBefore, officeModel);
		
		String jsp = "office/office";
		return jsp;
	}

	@RequestMapping(value = "office/cruds/search/name")
	public String officeSearchName(HttpServletRequest request, Model model) {
		String name = request.getParameter("nameKey");
		List<OfficeModel> officeModelList = new ArrayList<OfficeModel>();

		officeModelList = this.officeService.searchNama(name);
		model.addAttribute("officeModelList", officeModelList);

		String jsp = "office/cruds/list";
		return jsp;
	}

	//cancel office
	@RequestMapping(value = "office/cruds/cancel")
	public String officeCancel(HttpServletRequest request) {
//		Integer id = Integer.valueOf(request.getParameter("idOffice"));
		
		OfficeModel officeModel = new OfficeModel();
		officeModel = this.officeService.searchId(idOffice);
		this.officeService.delete(officeModel);

		String jsp = "office/office";
		return jsp;
	}
	
	
	
	
//===================================ROOM=====================================================
	
	@RequestMapping(value="office/room/add")
	public String room(HttpServletRequest request, Model model) {
		//Integer officeId = Integer.valueOf(request.getParameter("id"));
		
		//menyimpan id di atas
		//idOffice = officeId;
		//coba jelasin yg ini
		model.addAttribute("idOffice", idOffice);
		String jsp = "office/room/add";
		return jsp;	
	}
	
	@RequestMapping(value="office/room/create")
	public String roomCreate(HttpServletRequest request, Model model) {
		
		String code = request.getParameter("code");
		String name = request.getParameter("name");
		Integer capacity = Integer.valueOf(request.getParameter("capacity"));
		Boolean anyProjector = Boolean.valueOf(request.getParameter("anyProjector"));
		String notes = request.getParameter("notes");
		
		//Integer officeId = Integer.valueOf(request.getParameter("officeId"));
		
		RoomModel roomModel = new RoomModel();
		roomModel.setCode(code);
		roomModel.setName(name);
		roomModel.setCapacity(capacity);
		roomModel.setAnyProjector(anyProjector);
		roomModel.setNotes(notes);
		roomModel.setOfficeId(idOffice);
		roomModel.setIsDelete(false);
		
		Integer idUser = this.userSearch().getIdUser();
		roomModel.setCreatedBy(idUser);
		roomModel.setCreatedOn(new Date());
		
		this.roomService.create(roomModel);

		this.saveLog(roomModel);
		String jsp = "office/cruds/add"; 
		return jsp;
	}
	
	@RequestMapping(value="office/cruds/list-room")
	public String roomList(HttpServletRequest request, Model model) {
		List<RoomModel> roomModelList = new ArrayList<RoomModel>();

		roomModelList = this.roomService.searchRoomInOffice(idOffice);
		model.addAttribute("roomModelList", roomModelList);
		
		String jsp = "office/cruds/list-room"; 
		return jsp;
	}


	@RequestMapping(value = "office/room/edit")
	public String roomEdit(HttpServletRequest request, Model model) {

		Integer id = Integer.valueOf(request.getParameter("id"));
		
		//idRoom = id;
		
		RoomModel roomModel = new RoomModel();
		roomModel = this.roomService.searchId(id);
		model.addAttribute("roomModel", roomModel);

		//this.roomList(model);

		String jsp = "office/room/edit";
		return jsp;
	}

	@RequestMapping(value = "office/room/update")
	public String roomUpdate(HttpServletRequest request) {
		// get nilai dari variabel jsp
		Integer id = Integer.valueOf(request.getParameter("id"));
		String code = request.getParameter("code");
		String name = request.getParameter("name");
		Integer capacity = Integer.valueOf(request.getParameter("capacity"));
		Boolean anyProjector = Boolean.valueOf(request.getParameter("anyProjector"));
		String notes = request.getParameter("notes");

		//Integer createdBy = Integer.valueOf(request.getParameter("createdBy"));
		//String date = request.getParameter("createdOn");
	
		RoomModel roomModelBefroe = this.roomService.searchId(id);
		
		RoomModel roomModelAfter = new RoomModel();

		roomModelAfter = this.roomService.searchId(id);

		roomModelAfter.setCode(code);
		roomModelAfter.setName(name);
		roomModelAfter.setCapacity(capacity);
		roomModelAfter.setAnyProjector(anyProjector);
		roomModelAfter.setNotes(notes);
		roomModelAfter.setOfficeId(idOffice);
		roomModelAfter.setIsDelete(false);

		roomModelAfter.setCreatedBy(roomModelBefroe.getCreatedBy());
		roomModelAfter.setCreatedOn(roomModelBefroe.getCreatedOn());
		 		
		Integer idUser = this.userSearch().getIdUser();
		roomModelAfter.setModifiedBy(idUser);
		roomModelAfter.setModifiedOn(new Date());
		
		
		
		this.roomService.update(roomModelAfter);
		
		this.updateLog(roomModelBefroe, roomModelAfter);
		String jsp = "office/cruds/edit";
		return jsp;
	}


	@RequestMapping(value = "office/room/remove")
	public String roomRemove(HttpServletRequest request, Model model) {

		Integer id = Integer.valueOf(request.getParameter("id"));
		
		
		RoomModel roomModel = new RoomModel();
		roomModel = this.roomService.searchId(id);

		model.addAttribute("roomModel", roomModel);

		String jsp = "office/room/remove";
		return jsp;
	}

	@RequestMapping(value = "office/room/delete")
	public String roomDelete(HttpServletRequest request) {
		Integer id = Integer.valueOf(request.getParameter("id"));
		
		RoomModel roomModelBefore = new RoomModel();
		
		roomModelBefore = this.roomService.searchId(id);

		
		RoomModel roomModel = new RoomModel();
		roomModel = this.roomService.searchId(id);
		
		roomModel.setIsDelete(true);
		
		Integer idUser = this.userSearch().getIdUser();
		roomModel.setDeletedBy(idUser);
		roomModel.setDeletedOn(new Date());

		
		this.roomService.deleteTemporary(roomModel);

		this.updateLog(roomModelBefore, roomModel);
		String jsp = "office/cruds/edit";
		return jsp;
	}
	
}
