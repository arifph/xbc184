package com.xsis.winners.xbc184.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
//import java.time.LocalDate;
//import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
//import java.util.Calendar;
import java.util.Date;
//import java.util.GregorianCalendar;
import java.util.List;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.BatchModel;
import com.xsis.winners.xbc184.model.BatchTestModel;
import com.xsis.winners.xbc184.model.BiodataModel;
import com.xsis.winners.xbc184.model.BootcampTypeModel;
import com.xsis.winners.xbc184.model.ClassModel;
import com.xsis.winners.xbc184.model.RoomModel;
import com.xsis.winners.xbc184.model.TechnologyModel;
import com.xsis.winners.xbc184.model.TestModel;
import com.xsis.winners.xbc184.model.TrainerModel;
import com.xsis.winners.xbc184.service.BatchService;
import com.xsis.winners.xbc184.service.BatchTestService;
import com.xsis.winners.xbc184.service.BiodataService;
import com.xsis.winners.xbc184.service.BootcampTypeService;
import com.xsis.winners.xbc184.service.ClassService;
import com.xsis.winners.xbc184.service.RoomService;
import com.xsis.winners.xbc184.service.TechnologyService;
import com.xsis.winners.xbc184.service.TestService;
import com.xsis.winners.xbc184.service.TrainerService;

@Controller
public class BatchController /*extends UserController*/{

	@Autowired
	private BatchService batchService;
	
	@Autowired
	private RoomService roomService;
	
	@Autowired
	private TechnologyService technologyService;
	
	@Autowired
	private TrainerService trainerService;
	
	@Autowired
	private BootcampTypeService bootcampTypeService;
	
	@Autowired
	private BiodataService biodataService;
	
	@Autowired
	private ClassService classService;
	
	@Autowired
	private TestService testService;
	
	@Autowired
	private BatchTestService batchTestService;
	
	@Autowired
	private AuditLogController auditLogController;
	
//	@Autowired
//	private BootcampTestTypeService bootcampTestTypeService;
	
	@RequestMapping(value="batch") // url action
	public String batch() { // method
		String jsp = "batch/batch"; // target atau halaman
		return jsp;
	}
	
	 // untuk memunculkan POP UP tambah batch
	@RequestMapping(value="batch/cruds/add")
	public String batchAdd(Model model) {
		RoomModel roomModel = new RoomModel();
		TechnologyModel technologyModel = new TechnologyModel();
		TrainerModel trainerModel = new TrainerModel();
		BootcampTypeModel bootcampTypeModel = new BootcampTypeModel();
		
		model.addAttribute("roomModel", roomModel);
		model.addAttribute("technologyModel", technologyModel);
		model.addAttribute("trainerModel", trainerModel);
		model.addAttribute("bootcampTypeModel", bootcampTypeModel);
		
		// tampilkan isian untuk select option
		this.roomList(model);
		this.technologyList(model);
		this.trainerList(model);
		this.bootcampTypeList(model);
		
		String jsp = "batch/cruds/add";
		return jsp;
	}
	
	// untuk menambah batch
	@RequestMapping(value="batch/cruds/create")
	public String batchCreate(HttpServletRequest request) throws ParseException {
		
		// get nilai dari variabel nama di jsp
		String name = request.getParameter("name");
		Integer roomId = Integer.valueOf(request.getParameter("roomId"));
		Integer technologyId = Integer.valueOf(request.getParameter("technologyId"));
		Integer trainerId = Integer.valueOf(request.getParameter("trainerId"));
		
//		Date periodTo = new Date();
//		Date periodFrom = new Date();
		String jamMasuk = " 08.00.00";
		String jamPulang = " 17.00.00";
//		SimpleDateFormat formatter1 = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy HH.mm.ss");
//	    try {  
//	        periodTo = formatter2.parse(request.getParameter("periodTo") + jamPulang);
//	        periodFrom = formatter2.parse(request.getParameter("periodFrom") + jamMasuk);
	    	
//	    	String periodToStr = formatter2.format(formatter1.parse(request.getParameter("periodTo")) + jamPulang);
//	    	String periodFromStr = formatter2.format(formatter1.parse(request.getParameter("periodFrom")) + jamMasuk);
//	    	Date periodTo = formatter2.parse(periodToStr);
//		    Date periodFrom = formatter2.parse(periodFromStr);
//	    }
//	    catch (ParseException e) {
//	    	e.printStackTrace();
//	    }
		
		String periodToStr = ambilTgl(request.getParameter("periodTo"));
		String periodFromStr = ambilTgl(request.getParameter("periodFrom"));
		Date periodTo = formatter2.parse(periodToStr + jamPulang);
		Date periodFrom = formatter2.parse(periodFromStr + jamMasuk);
		
		Integer bootcampTypeId = Integer.valueOf(request.getParameter("bootcampTypeId"));
		String notes = request.getParameter("notes");
		
		// set nilai ke variabel di modelnya
		BatchModel batchModel = new BatchModel();
		batchModel.setName(name);
		batchModel.setRoomId(roomId);
		batchModel.setTechnologyId(technologyId);
		batchModel.setTrainerId(trainerId);
		batchModel.setPeriodTo(periodTo);
		batchModel.setPeriodFrom(periodFrom);
		batchModel.setBootcampTypeId(bootcampTypeId);
		batchModel.setNotes(notes);
		batchModel.setIsDelete(false);
		
		// save audit trail untuk created, username adalah arif, jadi line di bawah nggak dipake
//		//Integer createdBy = this.userSearch().getId();
		batchModel.setCreatedBy(2);
		batchModel.setCreatedOn(new Date());
		
		// simpan data
		this.batchService.create(batchModel);
		this.auditLogController.saveLog(batchModel);
		
		String jsp = "batch/batch"; // setelah nge-save, kembali ke sini lagi
		return jsp;
	}
	
	// untuk menambah batch
//		@RequestMapping(value="batch/cruds/create")
//		public String batchCreate(HttpServletRequest request, Model model) {
//			
//			// get nilai dari variabel nama di jsp
//			String kodeBatch = request.getParameter("kodeBatch");
//			String namaBatch = request.getParameter("namaBatch");
//			
//			// set join table
//			Integer idLokasi = Integer.valueOf(request.getParameter("idLokasi"));
//			
//			// set nilai ke variabel di modelnya
//			BatchModel batchModel = new BatchModel();
//			batchModel.setKodeBatch(kodeBatch);
//			batchModel.setNamaBatch(namaBatch);
//			batchModel.setIsDelete(0); // 0 artinya tidak di-delete
//			
//			// set join table
//			batchModel.setIdLokasi(idLokasi);
//			
//			/*Integer jumlahDataBatch = this.cekKodeBatch(kodeBatch);*/
//			Integer jumlahDataBatch2 = this.cekNamaBatch(namaBatch, 0);
//			Integer jumlahDataBatch2_2 = this.cekNamaBatch(namaBatch, 1);
//			
//			// save audit trail untuk created
//			Integer xIdCreatedBy = this.userSearch().getIdUser();
//			batchModel.setxIdCreatedBy(xIdCreatedBy);
//			batchModel.setxCreatedDate(new Date());
//			
//			if (/*jumlahDataBatch > 0 || */jumlahDataBatch2 > 0) {
//				/*model.addAttribute("jumlahDataBatch", jumlahDataBatch);*/
//				model.addAttribute("jumlahDataBatch2", jumlahDataBatch2);
//				/*model.addAttribute("kodeBatch", kodeBatch);*/
//				model.addAttribute("namaBatch", namaBatch);
//			} else {
//				if(jumlahDataBatch2_2 > 0) {
//					// simpan data
//					this.batchService.delete(batchModel);
//					this.batchService.create(batchModel);
//				}
//				else {
//					// simpan data
//					this.batchService.create(batchModel);
//				}
////				// simpan data
////				this.batchService.create(batchModel);
//			}
//			
//			// tampilkan lokasi untuk select option
//			this.lokasiList(model);
//			
//			String jsp = "batch/batch"; // setelah nge-save, kembali ke sini lagi
//			return jsp;
//		}
	
	@RequestMapping(value="batch/cruds/list")
	public String batchList(Model model) {
		List<BatchModel> batchModelList = new ArrayList<BatchModel>();
//		List<TechnologyModel> technologyModelList = new ArrayList<TechnologyModel>();
//		List<TrainerModel> trainerModelList = new ArrayList<TrainerModel>();
//		String kodeRole = this.userSearch().getRoleModel().getKodeRole();
		
		batchModelList = this.batchService.search();
//		technologyModelList = this.technologyService.search();
//		trainerModelList = this.trainerService.search();
		
		model.addAttribute("batchModelList", batchModelList);
//		model.addAttribute("technologyModelList", technologyModelList);
//		model.addAttribute("trainerModelList", trainerModelList);
		
		String jsp = "batch/cruds/list";
		return jsp;
	}
	
	// untuk memunculkan POP UP detil batch
//	@RequestMapping(value="batch/cruds/detail")
//	public String batchDetail(HttpServletRequest request, Model model) {
//		Integer idBatch = Integer.valueOf(request.getParameter("idBatch"));
//		
//		BatchModel batchModel = new BatchModel();
//		batchModel = this.batchService.searchId(idBatch);
//		
//		model.addAttribute("batchModel", batchModel);
//		
//		// tampilkan lokasi
//		this.lokasiList(model);
//		
//		String jsp = "batch/cruds/detail";
//		return jsp;
//	}
	
	// untuk memunculkan POP UP edit batch
	@RequestMapping(value="batch/cruds/edit")
	public String batchEdit(HttpServletRequest request, Model model) {
		Integer id = Integer.valueOf(request.getParameter("id"));
		
		BatchModel batchModel = new BatchModel();
		batchModel = this.batchService.searchId(id);
		model.addAttribute("batchModel", batchModel);
		
		// tampilkan isian select option
		RoomModel roomModel = new RoomModel();
		TechnologyModel technologyModel = new TechnologyModel();
		TrainerModel trainerModel = new TrainerModel();
		BootcampTypeModel bootcampTypeModel = new BootcampTypeModel();
		
		model.addAttribute("roomModel", roomModel);
		model.addAttribute("technologyModel", technologyModel);
		model.addAttribute("trainerModel", trainerModel);
		model.addAttribute("bootcampTypeModel", bootcampTypeModel);
		
		this.roomList(model);
		this.technologyList(model);
		this.trainerList(model);
		this.bootcampTypeList(model);
		
		String jsp = "batch/cruds/edit";
		return jsp;
	}
	
	// untuk melakukan update batch
	@RequestMapping(value="batch/cruds/update")
	public String batchUpdate(HttpServletRequest request, Model model) {
		Integer id = Integer.valueOf(request.getParameter("id"));
		
		// get nilai dari variabel2 di jsp
		String name = request.getParameter("name");
		Integer roomId = Integer.valueOf(request.getParameter("roomId"));
		Integer technologyId = Integer.valueOf(request.getParameter("technologyId"));
		Integer trainerId = Integer.valueOf(request.getParameter("trainerId"));
		
		String jamMasuk = " 08.00.00";
		String jamPulang = " 17.00.00";
		SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy HH.mm.ss");
		
		Date periodTo = new Date();
		Date periodFrom = new Date();
		String periodToStr = "";
		String periodFromStr = "";
		try {
			periodToStr = ambilTgl(request.getParameter("periodTo"));
			periodFromStr = ambilTgl(request.getParameter("periodFrom"));
			periodTo = formatter2.parse(periodToStr + jamPulang);
			periodFrom = formatter2.parse(periodFromStr + jamMasuk);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Integer bootcampTypeId = Integer.valueOf(request.getParameter("bootcampTypeId"));
		String notes = request.getParameter("notes");
		
		// set nilai ke variabel di modelnya
		BatchModel batchModel = new BatchModel();
		batchModel = this.batchService.searchId(id);
		
		// copy isi batchModel ke sebuah model baru
		BatchModel batchBefore = new BatchModel(); 
		batchBefore = this.batchService.searchId(batchModel.getId());
		
		// lanjut set nilai
		batchModel.setName(name);
		batchModel.setRoomId(roomId);
		batchModel.setTechnologyId(technologyId);
		batchModel.setTrainerId(trainerId);
		batchModel.setPeriodTo(periodTo);
		batchModel.setPeriodFrom(periodFrom);
		batchModel.setBootcampTypeId(bootcampTypeId);
		batchModel.setNotes(notes);
		batchModel.setIsDelete(false);
		
		// save audit trail untuk modified, username adalah arif, jadi line di bawah nggak dipake
//		//Integer modifiedBy = this.userSearch().getId();
		batchModel.setCreatedBy(batchBefore.getCreatedBy());
		batchModel.setCreatedOn(batchBefore.getCreatedOn());
		batchModel.setModifiedBy(2);
		batchModel.setModifiedOn(new Date());
		
		// simpan data
		this.batchService.update(batchModel);
		this.auditLogController.updateLog(batchBefore, batchModel); // isi update log
		
		String jsp = "batch/batch"; // setelah nge-save, kembali ke sini lagi
		return jsp;
	}
	
	// untuk memunculkan POP UP add participant
	@RequestMapping(value="batch/cruds/add-participant")
	public String batchAddParticipant(HttpServletRequest request, Model model) {
		Integer id = Integer.valueOf(request.getParameter("id"));
		
		BatchModel batchModel = new BatchModel();
		batchModel = this.batchService.searchId(id);
		model.addAttribute("batchModel", batchModel);
		
		// tampilkan isian select option
		BiodataModel biodataModel = new BiodataModel();
		
		model.addAttribute("biodataModel", biodataModel);
		
		this.biodataList(model, id);
		
		String jsp = "batch/cruds/add-participant";
		return jsp;
	}
	
	// untuk melakukan update batch
	@RequestMapping(value="batch/cruds/insert-participant")
	public String batchInsertParticipant(HttpServletRequest request, Model model) {
		
		// get nilai dari variabel2 di jsp
		String paramBioId = request.getParameter("biodataId");
//			ClassModel cekBio = new ClassModel();
		
		if(paramBioId == "") {
			// DO NOTHING
			String jsp = "batch/cruds/add-participant"; // setelah nge-save, kembali ke sini lagi
			return jsp;
		}
		else {
			Integer batchId = Integer.valueOf(request.getParameter("batchId"));
//				Integer biodataId = Integer.valueOf(request.getParameter("biodataId"));
			Integer biodataId = Integer.valueOf(paramBioId);
			
			ClassModel cekBio = new ClassModel();
//			cekBio = this.classService.searchBiodata(batchId, biodataId);
//			
//			if(cekBio == null) {
				// set nilai ke variabel di modelnya
//					BatchModel batchModel = new BatchModel();
//					BiodataModel biodataModel = new BiodataModel();
//					batchModel = this.batchService.searchId(batchId);
				ClassModel classModel = new ClassModel();
				
				// lanjut set nilai
				classModel.setBatchId(batchId);
				classModel.setBiodataId(biodataId);
				
				// save audit trail untuk modified, username adalah arif, jadi line di bawah nggak dipake
//					//Integer modifiedBy = this.userSearch().getId();
				classModel.setCreatedBy(2);
				classModel.setCreatedOn(new Date());
				
				// simpan data
				this.classService.create(classModel);
				this.auditLogController.saveLog(classModel); // isi update log
				
				String jsp = "batch/batch"; // setelah nge-save, kembali ke sini lagi
				return jsp;
//			}
//			else {
//				// DO NOTHING
//			}
		}
		
//		String jsp = "batch/batch"; // setelah nge-save, kembali ke sini lagi
//		return jsp;
	}
	
	// untuk memunculkan POP UP setup test
	@RequestMapping(value="batch/cruds/setup-test")
	public String batchSetupTest(HttpServletRequest request, Model model) {
		Integer id = Integer.valueOf(request.getParameter("id"));
		
		BatchModel batchModel = new BatchModel();
		batchModel = this.batchService.searchId(id);
		model.addAttribute("batchModel", batchModel);
		
		TestModel testModel = new TestModel();
//		testModel = this.batchTestService.searchBootcampTest();
		model.addAttribute("testModel", testModel);
		this.testList(model,id);
		
		// Mengecek apakah test sudah pernah dipilih untuk suatu batch
		BatchTestModel chosenTest = new BatchTestModel();
		model.addAttribute("chosenTest", chosenTest);
		this.batchTestList(model, id);
		
		String jsp = "batch/cruds/setup-test";
		return jsp;
	}
	
	// untuk melakukan insert/delete test
	@RequestMapping(value="batch/cruds/modify-test")
	public String batchModifyTest(HttpServletRequest request, Model model) {
		// Lakukan INSERT / DELETE pada Tabel T_CLAZZ >>> batchId = batchId, testId = id button pada pop up setup test
		// Kenapa INSERT / DELETE ??? 
		// Jika tombol 'Choose' ditekan sekali, label tombol berubah jadi 'Cancel', maka INSERT record ke T_CLAZZ
		// Jika tombol 'Choose' ditekan sekali lagi, label tombol berubah jadi 'Choose', maka DELETE record di T_CLAZZ
		// btw, DELETE pada tabel T_CLAZZ = delete permanen, bukan temporary
		
		Integer batchId = Integer.valueOf(request.getParameter("batchId"));
		Integer testId = Integer.valueOf(request.getParameter("testId"));
		
		BatchTestModel batchTestModel = new BatchTestModel();
		BatchTestModel cekSetupTest = new BatchTestModel(); 
		cekSetupTest = this.batchTestService.searchSetupTest(batchId, testId);
//		model.addAttribute("batchTestModel", cekSetupTest);
		
		if(cekSetupTest == null) {
			// INSERT
			batchTestModel.setBatchId(batchId);
			batchTestModel.setTestId(testId);
			
			// save audit trail untuk deleted, username adalah arif, jadi line di bawah nggak dipake
			//Integer deletedBy = this.userSearch().getId();
			batchTestModel.setCreatedBy(2);
			batchTestModel.setCreatedOn(new Date());
			
			// simpan data
			this.batchTestService.create(batchTestModel);
			this.auditLogController.saveLog(batchTestModel); // isi update log
		}
		else {
			// DELETE
			batchTestModel = this.batchTestService.searchIdBySetupTest(batchId, testId);
			this.batchTestService.delete(batchTestModel);
			this.auditLogController.deleteLog(batchTestModel); // isi update log
		}
		
//		String jsp = "batch/batch"; // setelah nge-save, kembali ke sini lagi
		String jsp = "batch/cruds/setup-test";
		return jsp;
	}
	
	// untuk memunculkan POP UP hapus batch
//	@RequestMapping(value="batch/cruds/remove")
//	public String batchRemove(HttpServletRequest request, Model model) {
//		
//		// get nilai dari variabel nama di jsp
//		Integer id = Integer.valueOf(request.getParameter("id"));
//		
//		// set nilai ke variabel di modelnya
//		BatchModel batchModel = new BatchModel();
//		batchModel = this.batchService.searchId(id);
//		
//		model.addAttribute("batchModel", batchModel);
//		
//		// tampilkan lokasi
////		this.lokasiList(model);
//		
//		String jsp = "batch/cruds/remove"; // setelah nge-save, kembali ke sini lagi
//		return jsp;
//	}
	
	/*// untuk menghapus data batch
	@RequestMapping(value="batch/cruds/delete")
	public String batchDelete(HttpServletRequest request) {
		
		// get nilai dari variabel nama di jsp
		Integer idBatch = Integer.valueOf(request.getParameter("idBatch"));
		
		// set nilai ke variabel di modelnya
		BatchModel batchModel = new BatchModel();
		batchModel = this.batchService.searchId(idBatch);
		
		// simpan data
		this.batchService.delete(batchModel);
		
		String jsp = "batch/batch"; // setelah nge-save, kembali ke sini lagi
		return jsp;
	}*/
	
	// untuk menghapus data batch
//		@RequestMapping(value="batch/cruds/delete")
//		public String batchDelete(HttpServletRequest request) {
//			
//			// get nilai dari variabel nama di jsp
//			Integer id = Integer.valueOf(request.getParameter("id"));
//			
//			// set nilai ke variabel di modelnya
//			BatchModel batchModel = new BatchModel();
//			batchModel = this.batchService.searchId(id);
//			batchModel.setIsDelete(true);
//
///*			// simpan data
//			this.batchService.delete(batchModel);*/
//			
//			// save audit trail untuk deleted
////			Integer xIdDeletedBy = this.userSearch().getIdUser();
////			batchModel.setxIdDeletedBy(xIdDeletedBy);
////			batchModel.setxDeletedDate(new Date());
//			
//			// save audit trail untuk deleted, username adalah arif, jadi line di bawah nggak dipake
////			//Integer modifiedBy = this.userSearch().getId();
//			batchModel.setDeletedBy(2);
//			batchModel.setDeletedOn(new Date());
//			
//			// simpan data
//			this.batchService.deleteTemporary(batchModel);
//			
//			String jsp = "batch/batch"; // setelah nge-save, kembali ke sini lagi
//			return jsp;
//		}
//	
	@RequestMapping(value="batch/cruds/search")
	public String batchSearch(HttpServletRequest request, Model model) {
		String technology = request.getParameter("keyword");
		String name = request.getParameter("keyword");
		List<BatchModel> batchModelList = new ArrayList<BatchModel>();
		batchModelList = this.batchService.searchTechnologyOrName(technology, name);
		model.addAttribute("batchModelList", batchModelList);
		
		String jsp = "batch/cruds/list";
		return jsp;
	}
//	
//	@RequestMapping(value="batch/cruds/search/kode")
//	public String batchSearchKode(HttpServletRequest request, Model model) {
//		String kodeBatch = request.getParameter("kodeBatchKey");
//		List<BatchModel> batchModelList = new ArrayList<BatchModel>();
//		batchModelList = this.batchService.searchKode(kodeBatch);
//		model.addAttribute("batchModelList", batchModelList);
//		
//		String jsp = "batch/cruds/list";
//		return jsp;
//	}
//	
//	@RequestMapping(value="batch/cruds/search")
//	public String batchSearchKodeOrNama(HttpServletRequest request, Model model) {
//		String kodeBatch = request.getParameter("keyword");
//		String namaBatch = request.getParameter("keyword");
//		List<BatchModel> batchModelList = new ArrayList<BatchModel>();
//		batchModelList = this.batchService.searchKodeOrNama(kodeBatch, namaBatch);
//		model.addAttribute("batchModelList", batchModelList);
//		
//		String jsp = "batch/cruds/list";
//		return jsp;
//	}
//	
//	public Integer cekKodeBatch(String kodeBatch) {
//		List<BatchModel> batchModelList = new ArrayList<BatchModel>();
//		batchModelList = this.batchService.searchKodeEqual(kodeBatch);
//		Integer jumlahDataBatch = 0;
//		if (batchModelList == null) {
//			jumlahDataBatch = 0;
//		} else {
//			jumlahDataBatch = batchModelList.size();
//		}
//		return jumlahDataBatch;
//	}
//	
//	public Integer cekNamaBatch(String namaBatch, Integer isDelete) {
//		List<BatchModel> batchModelList = new ArrayList<BatchModel>();
//		batchModelList = this.batchService.searchNamaEqual(namaBatch, isDelete);
//		Integer jumlahDataBatch = 0;
//		if (batchModelList == null) {
//			jumlahDataBatch = 0;
//		} else {
//			jumlahDataBatch = batchModelList.size();
//		}
//		return jumlahDataBatch;
//	}
//	
//	public String generateKodeBatch() {
//		String kodeBatchAuto = "FK";
//		
//		List<BatchModel> batchModelList = new ArrayList<BatchModel>();
//		String kodeRole = this.userSearch().getRoleModel().getKodeRole();
//		batchModelList = this.batchService.search(kodeRole);
//		Integer jumlahDataBatch = 0;
//		
//		if (batchModelList == null) {
//			jumlahDataBatch = 0;
//		} else {
//			jumlahDataBatch = batchModelList.size();
//		}
//		
//		kodeBatchAuto = kodeBatchAuto + "000" + (jumlahDataBatch+1);
//		
//		return kodeBatchAuto;
//	}
//	
	public void roomList(Model model) {
		List<RoomModel> roomModelList = new ArrayList<RoomModel>();
		roomModelList = this.roomService.search();
		model.addAttribute("roomModelList", roomModelList);
	}
	
	public void technologyList(Model model) {
		List<TechnologyModel> technologyModelList = new ArrayList<TechnologyModel>();
		technologyModelList = this.technologyService.search();
		model.addAttribute("technologyModelList", technologyModelList);
	}
	
	public void trainerList(Model model) {
		List<TrainerModel> trainerModelList = new ArrayList<TrainerModel>();
		trainerModelList = this.trainerService.search();
		model.addAttribute("trainerModelList", trainerModelList);
	}
	
	public void bootcampTypeList(Model model) {
		List<BootcampTypeModel> bootcampTypeModelList = new ArrayList<BootcampTypeModel>();
		bootcampTypeModelList = this.bootcampTypeService.search();
		model.addAttribute("bootcampTypeModelList", bootcampTypeModelList);
	}
	
	/* Isi dropdown dengan semua biodata */
//	public void biodataList(Model model) {
//		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
//		biodataModelList = this.biodataService.search();
//		model.addAttribute("biodataModelList", biodataModelList);
//	}
	
	/* Isi dropdown dengan biodata yang belum dipilih dalam suatu batch tertentu 
	 * dan biodata yang sudah terpilih di batch lain tapi dengan periode yang berbeda */
	public void biodataList(Model model, Integer batchId) {
		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
		biodataModelList = this.classService.unchosenPeople(batchId);
		model.addAttribute("biodataModelList", biodataModelList);
	}
	
	public void testList(Model model, Integer id) {
		List<TestModel> testModelList = new ArrayList<TestModel>();
//		testModelList = this.testService.search();
		
		/*
		 * Untuk mencari daftar Test yang merupakan test untuk bootcamp sehingga tidak
		 * perlu menggunakan <c:if
		 * test="${(testModel.isBootcampTest == true) && (testModel.isDelete != true)}">
		 * pada setup-test.jsp
		 */
		testModelList = this.batchTestService.searchBootcampTest();
		
		// tambahin booelan satu di TestModel, nama bebas, jangan diberi anotasi.
		// One-to-Many, seperti DocumentTest, lalu set boolean nya true/false
		// sehingga tidak perlu method batchTestList()
		// ketika semua test di-list di pop up nya, langsung bisa..
		List<BatchTestModel> chosenTestList = this.batchTestService.searchChosenTest(id);
		
		model.addAttribute("testModelList", testModelList);
	}
	
	public void batchTestList(Model model, Integer batchId) {
		List<BatchTestModel> listOfChosenTest = new ArrayList<BatchTestModel>();
		listOfChosenTest = this.batchTestService.searchChosenTest(batchId);
		
		model.addAttribute("listOfChosenTest", listOfChosenTest);
	}
	
	private String ambilTgl(String tgl) throws ParseException{
		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(tgl);
	    return new SimpleDateFormat("dd-MM-yyyy").format(date);
	}
	
//	public boolean regexDigit(String isian) {
//		String pattern = "(\\d+)";
//		Pattern p = Pattern.compile(pattern);
//		Matcher m = p.matcher(isian);
//		if(m.find()) {
//			return true;
//		}
//		else {
//			return false;
//		}
//	}
}
