package com.xsis.winners.xbc184.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.BootcampTypeModel;
import com.xsis.winners.xbc184.service.BootcampTypeService;

@Controller
public class BootcampTypeController extends AuditLogController {
	
	@Autowired
	private BootcampTypeService bootcampTypeService;

	@RequestMapping(value="bootcampType")
	public String bootcampType() {
		String jsp = "bootcampType/bootcampType";
		return jsp;
	}
	
	@RequestMapping(value="bootcampType//cruds/add")
	public String bootcampTypeAdd() {
		String jsp = "bootcampType/cruds/add";
		return jsp;
	}
	
	@RequestMapping(value="bootcampType/cruds/create")
	public String bootcampTypeCreate(HttpServletRequest request) {
		
		//get nilai dari jsp
		String nameBootcampType = request.getParameter("nameBootcampType");
		String notesBootcampType = request.getParameter("notesBootcampType");
		
		//set nilai ke variabel modelnya
		BootcampTypeModel bootcampTypeModel = new BootcampTypeModel();
		bootcampTypeModel.setNameBootcampType(nameBootcampType);
		bootcampTypeModel.setNotesBootcampType(notesBootcampType);
		
		//save audit trail untuk created
		Integer xIdCreatedBy = this.userSearch().getIdUser();
		bootcampTypeModel.setxIdCreatedBy(xIdCreatedBy);
		bootcampTypeModel.setxCreatedDate(new Date());
		bootcampTypeModel.setIsDelete(false);
		
		//save
		this.bootcampTypeService.create(bootcampTypeModel);
		this.saveLog(bootcampTypeModel);
		
		String jsp = "bootcampType/bootcampType";
		return jsp;
	}
	
	@RequestMapping(value="bootcampType/cruds/list")
	public String bootcampTypeList(Model model) {
		List<BootcampTypeModel> bootcampTypeModelList = new ArrayList<BootcampTypeModel>();
		//String kodeRole = this.userSearch().getRoleModel().getKodeRole();
		bootcampTypeModelList = this.bootcampTypeService.search();
		model.addAttribute("bootcampTypeModelList", bootcampTypeModelList);
		
		String jsp = "bootcampType/cruds/list";
		return jsp;
	}
	
	
	@RequestMapping(value = "bootcampType/cruds/edit")
	public String bootcampTypeEdit(HttpServletRequest request, Model model) {
		Integer idBootcampType = Integer.valueOf(request.getParameter("idBootcampType"));
		
		BootcampTypeModel bootcampTypeModel = new BootcampTypeModel();
		bootcampTypeModel = this.bootcampTypeService.searchId(idBootcampType);
				
		model.addAttribute("bootcampTypeModel", bootcampTypeModel);
			
		String jsp = "bootcampType/cruds/edit"; 
		return jsp;
	}
	
	@RequestMapping(value="bootcampType/cruds/update")
	public String bootcampTypeUpdate(HttpServletRequest request) {
		
		//get niali dari variabel name di jsp
		Integer idBootcampType = Integer.valueOf(request.getParameter("idBootcampType"));
		String nameBootcampType = request.getParameter("nameBootcampType");
		String notesBootcampType = request.getParameter("notesBootcampType");
		
		//set niali ke variabel modelnya
		BootcampTypeModel bootcampTypeModelAfter = new BootcampTypeModel();
		bootcampTypeModelAfter = this.bootcampTypeService.searchId(idBootcampType);
		bootcampTypeModelAfter.setNameBootcampType(nameBootcampType);
		bootcampTypeModelAfter.setNotesBootcampType(notesBootcampType);
		
		BootcampTypeModel bootcampTypeModelBefore = new BootcampTypeModel();
		bootcampTypeModelBefore = this.bootcampTypeService.searchId(idBootcampType);
		
		//audit trail
		Integer xIdModifiedBy = this.userSearch().getIdUser();
		bootcampTypeModelAfter.setxIdModifiedBy(xIdModifiedBy);
		bootcampTypeModelAfter.setxModifiedDate(new Date());
		//update
		this.bootcampTypeService.update(bootcampTypeModelAfter);
		this.updateLog(bootcampTypeModelBefore, bootcampTypeModelAfter);
		
		String jsp = "bootcampType/bootcampType";
		return jsp;
	}
	
	@RequestMapping(value="bootcampType/cruds/search/name")
	public String bootcampTypeSearchName(HttpServletRequest request, Model model) {
		String nameBootcampType = request.getParameter("nameBootcampTypeKey");
		List<BootcampTypeModel> bootcampTypeModelList = new ArrayList<BootcampTypeModel>();
		bootcampTypeModelList = this.bootcampTypeService.searchNama(nameBootcampType);
		
		model.addAttribute("bootcampTypeModelList", bootcampTypeModelList);
		
		String jsp = "bootcampType/cruds/list";
		return jsp;
	}
		
	@RequestMapping(value="bootcampType/cruds/remove")
	public String bootcampTypeRemove(HttpServletRequest request, Model model) {
		Integer idBootcampType = Integer.valueOf(request.getParameter("idBootcampType"));
		
		BootcampTypeModel bootcampTypeModel = new BootcampTypeModel();
		bootcampTypeModel = this.bootcampTypeService.searchId(idBootcampType);
		model.addAttribute("bootcampTypeModel", bootcampTypeModel);
		
		String jsp = "bootcampType/cruds/remove";
		return jsp;
	}
	
	@RequestMapping(value="bootcampType/cruds/delete")
	public String bootcampTypeDelete (HttpServletRequest request) {
		//get nilai dari jsp
		Integer idBootcampType = Integer.valueOf(request.getParameter("idBootcampType"));
		
		BootcampTypeModel bootcampTypeModelAfter = new BootcampTypeModel();
		bootcampTypeModelAfter = this.bootcampTypeService.searchId(idBootcampType);
		bootcampTypeModelAfter.setIsDelete(true); // 1 artinya telah dihapus temporary
		
		BootcampTypeModel bootcampTypeModelBefore = new BootcampTypeModel();
		bootcampTypeModelBefore = this.bootcampTypeService.searchId(idBootcampType);
		
		//audit trail
		Integer xIdDeletedBy = this.userSearch().getIdUser();
		bootcampTypeModelAfter.setxIdDeletedBy(xIdDeletedBy);
		bootcampTypeModelAfter.setxDeletedDate(new Date());
		
		//delete temporary
		this.bootcampTypeService.deleteTemporary(bootcampTypeModelAfter);
		this.updateLog(bootcampTypeModelBefore, bootcampTypeModelAfter);
		String jsp = "bootcampType/bootcampType";
		return jsp;
	}
		
}
