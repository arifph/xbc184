package com.xsis.winners.xbc184.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;

import com.xsis.winners.xbc184.model.UserModel;
import com.xsis.winners.xbc184.service.UserService;

@Controller
public class MUserController extends MenuListController{

	@Autowired
	private UserService userService;
	
	
	public UserModel userSearch() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		UserModel userModel = new UserModel();
		if (auth != null) {
			User user = (User) auth.getPrincipal();
			String username = user.getUsername(); //usernameSecurityCore
			String password = user.getPassword();
			
			try {
				userModel = this.userService.searchUsernamePassword(username, password);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		} else {

		}
		return userModel;
	}
	
	
	
}
