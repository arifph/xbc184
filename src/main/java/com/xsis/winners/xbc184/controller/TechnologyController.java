package com.xsis.winners.xbc184.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.TechnologyModel;
import com.xsis.winners.xbc184.model.TechnologyTrainerModel;
import com.xsis.winners.xbc184.model.TrainerModel;
import com.xsis.winners.xbc184.model.UserModel;
import com.xsis.winners.xbc184.service.TechnologyService;
import com.xsis.winners.xbc184.service.TechnologyTrainerService;
import com.xsis.winners.xbc184.service.TrainerService;
import com.xsis.winners.xbc184.service.UserService;

@Controller
public class TechnologyController extends AuditLogController {

	@Autowired
	private TechnologyService technologyService;
	@Autowired
	private UserService userService;
	@Autowired
	private TechnologyTrainerService technologyTrainerService;
	@Autowired
	private TrainerService trainerService;

	private List<TechnologyTrainerModel> technologyTrainerModelList = new ArrayList<>();
	private List<TechnologyTrainerModel> updateArray = new ArrayList<>();
	private Integer idTech ;

	@RequestMapping(value = "technology")
	public String Technology() {

		String jsp = "technology/technology";
		return jsp;
	}

	@RequestMapping(value = "technology/cruds/list")
	public String TechnologyList(Model model) {
		List<TechnologyModel> technologyModelList = new ArrayList<TechnologyModel>();
		String kodeRole = this.userSearch().getRoleModel().getKodeRole();
		technologyModelList = this.technologyService.search(kodeRole);

//		List<UserModel> userModelList = new ArrayList<UserModel>();
//		userModelList = this.userService.search();
		
		model.addAttribute("technologyModelList", technologyModelList);
//		model.addAttribute("userModelList", userModelList);

		String jsp = "technology/cruds/list";
		return jsp;
	}

	@RequestMapping(value = "technology/cruds/add")
	public String TechnologyAdd(Model model) {
		String jsp = "technology/cruds/add";

		return jsp;
	}

	@RequestMapping(value = "technology/cruds/trainer/add")
	public String TechListTrainer(HttpServletRequest request, Model model) {
		
		List<TrainerModel> trainerModelList = new ArrayList<TrainerModel>();
		String kodeRole = this.userSearch().getRoleModel().getKodeRole();
		trainerModelList = this.trainerService.search(kodeRole);
		
		//BELAJAR
		for ( TechnologyTrainerModel technologyTrainerModel : technologyTrainerModelList) {
			if(trainerModelList.contains(technologyTrainerModel.getTrainerModel())) {
				trainerModelList.remove(technologyTrainerModel.getTrainerModel());
			}
		}
		
		model.addAttribute("trainerModelList", trainerModelList);

		String jsp = "technology/cruds/chooseTrainer";
		return jsp;
	}
	//save trainer choosed
	@RequestMapping(value = "technologytrainer/cruds/add")
	public String TechnologyTrainerAdd(HttpServletRequest request) {
		Integer id = Integer.valueOf(request.getParameter("trainerId"));
		TrainerModel trainerModel = trainerService.searchId(id);

		TechnologyTrainerModel technologyTrainerModel = new TechnologyTrainerModel();

		Integer idUser = this.userSearch().getIdUser();
		technologyTrainerModel.setTrainerModel(trainerModel);
		technologyTrainerModel.setCreatedBy(idUser);
		technologyTrainerModel.setCreatedOn(new Date());
		technologyTrainerModelList.add(technologyTrainerModel);

//		model.addAttribute("technologyTrainerModelList", technologyTrainerModelList);

		String jsp = "technology/cruds/add";

		return jsp;
	}

	@RequestMapping(value = "technologytrainer/cruds/addList")
	public String TechnologyTrainerList(Model model) {
		model.addAttribute("technologyTrainerModelList", technologyTrainerModelList);

		String jsp = "technology/cruds/addList";
		return jsp;
	}
	
	@RequestMapping(value = "technology/cruds/trainer/deleteAdd")
	public String TechnologyTrainerDeleteTrainer(HttpServletRequest request, Model model) {
		Integer index = Integer.valueOf(request.getParameter("index"));

		model.addAttribute("index", index);

		String jsp = "technology/cruds/deleteAdd";
		return jsp;
	}
	
	@RequestMapping(value = "technology/cruds/trainer/removeAdd")
	public String TechnologyTrainerRemoveTrainer(HttpServletRequest request, Model model) {
		Integer index = Integer.valueOf(request.getParameter("index"));
		technologyTrainerModelList.remove(index.intValue());

		model.addAttribute("technologyTrainerModelList", technologyTrainerModelList);

		String jsp = "technology/cruds/addList";
		return jsp;
	}

	@RequestMapping(value = "technologytrainer/cruds/createTechnology")
	public String TechnologyTrainerCreateTechnology(HttpServletRequest request, Model model) {
		String name = request.getParameter("name");
		String notes = request.getParameter("notes");

		TechnologyModel technologyModel = new TechnologyModel();

		technologyModel.setName(name);
		technologyModel.setNotes(notes);

		technologyModel.setIsDelete(false);

		Integer idUser = this.userSearch().getIdUser();
		technologyModel.setCreatedBy(idUser);
		technologyModel.setCreatedOn(new Date());

		// TechnologyTrainerModel technologyTrainerModel = new TechnologyTrainerModel();
		for (int i = 0; i < technologyTrainerModelList.size(); i++) {
			technologyTrainerModelList.get(i).setTechnologyModel(technologyModel);
		}

		technologyModel.setTechnologyTrainerModelList(technologyTrainerModelList);

		this.technologyService.create(technologyModel);
		//AUDIT LOG
		this.saveLog(technologyModel);
		String jsp = "technology/technology";
		return jsp;
	}

	// Mulai mapping EDIT
	@RequestMapping(value = "technology/cruds/edit")
	public String TechnologyEdit(HttpServletRequest request, Model model) {

		Integer id = Integer.valueOf(request.getParameter("id"));
		TechnologyModel technologyModel = new TechnologyModel();
		technologyModel = this.technologyService.searchId(id);

		model.addAttribute("technologyModel", technologyModel);
		
		idTech = id;
		
		String jsp = "technology/cruds/edit";
		return jsp;
	}

	// List data lama yang sudah ada pada database
	@RequestMapping(value = "technology/cruds/edit/editList")
	public String TechnologyEditList(Model model) {
		technologyTrainerModelList = this.technologyTrainerService.searchIdTrainer(idTech);
		model.addAttribute("technologyTrainerModelList", technologyTrainerModelList);
		
		String jsp = "technology/cruds/editList";
		return jsp;
	}

	// LoadList data baru yang dimasukkan
	@RequestMapping(value = "technology/cruds/edit/editListUpdate")
	public String TechnologyEditListUpdate(Model model) {
		model.addAttribute("updateArray", updateArray);

		String jsp = "technology/cruds/editListUpdate";
		return jsp;
	}
	
	//tombol tambah trainer
	@RequestMapping(value = "technology/cruds/trainer/edit")
	public String TechListTrainerEdit(HttpServletRequest request, Model model) {

		List<TrainerModel> trainerModelList = new ArrayList<TrainerModel>();
		String kodeRole = this.userSearch().getRoleModel().getKodeRole();

		trainerModelList = this.trainerService.search(kodeRole);
		
		for ( TechnologyTrainerModel technologyTrainerModel : technologyTrainerModelList) {
			if(trainerModelList.contains(technologyTrainerModel.getTrainerModel())) {
				trainerModelList.remove(technologyTrainerModel.getTrainerModel());
			}
		}
		
		for ( TechnologyTrainerModel technologyTrainerModelUpdate : updateArray) {
			if(trainerModelList.contains(technologyTrainerModelUpdate.getTrainerModel())) {
				trainerModelList.remove(technologyTrainerModelUpdate.getTrainerModel());
			}
		}
		
		model.addAttribute("trainerModelList", trainerModelList);

		String jsp = "technology/cruds/chooseEditTrainer";
		return jsp;
	}
	
	//choose trainer baru
	@RequestMapping(value = "technologytrainer/cruds/edit/addTrainer")
	public String TechnologyTrainerEditAddTrainer(HttpServletRequest request) {
		Integer id = Integer.valueOf(request.getParameter("id"));
		TrainerModel trainerModel = trainerService.searchId(id);

		TechnologyTrainerModel technologyTrainerModel = new TechnologyTrainerModel();

		Integer idUser = this.userSearch().getIdUser();
		technologyTrainerModel.setTrainerModel(trainerModel);
		technologyTrainerModel.setCreatedBy(idUser);
		technologyTrainerModel.setCreatedOn(new Date());
		updateArray.add(technologyTrainerModel);

		String jsp = "technology/cruds/edit";

		return jsp;
	}

	// MEMUNCULKAN KONFIRMASI DELETE TRAINER
	@RequestMapping(value = "technologytrainer/cruds/edit/deleteTrainer")
	public String TechnologyTrainerDeleteTrainerEdit(HttpServletRequest request, Model model) {
		Integer idTechnologyTrainer = Integer.valueOf(request.getParameter("id"));
		model.addAttribute("idTechnologyTrainer", idTechnologyTrainer);
		
		String jsp = "technology/cruds/deleteEdit";
		return jsp;
	}

	// EKSEKUSI DELETE
	@RequestMapping(value = "technologytrainer/cruds/edit/removeTrainer")
	public String TechnologyTrainerEditDeleteTrainer(HttpServletRequest request, Model model) {
		Integer id = Integer.valueOf(request.getParameter("id"));
		TechnologyTrainerModel deleteOldData = new TechnologyTrainerModel();
		deleteOldData = this.technologyTrainerService.searchId(id);
		this.technologyTrainerService.delete(deleteOldData);
		
		String jsp = "technology/cruds/edit";
		return jsp;
	}
	
	// MEMUNCULKAN KONFIRMASI DELETE TRAINER UPDATE
		@RequestMapping(value = "technologytrainer/cruds/edit/deleteTrainerUpdate")
		public String TechnologyTrainerDeleteTrainerEditUpdate(HttpServletRequest request, Model model) {
			Integer idTechnologyTrainer = Integer.valueOf(request.getParameter("id"));
			model.addAttribute("idTechnologyTrainer", idTechnologyTrainer);

			String jsp = "technology/cruds/deleteEditUpdate";
			return jsp;
		}
	
	//EKSEKUSI DELETE TRAINER BARU
	@RequestMapping(value = "technologytrainer/cruds/edit/removeTrainerUpdate")
	public String TechnologyTrainerEditDeleteTrainerUpdate(HttpServletRequest request, Model model) {
		Integer index = Integer.valueOf(request.getParameter("id"));
		updateArray.remove(index.intValue());

		String jsp = "technology/cruds/edit";
		return jsp;
	}

	@RequestMapping(value = "technology/cruds/update")
	public String TechnologyUpdate(HttpServletRequest request) {
		
		Integer id = Integer.valueOf(request.getParameter("id"));
		Integer createdBy = Integer.valueOf(request.getParameter("createdBy"));
		String date = request.getParameter("createdOn");
		
		//AUDIT LOG
		TechnologyModel technologyModelBefore = this.technologyService.searchId(id);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		
		Date createdOn = null;
		try {
			createdOn = dateFormat.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String name = request.getParameter("name");
		String notes = request.getParameter("notes");

		TechnologyModel technologyModelAfter = new TechnologyModel();
		
		technologyModelAfter.setId(id);
		
		technologyModelAfter.setName(name);
		technologyModelAfter.setNotes(notes);
		
		technologyModelAfter.setIsDelete(false);
		
		technologyModelAfter.setCreatedBy(createdBy);
		technologyModelAfter.setCreatedOn(createdOn);
		 		
		Integer idUser = this.userSearch().getIdUser();
		technologyModelAfter.setModifiedBy(idUser);
		technologyModelAfter.setModifiedOn(new Date());
		
		// TechnologyTrainerModel technologyTrainerModel = new TechnologyTrainerModel();
		for (int i = 0; i < updateArray.size(); i++) {
			updateArray.get(i).setTechnologyModel(technologyModelAfter);
		}

		technologyModelAfter.setTechnologyTrainerModelList(updateArray);
		
		this.technologyService.update(technologyModelAfter);
		
		//AUDIT LOG
		this.updateLog(technologyModelBefore, technologyModelAfter);
		
		String jsp = "technology/technology";
		return jsp;
	}

	@RequestMapping(value = "technology/cruds/delete")
	public String TechnologyDelete(HttpServletRequest request, Model model) {
		Integer id = Integer.valueOf(request.getParameter("id"));

		TechnologyModel technologyModel = new TechnologyModel();

		technologyModel = this.technologyService.searchId(id);

		model.addAttribute("technologyModel", technologyModel);

		String jsp = "technology/cruds/deleteTechnology";
		return jsp;
	}

	@RequestMapping(value = "technology/cruds/remove")
	public String TechnologyRemove(HttpServletRequest request) {

		Integer id = Integer.valueOf(request.getParameter("id"));
		TechnologyModel technologyModelBefore = new TechnologyModel();
		
		technologyModelBefore = this.technologyService.searchId(id);
		
		TechnologyModel technologyModel = new TechnologyModel();

		technologyModel = this.technologyService.searchId(id);

		technologyModel.setIsDelete(true);

		Integer idUser = this.userSearch().getIdUser();
		technologyModel.setDeletedBy(idUser);
		technologyModel.setDeletedOn(new Date());

		this.technologyService.deleteTemporary(technologyModel);
		
		this.updateLog(technologyModelBefore, technologyModel);
		
//		List<TechnologyTrainerModel> deleteTrainerModelList = new ArrayList<TechnologyTrainerModel>();
//		deleteTrainerModelList = this.technologyTrainerService.searchIdTrainer(id);
//		
//		for (int i=0; i<deleteTrainerModelList.size(); i++) {
//			this.technologyTrainerService.delete(deleteTrainerModelList.get(i));
//		}
		
		String jsp = "technology/technology";
		return jsp;
	}

	@RequestMapping(value = "technology/cruds/search")
	public String TechnologySearch(HttpServletRequest request, Model model) {

		String name = request.getParameter("name");
		List<TechnologyModel> trtechnologyModelList = new ArrayList<TechnologyModel>();
		trtechnologyModelList = this.technologyService.searchNama(name);
		
		model.addAttribute("technologyModelList", trtechnologyModelList);
		
//		List<UserModel> userModelList = new ArrayList<UserModel>();
//		userModelList = this.userService.search();
		
//		model.addAttribute("userModelList", userModelList);
		
		String jsp = "technology/cruds/list";
		return jsp;
	}

	@RequestMapping(value = "technologytrainer/cruds/removeList")
	public String TechnologyTrainerRemoveList(Model model) {
		technologyTrainerModelList.clear();
		return null;
	}

	@RequestMapping(value = "technologytrainer/cruds/edit/removeList")
	public String TechnologyTrainerRemoveListEdit(Model model) {
		updateArray.clear();
		return null;
	}

}
