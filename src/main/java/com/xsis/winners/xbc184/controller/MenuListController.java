package com.xsis.winners.xbc184.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import com.xsis.winners.xbc184.model.MenuModel;
import com.xsis.winners.xbc184.service.MenuService;

@Controller
public class MenuListController {

	@Autowired
	private MenuService menuService;
	
	
	public void menuListSideMaster(Model model) {
		List<MenuModel> menuModelMasterList = new ArrayList<>(); 
		menuModelMasterList = this.menuService.searchMenuMaster();
		model.addAttribute("menuModelMasterList", menuModelMasterList);
	}
	
	public void menuListSideParent(Model model) {
		List<MenuModel> menuModelParentList = new ArrayList<>(); 
		menuModelParentList = this.menuService.searchMenuParent();
		model.addAttribute("menuModelParentList", menuModelParentList);
	}
	
	public void menuListAll(Model model) {
		List<MenuModel> menuModelList = new ArrayList<>();
		menuModelList = this.menuService.search();
		model.addAttribute("menuModelList", menuModelList);
	}
	
	
	
}
