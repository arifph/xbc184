package com.xsis.winners.xbc184.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.FeedbackModel;
import com.xsis.winners.xbc184.model.RoleModel;
import com.xsis.winners.xbc184.model.TestModel;
import com.xsis.winners.xbc184.service.FeedbackService;
import com.xsis.winners.xbc184.service.TestService;

@Controller
public class FeedbackController extends UserController{
	
	@Autowired
	private FeedbackService feedbackService;
	
	@Autowired
	private TestService testService;
	
	@RequestMapping(value= "feedback")
	public String feedback() {
		String jsp = "feedback/feedback";
		return jsp;
	}
	
	@RequestMapping(value= "feedback/cruds/list")
	public String feedbackList(Model model) {
		List<FeedbackModel> feedbackModelList = new ArrayList<FeedbackModel>();
		feedbackModelList = this.feedbackService.search();
		model.addAttribute("feedbackModelList", feedbackModelList);
		String jsp = "feedback/cruds/list";
		return jsp;
	}
	
	/*public void testList(Model model) {
		List<TestModel> testModelList = new ArrayList<TestModel>();
		testModelList = this.testService.searchFeedback();
		model.addAttribute("testModelList", testModelList);
	}*/
	
	@RequestMapping(value= "feedback/cruds/add")
	public String feedbackAdd(Model model) {
		
		String jsp = "feedback/cruds/add";
		return jsp;
	}
	
}
