package com.xsis.winners.xbc184.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.CategoryModel;
import com.xsis.winners.xbc184.service.CategoryService;

@Controller
public class CategoryController extends AuditLogController {

	private static Logger logger = LogManager.getLogger();
	
	@Autowired
	private CategoryService categoryService;

	@RequestMapping(value="category")
	public String category() {
		logger.info("Mapping category, you should see category page after this method");

		String jsp = "category/category";
		return jsp;
	}
	
	@RequestMapping(value="category/cruds/list")
	public String categoryList(Model model) {
		logger.info("Showing List of Category");
		
		List<CategoryModel> categoryModelList = this.categoryService.search(false);
		logger.info("Category List Size : " + categoryModelList == null ? 0 : categoryModelList.size());
		
		model.addAttribute("categoryModelList", categoryModelList);

		String jsp = "category/cruds/list";
		return jsp;
	}

	@RequestMapping(value="category/cruds/add")
	public String categoryAdd() {
		logger.info("Showing add dialog");

		String jsp = "category/cruds/add";
		return jsp;
	}
	
	@RequestMapping(value="category/cruds/create")
	public String categoryCreate(CategoryModel category) {
		
		logger.info("Building category object");
		String code = getGeneratedCode();
		Boolean isDelete = false;
		Integer createdBy = this.userSearch().getIdUser();
		Date createdOn = new Date();
		
		category.setCode(code);
		category.setCreatedBy(createdBy);
		category.setCreatedOn(createdOn);
		category.setIsDelete(isDelete);
		
		logger.info(category);
		
		logger.info("Saving category");
		this.categoryService.saveCategory(category);
		this.saveLog(category);
		
		String jsp = "category/category";
		return jsp;
	}
	
	@RequestMapping(value="category/cruds/edit")
	public String categoryEdit(HttpServletRequest request, Model model) {

		Integer id = Integer.valueOf(request.getParameter("id"));
		logger.info("Category id : " + id);
		
		CategoryModel categoryModel = this.categoryService.searchById(id);
		
		if (categoryModel == null) {
			logger.info("Category not Found");
		} else {
			logger.info("Category Found");
		}
		
		logger.info("Passing parameter to jsp");
		model.addAttribute(categoryModel);
		
		logger.info("Showing edit dialog");
		String jsp = "category/cruds/edit";
		return jsp;
	}
	
	@RequestMapping(value="category/cruds/update")
	public String categoryUpdate(CategoryModel categoryAfter) {

		logger.info("Building category object");
		// mock
		Integer modifiedBy = 2;
		Date modifiedOn = new Date();
		
		CategoryModel categoryBefore = this.categoryService.searchById(categoryAfter.getId());
		
		if (categoryBefore == null) {
			logger.info("Category not Found");
		} else {
			logger.info("Category Found");
		}
		
		// Set created by untuk category after karena category after tidak memiliki created by
		categoryAfter.setCreatedBy(categoryBefore.getCreatedBy());
		categoryAfter.setCreatedOn(categoryBefore.getCreatedOn());
		categoryAfter.setIsDelete(false);
		categoryAfter.setModifiedBy(modifiedBy);
		categoryAfter.setModifiedOn(modifiedOn);
		
		logger.info("Category After: " + categoryAfter);
		logger.info("Category Before: " + categoryBefore);
		
		logger.info("Saving category");
		this.categoryService.updateCategory(categoryAfter);
		this.updateLog(categoryBefore, categoryAfter);
		
		String jsp = "category/category";
		return jsp;
	}
	
	@RequestMapping(value="category/cruds/confirmation")
	public String categoryConfirmation(HttpServletRequest request, Model model) {

		Integer id = Integer.valueOf(request.getParameter("id"));
		logger.info("Category id : " + id);
		
		CategoryModel categoryModel = this.categoryService.searchById(id);
		
		if (categoryModel == null) {
			logger.info("Category not Found");
		} else {
			logger.info("Category Found");
		}
		
		logger.info("Passing parameter to jsp");
		model.addAttribute(categoryModel);
		
		logger.info("Showing edit dialog");
		String jsp = "category/cruds/delete";
		return jsp;
	}
	
	@RequestMapping(value="category/cruds/delete")
	public String categoryDelete(HttpServletRequest request) {

		logger.info("Building category object");
		Integer id = Integer.valueOf(request.getParameter("id"));
		Integer deletedBy = 2;
		Date deletedOn = new Date();
		
		CategoryModel categoryModel = this.categoryService.searchById(id);
		
		if (categoryModel == null) {
			logger.info("Category not Found");
		} else {
			logger.info("Category Found");
		}
		
		categoryModel.setIsDelete(true);
		categoryModel.setDeletedBy(deletedBy);
		categoryModel.setDeletedDate(deletedOn);
		
		logger.info(categoryModel);
		
		logger.info("Deleting category");
		this.categoryService.deleteCategory(categoryModel);
		this.deleteLog(categoryModel);
		
		String jsp = "category/category";
		return jsp;
	}
	
	@RequestMapping(value="category/search/code-and-name")
	public String categorySearchByCodeAndName(HttpServletRequest request, Model model) {
		logger.info("Searching data");
		String name = request.getParameter("keyword");
		String code = request.getParameter("keyword");
		Boolean isDelete = false;
		
		List<CategoryModel> categoryModelList = this.categoryService.search(code, name, isDelete);
		logger.info("Category List Size : " + (categoryModelList == null ? 0 : categoryModelList.size()));
		
		model.addAttribute("categoryModelList", categoryModelList);

		logger.info("Showing search result");
		String jsp = "category/cruds/list";
		return jsp;
	}
	
	private String getGeneratedCode() {
		List<CategoryModel> categoryModelList = this.categoryService.search();
		
		return String.format("C%04d", categoryModelList == null ? 1 : (categoryModelList.size() + 1));
	}
}
