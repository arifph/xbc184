package com.xsis.winners.xbc184.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.RoleModel;
import com.xsis.winners.xbc184.service.RoleService;

@Controller
public class RoleController extends AuditLogController {
	
	@Autowired
	private RoleService roleService;

	@RequestMapping(value= "role")
	public String role() {
		String jsp = "role/role";
		return jsp;
	}
	
	@RequestMapping(value = "role/cruds/add")
	public String roleAdd(Model model) {
		
		String jsp = "role/cruds/add";
		return jsp;
	}
	
	@RequestMapping(value = "role/cruds/create")
	public String roleCreate(HttpServletRequest request, Model model) {
		
		// get nilai dari variable name di jsp
		String kodeRole = request.getParameter("kodeRole");
		String namaRole = request.getParameter("namaRole");
		String deskripsi = request.getParameter("deskripsi");
		
		// set nilai ke variable di modelnya
		RoleModel roleModel = new RoleModel();
		roleModel.setKodeRole(kodeRole);
		roleModel.setNamaRole(namaRole);
		roleModel.setDeskripsi(deskripsi);
		roleModel.setIsDelete(false);
		
		// Cek DB
		Integer jumlahDataRole = this.cekNamaRole(namaRole);
		
		// Save Audit Trail Create
		Integer xIdCreatedBy = this.userSearch().getIdUser();
		roleModel.setxIdCreatedBy(xIdCreatedBy);
		roleModel.setxCreatedDate(new Date());
		
		if (jumlahDataRole > 0) {
			model.addAttribute("jumlahDataRole", jumlahDataRole);
			model.addAttribute("namaRole", namaRole);
		} else {
			this.roleService.create(roleModel);
			this.saveLog(roleModel);
		}
			
		String jsp = "role/role";
		return jsp;
	}
	
	@RequestMapping(value = "role/cruds/list")
	public String roleList(Model model) {
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		roleModelList = this.roleService.search();
		model.addAttribute("roleModelList", roleModelList);
		String jsp = "role/cruds/list";
		return jsp;
	}
	
	public Integer cekNamaRole(String namaRole) {
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		roleModelList = this.roleService.searchNamaEqual(namaRole);
		Integer jumlahDataRole = 0;
		if (roleModelList == null) {
			jumlahDataRole = 0;
		} else {
			jumlahDataRole = roleModelList.size();
		}
		return jumlahDataRole;
	}
	
	@RequestMapping(value = "role/cruds/edit")
	public String roleEdit(HttpServletRequest request, Model model) {
		Integer idRole = Integer.valueOf(request.getParameter("idRole"));
		
		RoleModel roleModel = new RoleModel();
		roleModel = this.roleService.searchId(idRole);
		model.addAttribute("roleModel", roleModel);
		
		String jsp = "role/cruds/edit";
		return jsp;
	}
	
	@RequestMapping(value = "role/cruds/update")
	public String roleUpdate(HttpServletRequest request) {
		
		// get nilai dari variable name di jsp
		Integer idRole = Integer.valueOf(request.getParameter("idRole"));
		String kodeRole = request.getParameter("kodeRole");
		String namaRole = request.getParameter("namaRole");
		String deskripsi = request.getParameter("deskripsi");
		
		// set nilai ke variable di modelnya
		RoleModel roleModelAfter = new RoleModel();
		roleModelAfter = this.roleService.searchId(idRole);
		roleModelAfter.setKodeRole(kodeRole);
		roleModelAfter.setNamaRole(namaRole);
		roleModelAfter.setDeskripsi(deskripsi);
		
		RoleModel roleModelBefore = new RoleModel();
		roleModelBefore = this.roleService.searchId(idRole);
		
		// Save Audit Trail Update
		Integer xIdUpdatedBy = this.userSearch().getIdUser();
		roleModelAfter.setxIdUpdatedBy(xIdUpdatedBy);
		roleModelAfter.setxUpdatedDate(new Date());
		
		// simpan data
		this.roleService.update(roleModelAfter);
		this.updateLog(roleModelBefore, roleModelAfter);
		
		String jsp = "role/role";
		return jsp;
	}
	
	@RequestMapping(value = "role/cruds/remove")
	public String roleRemove(HttpServletRequest request, Model model) {
		Integer idRole = Integer.valueOf(request.getParameter("idRole"));
		
		RoleModel roleModel = new RoleModel();
		roleModel = this.roleService.searchId(idRole);
		model.addAttribute("roleModel", roleModel);
		
		String jsp = "role/cruds/remove";
		return jsp;
	}
	
	@RequestMapping(value = "role/cruds/delete")
	public String roleDelete(HttpServletRequest request) {
		
		// get nilai dari variable name di jsp
		Integer idRole = Integer.valueOf(request.getParameter("idRole"));
		
		// set nilai ke variable di modelnya
		RoleModel roleModelAfter = new RoleModel();
		roleModelAfter = this.roleService.searchId(idRole);
		roleModelAfter.setIsDelete(true);
		
		RoleModel roleModelBefore = new RoleModel();
		roleModelBefore = this.roleService.searchId(idRole);
		
		// Save Audit Trail Delete
		Integer xIdDeletedBy = this.userSearch().getIdUser();
		roleModelAfter.setxIdDeletedBy(xIdDeletedBy);
		roleModelAfter.setxDeletedDate(new Date());
		
		// simpan data
		this.roleService.deleteTemp(roleModelAfter);
		this.updateLog(roleModelBefore, roleModelAfter);
		
		String jsp = "role/role";
		return jsp;
	}
	
	@RequestMapping(value= "role/cruds/search/nama")
	public String roleSearchNama(HttpServletRequest request, Model model) {
		String namaRole = request.getParameter("namaRoleKey");
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		roleModelList = this.roleService.searchNama(namaRole);
		
		model.addAttribute("roleModelList", roleModelList); 
		String jsp = "role/cruds/list";
		return jsp;
	}
	
}
