package com.xsis.winners.xbc184.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.TestModel;
import com.xsis.winners.xbc184.service.TestService;

@Controller
public class TestController extends AuditLogController {
	
	@Autowired
	private TestService testService;

	@RequestMapping(value="test")
	public String test() {
		String jsp = "test/test";
		return jsp;
	}

	@RequestMapping(value="test/cruds/add")
	public String testAdd() {
		String jsp = "test/cruds/add";
		return jsp;
	}
	
	@RequestMapping(value="test/cruds/create")
	public String testCreate(HttpServletRequest request) {
		
		String nameTest = request.getParameter("nameTest");
		String notesTest = request.getParameter("notesTest");
		Boolean isBootcampTest = Boolean.valueOf(request.getParameter("isBootcampTest"));
		
		TestModel testModel = new TestModel();
		testModel.setNameTest(nameTest);
		testModel.setNotesTest(notesTest);
		testModel.setIsBootcampTest(isBootcampTest);
		
		//audit trail
		Integer xIdCreatedBy = this.userSearch().getIdUser();
		testModel.setxIdCreatedBy(xIdCreatedBy);
		testModel.setxCreatedDate(new Date());
		testModel.setIsDelete(false);
		
		//save
		this.testService.create(testModel);
		this.saveLog(testModel);
		
		String jsp = "test/test";
		return jsp;
	}
	
	@RequestMapping(value="test/cruds/list")
	public String testList(Model model) {
		List<TestModel> testModelList = new ArrayList<TestModel>();
		testModelList = this.testService.search();
		model.addAttribute("testModelList", testModelList);
		
		String jsp = "test/cruds/list";
		return jsp;
	}
	
	@RequestMapping(value="test/cruds/search/name")
	public String testSearchName(HttpServletRequest request, Model model) {
		
		String nameTest = request.getParameter("nameTestKey");
		List<TestModel> testModelList = new ArrayList<TestModel>();
		testModelList = this.testService.searchNama(nameTest);
		model.addAttribute("testModelList", testModelList);
		
		String jsp = "test/cruds/list";
		return jsp;
	}
	
	@RequestMapping(value="test/cruds/edit")
	public String testEdit(HttpServletRequest request, Model model) {
		Integer idTest = Integer.valueOf(request.getParameter("idTest"));
		
		TestModel testModel = new TestModel();
		testModel = this.testService.searchId(idTest);
		
		model.addAttribute("testModel", testModel);
		
		String jsp = "test/cruds/edit";
		return jsp;
	}
	
	@RequestMapping(value="test/cruds/update")
	public String testUpdate(HttpServletRequest request) {
		
		Integer idTest = Integer.valueOf(request.getParameter("idTest"));
		String nameTest = request.getParameter("nameTest");
		String notesTest = request.getParameter("notesTest");
		Boolean isBootcampTest = Boolean.valueOf(request.getParameter("isBootcampTest"));
		
		TestModel testModelAfter = new TestModel();
		testModelAfter = this.testService.searchId(idTest);		
		testModelAfter.setNameTest(nameTest);
		testModelAfter.setNotesTest(notesTest);
		testModelAfter.setIsBootcampTest(isBootcampTest);
		
		TestModel testModelBefore = new TestModel();
		testModelBefore = this.testService.searchId(idTest);	
		
		//audit trail
		Integer xIdModifiedBy = this.userSearch().getIdUser();
		testModelAfter.setxIdModifiedBy(xIdModifiedBy);
		testModelAfter.setxModifiedDate(new Date());
		
		
		//update
		this.testService.update(testModelAfter);
		this.updateLog(testModelBefore, testModelAfter);
		
		String jsp = "test/test";
		return jsp;
	}
	
	@RequestMapping(value="test/cruds/remove")
	public String testRemove(HttpServletRequest request, Model model) {
		Integer idTest = Integer.valueOf(request.getParameter("idTest"));
		
		TestModel testModel = new TestModel();
		testModel = this.testService.searchId(idTest);
		
		model.addAttribute("testModel", testModel);
		
		String jsp = "test/cruds/remove";
		return jsp;
	}
	
	@RequestMapping(value="test/cruds/delete")
	public String testDelete(HttpServletRequest request) {
		
		Integer idTest = Integer.valueOf(request.getParameter("idTest"));
		
		TestModel testModelAfter = new TestModel();
		testModelAfter = this.testService.searchId(idTest);
		testModelAfter.setIsDelete(true);
		
		TestModel testModelBefore = new TestModel();
		testModelBefore = this.testService.searchId(idTest);
		
		//audit trail
		Integer xIdDeletedBy = this.userSearch().getIdUser();
		testModelAfter.setxIdDeletedBy(xIdDeletedBy);
		testModelAfter.setxDeletedDate(new Date());
		
		
		//delete
		this.testService.deleteTemporary(testModelAfter);
		this.updateLog(testModelBefore, testModelAfter);
		
		String jsp = "test/test";
		return jsp;
	}
}
