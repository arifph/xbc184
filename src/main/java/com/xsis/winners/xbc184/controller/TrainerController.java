	package com.xsis.winners.xbc184.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.TrainerModel;
import com.xsis.winners.xbc184.service.TrainerService;

@Controller
public class TrainerController extends AuditLogController{
	
	@Autowired
	private TrainerService trainerService;
	
	@RequestMapping(value ="trainer") 
	public String Trainer() { 

		String jsp = "trainer/trainer";
		return jsp;
	}
	
	@RequestMapping(value="trainer/cruds/list")
	public String TrainerList(Model model) {
		List<TrainerModel> trainerModelList = new ArrayList<TrainerModel>();
		String kodeRole = this.userSearch().getRoleModel().getKodeRole();
		
		trainerModelList = this.trainerService.search(kodeRole);
		model.addAttribute("trainerModelList", trainerModelList);
		
		String jsp = "trainer/cruds/list";
		return jsp;
	}
	
	@RequestMapping(value="trainer/cruds/add")
	public String TrainerAdd() {
		
		String jsp = "trainer/cruds/add";
		
		return jsp;
	}
	
	@RequestMapping(value="trainer/cruds/create")
	public String TrainerCreate(HttpServletRequest request) {
		
		String name = request.getParameter("name");
		String notes = request.getParameter("notes");
		
		TrainerModel trainerModel = new TrainerModel();
		
		trainerModel.setName(name);
		trainerModel.setNotes(notes);
		
		trainerModel.setIsDelete(false);
		
		Integer idUser = this.userSearch().getIdUser();
		trainerModel.setCreatedBy(idUser);
		trainerModel.setCreatedOn(new Date());
		
		this.trainerService.create(trainerModel);
		this.saveLog(trainerModel);
		
		String jsp = "trainer/trainer";
		
		return jsp;
	}
	
	@RequestMapping(value="trainer/cruds/edit")
	public String TrainerEdit(HttpServletRequest request, Model model) {
		
		Integer id = Integer.valueOf(request.getParameter("id"));
		TrainerModel trainerModel = new TrainerModel();
		
		trainerModel = this.trainerService.searchId(id);
		
		model.addAttribute("trainerModel", trainerModel);
		
		String jsp = "trainer/cruds/edit";
		return jsp;
	}
	
	@RequestMapping(value="trainer/cruds/update")
	public String TrainerUpdate(HttpServletRequest request) {
		
		Integer id = Integer.valueOf(request.getParameter("id"));
		
		TrainerModel trainerModelBefore = new TrainerModel();
		trainerModelBefore= this.trainerService.searchId(id);
		
		String name = request.getParameter("name");
		String notes = request.getParameter("notes");
		
		TrainerModel trainerModelAfter = new TrainerModel();
		
		trainerModelAfter = this.trainerService.searchId(id);
		
		trainerModelAfter.setName(name);
		trainerModelAfter.setNotes(notes);
		
		Integer idUser = this.userSearch().getIdUser();
		trainerModelAfter.setModifiedBy(idUser);
		trainerModelAfter.setModifiedOn(new Date());
		
		this.trainerService.update(trainerModelAfter);
		this.updateLog(trainerModelBefore, trainerModelAfter);
		
		String jsp = "trainer/trainer";
		
		return jsp;
	}
	
	@RequestMapping(value="trainer/cruds/delete")
	public String TrainerDelete(HttpServletRequest request, Model model) {
		Integer id = Integer.valueOf(request.getParameter("id"));
		
		TrainerModel trainerModel = new TrainerModel();
		
		trainerModel = this.trainerService.searchId(id);
		
		model.addAttribute("trainerModel", trainerModel);
		
		String jsp = "trainer/cruds/delete";
		return jsp;
	}
	
	@RequestMapping(value="trainer/cruds/remove")
	public String TrainerRemove(HttpServletRequest request) {
		
		Integer id = Integer.valueOf(request.getParameter("id"));
		
		TrainerModel trainerModelBeforeDelete = new TrainerModel();
		trainerModelBeforeDelete = this.trainerService.searchId(id);
		
		TrainerModel trainerModel = new TrainerModel();
		
		trainerModel = this.trainerService.searchId(id);
		
		trainerModel.setIsDelete(true);
		
		Integer idUser = this.userSearch().getIdUser();
		trainerModel.setDeletedBy(idUser);
		trainerModel.setDeletedOn(new Date());
		
		this.trainerService.deleteTemporary(trainerModel);
		this.updateLog(trainerModelBeforeDelete, trainerModel);
		
		String jsp = "trainer/trainer";
		return jsp;
	}
	
	@RequestMapping(value="trainer/cruds/search")
	public String TrainerSearch(HttpServletRequest request, Model model) {
		
		String name = request.getParameter("name");
		List<TrainerModel> trainerModelList = new ArrayList<TrainerModel>();
		trainerModelList = this.trainerService.searchNama(name);
		
		model.addAttribute("trainerModelList",trainerModelList);
		String jsp = "trainer/cruds/list";
		return jsp;
	}
}
