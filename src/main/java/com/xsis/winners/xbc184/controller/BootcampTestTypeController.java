package com.xsis.winners.xbc184.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.BootcampTestTypeModel;
import com.xsis.winners.xbc184.model.UserModel;
import com.xsis.winners.xbc184.service.BootcampTestTypeService;

@Controller
public class BootcampTestTypeController extends UserController {
	
	@Autowired
	private BootcampTestTypeService bootcampTestTypeService;

	@RequestMapping(value="bootcampTestType")
	public String bootcampTestType() {
		String jsp = "bootcampTestType/bootcampTestType";
		return jsp;
	}
	
	@RequestMapping(value="bootcampTestType//cruds/add")
	public String bootcampTestTypeAdd() {
		String jsp = "bootcampTestType/cruds/add";
		return jsp;
	}
	
	@RequestMapping(value="bootcampTestType/cruds/create")
	public String bootcampTestTypeCreate(HttpServletRequest request) {
		
		//get nilai dari jsp
		String nameBootcampTestType = request.getParameter("nameBootcampTestType");
		String notesBootcampTestType = request.getParameter("notesBootcampTestType");
		
		//set nilai ke variabel modelnya
		BootcampTestTypeModel bootcampTestTypeModel = new BootcampTestTypeModel();
		bootcampTestTypeModel.setNameBootcampTestType(nameBootcampTestType);
		bootcampTestTypeModel.setNotesBootcampTestType(notesBootcampTestType);
		
		//save audit trail untuk created
		Integer xIdCreatedBy = this.userSearch().getIdUser();
		bootcampTestTypeModel.setxIdCreatedBy(xIdCreatedBy);
		bootcampTestTypeModel.setxCreatedDate(new Date());
		bootcampTestTypeModel.setIsDelete(false);
		
		//save
		this.bootcampTestTypeService.create(bootcampTestTypeModel);
		
		String jsp = "bootcampTestType/bootcampTestType";
		return jsp;
	}
	
	@RequestMapping(value="bootcampTestType/cruds/list")
	public String bootcampTestTypeList(Model model) {
		List<BootcampTestTypeModel> bootcampTestTypeModelList = new ArrayList<BootcampTestTypeModel>();
		bootcampTestTypeModelList = this.bootcampTestTypeService.search();
		model.addAttribute("bootcampTestTypeModelList", bootcampTestTypeModelList);
		
		String jsp = "bootcampTestType/cruds/list";
		return jsp;
	}
	
	
	@RequestMapping(value = "bootcampTestType/cruds/edit")
	public String bootcampTestTypeEdit(HttpServletRequest request, Model model) {
		Integer idBootcampTestType = Integer.valueOf(request.getParameter("idBootcampTestType"));
		
		BootcampTestTypeModel bootcampTestTypeModel = new BootcampTestTypeModel();
		bootcampTestTypeModel = this.bootcampTestTypeService.searchId(idBootcampTestType);
				
		model.addAttribute("bootcampTestTypeModel", bootcampTestTypeModel);
			
		String jsp = "bootcampTestType/cruds/edit"; 
		return jsp;
	}
	
	@RequestMapping(value="bootcampTestType/cruds/update")
	public String bootcampTestTypeUpdate(HttpServletRequest request) {
		
		//get niali dari variabel name di jsp
		Integer idBootcampTestType = Integer.valueOf(request.getParameter("idBootcampTestType"));
		String nameBootcampTestType = request.getParameter("nameBootcampTestType");
		String notesBootcampTestType = request.getParameter("notesBootcampTestType");
		
		//set niali ke variabel modelnya
		BootcampTestTypeModel bootcampTestTypeModel = new BootcampTestTypeModel();
		bootcampTestTypeModel = this.bootcampTestTypeService.searchId(idBootcampTestType);
		
		bootcampTestTypeModel.setNameBootcampTestType(nameBootcampTestType);
		bootcampTestTypeModel.setNotesBootcampTestType(notesBootcampTestType);
		
		//audit trail
		Integer xIdModifiedBy = this.userSearch().getIdUser();
		bootcampTestTypeModel.setxIdModifiedBy(xIdModifiedBy);
		bootcampTestTypeModel.setxModifiedDate(new Date());
		//save
		this.bootcampTestTypeService.update(bootcampTestTypeModel);
		
		String jsp = "bootcampTestType/bootcampTestType";
		return jsp;
	}
	
	@RequestMapping(value="bootcampTestType/cruds/search/name")
	public String bootcampTestTypeSearchName(HttpServletRequest request, Model model) {
		String nameBootcampTestType= request.getParameter("nameBootcampTestTypeKey");
		List<BootcampTestTypeModel> bootcampTestTypeModelList = new ArrayList<BootcampTestTypeModel>();
		bootcampTestTypeModelList = this.bootcampTestTypeService.searchNama(nameBootcampTestType);
		
		model.addAttribute("bootcampTestTypeModelList", bootcampTestTypeModelList);
		
		String jsp = "bootcampTestType/cruds/list";
		return jsp;
	}
		
	@RequestMapping(value="bootcampTestType/cruds/remove")
	public String bootcampTestTypeRemove(HttpServletRequest request, Model model) {
		Integer idBootcampTestType = Integer.valueOf(request.getParameter("idBootcampTestType"));
		
		BootcampTestTypeModel bootcampTestTypeModel = new BootcampTestTypeModel();
		bootcampTestTypeModel = this.bootcampTestTypeService.searchId(idBootcampTestType);
		model.addAttribute("bootcampTestTypeModel", bootcampTestTypeModel);
		
		String jsp = "bootcampTestType/cruds/remove";
		return jsp;
	}
	
	@RequestMapping(value="bootcampTestType/cruds/delete")
	public String bootcampTestTypeDelete (HttpServletRequest request) {
		//get nilai dari jsp
		Integer idBootcampTestType = Integer.valueOf(request.getParameter("idBootcampTestType"));
		
		BootcampTestTypeModel bootcampTestTypeModel = new BootcampTestTypeModel();
		bootcampTestTypeModel = this.bootcampTestTypeService.searchId(idBootcampTestType);
		bootcampTestTypeModel.setIsDelete(true); // 1 artinya telah dihapus temporary
		
		//audit trail
		Integer xIdDeletedBy = this.userSearch().getIdUser();
		bootcampTestTypeModel.setxIdDeletedBy(xIdDeletedBy);
		bootcampTestTypeModel.setxDeletedDate(new Date());
		
		//save
		this.bootcampTestTypeService.deleteTemporary(bootcampTestTypeModel);
		String jsp = "bootcampTestType/bootcampTestType";
		return jsp;
	}
		
}
