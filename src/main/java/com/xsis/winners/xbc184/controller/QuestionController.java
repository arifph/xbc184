package com.xsis.winners.xbc184.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.QuestionModel;
import com.xsis.winners.xbc184.model.RoleModel;
import com.xsis.winners.xbc184.service.QuestionService;

@Controller
public class QuestionController extends AuditLogController {
	
	@Autowired
	private QuestionService questionService;
	
	@RequestMapping(value= "question")
	public String question() {
		String jsp = "question/question";
		return jsp;
	}

	@RequestMapping(value = "question/cruds/add")
	public String questionAdd(Model model) {
		
		String jsp = "question/cruds/add";
		return jsp;
	}
	
	@RequestMapping(value = "question/cruds/create")
	public String questionCreate(HttpServletRequest request, Model model) {
		
		// get nilai dari variable name di jsp
		String questionType = request.getParameter("questionType");
		String question = request.getParameter("question");
		String imageUrl = request.getParameter("imageUrl");
		
		// set nilai ke variable di modelnya
		QuestionModel questionModel = new QuestionModel();
		questionModel.setQuestionType(questionType);
		questionModel.setQuestion(question);
		questionModel.setImageUrl(imageUrl);
		questionModel.setIsDelete(false);
		
		// Save Audit Trail Create
		Integer xIdCreatedBy = this.userSearch().getIdUser();
		questionModel.setxIdCreatedBy(xIdCreatedBy);
		questionModel.setxCreatedDate(new Date());
		this.questionService.create(questionModel);
		this.saveLog(questionModel);
			
		String jsp = "question/question";
		return jsp;
	}
	
	@RequestMapping(value = "question/cruds/list")
	public String questionList(Model model) {
		List<QuestionModel> questionModelList = new ArrayList<QuestionModel>();
		questionModelList = this.questionService.search();
		model.addAttribute("questionModelList", questionModelList);
		String jsp = "question/cruds/list";
		return jsp;
	}
	
	@RequestMapping(value = "question/cruds/edit")
	public String questionEdit(HttpServletRequest request, Model model) {
		Integer idQuestion = Integer.valueOf(request.getParameter("idQuestion"));
		
		QuestionModel questionModel = new QuestionModel();
		questionModel = this.questionService.searchId(idQuestion);
		model.addAttribute("questionModel", questionModel);
		
		String jsp = "question/cruds/edit";
		return jsp;
	}
	
	@RequestMapping(value = "question/cruds/update")
	public String questionUpdate(HttpServletRequest request) {
		
		// get nilai dari variable name di jsp
		Integer idQuestion = Integer.valueOf(request.getParameter("idQuestion"));
		String optionA = request.getParameter("optionA");
		String optionB = request.getParameter("optionB");
		String optionC = request.getParameter("optionC");
		String optionD = request.getParameter("optionD");
		String optionE = request.getParameter("optionE");
		
		// set nilai ke variable di modelnya
		QuestionModel questionModelAfter = new QuestionModel();
		questionModelAfter = this.questionService.searchId(idQuestion);
		questionModelAfter.setOptionA(optionA);
		questionModelAfter.setOptionB(optionB);
		questionModelAfter.setOptionC(optionC);
		questionModelAfter.setOptionD(optionD);
		questionModelAfter.setOptionE(optionE);
		
		QuestionModel questionModelBefore = new QuestionModel();
		questionModelBefore = this.questionService.searchId(idQuestion);
		
		// Save Audit Trail Update
		Integer xIdUpdatedBy = this.userSearch().getIdUser();
		questionModelAfter.setxIdUpdatedBy(xIdUpdatedBy);
		questionModelAfter.setxUpdatedDate(new Date());
		
		// simpan data
		this.questionService.update(questionModelAfter);
		this.updateLog(questionModelBefore, questionModelAfter);
		
		String jsp = "question/question";
		return jsp;
	}
	
	@RequestMapping(value = "question/cruds/detail")
	public String questionDetail(HttpServletRequest request, Model model) {
		Integer idQuestion = Integer.valueOf(request.getParameter("idQuestion"));
		
		QuestionModel questionModel = new QuestionModel();
		questionModel = this.questionService.searchId(idQuestion);
		
		model.addAttribute("questionModel", questionModel);
		
		String jsp = "question/cruds/detail";
		return jsp;
	}
	
	@RequestMapping(value = "question/cruds/remove")
	public String questionRemove(HttpServletRequest request, Model model) {
		Integer idQuestion = Integer.valueOf(request.getParameter("idQuestion"));
		
		QuestionModel questionModel = new QuestionModel();
		questionModel = this.questionService.searchId(idQuestion);
		model.addAttribute("questionModel", questionModel);
		
		String jsp = "question/cruds/remove";
		return jsp;
	}
	
	@RequestMapping(value = "question/cruds/delete")
	public String questionDelete(HttpServletRequest request) {
		
		// get nilai dari variable name di jsp
		Integer idQuestion = Integer.valueOf(request.getParameter("idQuestion"));
		
		// set nilai ke variable di modelnya
		QuestionModel questionModelAfter = new QuestionModel();
		questionModelAfter = this.questionService.searchId(idQuestion);
		questionModelAfter.setIsDelete(true);
		
		QuestionModel questionModelBefore = new QuestionModel();
		questionModelBefore = this.questionService.searchId(idQuestion);
		
		// Save Audit Trail Delete
		Integer xIdDeletedBy = this.userSearch().getIdUser();
		questionModelAfter.setxIdDeletedBy(xIdDeletedBy);
		questionModelAfter.setxDeletedDate(new Date());
		
		// simpan data
		this.questionService.deleteTemp(questionModelAfter);
		this.updateLog(questionModelBefore, questionModelAfter);
		
		String jsp = "question/question";
		return jsp;
	}
	
	@RequestMapping(value= "question/cruds/search/question")
	public String questionSearchQuestion(HttpServletRequest request, Model model) {
		String question = request.getParameter("questionQuestionKey");
		List<QuestionModel> questionModelList = new ArrayList<QuestionModel>();
		questionModelList = this.questionService.searchQuestion(question);
		
		model.addAttribute("questionModelList", questionModelList); 
		String jsp = "question/cruds/list";
		return jsp;
	}
	
}
