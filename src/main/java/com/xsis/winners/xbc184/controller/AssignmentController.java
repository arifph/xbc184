package com.xsis.winners.xbc184.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.AssignmentModel;
import com.xsis.winners.xbc184.model.BiodataModel;
import com.xsis.winners.xbc184.service.AssignmentService;
import com.xsis.winners.xbc184.service.BiodataService;

@Controller
public class AssignmentController extends AuditLogController {

	// private static final Integer biodataId = null;

	private static Logger logger = LogManager.getLogger();

	@Autowired
	private AssignmentService assignmentService;

	@Autowired
	private BiodataService biodataService;

	@RequestMapping(value = "assignment")
	public String assignment() {
		logger.info("Mapping assignment, you should see assignment page after this method");

		String jsp = "assignment/assignment";
		return jsp;
	}

	@RequestMapping(value = "assignment/cruds/list")
	public String assignmentList(Model model) {
		logger.info("Showing List of Assignment");

		List<AssignmentModel> assignmentModelList = this.assignmentService.search(false);
		logger.info("Assignment List Size : " + assignmentModelList == null ? 0 : assignmentModelList.size());

		model.addAttribute("assignmentModelList", assignmentModelList);

		String jsp = "assignment/cruds/list";
		return jsp;
	}

	@RequestMapping(value = "assignment/cruds/add")
	public String assignmentAdd(Model model) {
		logger.info("Showing add dialog");

		List<BiodataModel> biodataModelList = this.biodataService.search();
		model.addAttribute("biodataModelList", biodataModelList);

		String jsp = "assignment/cruds/add";
		return jsp;
	}

	@RequestMapping(value = "assignment/cruds/create")
	public String assignmentCreate(HttpServletRequest request) throws ParseException {

		logger.info("Building assignment object");
		Boolean isDelete = false;
		Integer createdBy = this.userSearch().getIdUser();
		Date createdOn = new Date();
		Integer biodataId = Integer.parseInt(request.getParameter("biodataId"));
		String title = request.getParameter("title");

		Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("startDate"));
		Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("endDate"));

		String description = request.getParameter("description");

		logger.info(request.getParameter("startDate"));

		// assignment.setBiodataId(biodataId);
		AssignmentModel assignmentModel = new AssignmentModel();
		assignmentModel.setBiodataId(biodataId);
		assignmentModel.setTitle(title);
		assignmentModel.setStartDate(startDate);
		assignmentModel.setEndDate(endDate);
		assignmentModel.setDescription(description);
		assignmentModel.setCreatedBy(createdBy);
		assignmentModel.setCreatedOn(createdOn);
		assignmentModel.setIsDelete(isDelete);

		logger.info(assignmentModel);

		logger.info("Saving assignment");
		this.assignmentService.saveAssignment(assignmentModel);
		this.saveLog(assignmentModel);

		String jsp = "assignment/assignment";
		return jsp;
	}

	@RequestMapping(value = "assignment/cruds/edit")
	public String assignmentEdit(HttpServletRequest request, Model model) {

		Integer id = Integer.valueOf(request.getParameter("idAssignment"));
		logger.info("Assignment id : " + id);

		AssignmentModel assignmentModel = this.assignmentService.searchById(id);

		if (assignmentModel == null) {
			logger.info("Category not Found");
		} else {
			logger.info("Category Found");
		}

		logger.info("Passing parameter to jsp");
		model.addAttribute(assignmentModel);

		logger.info("Showing edit dialog");
		String jsp = "assignment/cruds/edit";
		return jsp;
	}

	@RequestMapping(value = "assignment/cruds/update")
	public String assignmentUpdate(HttpServletRequest request) {

		logger.info("Building assignment object");

		Date modifiedOn = new Date();
		AssignmentModel assignmentAfter = this.assignmentService
				.searchById(Integer.valueOf(request.getParameter("idAssignment")));

		AssignmentModel assignmentBefore = this.assignmentService.searchById(assignmentAfter.getId());

		if (assignmentBefore == null) {
			logger.info("Assignment not Found");
		} else {
			logger.info("Assignment Found");
		}

		// Set created by untuk category after karena category after tidak memiliki
		// created by
		assignmentAfter.setCreatedBy(assignmentBefore.getCreatedBy());
		assignmentAfter.setCreatedOn(assignmentBefore.getCreatedOn());
		assignmentAfter.setIsDelete(false);
		assignmentAfter.setModifiedBy(this.userSearch().getIdUser());
		assignmentAfter.setModifiedOn(modifiedOn);

		logger.info("Assignment After: " + assignmentAfter);
		logger.info("Assignment Before: " + assignmentBefore);

		logger.info("Saving assignment");
		this.assignmentService.updateAssignment(assignmentAfter);
		this.updateLog(assignmentBefore, assignmentAfter);

		String jsp = "assignment/assignment";
		return jsp;
	}

	@RequestMapping(value = "assignment/cruds/confirmation")
	public String assignmentConfirmation(HttpServletRequest request, Model model) {

		Integer id = Integer.valueOf(request.getParameter("idAssignment"));
		logger.info("Assignment id : " + id);

		AssignmentModel assignmentModel = this.assignmentService.searchById(id);

		if (assignmentModel == null) {
			logger.info("Assignment not Found");
		} else {
			logger.info("Assignment Found");
		}

		logger.info("Passing parameter to jsp");
		model.addAttribute(assignmentModel);

		logger.info("Showing edit dialog");
		String jsp = "assignment/cruds/delete";
		return jsp;
	}

	@RequestMapping(value = "assignment/cruds/delete")
	public String assignmentDelete(HttpServletRequest request) {

		logger.info("Building assignment object");
		Integer id = Integer.valueOf(request.getParameter("idAssignment"));
		Integer deletedBy = this.userSearch().getIdUser();
		Date deletedOn = new Date();

		AssignmentModel assignmentModel = this.assignmentService.searchById(id);

		if (assignmentModel == null) {
			logger.info("Category not Found");
		} else {
			logger.info("Category Found");
		}

		assignmentModel.setIsDelete(true);
		assignmentModel.setDeletedBy(deletedBy);
		assignmentModel.setDeletedDate(deletedOn);

		logger.info(assignmentModel);

		logger.info("Deleting assignment");
		try {
			this.assignmentService.deleteAssignment(assignmentModel);
			this.deleteLog(assignmentModel);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		

		String jsp = "assignment/assignment";
		return jsp;
	}
	
	@RequestMapping(value = "assignment/cruds/confirmation2")
	public String assignmentConfirmation2(HttpServletRequest request, Model model) {

		Integer id = Integer.valueOf(request.getParameter("idAssignment"));
		logger.info("Assignment id : " + id);

		AssignmentModel assignmentModel = this.assignmentService.searchById(id);

		if (assignmentModel == null) {
			logger.info("Assignment not Found");
		} else {
			logger.info("Assignment Found");
		}

		logger.info("Passing parameter to jsp");
		model.addAttribute(assignmentModel);

		logger.info("Showing edit dialog");
		String jsp = "assignment/cruds/hold";
		return jsp;
	
	}
	
	@RequestMapping(value = "assignment/cruds/hold")
	public String assignmentHold(HttpServletRequest request) {

		logger.info("Building assignment object");
		Integer id = Integer.valueOf(request.getParameter("idAssignment"));
		//Integer isHold = this.userSearch().getIdUser();
		Date modifiedOn = new Date();

		AssignmentModel assignmentModel = this.assignmentService.searchById(id);

		if (assignmentModel == null) {
			logger.info("Category not Found");
		} else {
			logger.info("Category Found");
		}

		assignmentModel.setIsHold(true);
		assignmentModel.setModifiedBy(this.userSearch().getIdUser());
		assignmentModel.setModifiedOn(modifiedOn);
		/*
		 * assignmentModel.setDeletedDate(deletedOn);
		 */
		logger.info(assignmentModel);

		logger.info("Deleting assignment");
		try {
			this.assignmentService.deleteAssignment(assignmentModel);
			this.deleteLog(assignmentModel);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		

		String jsp = "assignment/assignment";
		return jsp;
	}

	@RequestMapping(value = "assignment/cruds/done2")
	public String assignmentDone2(HttpServletRequest request, Model model) {

		Integer id = Integer.valueOf(request.getParameter("idAssignment"));
		logger.info("Assignment id : " + id);

		AssignmentModel assignmentModel = this.assignmentService.searchById(id);

		if (assignmentModel == null) {
			logger.info("Assignment not Found");
		} else {
			logger.info("Assignment Found");
		}

		logger.info("Passing parameter to jsp");
		model.addAttribute(assignmentModel);

		logger.info("Showing edit dialog");
		String jsp = "assignment/cruds/done";
		return jsp;
	
	}
	@RequestMapping(value = "assignment/cruds/done")
	public String assignmentDone(HttpServletRequest request) {

		logger.info("Building assignment object");
		Integer id = Integer.valueOf(request.getParameter("idAssignment"));
		//Integer isHold = this.userSearch().getIdUser();
		Date modifiedOn = new Date();

		AssignmentModel assignmentModel = this.assignmentService.searchById(id);

		if (assignmentModel == null) {
			logger.info("Category not Found");
		} else {
			logger.info("Category Found");
		}

		assignmentModel.setIsDone(true);
		assignmentModel.setModifiedBy(this.userSearch().getIdUser());
		assignmentModel.setModifiedOn(modifiedOn);
		/*
		 * assignmentModel.setDeletedDate(deletedOn);
		 */
		logger.info(assignmentModel);

		logger.info("Deleting assignment");
		try {
			this.assignmentService.deleteAssignment(assignmentModel);
			this.deleteLog(assignmentModel);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		

		String jsp = "assignment/assignment";
		return jsp;
	}

	@RequestMapping(value = "assignment/cruds/search/startDate")
	public String assignmentSearchByCodeAndName(HttpServletRequest request, Model model) throws ParseException {
		logger.info("Searching data");
		String dateString = request.getParameter("dateAssignment");
		logger.info("Showing search result");

		List<AssignmentModel> assignmentModelList = new ArrayList<AssignmentModel>();

		String jsp = "assignment/cruds/list";
		if (dateString == null || dateString.isEmpty()) {
			assignmentModelList = this.assignmentService.search(false);
			model.addAttribute("assignmentModelList", assignmentModelList);

			return jsp;
		} else {
			Date searchDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("dateAssignment"));
			Boolean isDelete = false;

			assignmentModelList = this.assignmentService.search(searchDate, isDelete);
			logger.info("Assignment List Size : " + (assignmentModelList == null ? 0 : assignmentModelList.size()));

			model.addAttribute("assignmentModelList", assignmentModelList);

			return jsp;
		}
	}

	private String getGeneratedCode() {
		List<AssignmentModel> assignmentModelList = this.assignmentService.search();

		return String.format("C%04d", assignmentModelList == null ? 1 : (assignmentModelList.size() + 1));
	}
}
