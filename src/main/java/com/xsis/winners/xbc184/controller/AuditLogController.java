package com.xsis.winners.xbc184.controller;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xsis.winners.xbc184.model.AuditLogModel;
import com.xsis.winners.xbc184.service.AuditLogService;

@Controller
public class AuditLogController extends MUserController {

	@Autowired
	private AuditLogService auditLogService;

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
	private static final Logger LOGGER = LogManager.getLogger(AuditLogController.class);

	public void saveLog(Object object) {
		try {
			LOGGER.info("Audit Log save");

			String type = "INSERT";
			Integer createdBy = this.userSearch().getId();
			Date createdOn = new Date();
			String jsonInsert = OBJECT_MAPPER.writeValueAsString(object);
			
			LOGGER.info(jsonInsert);

			AuditLogModel auditLogModel = new AuditLogModel();
			auditLogModel.setJsonInsert(jsonInsert);
			auditLogModel.setType(type);
			auditLogModel.setCreatedBy(createdBy);
			auditLogModel.setCreatedOn(createdOn);

			auditLogService.save(auditLogModel);
			LOGGER.info("Saving Audit Log success");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void updateLog(Object objectBefore, Object objectAfter) {
		LOGGER.info("audit log update");

		try {
			String jsonBefore = OBJECT_MAPPER.writeValueAsString(objectBefore);
			String jsonAfter = OBJECT_MAPPER.writeValueAsString(objectAfter);
			String type = "MODIFY";
			Integer createdBy = this.userSearch().getId();
			Date createdOn = new Date();

			AuditLogModel auditLogModel = new AuditLogModel();
			auditLogModel.setJsonBefore(jsonBefore);
			auditLogModel.setJsonAfter(jsonAfter);
			auditLogModel.setType(type);
			auditLogModel.setCreatedBy(createdBy);
			auditLogModel.setCreatedOn(createdOn);
			
			LOGGER.info("jsonBefore: " + jsonBefore);
			LOGGER.info("jsonAfter: " + jsonAfter);
			
			LOGGER.info(jsonBefore.equals(jsonAfter) ? "Before == After" : "Before != After");

			auditLogService.save(auditLogModel);
			LOGGER.info("Saving Audit Log success");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void deleteLog(Object object) {
		LOGGER.info("audit log delete");
		try {
			String type = "DELETE";
			String jsonDelete = OBJECT_MAPPER.writeValueAsString(object);
			Integer createdBy = this.userSearch().getId();
			Date createdOn = new Date();

			LOGGER.info("jsonDelete: " + jsonDelete);

			AuditLogModel auditLogModel = new AuditLogModel();
			auditLogModel.setType(type);
			auditLogModel.setJsonDelete(jsonDelete);
			auditLogModel.setCreatedBy(createdBy);
			auditLogModel.setCreatedOn(createdOn);
			
			this.auditLogService.save(auditLogModel);
			LOGGER.info("Saving Audit Log success");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
