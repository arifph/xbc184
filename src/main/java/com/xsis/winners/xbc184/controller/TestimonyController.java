package com.xsis.winners.xbc184.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.TestimonyModel;
import com.xsis.winners.xbc184.service.TestimonyService;

@Controller
public class TestimonyController extends AuditLogController {
	
	@Autowired
	private TestimonyService testimonyService;
	
	
	@RequestMapping(value="testimony")
	public String testimony() {
		
		String jsp = "testimony/testimony";
		return jsp;
		
	}
	
	@RequestMapping(value="testimony/cruds/add")
	public String testimonyAdd() {
		
		
		String jsp = "testimony/cruds/add"; 
		return jsp;
		
	}
	
	@RequestMapping(value="testimony/cruds/create")
	public String testimonyCreate(HttpServletRequest request, Model model) {
		//get nilai dari variable di jsp
				String title = request.getParameter("title");
				String content = request.getParameter("content");
				
				//set nilai ke variable di modelnya
				TestimonyModel testimonyModel = new TestimonyModel();
				testimonyModel.setTitle(title);
				testimonyModel.setContent(content);
				testimonyModel.setIsDelete(false);
				
				Integer idUser = this.userSearch().getIdUser();
				testimonyModel.setCreatedBy(idUser);
				testimonyModel.setCreatedOn(new Date());
								
				this.testimonyService.create(testimonyModel);
				
				this.saveLog(testimonyModel);
				String jsp = "testimony/testimony"; 
				return jsp;
		
	}
	
	@RequestMapping(value="testimony/cruds/list")
	public String testimonyList(Model model) {
		List<TestimonyModel> testimonyModelList = new ArrayList<TestimonyModel>();

		testimonyModelList = this.testimonyService.search();
		model.addAttribute("testimonyModelList", testimonyModelList);

		String jsp = "testimony/cruds/list";
		return jsp;
		
	}
	
	@RequestMapping(value="testimony/cruds/edit")
	public String testimonyEdit(HttpServletRequest request, Model model) {
		
		Integer id = Integer.valueOf(request.getParameter("id"));

		TestimonyModel testimonyModel = new TestimonyModel();
		testimonyModel = this.testimonyService.searchId(id);

		model.addAttribute("testimonyModel", testimonyModel);

		String jsp = "testimony/cruds/edit";
		return jsp;
		
	}
	
	@RequestMapping(value="testimony/cruds/update")
	public String testimonyUpdate(HttpServletRequest request, Model model) {
		
		Integer id = Integer.valueOf(request.getParameter("id"));
		String title = request.getParameter("title");
		String content = request.getParameter("content");
	
		//AUDIT LOG
		TestimonyModel testimonyModelBefore = this.testimonyService.searchId(id);
		
		TestimonyModel testimonyModelAfter = new TestimonyModel();

		testimonyModelAfter = this.testimonyService.searchId(id);
		
		testimonyModelAfter.setTitle(title);
		testimonyModelAfter.setContent(content);
		testimonyModelAfter.setIsDelete(false);
		
		testimonyModelAfter.setCreatedBy(testimonyModelBefore.getCreatedBy());
		testimonyModelAfter.setCreatedOn(testimonyModelBefore.getCreatedOn());
		 		
		Integer idUser = this.userSearch().getIdUser();
		testimonyModelAfter.setModifiedBy(idUser);
		testimonyModelAfter.setModifiedOn(new Date());
		
	
		this.testimonyService.update(testimonyModelAfter);
		
		
		this.updateLog(testimonyModelAfter, testimonyModelAfter);
		String jsp = "testimony/testimony";
		return jsp;
	}
	
	@RequestMapping(value="testimony/cruds/remove")
	public String testimonyRemove(HttpServletRequest request, Model model) {
		
		Integer id = Integer.valueOf(request.getParameter("id"));

		TestimonyModel testimonyModel = new TestimonyModel();
		testimonyModel = this.testimonyService.searchId(id);

		model.addAttribute("testimonyModel", testimonyModel);

		String jsp = "testimony/cruds/remove";
		return jsp;
		
	}
	
	@RequestMapping(value="testimony/cruds/delete")
	public String testimonyDelete(HttpServletRequest request) {
		
		Integer id = Integer.valueOf(request.getParameter("id"));
		TestimonyModel testimonyModelBefore = new TestimonyModel();
		
		testimonyModelBefore = this.testimonyService.searchId(id);


		TestimonyModel testimonyModel = new TestimonyModel();
		testimonyModel = this.testimonyService.searchId(id);
		
		testimonyModel.setIsDelete(true);
		Integer idUser = this.userSearch().getIdUser();
		testimonyModel.setDeletedBy(idUser);
		testimonyModel.setDeletedOn(new Date());

		
		this.testimonyService.deleteTemporary(testimonyModel);

		this.updateLog(testimonyModelBefore, testimonyModel);
		String jsp = "testimony/testimony";
		return jsp;
	}
	
	
	@RequestMapping(value = "testimony/cruds/search/name")
	public String testimonySearchTitle(HttpServletRequest request, Model model) {
		String title = request.getParameter("titleKey");
		List<TestimonyModel> testimonyModelList = new ArrayList<TestimonyModel>();

		testimonyModelList = this.testimonyService.searchTitle(title);
		model.addAttribute("testimonyModelList", testimonyModelList);

		String jsp = "testimony/cruds/list";
		return jsp;
	}
	
	
}
