package com.xsis.winners.xbc184.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.BiodataModel;
import com.xsis.winners.xbc184.model.BootcampTestTypeModel;
//import com.xsis.winners.xbc184.service.AuditLogService;
//import com.xsis.winners.xbc184.model.LokasiModel;
import com.xsis.winners.xbc184.service.BiodataService;
import com.xsis.winners.xbc184.service.BootcampTestTypeService;
//import com.xsis.winners.xbc184.service.LokasiService;

@Controller
public class BiodataController /*extends UserController*/{

	@Autowired
	private BiodataService biodataService;
	
	@Autowired
	private BootcampTestTypeService bootcampTestTypeService;
	
	@Autowired
	private AuditLogController auditLogController;
	
	@RequestMapping(value="biodata") // url action
	public String biodata() { // method
		String jsp = "biodata/biodata"; // target atau halaman
		return jsp;
	}
	
	// untuk memunculkan POP UP tambah biodata
	@RequestMapping(value="biodata/cruds/add")
	public String biodataAdd(Model model) {
//		String kodeBiodataAuto = this.generateKodeBiodata();
//		model.addAttribute("kodeBiodataAuto", kodeBiodataAuto);
		
//		// tampilkan lokasi untuk select option
//		this.lokasiList(model);
		
		String jsp = "biodata/cruds/add";
		return jsp;
	}
	
	// untuk menambah biodata
	@RequestMapping(value="biodata/cruds/create")
	public String biodataCreate(HttpServletRequest request) {
		
		// get nilai dari variabel nama di jsp
//		String kodeBiodata = request.getParameter("kodeBiodata");
		String name = request.getParameter("name");
		String lastEducation = request.getParameter("lastEducation");
		String educationalLevel = request.getParameter("educationalLevel");
		String graduationYear = request.getParameter("graduationYear");
		String majors = request.getParameter("majors");
		String gpa = request.getParameter("gpa");
		
		// set nilai ke variabel di modelnya
		BiodataModel biodataModel = new BiodataModel();
//		biodataModel.setKodeBiodata(kodeBiodata);
		biodataModel.setName(name);
		biodataModel.setLastEducation(lastEducation);
		biodataModel.setEducationalLevel(educationalLevel);
		biodataModel.setGraduationYear(graduationYear);
		biodataModel.setMajors(majors);
		biodataModel.setGpa(gpa);
		biodataModel.setIsDelete(false);
		
		// save audit trail untuk created, username adalah arif, jadi line di bawah nggak dipake
//		//Integer createdBy = this.userSearch().getId();
		biodataModel.setCreatedBy(2);
		biodataModel.setCreatedOn(new Date());
		
		// simpan data
		this.biodataService.create(biodataModel);
		this.auditLogController.saveLog(biodataModel);
		
		String jsp = "biodata/biodata"; // setelah nge-save, kembali ke sini lagi
		return jsp;
	}
	
	// untuk menambah biodata
//		@RequestMapping(value="biodata/cruds/create")
//		public String biodataCreate(HttpServletRequest request, Model model) {
//			
//			// get nilai dari variabel nama di jsp
//			String kodeBiodata = request.getParameter("kodeBiodata");
//			String namaBiodata = request.getParameter("namaBiodata");
//			
//			// set join table
//			Integer idLokasi = Integer.valueOf(request.getParameter("idLokasi"));
//			
//			// set nilai ke variabel di modelnya
//			BiodataModel biodataModel = new BiodataModel();
//			biodataModel.setKodeBiodata(kodeBiodata);
//			biodataModel.setNamaBiodata(namaBiodata);
//			biodataModel.setIsDelete(0); // 0 artinya tidak di-delete
//			
//			// set join table
//			biodataModel.setIdLokasi(idLokasi);
//			
//			/*Integer jumlahDataBiodata = this.cekKodeBiodata(kodeBiodata);*/
//			Integer jumlahDataBiodata2 = this.cekNamaBiodata(namaBiodata, 0);
//			Integer jumlahDataBiodata2_2 = this.cekNamaBiodata(namaBiodata, 1);
//			
//			// save audit trail untuk created
//			Integer xIdCreatedBy = this.userSearch().getIdUser();
//			biodataModel.setxIdCreatedBy(xIdCreatedBy);
//			biodataModel.setxCreatedDate(new Date());
//			
//			if (/*jumlahDataBiodata > 0 || */jumlahDataBiodata2 > 0) {
//				/*model.addAttribute("jumlahDataBiodata", jumlahDataBiodata);*/
//				model.addAttribute("jumlahDataBiodata2", jumlahDataBiodata2);
//				/*model.addAttribute("kodeBiodata", kodeBiodata);*/
//				model.addAttribute("namaBiodata", namaBiodata);
//			} else {
//				if(jumlahDataBiodata2_2 > 0) {
//					// simpan data
//					this.biodataService.delete(biodataModel);
//					this.biodataService.create(biodataModel);
//				}
//				else {
//					// simpan data
//					this.biodataService.create(biodataModel);
//				}
////				// simpan data
////				this.biodataService.create(biodataModel);
//			}
//			
//			// tampilkan lokasi untuk select option
//			this.lokasiList(model);
//			
//			String jsp = "biodata/biodata"; // setelah nge-save, kembali ke sini lagi
//			return jsp;
//		}
	
	@RequestMapping(value="biodata/cruds/list")
	public String biodataList(Model model) {
		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
//		String kodeRole = this.userSearch().getRoleModel().getKodeRole();
		biodataModelList = this.biodataService.search();
		model.addAttribute("biodataModelList", biodataModelList);
		
		String jsp = "biodata/cruds/list";
		return jsp;
	}
	
	// untuk memunculkan POP UP detil biodata
//	@RequestMapping(value="biodata/cruds/detail")
//	public String biodataDetail(HttpServletRequest request, Model model) {
//		Integer idBiodata = Integer.valueOf(request.getParameter("idBiodata"));
//		
//		BiodataModel biodataModel = new BiodataModel();
//		biodataModel = this.biodataService.searchId(idBiodata);
//		
//		model.addAttribute("biodataModel", biodataModel);
//		
//		// tampilkan lokasi
//		this.lokasiList(model);
//		
//		String jsp = "biodata/cruds/detail";
//		return jsp;
//	}
	
	// untuk memunculkan POP UP edit biodata
	@RequestMapping(value="biodata/cruds/edit")
	public String biodataList(HttpServletRequest request, Model model) {
		Integer id = Integer.valueOf(request.getParameter("id"));
		
		BiodataModel biodataModel = new BiodataModel();
		biodataModel = this.biodataService.searchId(id);
		model.addAttribute("biodataModel", biodataModel);
		
		// tampilkan bootcamp test type untuk select option
		this.bootcampTestTypeList(model);
		
		String jsp = "biodata/cruds/edit";
		return jsp;
	}
	
	// untuk melakukan update biodata
	@RequestMapping(value="biodata/cruds/update")
	public String biodataUpdate(HttpServletRequest request, Model model) {
		
		// get nilai dari variabel nama di jsp
		Integer id = Integer.valueOf(request.getParameter("id"));
		String name = request.getParameter("name");
		String lastEducation = request.getParameter("lastEducation");
		String educationalLevel = request.getParameter("educationalLevel");
		String graduationYear = request.getParameter("graduationYear");
		String majors = request.getParameter("majors");
		String gpa = request.getParameter("gpa");
		String gender = request.getParameter("gender");
		String du = request.getParameter("du");
		String interviewer = request.getParameter("interviewer");
		String tro = request.getParameter("tro");
		String notes = request.getParameter("notes");
		
//		Integer idLokasi = Integer.valueOf(request.getParameter("idLokasi"));
		
		// set nilai ke variabel di modelnya
		BiodataModel biodataModel = new BiodataModel();
		biodataModel = this.biodataService.searchId(id);
		
		// copy isi biodataModel ke sebuah model baru
		BiodataModel biodataBefore = new BiodataModel(); 
		biodataBefore = this.biodataService.searchId(biodataModel.getId());

		// lanjut set nilai ke variabel di modelnya
		biodataModel.setName(name);
		biodataModel.setLastEducation(lastEducation);
		biodataModel.setEducationalLevel(educationalLevel);
		biodataModel.setGraduationYear(graduationYear);
		biodataModel.setMajors(majors);
		biodataModel.setGpa(gpa);
		biodataModel.setGender(gender);
		biodataModel.setDu(du);
		biodataModel.setInterviewer(interviewer);
		biodataModel.setTro(tro);
		biodataModel.setNotes(notes);
		
		// Jika isian combo box bootcamp test type tidak dipilih
		String paramBtt = request.getParameter("bootcampTestType");
		if(paramBtt != "") {
			Integer bootcampTestType = Integer.valueOf(paramBtt);
			biodataModel.setBootcampTestType(bootcampTestType);
		}
		
		// Jika isian IQ adalah null
		String paramIQ = request.getParameter("iq");
		if(paramIQ != "" && regexDigit(paramIQ)) {
			Integer iq = Integer.valueOf(paramIQ);
			biodataModel.setIq(iq);
		}
		
		// Jika isian nilai nested logic adalah null
		String paramNL = request.getParameter("nestedLogic");
		if(paramNL != "" && regexDigit(paramNL)) {
			Integer nestedLogic = Integer.valueOf(paramNL);
			biodataModel.setNestedLogic(nestedLogic);
		}
		
		// Jika isian nilai join table adalah null
		String paramJT = request.getParameter("joinTable");
		if(paramJT != "" && regexDigit(paramJT)) {
			Integer joinTable = Integer.valueOf(paramJT);
			biodataModel.setJoinTable(joinTable);
		}
		
		// Jika isian nilai arithmetic adalah null
		String paramArithmetic = request.getParameter("arithmetic");
		if(paramArithmetic != "" && regexDigit(paramArithmetic)) {
			Integer arithmetic = Integer.valueOf(paramArithmetic);
			biodataModel.setArithmetic(arithmetic);
		}
		
//		biodataModel.setIdLokasi(idLokasi);
		
		// tampilkan bootcamp test type untuk select option
//		this.bootcampTestTypeList(model);
		
		// save audit trail untuk modified, username adalah arif, jadi line di bawah nggak dipake
//		//Integer modifiedBy = this.userSearch().getId();
		biodataModel.setCreatedBy(biodataBefore.getCreatedBy());
		biodataModel.setCreatedOn(biodataBefore.getCreatedOn());
		biodataModel.setModifiedBy(2);
		biodataModel.setModifiedOn(new Date());
		
		// simpan data
		this.biodataService.update(biodataModel); // update biodataModel
		this.auditLogController.updateLog(biodataBefore, biodataModel); // isi update log
		
		String jsp = "biodata/biodata"; // setelah nge-save, kembali ke sini lagi
		return jsp;
	}
	
	// untuk memunculkan POP UP hapus biodata
	@RequestMapping(value="biodata/cruds/remove")
	public String biodataRemove(HttpServletRequest request, Model model) {
		
		// get nilai dari variabel nama di jsp
		Integer id = Integer.valueOf(request.getParameter("id"));
		
		// set nilai ke variabel di modelnya
		BiodataModel biodataModel = new BiodataModel();
		biodataModel = this.biodataService.searchId(id);
		
		model.addAttribute("biodataModel", biodataModel);
		
		// tampilkan lokasi
//		this.lokasiList(model);
		
		String jsp = "biodata/cruds/remove"; // setelah nge-save, kembali ke sini lagi
		return jsp;
	}
	
	/*// untuk menghapus data biodata
	@RequestMapping(value="biodata/cruds/delete")
	public String biodataDelete(HttpServletRequest request) {
		
		// get nilai dari variabel nama di jsp
		Integer idBiodata = Integer.valueOf(request.getParameter("idBiodata"));
		
		// set nilai ke variabel di modelnya
		BiodataModel biodataModel = new BiodataModel();
		biodataModel = this.biodataService.searchId(idBiodata);
		
		// simpan data
		this.biodataService.delete(biodataModel);
		
		String jsp = "biodata/biodata"; // setelah nge-save, kembali ke sini lagi
		return jsp;
	}*/
	
	// untuk menghapus data biodata
		@RequestMapping(value="biodata/cruds/delete")
		public String biodataDelete(HttpServletRequest request) {
			
			// get nilai dari variabel nama di jsp
			Integer id = Integer.valueOf(request.getParameter("id"));
			
			// set nilai ke variabel di modelnya
			BiodataModel biodataModel = new BiodataModel();
			biodataModel = this.biodataService.searchId(id);
			biodataModel.setIsDelete(true);

/*			// simpan data
			this.biodataService.delete(biodataModel);*/
			
			// save audit trail untuk deleted
//			Integer xIdDeletedBy = this.userSearch().getIdUser();
//			biodataModel.setxIdDeletedBy(xIdDeletedBy);
//			biodataModel.setxDeletedDate(new Date());
			
			// save audit trail untuk deleted, username adalah arif, jadi line di bawah nggak dipake
//			//Integer modifiedBy = this.userSearch().getId();
			biodataModel.setDeletedBy(2);
			biodataModel.setDeletedOn(new Date());
			
			// simpan data
			this.biodataService.deleteTemporary(biodataModel);
			this.auditLogController.deleteLog(biodataModel);
			
			String jsp = "biodata/biodata"; // setelah nge-save, kembali ke sini lagi
			return jsp;
		}
	
	@RequestMapping(value="biodata/cruds/search/nameORmajors")
	public String biodataSearchNama(HttpServletRequest request, Model model) {
		String name = request.getParameter("keyword");
		String majors = request.getParameter("keyword");
		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
		biodataModelList = this.biodataService.searchNameOrMajors(name, majors);
		model.addAttribute("biodataModelList", biodataModelList);
		
		String jsp = "biodata/cruds/list";
		return jsp;
	}
//	
//	@RequestMapping(value="biodata/cruds/search/kode")
//	public String biodataSearchKode(HttpServletRequest request, Model model) {
//		String kodeBiodata = request.getParameter("kodeBiodataKey");
//		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
//		biodataModelList = this.biodataService.searchKode(kodeBiodata);
//		model.addAttribute("biodataModelList", biodataModelList);
//		
//		String jsp = "biodata/cruds/list";
//		return jsp;
//	}
//	
//	@RequestMapping(value="biodata/cruds/search")
//	public String biodataSearchKodeOrNama(HttpServletRequest request, Model model) {
//		String kodeBiodata = request.getParameter("keyword");
//		String namaBiodata = request.getParameter("keyword");
//		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
//		biodataModelList = this.biodataService.searchKodeOrNama(kodeBiodata, namaBiodata);
//		model.addAttribute("biodataModelList", biodataModelList);
//		
//		String jsp = "biodata/cruds/list";
//		return jsp;
//	}
//	
//	public Integer cekKodeBiodata(String kodeBiodata) {
//		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
//		biodataModelList = this.biodataService.searchKodeEqual(kodeBiodata);
//		Integer jumlahDataBiodata = 0;
//		if (biodataModelList == null) {
//			jumlahDataBiodata = 0;
//		} else {
//			jumlahDataBiodata = biodataModelList.size();
//		}
//		return jumlahDataBiodata;
//	}
//	
//	public Integer cekNamaBiodata(String namaBiodata, Integer isDelete) {
//		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
//		biodataModelList = this.biodataService.searchNamaEqual(namaBiodata, isDelete);
//		Integer jumlahDataBiodata = 0;
//		if (biodataModelList == null) {
//			jumlahDataBiodata = 0;
//		} else {
//			jumlahDataBiodata = biodataModelList.size();
//		}
//		return jumlahDataBiodata;
//	}
//	
//	public String generateKodeBiodata() {
//		String kodeBiodataAuto = "FK";
//		
//		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
//		String kodeRole = this.userSearch().getRoleModel().getKodeRole();
//		biodataModelList = this.biodataService.search(kodeRole);
//		Integer jumlahDataBiodata = 0;
//		
//		if (biodataModelList == null) {
//			jumlahDataBiodata = 0;
//		} else {
//			jumlahDataBiodata = biodataModelList.size();
//		}
//		
//		kodeBiodataAuto = kodeBiodataAuto + "000" + (jumlahDataBiodata+1);
//		
//		return kodeBiodataAuto;
//	}
//	
	public void bootcampTestTypeList(Model model) {
		List<BootcampTestTypeModel> bootcampTestTypeModelList = new ArrayList<BootcampTestTypeModel>();
		bootcampTestTypeModelList = this.bootcampTestTypeService.search();
		model.addAttribute("bootcampTestTypeModelList", bootcampTestTypeModelList);
	}
	
	public boolean regexDigit(String isian) {
		String pattern = "(\\d+)";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(isian);
		if(m.find()) {
			return true;
		}
		else {
			return false;
		}
	}
}
