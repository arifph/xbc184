package com.xsis.winners.xbc184.controller;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.MenuModel;
import com.xsis.winners.xbc184.model.RoleModel;
import com.xsis.winners.xbc184.model.UserModel;
import com.xsis.winners.xbc184.service.MenuService;


@Controller
public class MenuController extends MUserController {
	
	@Autowired
	private MenuService menuService;
	

	@RequestMapping(value ="menu") //url action
	public String menu(Model model) { //method
		
		this.menuListSideMaster(model);
		this.menuListSideParent(model);
		
		String jsp ="menu/menu"; //target url atau halaman
		return jsp;
	}
	
	@RequestMapping(value ="") //url action
	public String menuU(Model model) { //method
		
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();
		menuModelList = this.menuService.search();
		
		model.addAttribute("menuModelList", menuModelList);
		
		String jsp ="index"; //target url atau halaman
		return jsp;
	}
	
	@RequestMapping(value ="menu/cruds/add")
	public String menuAdd(Model model) {
		
		//tampilkan lokasi select option
	//	this.roleList(model);
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();
		menuModelList = this.menuService.search();
		
		model.addAttribute("menuModelList", menuModelList);
		//this.menuParentList(model);
		
		String jsp ="menu/cruds/add";
		return jsp;
	}
//	
	@RequestMapping(value ="menu/cruds/create") 
	public String menuCreate(HttpServletRequest request, Model model) { 
		String menuParentStr = request.getParameter("menuParent");
		
		
		//get nilai dari variabel name di jsp
		String title	 = request.getParameter("title");
		String description	 = request.getParameter("description");
		String imageUrl	 = request.getParameter("imageUrl");
		Integer menuOrder	 = Integer.valueOf(request.getParameter("menuOrder"));
//		Integer menuParent = Integer.valueOf(request.getParameter("menuParent"));
		String kodeMenuAutor = this.generateKodeMenu();
		String menuUrl = request.getParameter("menuUrl");
//		
//		//set join table
//		//Integer idKota = Integer.valueOf(request.getParameter("idKota"));
//		
//		// set nilai ke variabel modelnya
		MenuModel menuModel = new MenuModel();
		
		menuModel.setCode(kodeMenuAutor);
		menuModel.setTitle(title);
		menuModel.setDescription(description);
		menuModel.setImageUrl(imageUrl);
		menuModel.setMenuOrder(menuOrder);
		
		menuModel.setMenuUrl(menuUrl);
		menuModel.setIsDelete(false);
		
		if (menuParentStr == "") {
			
		} else {
			Integer menuParent = Integer.valueOf(menuParentStr);
			menuModel.setMenuParent(menuParent);
		}

//	//	userModel.setMobileToken(mobileToken);
//		
//		//panggil method untuk cek kode dan nama
//		//Integer jumlahDataFakultas = this.cekKodeFakultas(kodeFakultas);
//		Integer jumlahDataUsername = this.cekUsername(username); //kalo disuruh ganti nama tinggal aktifin yg ini
//		
//		//Save audit trail untuk Created
		Integer createdBy = this.userSearch().getId();
		menuModel.setCreatedBy(createdBy);
		menuModel.setCreatedOn(new Date());
//		
//		if (jumlahDataUsername > 0) {
//			//tidak bisa simpan
//			model.addAttribute("jumlahDataUsername", jumlahDataUsername); // Lempar ke jsp jumlah datanya
//			//model.addAttribute("jumlahDataFakultas2", jumlahDataFakultas2);
//			model.addAttribute("username", username);
//			
//		} 
//			 else if (!password.equals(repassword)) {
//				 model.addAttribute("password", password);
//				 model.addAttribute("repassword", repassword);  }
//			 else {
//			//simpan data
//				this.userService.create(userModel);			
//			}
//		
//		
		this.menuService.create(menuModel);
		String jsp ="menu/menu"; //abis save balik ke halaman utama
		return jsp;
	}
////	
	@RequestMapping(value ="menu/cruds/list") //url action
	public String menuList(Model model) { //method
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();
		menuModelList = this.menuService.search();
		model.addAttribute("menuModelList", menuModelList);
		
		this.menuListSideMaster(model);
		String jsp ="menu/cruds/list"; //target url atau halaman
		return jsp;
	}
//	
//// tampil halaman Edit
	@RequestMapping(value ="menu/cruds/edit") //url action
	public String menuEdit(HttpServletRequest request, Model model) { //method
		Integer id 			 = Integer.valueOf(request.getParameter("id"));
		
		MenuModel menuModel  = new MenuModel();
		menuModel = this.menuService.searchId(id);
		
		model.addAttribute("menuModel", menuModel);
		
		this.menuParentList(model);
	
		String jsp ="menu/cruds/edit"; //target url atau halaman
		return jsp;
	}
//	
////	Action untuk update
	@RequestMapping(value ="menu/cruds/update") 
	public String menuUpdate(HttpServletRequest request) { 
		Integer id		= Integer.valueOf(request.getParameter("id"));
		String title	 = request.getParameter("title");
		String description	 = request.getParameter("description");
		String imageUrl	 = request.getParameter("imageUrl");
		Integer menuOrder	 = Integer.valueOf(request.getParameter("menuOrder"));
		Integer menuParent = Integer.valueOf(request.getParameter("menuParent"));
		String menuUrl = request.getParameter("menuUrl");
		
		MenuModel menuModel = new MenuModel();
		
		menuModel = this.menuService.searchId(id);
		menuModel.setTitle(title);
		menuModel.setDescription(description);
		menuModel.setImageUrl(imageUrl);
		menuModel.setMenuOrder(menuOrder);
		menuModel.setMenuParent(menuParent);
		menuModel.setMenuUrl(menuUrl);
		
		Integer modifiedBy = this.userSearch().getId();
		menuModel.setModifiedBy(modifiedBy);
		menuModel.setModifiedOn(new Date());
		
		
		this.menuService.update(menuModel);
		String jsp ="menu/menu"; //abis save balik ke halaman utama
		return jsp;
		
		//get nilai dari variabel name di jsp
		//Integer id		= Integer.valueOf(request.getParameter("id"));
//		String title		 = request.getParameter("title");
//		String description	 = request.getParameter("description");
//		String imageUrl		 = request.getParameter("imageUrl");
//		String menuOrder	 = request.getParameter("menuOrder");
//		String menuParent	 = request.getParameter("menuParent");
//
//		String menuUrl		 = request.getParameter("menuUrl");
//		
		// set nilai ke variabel modelnya
//		MenuModel menuModel  = new MenuModel();
		
		//menuModel = this.menuService.searchId(id);
//		menuModel.setTitle(title);
//		menuModel.setDescription(description);
//		menuModel.setImageUrl(imageUrl);
//		//menuModel.setMenuOrder(menuOrder);
//		//menuModel.setMenuParent(menuParent);
//		menuModel.setMenuUrl(menuUrl);
		
		//set join table
		//terminalModel.setIdKota(idKota);
		
		
		//Save audit trail untuk Created
//		Integer modifiedBy = this.userSearch().getId();
//		menuModel.setModifiedBy(modifiedBy);
//		menuModel.setModifiedOn(new Date());
//		
//		//simpan data
//		this.menuService.create(menuModel);
//		String jsp ="menu/menu"; //abis save balik ke halaman utama
//		return jsp;
	}
//	
//	//untuk reset password
//	@RequestMapping(value ="user/cruds/resetPassword") //url action
//	public String userResetPassword(HttpServletRequest request, Model model) { //method
//		Integer id = Integer.valueOf(request.getParameter("id"));
//		
//		UserModel userModel = new UserModel();
//		userModel = this.userService.searchId(id);
//		model.addAttribute("userModel", userModel);
//				
//		String jsp ="user/cruds/resetPassword"; //target url atau halaman
//		return jsp;
//	}
//	
//	@RequestMapping(value="user/cruds/reset")
//	public String userResetPass(HttpServletRequest request, Model model) {
//		Integer id		= Integer.valueOf(request.getParameter("id"));
//		String password	= request.getParameter("password");
//		String repassword = request.getParameter("repassword");
//		
//		UserModel userModel = new UserModel();
//		
//		userModel = this.userService.searchId(id);
//		userModel.setPassword(password);
//		
//		//Save audit trail untuk Created
//		Integer modifiedBy = this.userSearch().getId();
//		userModel.setModifiedBy(modifiedBy);
//		userModel.setModifiedOn(new Date());
//		
//		if (!password.equals(repassword)) {
//			
//			model.addAttribute("password", password);
//			model.addAttribute("repassword", repassword);
//			
//		} else {
//			//simpan data
//			this.userService.update(userModel);
//		}
//		
//		String jsp ="user/user"; //abis save balik ke halaman utama
//		return jsp;
//	}
////	
//
//	
	@RequestMapping(value ="menu/cruds/delete") 
	public String menuDelete(HttpServletRequest request) { 
		
		Integer id 	   = Integer.valueOf(request.getParameter("id"));
		
		// set nilai ke variabel modelnya
		MenuModel menuModel = new MenuModel();
		menuModel = this.menuService.searchId(id);
		
		menuModel.setIsDelete(true);

		//Save audit trail untuk Created
		Integer deletedBy = this.userSearch().getId();
		menuModel.setDeletedBy(deletedBy);
		menuModel.setDeletedOn(new Date());

		//simpan data
		this.menuService.deleteTemp(menuModel);
		String jsp ="menu/menu"; //abis save balik ke halaman utama
		return jsp;
	}

	
	@RequestMapping(value="menu/cruds/search/search")
	public String menuSearch(HttpServletRequest request, Model model) {
		String title = request.getParameter("tittleSearch");
		
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();
		menuModelList = this.menuService.searchByTitle(title);
		
//		Integer hasil = this.generateNumber();
//		System.out.println(hasil);
		model.addAttribute("menuModelList", menuModelList); // menampilkan ke jsp
		String jsp ="menu/cruds/list";
		return jsp;
	}
	

//	public Integer cekUsername(String username) {
//		List<UserModel> userModelList = new ArrayList<UserModel>(); //object
//		userModelList = this.userService.searchUsername(username);
//		
//		Integer jumlahDataUsername=0;
//		
//		if (userModelList == null) {
//			jumlahDataUsername=0;
//		} else {
//			jumlahDataUsername=userModelList.size();
//		}
//		return jumlahDataUsername; 
//	}
	//fungsi generatekode
	public String generateKodeMenu() {
		String kodeMenuAuto = "M";
		
		List<MenuModel> menuModelList = new ArrayList<MenuModel>(); 
		menuModelList = this.menuService.search();
		Integer jumlahDataMenu=0;
		if (menuModelList == null) {
			jumlahDataMenu=0;
		} else {
			jumlahDataMenu=menuModelList.size();
		}
		
		kodeMenuAuto = kodeMenuAuto + "000" +(jumlahDataMenu+1);
		
		return kodeMenuAuto;
	}
////	public String Token() {
////		List<UserModel> userModelList = new ArrayList<UserModel>();
////		int kodeToken = 0;
////		userModelList = this.userService.search();
////		Integer jumlahDataUser=0;
////		if (userModelList == null) {
////			
////		} else {
////			jumlahDataUser=userModelList.size();
////		}
////		kodeToken = "000000"+(jumlahDataUser+1);
////		
////		return kodeToken;
////	}
//
////    public static String generateActivationCode(int length) {
////    	Random random = new Random();
////	        String code = new String("");
////	        for (int i = 0; i < length; i++) {
////	            code += (char) (random.nextInt(10) + '0');
////	        }
////	        return code;
////	    }
//	public Integer generateNumber() {
//		Random angkaRandom = new Random();
//		int hasil = 0;
//		for(int counter = 1; counter <=10; counter++) {
//			hasil = 1+angkaRandom.nextInt(9);
//		}
//		return hasil;
//		
//	}
	public void menuParentList(Model model) {
		List<MenuModel> menuModelList = new ArrayList<>(); 
		menuModelList = this.menuService.search();
		model.addAttribute("menuModelList", menuModelList);
	}
//	
//	public void roleList(Model model) {
//		List<RoleModel> roleModelList = new ArrayList<>();
//		roleModelList = this.roleService.search();
//		model.addAttribute("roleModelList", roleModelList);
//	}
}
