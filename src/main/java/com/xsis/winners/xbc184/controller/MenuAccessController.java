package com.xsis.winners.xbc184.controller;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.MenuAccessModel;
import com.xsis.winners.xbc184.model.MenuModel;
import com.xsis.winners.xbc184.model.RoleModel;
import com.xsis.winners.xbc184.model.UserModel;
import com.xsis.winners.xbc184.service.MenuAccessService;
import com.xsis.winners.xbc184.service.MenuService;
import com.xsis.winners.xbc184.service.RoleService;


@Controller
public class MenuAccessController extends MUserController {
	
	@Autowired
	private MenuAccessService menuAccessService;
	
	@Autowired
	private RoleService roleService;
	
//	@Autowired
//	private MenuService menuService;
	

	@RequestMapping(value ="menuaccess") //url action
	public String menuAccess(Model model) { //method
		
		this.roleList(model);
		this.menuListAll(model);
		this.menuListSideMaster(model);
		this.menuListSideParent(model);
		
		String jsp ="menuAccess/menuAccess"; //target url atau halaman
		return jsp;
	}
	
//	@RequestMapping(value ="menu/cruds/add")
//	public String menuAdd(Model model) {
//		
//		//tampilkan lokasi select option
//	//	this.roleList(model);
//		
//		String jsp ="menu/cruds/add";
//		return jsp;
//	}
//	
	@RequestMapping(value ="menuAccess/cruds/create") 
	public String menuAccessCreate(HttpServletRequest request, Model model) { 
		
		//get nilai dari variabel name di jsp
		Integer roleId	 = Integer.valueOf(request.getParameter("role"));
		Integer menuId	 = Integer.valueOf(request.getParameter("menu"));
		
////		
////		//set join table
////		//Integer idKota = Integer.valueOf(request.getParameter("idKota"));
////		
////		// set nilai ke variabel modelnya
		MenuAccessModel menuAccessModel = new MenuAccessModel();
//		
		menuAccessModel.setRoleId(roleId);
		menuAccessModel.setMenuId(menuId);



//	//	userModel.setMobileToken(mobileToken);
//		
//		//panggil method untuk cek kode dan nama
//		//Integer jumlahDataFakultas = this.cekKodeFakultas(kodeFakultas);
//		Integer jumlahDataUsername = this.cekUsername(username); //kalo disuruh ganti nama tinggal aktifin yg ini
//		
//		//Save audit trail untuk Created
//		Integer createdBy = this.userSearch().getId();
//		menuAccessModel.setCreatedBy(createdBy);
//		menuAccessModel.setCreatedOn(new Date());
////		
//		if (jumlahDataUsername > 0) {
//			//tidak bisa simpan
//			model.addAttribute("jumlahDataUsername", jumlahDataUsername); // Lempar ke jsp jumlah datanya
//			//model.addAttribute("jumlahDataFakultas2", jumlahDataFakultas2);
//			model.addAttribute("username", username);
//			
//		} 
//			 else if (!password.equals(repassword)) {
//				 model.addAttribute("password", password);
//				 model.addAttribute("repassword", repassword);  }
//			 else {
//			//simpan data
//				this.userService.create(userModel);			
//			}
//		
//		
		this.menuAccessService.create(menuAccessModel);
		String jsp ="menuAccess/menuAccess"; //abis save balik ke halaman utama
		return jsp;
	}
////	
	@RequestMapping(value ="menuAccess/cruds/list") //url action
	public String menuAccessList(Model model) { //method
		List<MenuAccessModel> menuAccessModelList = new ArrayList<MenuAccessModel>();
		menuAccessModelList = this.menuAccessService.search();
		this.roleList(model);
		this.menuListSideMaster(model);
		model.addAttribute("menuAccessModelList", menuAccessModelList);
		String jsp ="menuAccess/cruds/list"; //target url atau halaman
		return jsp;
	}
//	
//// tampil halaman Edit
//	@RequestMapping(value ="menu/cruds/edit") //url action
//	public String menuEdit(HttpServletRequest request, Model model) { //method
//		Integer id 			 = Integer.valueOf(request.getParameter("id"));
//		
//		MenuModel menuModel  = new MenuModel();
//		menuModel = this.menuService.searchId(id);
//		
//		model.addAttribute("menuModel", menuModel);
//	
//		String jsp ="menu/cruds/edit"; //target url atau halaman
//		return jsp;
//	}

	
//	//untuk reset password
//	@RequestMapping(value ="user/cruds/resetPassword") //url action
//	public String userResetPassword(HttpServletRequest request, Model model) { //method
//		Integer id = Integer.valueOf(request.getParameter("id"));
//		
//		UserModel userModel = new UserModel();
//		userModel = this.userService.searchId(id);
//		model.addAttribute("userModel", userModel);
//				
//		String jsp ="user/cruds/resetPassword"; //target url atau halaman
//		return jsp;
//	}
//	
//	@RequestMapping(value="user/cruds/reset")
//	public String userResetPass(HttpServletRequest request, Model model) {
//		Integer id		= Integer.valueOf(request.getParameter("id"));
//		String password	= request.getParameter("password");
//		String repassword = request.getParameter("repassword");
//		
//		UserModel userModel = new UserModel();
//		
//		userModel = this.userService.searchId(id);
//		userModel.setPassword(password);
//		
//		//Save audit trail untuk Created
//		Integer modifiedBy = this.userSearch().getId();
//		userModel.setModifiedBy(modifiedBy);
//		userModel.setModifiedOn(new Date());
//		
//		if (!password.equals(repassword)) {
//			
//			model.addAttribute("password", password);
//			model.addAttribute("repassword", repassword);
//			
//		} else {
//			//simpan data
//			this.userService.update(userModel);
//		}
//		
//		String jsp ="user/user"; //abis save balik ke halaman utama
//		return jsp;
//	}
////	
//
//	
	@RequestMapping(value ="menuAccess/cruds/delete") 
	public String menuAccessDelete(HttpServletRequest request) { 
		
		Integer id 	   = Integer.valueOf(request.getParameter("id"));
		
		// set nilai ke variabel modelnya
		MenuAccessModel menuAccessModel = new MenuAccessModel();
		menuAccessModel = this.menuAccessService.searchId(id);
		

		//simpan data
		this.menuAccessService.delete(menuAccessModel);
		String jsp ="menuAccess/menuAccess"; //abis save balik ke halaman utama
		return jsp;
	}

	
	@RequestMapping(value="menuaccess/cruds/search/search")
	public String menuAccessSearch(HttpServletRequest request, Model model) {
		Integer roleId = Integer.valueOf(request.getParameter("roleId"));
		
		List<MenuAccessModel> menuAccessModelList = new ArrayList<MenuAccessModel>();
		menuAccessModelList = this.menuAccessService.searchByRoleId(roleId);
		
		model.addAttribute("menuAccessModelList", menuAccessModelList); // menampilkan ke jsp
		String jsp ="menuAccess/cruds/list";
		return jsp;
	}
	
//	
	public void roleList(Model model) {
		List<RoleModel> roleModelList = new ArrayList<>();
		roleModelList = this.roleService.search();
		model.addAttribute("roleModelList", roleModelList);
	}
	
//	public void menuList(Model model) {
//		List<MenuModel> menuModelList = new ArrayList<>();
//		menuModelList = this.menuService.search();
//		model.addAttribute("menuModelList", menuModelList);
//	}
//	
}
