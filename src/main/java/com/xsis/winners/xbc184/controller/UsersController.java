package com.xsis.winners.xbc184.controller;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.RoleModel;
import com.xsis.winners.xbc184.model.UserModel;
import com.xsis.winners.xbc184.service.RoleService;
import com.xsis.winners.xbc184.service.UserService;

@Controller
public class UsersController extends MUserController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleService roleService;

	@RequestMapping(value ="user") //url action
	public String user() { //method
		String jsp ="user/user"; //target url atau halaman
		return jsp;
	}
	
	@RequestMapping(value ="user/cruds/add")
	public String userAdd(Model model) {
		
		//tampilkan lokasi select option
		this.roleList(model);
		
		String jsp ="user/cruds/add";
		return jsp;
	}
//	
	@RequestMapping(value ="user/cruds/create") 
	public String userCreate(HttpServletRequest request, Model model) { 
		
		//get nilai dari variabel name di jsp
		Integer idRole	 = Integer.valueOf(request.getParameter("idRole"));
		String email	 = request.getParameter("email");
		String username	 = request.getParameter("username");
		String password	 = request.getParameter("password");
		String repassword= request.getParameter("repassword");
		
		//set join table
		//Integer idKota = Integer.valueOf(request.getParameter("idKota"));
		
		// set nilai ke variabel modelnya
		UserModel userModel = new UserModel();
		userModel.setRoleId(idRole);
		userModel.setEmail(email);
		userModel.setUsername(username);
		userModel.setPassword(password);
		
		
		userModel.setIsDelete(false);
		userModel.setMobileFlag(false);
		
	//	userModel.setMobileToken(mobileToken);
		
		//panggil method untuk cek kode dan nama
		//Integer jumlahDataFakultas = this.cekKodeFakultas(kodeFakultas);
		Integer jumlahDataUsername = this.cekUsername(username); //kalo disuruh ganti nama tinggal aktifin yg ini
		
		//Save audit trail untuk Created
		Integer createdBy = this.userSearch().getId();
		userModel.setCreatedBy(createdBy);
		userModel.setCreatedOn(new Date());
		
//		Integer xIdCreatedBy = this.userSearch().getIdUser();
//		terminalModel.setxIdCreatedBy(xIdCreatedBy);
//		terminalModel.setxCreatedDate(new Date());
		
		if (jumlahDataUsername > 0) {
			//tidak bisa simpan
			model.addAttribute("jumlahDataUsername", jumlahDataUsername); // Lempar ke jsp jumlah datanya
			//model.addAttribute("jumlahDataFakultas2", jumlahDataFakultas2);
			model.addAttribute("username", username);
			
		} 
			 else if (!password.equals(repassword)) {
				 model.addAttribute("password", password);
				 model.addAttribute("repassword", repassword);  }
			 else {
			//simpan data
				this.userService.create(userModel);			
			}
		
		
		//this.userService.create(userModel);
		String jsp ="user/user"; //abis save balik ke halaman utama
		return jsp;
	}
//	
	@RequestMapping(value ="user/cruds/list") //url action
	public String userList(Model model) { //method
		List<UserModel> userModelList = new ArrayList<UserModel>();
		userModelList = this.userService.search();
		model.addAttribute("userModelList", userModelList);
		String jsp ="user/cruds/list"; //target url atau halaman
		return jsp;
	}
//	
//	@RequestMapping(value ="terminal/cruds/detail") //url action
//	public String terminalDetail(HttpServletRequest request, Model model) { //method
//		Integer idTerminal = Integer.valueOf(request.getParameter("idTerminal"));
//		
//		TerminalModel terminalModel = new TerminalModel();
//		terminalModel = this.terminalService.searchId(idTerminal);
//		
//		model.addAttribute("terminalModel", terminalModel);
//		
//		//tampilkan lokasi select option
//		this.kotaList(model);
//		/*
//		 * String var1= "JAKARTA"; model.addAttribute("var2", var1); String var2 =
//		 * "ES JERUK"; model.addAttribute("minum", var2); String var3 = "PIZZA";
//		 * model.addAttribute("makan", var3);
//		 */
//		
//		String jsp ="terminal/cruds/detail"; //target url atau halaman
//		return jsp;
//	}
//	
	@RequestMapping(value ="user/cruds/edit") //url action
	public String userEdit(HttpServletRequest request, Model model) { //method
		Integer id = Integer.valueOf(request.getParameter("id"));
		
		UserModel userModel = new UserModel();
		userModel = this.userService.searchId(id);
		
		model.addAttribute("userModel", userModel);
		//tampilkan lokasi select option
		
		this.roleList(model);
		
		String jsp ="user/cruds/edit"; //target url atau halaman
		return jsp;
	}
//	
	@RequestMapping(value ="user/cruds/update") 
	public String userUpdate(HttpServletRequest request) { 
		
		//get nilai dari variabel name di jsp
		Integer id		= Integer.valueOf(request.getParameter("id"));
		Integer roleId	= Integer.valueOf(request.getParameter("idRole"));
		String email	= request.getParameter("email"); 
		String username	= request.getParameter("username");
		Boolean mobileFlag = Boolean.valueOf(request.getParameter("mobileFlag"));
		Long mobileToken = Long.valueOf(request.getParameter("mobileToken"));
		
		// set nilai ke variabel modelnya
		UserModel userModel = new UserModel();
		
		userModel = this.userService.searchId(id);
		userModel.setRoleId(roleId);
		userModel.setEmail(email);
		userModel.setUsername(username);
		userModel.setMobileFlag(mobileFlag);
		userModel.setMobileToken(mobileToken.intValue());
		
		//set join table
		//terminalModel.setIdKota(idKota);
		
		
		//Save audit trail untuk Created
		Integer modifiedBy = this.userSearch().getId();
		userModel.setModifiedBy(modifiedBy);
		userModel.setModifiedOn(new Date());
		
		//simpan data
		this.userService.update(userModel);
		String jsp ="user/user"; //abis save balik ke halaman utama
		return jsp;
	}
	
	//untuk reset password
	@RequestMapping(value ="user/cruds/resetPassword") //url action
	public String userResetPassword(HttpServletRequest request, Model model) { //method
		Integer id = Integer.valueOf(request.getParameter("id"));
		
		UserModel userModel = new UserModel();
		userModel = this.userService.searchId(id);
		model.addAttribute("userModel", userModel);
				
		String jsp ="user/cruds/resetPassword"; //target url atau halaman
		return jsp;
	}
	
	@RequestMapping(value="user/cruds/reset")
	public String userResetPass(HttpServletRequest request, Model model) {
		Integer id		= Integer.valueOf(request.getParameter("id"));
		String password	= request.getParameter("password");
		String repassword = request.getParameter("repassword");
		
		UserModel userModel = new UserModel();
		
		userModel = this.userService.searchId(id);
		userModel.setPassword(password);
		
		//Save audit trail untuk Created
		Integer modifiedBy = this.userSearch().getId();
		userModel.setModifiedBy(modifiedBy);
		userModel.setModifiedOn(new Date());
		
		if (!password.equals(repassword)) {
			
			model.addAttribute("password", password);
			model.addAttribute("repassword", repassword);
			
		} else {
			//simpan data
			this.userService.update(userModel);
		}
		
		String jsp ="user/user"; //abis save balik ke halaman utama
		return jsp;
	}
//	
//	@RequestMapping(value ="terminal/cruds/remove") //url action
//	public String terminalRemove(HttpServletRequest request, Model model) { //method
//		Integer idTerminal = Integer.valueOf(request.getParameter("idTerminal"));
//		
//		TerminalModel terminalModel = new TerminalModel();
//		terminalModel = this.terminalService.searchId(idTerminal);
//		
//		model.addAttribute("terminalModel", terminalModel);
//		//tampilkan lokasi select option
//		this.kotaList(model);
//		
//		String jsp ="terminal/cruds/remove"; //target url atau halaman
//		return jsp;
//	}
//	
	
	@RequestMapping(value ="user/cruds/delete") 
	public String userDelete(HttpServletRequest request) { 
//		
//		//get nilai dari variabel name di jsp
//		Integer idTerminal = Integer.valueOf(request.getParameter("idTerminal"));
		Integer id 	   = Integer.valueOf(request.getParameter("id"));
//		
//		// set nilai ke variabel modelnya
		UserModel userModel = new UserModel();
		userModel = this.userService.searchId(id);
//		
		userModel.setIsDelete(true);
//		terminalModel = this.terminalService.searchId(idTerminal);
//	
//		terminalModel.setIsDelete(1); //status 1 artinya didelete
//		
//		//Save audit trail untuk Created
		Integer deletedBy = this.userSearch().getId();
		userModel.setDeletedBy(deletedBy);
		userModel.setDeletedOn(new Date());
//		Integer IdDeletedBy = this.userSearch().getIdUser();
//		terminalModel.setxIdDeletedBy(xIdDeletedBy);
//		terminalModel.setxDeletedDate(new Date());
//		
//		//simpan data
		this.userService.deleteTemp(userModel);
		String jsp ="user/user"; //abis save balik ke halaman utama
		return jsp;
	}
		
//	@RequestMapping(value ="terminal/cruds/delete") 
//	public String terminalDelete(HttpServletRequest request) { 
//		
//		//get nilai dari variabel name di jsp
//		Integer idTerminal = Integer.valueOf(request.getParameter("idTerminal"));
//		
//		// set nilai ke variabel modelnya
//		TerminalModel terminalModel = new TerminalModel();
//		
//		terminalModel = this.terminalService.searchId(idTerminal);
//	
//		terminalModel.setIsDelete(1); //status 1 artinya didelete
//		
//		//Save audit trail untuk Created
//		Integer xIdDeletedBy = this.userSearch().getIdUser();
//		terminalModel.setxIdDeletedBy(xIdDeletedBy);
//		terminalModel.setxDeletedDate(new Date());
//		
//		//simpan data
//		this.terminalService.deleteTemporary(terminalModel);
//		String jsp ="terminal/terminal"; //abis save balik ke halaman utama
//		return jsp;
//	}
	
//	@RequestMapping(value="fakultas/cruds/search/nama")
//	public String fakultasSearchNama(HttpServletRequest request, Model model) {
//		String namaFakultas = request.getParameter("namaFakultasKey");
//		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>();
//		fakultasModelList = this.fakultasService.searchNama(namaFakultas);
//		
//		model.addAttribute("fakultasModelList", fakultasModelList); // menampilkan ke jsp
//		String jsp ="fakultas/cruds/list";
//		return jsp;
//	}
	
	@RequestMapping(value="user/cruds/search/usernamepass")
	public String userSearchUsernamePass(HttpServletRequest request, Model model) {
		String username = request.getParameter("usernameEmailSearch");
		String email = request.getParameter("usernameEmailSearch");
		
		List<UserModel> userModelList = new ArrayList<UserModel>();
		userModelList = this.userService.searchUsernameOrEmail(username, email);
		
//		Integer hasil = this.generateNumber();
//		System.out.println(hasil);
		model.addAttribute("userModelList", userModelList); // menampilkan ke jsp
		String jsp ="user/cruds/list";
		return jsp;
	}
	
//	@RequestMapping(value="fakultas/cruds/search")
//	public String fakultasSearch(HttpServletRequest request, Model model) {
//		String kodeFakultas = request.getParameter("keyword");
//		String namaFakultas = request.getParameter("keyword");
//		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>();
//		fakultasModelList = this.fakultasService.searchKodeOrNama(kodeFakultas, namaFakultas);
//		
//		model.addAttribute("fakultasModelList", fakultasModelList); // menampilkan ke jsp
//		String jsp ="fakultas/cruds/list";
//		return jsp;
//	}
	
//	public Integer cekKodeTerminal(String kodeTerminal) {
//		List<TerminalModel> terminalModelList = new ArrayList<TerminalModel>();
//		terminalModelList = this.terminalService.searchKodeEqual(kodeTerminal);
//		Integer jumlahDataTerminal=0;
//		if (terminalModelList == null) {
//			jumlahDataTerminal=0;
//		} else {
//			jumlahDataTerminal = terminalModelList.size();
//		}
//		return jumlahDataTerminal;
//	}
//	
	public Integer cekUsername(String username) {
		List<UserModel> userModelList = new ArrayList<UserModel>(); //object
		userModelList = this.userService.searchUsername(username);
		
		Integer jumlahDataUsername=0;
		
		if (userModelList == null) {
			jumlahDataUsername=0;
		} else {
			jumlahDataUsername=userModelList.size();
		}
		return jumlahDataUsername; 
	}
	//fungsi generatekode
//	public String generateKodeFakultas() {
//		String kodeFakultasAuto = "FK";
//		
//		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>(); 
//		fakultasModelList = this.fakultasService.search();
//		Integer jumlahDataFakultas=0;
//		if (fakultasModelList == null) {
//			jumlahDataFakultas=0;
//		} else {
//			jumlahDataFakultas=fakultasModelList.size();
//		}
//		
//		kodeFakultasAuto = kodeFakultasAuto + "000" +(jumlahDataFakultas+1);
//		
//		return kodeFakultasAuto;
//	}
//	public String Token() {
//		List<UserModel> userModelList = new ArrayList<UserModel>();
//		int kodeToken = 0;
//		userModelList = this.userService.search();
//		Integer jumlahDataUser=0;
//		if (userModelList == null) {
//			
//		} else {
//			jumlahDataUser=userModelList.size();
//		}
//		kodeToken = "000000"+(jumlahDataUser+1);
//		
//		return kodeToken;
//	}

//    public static String generateActivationCode(int length) {
//    	Random random = new Random();
//	        String code = new String("");
//	        for (int i = 0; i < length; i++) {
//	            code += (char) (random.nextInt(10) + '0');
//	        }
//	        return code;
//	    }
	public Integer generateNumber() {
		Random angkaRandom = new Random();
		int hasil = 0;
		for(int counter = 1; counter <=10; counter++) {
			hasil = 1+angkaRandom.nextInt(9);
		}
		return hasil;
		
	}
//	public void kotaList(Model model) {
//		List<KotaModel> kotaModelList = new ArrayList<>(); 
//		kotaModelList = this.kotaService.search();
//		model.addAttribute("kotaModelList", kotaModelList);
//	}
	
	public void roleList(Model model) {
		List<RoleModel> roleModelList = new ArrayList<>();
		roleModelList = this.roleService.search();
		model.addAttribute("roleModelList", roleModelList);
	}
}
