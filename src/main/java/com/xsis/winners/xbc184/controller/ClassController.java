package com.xsis.winners.xbc184.controller;

import java.util.ArrayList;
//import java.util.Date;
import java.util.List;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.winners.xbc184.model.ClassModel;
//import com.xsis.winners.xbc184.model.BootcampTestTypeModel;
//import com.xsis.winners.xbc184.service.AuditLogService;
//import com.xsis.winners.xbc184.model.LokasiModel;
import com.xsis.winners.xbc184.service.ClassService;
//import com.xsis.winners.xbc184.service.BootcampTestTypeService;

@Controller
public class ClassController /*extends UserController*/{

	@Autowired
	private ClassService classService;
	
//	@Autowired
//	private BootcampTestTypeService bootcampTestTypeService;
	
	@Autowired
	private AuditLogController auditLogController;
	
	@RequestMapping(value="class") // url action
	public String classPage() { // method
		String jsp = "class/class"; // target atau halaman
		return jsp;
	}
	
	@RequestMapping(value="class/cruds/list")
	public String classList(Model model) {
		List<ClassModel> classModelList = new ArrayList<ClassModel>();
//		String kodeRole = this.userSearch().getRoleModel().getKodeRole();
		classModelList = this.classService.search();
		model.addAttribute("classModelList", classModelList);
		
		String jsp = "class/cruds/list";
		return jsp;
	}
	
	// untuk memunculkan POP UP hapus class
	@RequestMapping(value="class/cruds/remove")
	public String classRemove(HttpServletRequest request, Model model) {
		
		// get nilai dari variabel nama di jsp
		Integer id = Integer.valueOf(request.getParameter("id"));
		
		// set nilai ke variabel di modelnya
		ClassModel classModel = new ClassModel();
		classModel = this.classService.searchId(id);
		
		model.addAttribute("classModel", classModel);
		
		String jsp = "class/cruds/remove"; // setelah nge-save, kembali ke sini lagi
		return jsp;
	}
	
	/*// untuk menghapus data class
	@RequestMapping(value="class/cruds/delete")
	public String classDelete(HttpServletRequest request) {
		
		// get nilai dari variabel nama di jsp
		Integer idClass = Integer.valueOf(request.getParameter("idClass"));
		
		// set nilai ke variabel di modelnya
		ClassModel classModel = new ClassModel();
		classModel = this.classService.searchId(idClass);
		
		// simpan data
		this.classService.delete(classModel);
		
		String jsp = "class/class"; // setelah nge-save, kembali ke sini lagi
		return jsp;
	}*/
	
	// untuk menghapus data class
		@RequestMapping(value="class/cruds/delete")
		public String classDelete(HttpServletRequest request) {
			
			// get nilai dari variabel nama di jsp
			Integer id = Integer.valueOf(request.getParameter("id"));
			
			// set nilai ke variabel di modelnya
			ClassModel classModel = new ClassModel();
			classModel = this.classService.searchId(id);
//			classModel.setIsDelete(true);
			
			// save audit trail untuk deleted
//			Integer xIdDeletedBy = this.userSearch().getIdUser();
//			classModel.setxIdDeletedBy(xIdDeletedBy);
//			classModel.setxDeletedDate(new Date());
			
			// save audit trail untuk deleted, username adalah arif, jadi line di bawah nggak dipake
//			//Integer modifiedBy = this.userSearch().getId();
//			classModel.setDeletedBy(2);
//			classModel.setDeletedOn(new Date());
			
			// simpan data
//			this.classService.deleteTemporary(classModel);
			this.classService.delete(classModel);
			this.auditLogController.deleteLog(classModel);
			
			String jsp = "class/class"; // setelah nge-save, kembali ke sini lagi
			return jsp;
		}
	
	@RequestMapping(value="class/cruds/search")
	public String classSearch(HttpServletRequest request, Model model) {
		String batch = request.getParameter("keyword");
		String technology = request.getParameter("keyword");
		List<ClassModel> classModelList = new ArrayList<ClassModel>();
		classModelList = this.classService.searchBatchOrTechnology(batch, technology);
		model.addAttribute("classModelList", classModelList);
		
		String jsp = "class/cruds/list";
		return jsp;
	}
}
