package com.xsis.winners.xbc184.controller;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xsis.winners.xbc184.model.DocumentTestDetailModel;
import com.xsis.winners.xbc184.model.DocumentTestModel;
import com.xsis.winners.xbc184.model.QuestionModel;
import com.xsis.winners.xbc184.model.TestModel;
import com.xsis.winners.xbc184.model.TestTypeModel;
import com.xsis.winners.xbc184.service.DocumentTestService;
import com.xsis.winners.xbc184.service.QuestionService;
import com.xsis.winners.xbc184.service.TestService;
import com.xsis.winners.xbc184.service.TestTypeService;

@Controller
public class DocumentTestController extends AuditLogController {

	@Autowired
	private DocumentTestService documentTestService;

	@Autowired
	private TestService testService;

	@Autowired
	private TestTypeService testTypeService;

	@Autowired
	private QuestionService questionService;

	private List<DocumentTestDetailModel> detailList;

	private DocumentTestModel documentTestModel;

	private static final String ALPHANUMERIC = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	private static final SecureRandom RANDOMIZER = new SecureRandom();

	private static final Logger LOGGER = LogManager.getLogger(DocumentTestController.class);

	@RequestMapping(value = "document-test")
	public String documentTest() {
		String jsp = "document-test/document-test";
		return jsp;
	}

	@RequestMapping(value = "document-test/cruds/list")
	public String documentTestList(Model model) {
		LOGGER.info("Showing Document Test List");
		List<DocumentTestModel> documentTestModelList = new ArrayList<>();
		try {
		documentTestModelList = this.documentTestService.searchAll();
		LOGGER.info("Document Test List Size: " + documentTestModelList.size());
		} catch(Exception e) {
			e.printStackTrace();
		}

		LOGGER.info("Passing documentTestModelList to list.jsp");
		model.addAttribute("documentTestModelList", documentTestModelList);

		LOGGER.info("Showing list");
		String jsp = "document-test/cruds/list";
		return jsp;
	}

	@RequestMapping(value = "document-test/cruds/add")
	public String documentTestAdd(Model model) {
		LOGGER.info("Showing Document Test Add Dialog");

		LOGGER.info("Filling test and test type list");
		List<TestModel> testModelList = this.testService.search();
		List<TestTypeModel> testTypeModelList = this.testTypeService.search();
		LOGGER.info("testModelList size: " + testModelList.size() + ", testTypeModelList size: "
				+ testTypeModelList.size());

		model.addAttribute("testModelList", testModelList);
		model.addAttribute("testTypeModelList", testTypeModelList);

		String jsp = "document-test/cruds/add";
		return jsp;
	}

	@RequestMapping(value = "document-test/cruds/add/set-version")
	@ResponseBody
	public String documentTestVersion(HttpServletRequest request) {
		LOGGER.info("Calculating version");
		String version = null;
		Integer testTypeId = Integer.valueOf(request.getParameter("testTypeId"));
		String documentTestId = request.getParameter("documentTestId");
		
		if(documentTestId == null) {
			version = String.valueOf(getVersion(testTypeId));
		}
		else {
			if (testTypeId == this.documentTestModel.getTestTypeId()) {
				version = String.valueOf(this.documentTestModel.getVersion());
			} else {
				version = String.valueOf(getVersion(testTypeId));
			}
		}

		return version;
	}

	@RequestMapping(value = "document-test/generate-token")
	@ResponseBody
	public String documentTestToken() {
		LOGGER.info("Generating and returning token");

		String newToken = randomStringGenerator(10);
		LOGGER.info("The generated token = " + newToken);

		return newToken;
	}

	@RequestMapping(value = "document-test/cruds/save")
	public String documentTestSave(DocumentTestModel documentTest) {
		LOGGER.info("Saving document test");
		LOGGER.info(documentTest);

		this.documentTestModel = documentTest;
		this.documentTestModel.setCreatedBy(this.userSearch().getId());
		this.documentTestModel.setCreatedOn(new Date());
		this.documentTestModel.setIsDelete(false);

		documentTestService.save(this.documentTestModel);
		this.saveLog(this.documentTestModel);

		LOGGER.info("Successfully saved");
		documentTestModel = null;

		String jsp = "document-test/cruds/list";
		return jsp;
	}

	@RequestMapping(value = "document-test/cruds/edit")
	public String documentTestEdit(HttpServletRequest request, Model model) {
		LOGGER.info("Showing edit dialog");
		Integer id = Integer.valueOf(request.getParameter("id"));

		LOGGER.info("Searching document test model with id " + id);
		documentTestModel = this.documentTestService.searchById(id);

		if (documentTestModel == null) {
			LOGGER.info("Document test not found");
		} else {
			LOGGER.info("Document test found");
		}

		detailList = this.documentTestModel.getDocumentTestDetailModel();

		LOGGER.info("Filling test and test type list");
		List<TestModel> testModelList = this.testService.search();
		List<TestTypeModel> testTypeModelList = this.testTypeService.search();
		
		LOGGER.info("testModelList size: " + testModelList.size() + ", testTypeModelList size: "
				+ testTypeModelList.size());

		model.addAttribute("testModelList", testModelList);
		model.addAttribute("testTypeModelList", testTypeModelList);
		model.addAttribute("documentTestModel", documentTestModel);

		String jsp = "document-test/cruds/edit";
		return jsp;
	}

	@RequestMapping(value = "document-test/cruds/edit/detail-list")
	public String documentTestDetailList(Model model) {
		LOGGER.info("Showing document test detail list");

		LOGGER.info("Detail list size : " + detailList.size());

		model.addAttribute("documentTestDetailList", detailList);

		String jsp = "document-test/cruds/detail-list";
		return jsp;
	}

	@RequestMapping(value = "document-test/cruds/edit/add-question")
	public String documentTestAddQuestion(Model model) {
		LOGGER.info("Showing add test dialog");

		List<QuestionModel> questionList = this.questionService.search();
		
		LOGGER.info("Before question list size : " + questionList.size());
		for(DocumentTestDetailModel detailModel : detailList) {
			if(questionList.contains(detailModel.getQuestionId())) {
				questionList.remove(detailModel.getQuestionId());
			}
		}
		LOGGER.info("After question list size : " + questionList.size());
		
		model.addAttribute("questionModelList", questionList);

		String jsp = "document-test/cruds/add-question";

		return jsp;

	}

	@RequestMapping(value = "document-test/cruds/edit/save-question")
	public String documentTestSaveQuestion(HttpServletRequest request, Model model) {
		LOGGER.info("Adding question to document test detail");

		Integer questionId = Integer.valueOf(request.getParameter("questionId"));
		QuestionModel questionModel = this.questionService.searchId(questionId);

		DocumentTestDetailModel detailModel = new DocumentTestDetailModel();
		detailModel.setQuestionId(questionModel);
		detailModel.setCreatedBy(this.userSearch().getId());
		detailModel.setCreatedOn(new Date());
		detailModel.setDocumentTestId(documentTestModel);
		LOGGER.info("detailModel : " + detailModel);

		detailList.add(detailModel);

		String jsp = "document-test/cruds/edit/detail-list";
		return jsp;
	}

	@RequestMapping(value = "document-test/cruds/update")
	public String documentTestUpdate(DocumentTestModel documentTestAfter) {
		LOGGER.info("Updating document test");

		documentTestAfter.setId(this.documentTestModel.getId());
		documentTestAfter.setCreatedBy(this.documentTestModel.getCreatedBy());
		documentTestAfter.setCreatedOn(this.documentTestModel.getCreatedOn());
		documentTestAfter.setIsDelete(this.documentTestModel.getIsDelete());
		documentTestAfter.setModifiedBy(this.userSearch().getId());
		documentTestAfter.setModifiedOn(new Date());
		documentTestAfter.setDocumentTestDetailModel(detailList);

//		DocumentTestModel documentTestBefore = new DocumentTestModel();
//		BeanUtils.copyProperties(documentTestModel, documentTestBefore);
//
//		LOGGER.info(documentTestBefore);
//
//		this.documentTestModel.setTestId(documentTestAfter.getTestId());
//		this.documentTestModel.setTestTypeId(documentTestAfter.getTestTypeId());
//		this.documentTestModel.setVersion(documentTestAfter.getVersion());
//		this.documentTestModel.setToken(documentTestAfter.getToken());
//		this.documentTestModel.setModifiedBy(this.userSearch().getIdUser());
//		this.documentTestModel.setModifiedOn(new Date());
//		this.documentTestModel.setDocumentTestDetailModel(detailList);

		try {
			LOGGER.info("Begin updating");
			this.documentTestService.update(documentTestAfter);
			LOGGER.info("Done updating");
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.updateLog(this.documentTestModel, documentTestAfter);
		
		this.documentTestModel = null;
		detailList.clear();

		String jsp = "document-test/cruds/list";
		return jsp;
	}
	
	@RequestMapping(value = "document-test/cruds/edit/confirm")
	public String documentTestConfirmDeleteQuestion(HttpServletRequest request, Model model) {
		LOGGER.info("Showing confirmation dialog");

		int index = Integer.valueOf(request.getParameter("index"));
		
		model.addAttribute("id", index);

		String jsp = "document-test/cruds/delete";
		return jsp;
	}

	@RequestMapping(value = "document-test/cruds/edit/delete-question")
	public String documentTestDeleteQuestion(HttpServletRequest request) {
		LOGGER.info("Deleting question");

		LOGGER.info("Size before delete : " + detailList.size());
		int index = Integer.valueOf(request.getParameter("index"));
		detailList.remove(index);
		LOGGER.info("Size after delete : " + detailList.size());

		String jsp = "document-test/cruds/edit/detail-list";
		return jsp;
	}

	@RequestMapping(value = "document-test/cruds/copy-document")
	public String documentTestCopy(HttpServletRequest request, Model model) {
		LOGGER.info("Showing copy document dialog");
		Integer documentTestId = Integer.valueOf(request.getParameter("documentTestId"));
		this.documentTestModel = this.documentTestService.searchById(documentTestId);

		Integer newVersion = getVersion(this.documentTestModel.getTestTypeId());

		LOGGER.info("Filling test and test type list");
		List<TestModel> testModelList = this.testService.search();
		List<TestTypeModel> testTypeModelList = this.testTypeService.search();
		String newToken = randomStringGenerator(10);
		LOGGER.info("New Token: " + newToken);
		LOGGER.info("testModelList size: " + testModelList.size() + ", testTypeModelList size: "
				+ testTypeModelList.size());

		model.addAttribute("documentTestModel", this.documentTestModel);
		model.addAttribute("newVersion", newVersion);
		model.addAttribute("newToken", newToken);
		model.addAttribute("testModelList", testModelList);
		model.addAttribute("testTypeModelList", testTypeModelList);

		String jsp = "document-test/cruds/copy";
		return jsp;
	}

	@RequestMapping(value = "document-test/cruds/copy-document/save")
	public String documentTestSaveCopy(DocumentTestModel documentTestModelCopy) {
		LOGGER.info("Saving copy document");
		documentTestModelCopy.setCreatedBy(this.userSearch().getId());
		documentTestModelCopy.setCreatedOn(new Date());
		documentTestModelCopy.setIsDelete(false);
		detailList = new ArrayList<DocumentTestDetailModel>(this.documentTestModel.getDocumentTestDetailModel());
		detailList.stream().forEach(detail -> {
			detail.setId(null);
			detail.setDocumentTestId(documentTestModelCopy);
			detail.setCreatedOn(new Date());
			detail.setCreatedBy(this.userSearch().getId());
		});
		
		documentTestModelCopy
				.setDocumentTestDetailModel(detailList);		
		LOGGER.info("Copy Detail List Size: " + documentTestModelCopy.getDocumentTestDetailModel().size());
		
		try {
			LOGGER.info("Begin saving document copy");
			this.documentTestService.save(documentTestModelCopy);
			LOGGER.info("Done saving document copy");
			this.documentTestModel = null;
			detailList.clear();
		}
		catch(Exception e) {
			e.printStackTrace();
		}

		String jsp = "document-test/cruds/list";
		return jsp;
	}
	
	@RequestMapping(value="document-test/cruds/search")
	public String DocumentTestSearch(HttpServletRequest request, Model model) {
		LOGGER.info("Searching Data");
		String keyword = request.getParameter("keyword");
		List<DocumentTestModel> documentTestList = this.documentTestService.searchByTestTypeNameOrVersion(keyword);
		LOGGER.info("Document Test List Size : " + documentTestList.size());
		
		model.addAttribute("documentTestModelList", documentTestList);
		
		String jsp = "document-test/cruds/list";
		return jsp;
	}
	
	@RequestMapping(value="document-test/cruds/confirm")
	public String DocumentTestConfirm(HttpServletRequest request, Model model) {
		Integer id = Integer.valueOf(request.getParameter("id"));
		
		model.addAttribute("id", id);
		
		String jsp = "document-test/cruds/delete";
		return jsp;
	}
	
	@RequestMapping(value="document-test/cruds/delete")
	public String DocumentTestDelete(HttpServletRequest request) {
		LOGGER.info("Deleting Data");
		Integer id = Integer.valueOf(request.getParameter("id"));
		DocumentTestModel documentTestAfter = this.documentTestService.searchById(id);
		DocumentTestModel documentTestBefore = this.documentTestService.searchById(id);
		
		documentTestAfter.setIsDelete(true);
		documentTestAfter.setDeletedBy(this.userSearch().getId());
		documentTestAfter.setDeletedOn(new Date());		
		
		LOGGER.info(documentTestAfter);
		
		LOGGER.info("Updating Document Test Model");
		this.documentTestService.update(documentTestAfter);
		this.updateLog(documentTestBefore, documentTestAfter);
		LOGGER.info("Done Updating DocumentTest Model");
		
		String jsp = "document-test/cruds/list";
		return jsp;
	}
	
	@RequestMapping("document-test/cruds/view")
	public String DocumentTestView(HttpServletRequest request, Model model) {
		LOGGER.info("Showing View Test dialog");
		
		Integer id = Integer.valueOf(request.getParameter("id"));
		DocumentTestModel documentTest = this.documentTestService.searchById(id);
		
		List<QuestionModel> questionList = new ArrayList<>();
		
		for(DocumentTestDetailModel detail : documentTest.getDocumentTestDetailModel()) {
			questionList.add(detail.getQuestionId());
			LOGGER.info(detail.getQuestionId());
		}
		
		LOGGER.info("Question List Size : " + questionList.size());
		
		model.addAttribute("questionModelList", questionList);
		
		String jsp = "document-test/cruds/view";
		return jsp;
	}

	private String randomStringGenerator(int size) {
		LOGGER.info("Generating token");
		StringBuilder stringBuilder = new StringBuilder(size);
		boolean isAvailable = false;
		
		while(!isAvailable) {
			try {
				for (int i = 0; i < size; i++) {
					stringBuilder.append(ALPHANUMERIC.charAt(RANDOMIZER.nextInt(ALPHANUMERIC.length())));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			isAvailable = this.documentTestService.isAvailable(stringBuilder.toString());
		}
		
		return stringBuilder.toString();
	}

	private Integer getVersion(Integer testTypeId) {
		Integer version;
		List<DocumentTestModel> documentTestModelList = this.documentTestService.searchByTestTypeId(testTypeId);

		if (documentTestModelList == null || documentTestModelList.isEmpty()) {
			LOGGER.info("Test Type not found!");
			version = 1;
		} else {
			LOGGER.info("Test Type found!");
			version = documentTestModelList.get(0).getVersion() + 1;
		}

		return version;
	}
	
//	private List<QuestionModel> getQuestionList() {
//		List<QuestionModel> questionModelList = this.questionService.searchAll();
//		
//		if(detailList == null || detailList.isEmpty()) {
//			return questionModelList;
//		}
//		else {
//			for(DocumentTestDetailModel detail : detailList) {
//				QuestionModel question = detail.getQuestionId();
//				if(questionModelList.contains(question)) {
//					questionModelList.remove(question);
//				}
//				LOGGER.info("Question list size : " + questionModelList.size());
//			}
//			return questionModelList;
//		}
//	}
}
