package com.xsis.winners.xbc184.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.xsis.winners.xbc184.model.UserModel;
import com.xsis.winners.xbc184.service.RoleService;
import com.xsis.winners.xbc184.service.UserService;

@Controller
public class ForgetController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleService roleService;
	
//	@RequestMapping(value="/forget")
//	public ModelAndView forget() {
//		ModelAndView modelAndView = new ModelAndView();//model and view servlet
//		return modelAndView;
//	}
	
	@RequestMapping(value ="forget") //url action
	public String forget() { //method
		String jsp ="forget"; //target url atau halaman
		return jsp;
	}
	
	@RequestMapping(value ="forget/cruds/reset") 
	public String forgetCreate(HttpServletRequest request, Model model) { 
		String email	= request.getParameter("email");
		String password	= request.getParameter("password");
		String repassword = request.getParameter("repassword");
		
		// set nilai ke variabel modelnya
		List<UserModel> userModelList = new ArrayList<UserModel>();
		userModelList = this.userService.searchEmail(email);
		
		if (!password.equals(repassword)) {
			model.addAttribute("password", password);
			model.addAttribute("repassword", repassword);
		} else {
			for (int i = 0; i < userModelList.size(); i++) {
				userModelList.get(i).setPassword(password);
				this.userService.update(userModelList.get(i));
			}
		}
		
		
		//((UserModel) userModelList).setPassword(password);
		
//		Integer modifiedBy = this.userSearch().getId();
//		((UserModel) userModelList).setModifiedBy(modifiedBy);
//		((UserModel) userModelList).setModifiedOn(new Date());
		
		//this.userService.forgetPass(email);		

		String jsp = "forget";
		return jsp;
		
		
	}
//	public void aksesLogin(Model model) {
//		model.addAttribute("username", this.userSearch().getUsername());
//		model.addAttribute("name", this.userSearch().getRoleModel().getName());
//	}
}
