package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.MenuAccessDao;
import com.xsis.winners.xbc184.model.MenuAccessModel;
import com.xsis.winners.xbc184.model.MenuModel;

@Repository
public class MenuAccessDaoImpl implements MenuAccessDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(MenuAccessModel menuAccessModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(menuAccessModel);
	}

	@Override
	public void delete(MenuAccessModel menuAccessModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(menuAccessModel);
	}

	@Override
	public MenuAccessModel searchId(Integer id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		MenuAccessModel menuAccessModel = new MenuAccessModel();
		
		String query = " from MenuAccessModel where id="+id+" ";
		
		menuAccessModel = (MenuAccessModel) session.createQuery(query).uniqueResult();
		return menuAccessModel;
	}

	@Override
	public List<MenuAccessModel> searchByRoleId(Integer roleId) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<MenuAccessModel> menuAccessModelList = new ArrayList<MenuAccessModel>();
		
		String query = " from MenuAccessModel where roleId="+roleId+" ";
		
		menuAccessModelList = session.createQuery(query).list();
		return menuAccessModelList;
	}

	@Override
	public List<MenuAccessModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<MenuAccessModel> menuAccessModelList = new ArrayList<MenuAccessModel>();
		
		String query = " from MenuAccessModel ";
		
		menuAccessModelList = session.createQuery(query).list();
		return menuAccessModelList;
	}
	
	
}
