package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.BiodataModel;

public interface BiodataDao {
	
	// Yang ada di BiodataService harus ada di BiodataDao
	public void create(BiodataModel biodataModel);
	public void update(BiodataModel biodataModel);
	public void delete(BiodataModel biodataModel);
	public List<BiodataModel> search();
	public BiodataModel searchId(Integer id);
	public List<BiodataModel> searchNameOrMajors(String name, String majors);
//	public List<BiodataModel> searchNama(String namaBiodata);
//	public List<BiodataModel> searchKode(String kodeBiodata);
//	public List<BiodataModel> searchKodeOrNama(String kodeBiodata, String namaBiodata);
//	public List<BiodataModel> searchKodeEqual(String kodeBiodata);
//	public List<BiodataModel> searchNamaEqual(String namaBiodata, Integer isDelete);
	public List<BiodataModel> searchBiodataNotInMonitoring();
}
