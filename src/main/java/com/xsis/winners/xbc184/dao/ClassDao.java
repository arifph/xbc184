package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.BiodataModel;
import com.xsis.winners.xbc184.model.ClassModel;

public interface ClassDao {
	
	// Yang ada di ClassService harus ada di ClassDao
	public void create(ClassModel classModel);
	public void update(ClassModel classModel);
	public void delete(ClassModel classModel);
	public List<ClassModel> search();
	public ClassModel searchId(Integer id);
	public ClassModel searchBiodata(Integer batchId, Integer biodataId);
	public List<ClassModel> searchBatchOrTechnology(String batch, String technology);
//	public List<ClassModel> searchNama(String namaClass);
//	public List<ClassModel> searchKode(String kodeClass);
//	public List<ClassModel> searchKodeOrNama(String kodeClass, String namaClass);
//	public List<ClassModel> searchKodeEqual(String kodeClass);
//	public List<ClassModel> searchNamaEqual(String namaClass, Integer isDelete);
	public List<BiodataModel> unchosenPeople(Integer batchId);
}
