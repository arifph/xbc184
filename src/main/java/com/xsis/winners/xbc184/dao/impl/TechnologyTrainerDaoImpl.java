package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.TechnologyTrainerDao;
import com.xsis.winners.xbc184.model.TechnologyTrainerModel;

@Repository
public class TechnologyTrainerDaoImpl implements TechnologyTrainerDao{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void create(TechnologyTrainerModel technologyTrainerModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(technologyTrainerModel);
	}

	@Override
	public void delete(TechnologyTrainerModel technologyTrainerModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(technologyTrainerModel);
	}
	
	@Override
	public void deleteList(List<TechnologyTrainerModel> deleteTechnologyTrainerModelList) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(deleteTechnologyTrainerModelList);
	}

	@Override
	public List<TechnologyTrainerModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String query =" from TechnologyTrainerModel";
		List<TechnologyTrainerModel> technologyTrainerModelList = new ArrayList<TechnologyTrainerModel>();
		technologyTrainerModelList = session.createQuery(query).list();
		return technologyTrainerModelList;
	}

	@Override
	public List<TechnologyTrainerModel> searchIdTrainer(Integer id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String query =" from TechnologyTrainerModel t where t.technologyModel.id='"+id+"'";
		List<TechnologyTrainerModel> technologyTrainerModelList = new ArrayList<TechnologyTrainerModel>();
		technologyTrainerModelList = session.createQuery(query).list();
		return technologyTrainerModelList;
	}

	@Override
	public TechnologyTrainerModel searchId(Integer id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String query = " from TechnologyTrainerModel where id='"+ id +"'";
		TechnologyTrainerModel technologyTrainerModel = new TechnologyTrainerModel();
		technologyTrainerModel = (TechnologyTrainerModel) session.createQuery(query).uniqueResult();
		return technologyTrainerModel;
	}

	@Override
	public void update(TechnologyTrainerModel technologyTrainerModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(technologyTrainerModel);
	}

}
