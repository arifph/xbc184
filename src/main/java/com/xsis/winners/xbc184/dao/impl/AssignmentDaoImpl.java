package com.xsis.winners.xbc184.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.AssignmentDao;
import com.xsis.winners.xbc184.model.AssignmentModel;

@Repository
public class AssignmentDaoImpl implements AssignmentDao {

	@Autowired
	private SessionFactory sessionFactory;

	private static Logger logger = LogManager.getLogger(AssignmentDaoImpl.class);

	@Override
	public void saveAssignment(AssignmentModel assignmentModel) {
		// TODO Auto-generated method stub
		logger.info("Saving assignment model to database");

		Session session = sessionFactory.getCurrentSession();
		session.save(assignmentModel);

		logger.info("assignment saved");
	}

	@Override
	public void updateAssignment(AssignmentModel assignmentModel) {
		// TODO Auto-generated method stub
		logger.info("Updating assignment model to database");

		Session session = sessionFactory.getCurrentSession();
		session.update(assignmentModel);

		logger.info("assignment updated");
	}

	@Override
	public void deleteAssignment(AssignmentModel assignmentModel) {
		// TODO Auto-generated method stub
		logger.info("Deleting assignment model to database");

		Session session = sessionFactory.getCurrentSession();
		session.delete(assignmentModel);

		logger.info("assignment deleted");
	}

	@Override
	public List<AssignmentModel> search() {
		// TODO Auto-generated method stub
		logger.info("Searching all assignment from database");

		Session session = sessionFactory.getCurrentSession();
		String query = "from AssignmentModel";

		return session.createQuery(query, AssignmentModel.class).getResultList();
	}

	@Override
	public AssignmentModel searchById(Integer id) {
		// TODO Auto-generated method stub
		logger.info("Searching assignment with id : " + id + " from database");

		Session session = sessionFactory.getCurrentSession();
		String query = "from AssignmentModel where id = " + id;

		return session.createQuery(query, AssignmentModel.class).uniqueResult();
	}

	@Override
	public List<AssignmentModel> search(Boolean isDelete) {
		// TODO Auto-generated method stub
		logger.info("Searching all assignment from database");

		Session session = sessionFactory.getCurrentSession();
		String query = "from AssignmentModel where isDelete = " + isDelete;

		return session.createQuery(query, AssignmentModel.class).getResultList();
	}

	@Override
	public List<AssignmentModel> search(Date searchDate, Boolean isDelete) {
		// TODO Auto-generated method stub
		logger.info("Searching date of assignment from database");

		Session session = sessionFactory.getCurrentSession();
		String query = "from AssignmentModel where startDate = :searchDate and isDelete = " + isDelete;

		List<AssignmentModel> assignmentModelList = session.createQuery(query, AssignmentModel.class)
				.setParameter("searchDate", searchDate).getResultList();
		
		logger.info("Assignment List Size: " + assignmentModelList.size());

		return assignmentModelList;
	}

}
