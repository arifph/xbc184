package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.BootcampTypeDao;
import com.xsis.winners.xbc184.model.BootcampTypeModel;

@Repository
public class BootcampTypeDaoImpl implements BootcampTypeDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(BootcampTypeModel bootcampTypeModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(bootcampTypeModel);
		
	}

	@Override
	public void update(BootcampTypeModel bootcampTypeModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(bootcampTypeModel);
	}

	@Override
	public void delete(BootcampTypeModel bootcampTypeModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(bootcampTypeModel);
	}

	@Override
	public List<BootcampTypeModel> search(String kodeRole) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String query = " from BootcampTypeModel ";
		String kondisi = "";
		if (kodeRole.equals("ROLE_ADMIN")) {
			kondisi = "";
		} else {
			kondisi = " where isDelete !=1";
		}
		List<BootcampTypeModel> bootcampTypeModelList = new ArrayList<BootcampTypeModel>();
		bootcampTypeModelList = session.createQuery(query+kondisi).list();
		
		return bootcampTypeModelList;
	}

	@Override
	public BootcampTypeModel searchId(Integer idBootcampType) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		BootcampTypeModel bootcampTypeModel = new BootcampTypeModel();
		String query = " from BootcampTypeModel where idBootcampType = "+idBootcampType+" ";
		bootcampTypeModel = (BootcampTypeModel) session.createQuery(query).uniqueResult();
		return bootcampTypeModel;
	}

	@Override
	public List<BootcampTypeModel> searchNama(String nameBootcampType) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<BootcampTypeModel> bootcampTypeModelList = new ArrayList<BootcampTypeModel>();
		
		String query = " from BootcampTypeModel where upper(nameBootcampType) like '%"+nameBootcampType.toUpperCase()+"%' ";
		bootcampTypeModelList = session.createQuery(query).list();
		
		return bootcampTypeModelList;
	}

	@Override
	public List<BootcampTypeModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<BootcampTypeModel> bootcampTypeModelList = new ArrayList<BootcampTypeModel>();
		String query = " from BootcampTypeModel ";
		String kondisi = " where isDelete !=1";
		bootcampTypeModelList = session.createQuery(query+kondisi).list();
		
		return bootcampTypeModelList;
	}

	

}
