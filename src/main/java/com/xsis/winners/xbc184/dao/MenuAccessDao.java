package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.MenuAccessModel;

public interface MenuAccessDao {

	public void create(MenuAccessModel menuAccessModel);
	public void delete(MenuAccessModel menuAccessModel);
	public MenuAccessModel searchId(Integer id);
	public List<MenuAccessModel> searchByRoleId(Integer roleId);
	public List<MenuAccessModel> search();
}
