package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.UserDao;
import com.xsis.winners.xbc184.model.UserModel;

@Repository
public class UserDaoImpl implements UserDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public UserModel searchUsernamePassword(String username, String password) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //1.syntax start query
		
		UserModel userModel = new UserModel(); //2.instance return
		String query = " from UserModel "
				+ " where upper(username)= upper('"+username+"') "
				+ " and password= '"+password+"' ";
				//+ " and isDelete = '0' ";  
		userModel = (UserModel) session.createQuery(query).uniqueResult(); //<< isinya satu pake unique result
		
		return userModel;
	}

	@Override
	public void create(UserModel userModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(userModel);
	}

	@Override
	public void update(UserModel userModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(userModel);
	}

	@Override
	public void delete(UserModel userModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(userModel);
	}

	@Override
	public List<UserModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<UserModel> userModelList = new ArrayList<UserModel>();
		
		String query = " from UserModel where isDelete !=1";
		
		userModelList = session.createQuery(query).list();
		
		return userModelList;
	}

	@Override
	public UserModel searchId(Integer id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		UserModel userModel = new UserModel();
		
		String query = " from UserModel where id="+id+" ";
		
		userModel = (UserModel) session.createQuery(query).uniqueResult();
		
		return userModel;
	}

	@Override
	public List<UserModel> searchUsernameOrEmail(String username, String email) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<UserModel> userModelList = new ArrayList<UserModel>();
		
		String query = " from UserModel where upper(username) like upper('%"+username+"%') "
					+ " OR upper(email) like upper('%"+email+"%') ";
		
		userModelList = session.createQuery(query).list();
		
		return userModelList;
	}

	@Override
	public void deleteTemp(UserModel userModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(userModel);
		
	}

	@Override
	public List<UserModel> searchUsername(String username) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<UserModel> userModelList = new ArrayList<UserModel>();
		
		String query = " from UserModel where username = '"+username+"' ";
		
		userModelList = session.createQuery(query).list();
		
		return userModelList;
	}

	@Override
	public List<UserModel> searchEmail(String email) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<UserModel> userModelList = new ArrayList<UserModel>();
		
		String query = " from UserModel where email = '"+email+"' ";
		userModelList = session.createQuery(query).list();
		
		return userModelList;
	}

	@Override
	public void update(List<UserModel> userModelList) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(userModelList);
	}

	@Override
	public UserModel searchEmailX(String email) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		UserModel userModel = new UserModel();
		
		String query = " from UserModel where email='"+email+"' ";
		
		userModel = (UserModel) session.createQuery(query).uniqueResult();
		
		return userModel;
	}

	@Override
	public void forgetPass(String email) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		String query = " from UserModel where email='"+email+"' ";
		session.update(query);
	}

	
	
}
