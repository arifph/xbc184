package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.BatchTestDao;
import com.xsis.winners.xbc184.dao.TestDao;
//import com.xsis.winners.xbc184.model.BootcampTypeModel;
import com.xsis.winners.xbc184.model.BatchTestModel;
import com.xsis.winners.xbc184.model.TestModel;

@Repository
public class BatchTestDaoImpl implements BatchTestDao{

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(BatchTestModel batchTestModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(batchTestModel);
	}

//	@Override
//	public void update(BatchTestModel batchTestModel) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession();
//		session.update(batchTestModel);
//	}

	@Override
	public void delete(BatchTestModel batchTestModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(batchTestModel);
	}

	// Untuk mencari daftar Test yang merupakan test untuk bootcamp
	@Override
	public List<TestModel> searchBootcampTest() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
//		TestModel testModel = new TestModel();
		List<TestModel> testModelList = new ArrayList<TestModel>();
		String query = " from TestModel where isBootcampTest = 1 and isDelete != 1";
//		testModel = (TestModel) session.createQuery(query);
		testModelList = session.createQuery(query).list();
		
//		return testModel;
		return testModelList;
	}

//	@Override
//	public List<BatchTestModel> search() {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession();
//		List<BatchTestModel> testModelList = new ArrayList<BatchTestModel>();
//		String query = " from BatchTestModel ";
//		String kondisi = " where isDelete !=1 ";
//		testModelList = session.createQuery(query+kondisi).list();
//		
//		return testModelList;
//	}
//
//	@Override
//	public List<BatchTestModel> searchNama(String nameTest) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession();
//		List<BatchTestModel> testModelList = new ArrayList<BatchTestModel>();
//		String query = " from BatchTestModel where upper(nameTest) like '%"+nameTest.toUpperCase()+"%' ";
//		testModelList = session.createQuery(query).list();
//		
//		return testModelList;
//	}
//
//	@Override
//	public BatchTestModel searchId(Integer idTest) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession();
//		BatchTestModel batchTestModel = new BatchTestModel();
//		String query = " from BatchTestModel where idTest = "+idTest+" ";
//		batchTestModel = (BatchTestModel) session.createQuery(query).uniqueResult();
//		
//		return batchTestModel;
//	}
	
	@Override
	public BatchTestModel searchSetupTest(Integer batchId, Integer testId) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		BatchTestModel batchTestModel = new BatchTestModel();
		String query = " from BatchTestModel ";
		String kondisi = " where batchId = " + batchId + " and testId = " + testId + " ";
		batchTestModel = (BatchTestModel) session.createQuery(query+kondisi).uniqueResult();
		
		return batchTestModel;
	}
}
