package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.TrainerDao;
import com.xsis.winners.xbc184.model.TrainerModel;

@Repository
public class TrainerDaoImpl implements TrainerDao{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void create(TrainerModel trainerModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(trainerModel);
	}

	@Override
	public void update(TrainerModel trainerModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(trainerModel);
	}

	@Override
	public void delete(TrainerModel trainerModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(trainerModel);
	}

	@Override
	public List<TrainerModel> search(String kodeRole) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String query = " from TrainerModel";
		String kondisi = "";
		
		if(kodeRole.equals("ROLE_ADMIN")) {
			kondisi = "";
		}
		else {
			kondisi = " where isDelete != 1";
		}
		
		List<TrainerModel> trainerModelList = new ArrayList<TrainerModel>();
		trainerModelList = session.createQuery(query+kondisi).list();
		return trainerModelList;
	}

	@Override
	public TrainerModel searchId(Integer id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String query = " from TrainerModel where id ='"+ id +"'";
		TrainerModel trainerModel = new TrainerModel();
		trainerModel = (TrainerModel) session.createQuery(query).uniqueResult();
		return trainerModel;
	}

	@Override
	public List<TrainerModel> searchNama(String name) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String query = " from TrainerModel where lower(name) like lower('%"+ name +"%') and isDelete !=1";
		List<TrainerModel> trainerModelList = new ArrayList<TrainerModel>();
		trainerModelList = session.createQuery(query).list();
		return trainerModelList;
	}

	@Override
	public List<TrainerModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String query = " from TrainerModel where isDelete != 1";
		
		List<TrainerModel> trainerModelList = new ArrayList<TrainerModel>();
		trainerModelList = session.createQuery(query).list();
		return trainerModelList;
	}
	
}
