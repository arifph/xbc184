package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.BootcampTestTypeDao;
import com.xsis.winners.xbc184.model.BootcampTestTypeModel;

@Repository
public class BootcampTestTypeDaoImpl implements BootcampTestTypeDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(BootcampTestTypeModel bootcampTestTypeModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(bootcampTestTypeModel);
		
	}

	@Override
	public void update(BootcampTestTypeModel bootcampTestTypeModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(bootcampTestTypeModel);
	}

	@Override
	public void delete(BootcampTestTypeModel bootcampTestTypeModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(bootcampTestTypeModel);
	}

	@Override
	public List<BootcampTestTypeModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<BootcampTestTypeModel> bootcampTestTypeModelList = new ArrayList<BootcampTestTypeModel>();
		String query = " from BootcampTestTypeModel ";
		String kondisi = " where isDelete !=1 ";
		bootcampTestTypeModelList = session.createQuery(query+kondisi).list();
		
		return bootcampTestTypeModelList;
	}

	@Override
	public BootcampTestTypeModel searchId(Integer idbootcampTestType) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		BootcampTestTypeModel bootcampTestTypeModel = new BootcampTestTypeModel();
		String query = " from BootcampTestTypeModel where idBootcampTestType = "+idbootcampTestType+" ";
		bootcampTestTypeModel = (BootcampTestTypeModel) session.createQuery(query).uniqueResult();
		return bootcampTestTypeModel;
	}

	@Override
	public List<BootcampTestTypeModel> searchNama(String nameBootcampTestType) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<BootcampTestTypeModel> bootcampTestTypeModelList = new ArrayList<BootcampTestTypeModel>();
		
		String query = " from BootcampTestTypeModel where upper(nameBootcampTestType) like '%"+nameBootcampTestType.toUpperCase()+"%' ";
		bootcampTestTypeModelList = session.createQuery(query).list();
		
		return bootcampTestTypeModelList;
	}

	

}
