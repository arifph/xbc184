package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.BiodataDao;
import com.xsis.winners.xbc184.model.BiodataModel;

@Repository
public class BiodataDaoImpl implements BiodataDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(BiodataModel biodataModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(biodataModel); // .save itu fungsi untuk insert into
//		session.saveOrUpdate(biodataModel);
	}

	@Override
	public void update(BiodataModel biodataModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(biodataModel);
	}
	
//	@Override
//	public void updateByNama(String namaBiodata) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession();
//		//session.update(biodataModel);
//	}

	@Override
	public void delete(BiodataModel biodataModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(biodataModel);
	}

	@Override
	public List<BiodataModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
		String query = " from BiodataModel ";
		String kondisi = " where isDelete != 1";
		biodataModelList = session.createQuery(query+kondisi).list();
		
		return biodataModelList;
	}
	
//	@Override
//	public List<BiodataModel> search(String kodeRole) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession();
//		
//		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
//		String query = " from BiodataModel";
//		String kondisi = " where isDelete != 1";
//		
//		if (kodeRole.equals("ROLE_ADMIN")) {
//			kondisi = " ";
//		} else {
//			kondisi = " where isDelete != 1";
//		}
//		
//		biodataModelList = session.createQuery(query+kondisi).list();
//		
//		return biodataModelList;
//	}

	@Override
	public BiodataModel searchId(Integer id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		BiodataModel biodataModel = new BiodataModel();
		String query = " from BiodataModel where id =" + id + " ";
		biodataModel = (BiodataModel) session.createQuery(query).uniqueResult();
		
		return biodataModel;
	}

	@Override
	public List<BiodataModel> searchNameOrMajors(String name, String majors) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
		
		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>(); // instance return
		String query = " from BiodataModel where "
						+ "(upper(name) like upper('%" + name + "%') or "
						+ "upper(majors) like upper('%" + majors + "%')) and "
						+ "isDelete != 1";
		biodataModelList = session.createQuery(query).list();
		
		return biodataModelList;
	}

//	@Override
//	public List<BiodataModel> searchNama(String namaBiodata) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>(); // instance return
//		String query = " from BiodataModel where "
//				+ "upper(namaBiodata) like upper('%" + namaBiodata + "%')";
//		biodataModelList = session.createQuery(query).list();
//		
//		return biodataModelList;
//	}
//
//	@Override
//	public List<BiodataModel> searchKode(String kodeBiodata) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>(); // instance return
//		String query = " from BiodataModel where "
//				+ "upper(kodeBiodata) like upper('%" + kodeBiodata + "%')";
//		biodataModelList = session.createQuery(query).list();
//		
//		return biodataModelList;
//	}
//	
//	@Override
//	public List<BiodataModel> searchKodeOrNama(String kodeBiodata, String namaBiodata) {
//		// // TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>(); // instance return
//		String query = " from BiodataModel "
//				+ "where upper(kodeBiodata) like upper('%" + kodeBiodata + "%') "
//				+ "or upper(namaBiodata) like upper('%" + namaBiodata + "%')";
//		biodataModelList = session.createQuery(query).list();
//		
//		return biodataModelList;
//	}
//
//	@Override
//	public List<BiodataModel> searchKodeEqual(String kodeBiodata) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>(); // instance return
//		String query = " from BiodataModel where "
//				+ "upper(kodeBiodata) = upper('" + kodeBiodata + "')";
//		biodataModelList = session.createQuery(query).list();
//		
//		return biodataModelList;
//	}

//	@Override
//	public List<BiodataModel> searchNamaEqual(String namaBiodata, Integer isDelete) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>(); // instance return
//		String query = " from BiodataModel where "
//					+ "upper(namaBiodata) = upper('" + namaBiodata + "')"
//					+ " and isDelete = " + isDelete + " ";
//		biodataModelList = session.createQuery(query).list();
//		
//		return biodataModelList;
//	}
	
	// Dari Ulfah
	public List<BiodataModel> searchBiodataNotInMonitoring(){
		Session session = this.sessionFactory.getCurrentSession();
		
		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
		String querys = "select biodata from BiodataModel biodata where biodata.id not in (select distinct monitoringModel.idBiodata "
				+ " from MonitoringModel monitoringModel where monitoringModel.isDelete = 0) and biodata.isDelete = 0";
//			biodataModelList = session.createNativeQuery(querys).list();
		biodataModelList = session.createQuery(querys).list();
		
		return biodataModelList;
	}
}
