package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.TestModel;
import com.xsis.winners.xbc184.model.TestTypeModel;

public interface TestTypeDao {

	public List<TestTypeModel> searchAll();
}
