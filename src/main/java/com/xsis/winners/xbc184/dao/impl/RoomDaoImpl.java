package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.RoomDao;
import com.xsis.winners.xbc184.model.RoomModel;

@Repository
public class RoomDaoImpl implements RoomDao{

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(RoomModel roomModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(roomModel);
	}

	@Override
	public void update(RoomModel roomModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(roomModel);
	}

	@Override
	public void delete(RoomModel roomModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(roomModel);
	}

	@Override
	public RoomModel searchId(Integer id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		RoomModel roomModel = new RoomModel();
		String query = " from RoomModel where id = "+ id+" ";
		roomModel = (RoomModel) session.createQuery(query).uniqueResult();
		
		return roomModel;
	}

	@Override
	public List<RoomModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<RoomModel> roomModelList = new ArrayList<RoomModel>();
		String query = " from RoomModel ";
		String kondisi = " where isDelete != 1 ";
		
		roomModelList = session.createQuery(query + kondisi).list();
		return roomModelList;
		
	}

	@Override
	public List<RoomModel> searchRoomInOffice(Integer id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<RoomModel> roomModelList = new ArrayList<RoomModel>();
		String query = " from RoomModel where officeId ='"+ id+"' and  isDelete != 1" ;
		roomModelList = session.createQuery(query).list();
		return roomModelList;
		
	}

	

	
	
}
