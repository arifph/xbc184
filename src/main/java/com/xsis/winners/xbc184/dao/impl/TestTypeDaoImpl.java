package com.xsis.winners.xbc184.dao.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.xsis.winners.xbc184.dao.TestDao;
import com.xsis.winners.xbc184.dao.TestTypeDao;
import com.xsis.winners.xbc184.model.TestModel;
import com.xsis.winners.xbc184.model.TestTypeModel;

@Repository
public class TestTypeDaoImpl implements TestTypeDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<TestTypeModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String query = "from TestTypeModel";
		return session.createQuery(query, TestTypeModel.class).list();
	}
}
