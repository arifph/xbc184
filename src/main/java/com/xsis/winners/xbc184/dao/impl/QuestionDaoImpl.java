package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.QuestionDao;
import com.xsis.winners.xbc184.model.QuestionModel;

@Repository
public class QuestionDaoImpl implements QuestionDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(QuestionModel questionModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(questionModel);
	}

	@Override
	public void update(QuestionModel questionModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(questionModel);
	}

	@Override
	public List<QuestionModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<QuestionModel> questionModelList = new ArrayList<QuestionModel>();
		String query = " from QuestionModel ";
		String kondisi = " where isDelete != 1";
		
		questionModelList = session.createQuery(query + kondisi).list();
		return questionModelList;
	}

	@Override
	public QuestionModel searchId(Integer idQuestion) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		QuestionModel questionModel = new QuestionModel();
		String query = " from QuestionModel where idQuestion= "+idQuestion+" ";
		
		questionModel = (QuestionModel) session.createQuery(query).uniqueResult();
		return questionModel;
	}

	@Override
	public List<QuestionModel> searchQuestion(String question) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<QuestionModel> questionModelList = new ArrayList<QuestionModel>();
		String query = " from QuestionModel where upper(question) like '%"+question.toUpperCase()+"%' ";
		String kondisi = " and isDelete != 1";
		
		questionModelList = session.createQuery(query + kondisi).list();
		
		return questionModelList;
	}

}
