package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.BatchModel;

public interface BatchDao {
	
	// Yang ada di BatchService harus ada di BatchDao
	public void create(BatchModel batchModel);
	public void update(BatchModel batchModel);
	public void delete(BatchModel batchModel);
	public List<BatchModel> search();
	public BatchModel searchId(Integer id);
	public List<BatchModel> searchTechnologyOrName(String technology, String name);
//	public List<BatchModel> searchNama(String namaBatch);
//	public List<BatchModel> searchKode(String kodeBatch);
//	public List<BatchModel> searchKodeOrNama(String kodeBatch, String namaBatch);
//	public List<BatchModel> searchKodeEqual(String kodeBatch);
//	public List<BatchModel> searchNamaEqual(String namaBatch, Integer isDelete);
}
