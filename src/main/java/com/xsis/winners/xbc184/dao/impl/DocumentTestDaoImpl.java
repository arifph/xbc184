package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.DocumentTestDao;
import com.xsis.winners.xbc184.model.DocumentTestModel;

@Repository
public class DocumentTestDaoImpl implements DocumentTestDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void save(DocumentTestModel documentTest) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(documentTest);
	}

	@Override
	public void update(DocumentTestModel documentTest) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(documentTest);
	}

	@Override
	public void delete(DocumentTestModel documentTest) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(documentTest);
	}

	@Override
	public DocumentTestModel searchById(Integer id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String query = "from DocumentTestModel where id = " + id;
		return session.createQuery(query, DocumentTestModel.class).uniqueResult();
	}

	@Override
	public List<DocumentTestModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String query = "from DocumentTestModel where isDelete = 0";
		return session.createQuery(query, DocumentTestModel.class).list();
	}

	@Override
	public List<DocumentTestModel> searchByTestTypeId(Integer testTypeId) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String query = "from DocumentTestModel where testTypeId = " + testTypeId + " order by version desc";
		return session.createQuery(query, DocumentTestModel.class).list();
	}

	@Override
	public List<DocumentTestModel> searchByTestTypeNameOrVersion(String keyword) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		Integer version;
		try {
			version = Integer.valueOf(keyword);
		} catch (Exception e) {
			version = 0;
		}
		String query = "select documentTest from DocumentTestModel documentTest join documentTest.testType testType where (documentTest.version = "
				+ version + " or lower(testType.name) like '%" + keyword.toLowerCase()
				+ "%') and documentTest.isDelete = 0";

		List<DocumentTestModel> documentTestModelList = new ArrayList<DocumentTestModel>();
		try {
			documentTestModelList = session.createQuery(query, DocumentTestModel.class).getResultList();
			System.out.println("DocumentTestModelList.size() = " + documentTestModelList.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return documentTestModelList;
	}

	@Override
	public Boolean isAvailable(String token) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String query = "from DocumentTestModel where token = '" + token + "'";
		DocumentTestModel documentTestModel = session.createQuery(query, DocumentTestModel.class).uniqueResult();
		
		return documentTestModel == null ? true : false;
	}

}
