package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.BatchDao;
import com.xsis.winners.xbc184.model.BatchModel;

@Repository
public class BatchDaoImpl implements BatchDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(BatchModel batchModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(batchModel); // .save itu fungsi untuk insert into
//		session.saveOrUpdate(batchModel);
	}

	@Override
	public void update(BatchModel batchModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(batchModel);
	}
	
//	@Override
//	public void updateByNama(String namaBatch) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession();
//		//session.update(batchModel);
//	}

	@Override
	public void delete(BatchModel batchModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(batchModel);
	}

	@Override
	public List<BatchModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<BatchModel> batchModelList = new ArrayList<BatchModel>();
		String query = " from BatchModel ";
		String kondisi = " where isDelete != 1";
		batchModelList = session.createQuery(query+kondisi).list();
		
		return batchModelList;
	}
	
//	@Override
//	public List<BatchModel> search(String kodeRole) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession();
//		
//		List<BatchModel> batchModelList = new ArrayList<BatchModel>();
//		String query = " from BatchModel";
//		String kondisi = " where isDelete != 1";
//		
//		if (kodeRole.equals("ROLE_ADMIN")) {
//			kondisi = " ";
//		} else {
//			kondisi = " where isDelete != 1";
//		}
//		
//		batchModelList = session.createQuery(query+kondisi).list();
//		
//		return batchModelList;
//	}

	@Override
	public BatchModel searchId(Integer id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		BatchModel batchModel = new BatchModel();
		String query = " from BatchModel where id =" + id + " ";
		batchModel = (BatchModel) session.createQuery(query).uniqueResult();
		
		return batchModel;
	}

//	@Override
//	public List<BatchModel> searchTechnologyOrName(String technology, String name) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<BatchModel> batchModelList = new ArrayList<BatchModel>(); // instance return
//		String query = "select BatchModel, TechnologyModel from BatchModel left join TechnologyModel "
//				+ "on (BatchModel.technologyId = TechnologyModel.id) where "
//						+ "((upper(TechnologyModel.name) like upper('%" + technology + "%') or "
//						+ "upper(BatchModel.name) like upper('%" + name + "%')) and "
//						+ "BatchModel.isDelete != 1)";
//		batchModelList = session.createQuery(query).list();
//		
//		return batchModelList;
//	}
	
//	@Override
//	public List<BatchModel> searchTechnologyOrName(String technology, String name) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<BatchModel> batchModelList = new ArrayList<BatchModel>(); // instance return
//		String query = "select BatchModel, TechnologyModel from BatchModel, TechnologyModel "
//				+ "where BatchModel.technologyId = TechnologyModel.id and "
//						+ "((upper(TechnologyModel.name) like upper('%" + technology + "%') or "
//						+ "upper(BatchModel.name) like upper('%" + name + "%')) and "
//						+ "BatchModel.isDelete != 1)";
//		batchModelList = session.createQuery(query).list();
//		
//		return batchModelList;
//	}
	
//	@Override
//	public List<BatchModel> searchTechnologyOrName(String technology, String name) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<BatchModel> batchModelList = new ArrayList<BatchModel>(); // instance return
//		String query = "select BatchModel, BatchModel.technologyModel.name, BatchModel.trainerModel.name "
//				+ "from BatchModel left join BatchModel.technologyModel "
//				+ "on (BatchModel.technologyId = BatchModel.technologyModel.id) left join BatchModel.trainerModel "
//				+ "on (BatchModel.trainerId = BatchModel.trainerModel.id) where "
//						+ "((upper(BatchModel.technologyModel.name) like upper('%" + technology + "%') or "
//						+ "upper(BatchModel.name) like upper('%" + name + "%')) and "
//						+ "BatchModel.isDelete != 1)";
//		batchModelList = session.createQuery(query).list();
//		
//		return batchModelList;
//	}
	
//	@Override
//	public List<BatchModel> searchTechnologyOrName(String technology, String name) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<BatchModel> batchModelList = new ArrayList<BatchModel>(); // instance return
//		String query = "select BatchModel, BatchModel.technologyModel.name "
//						+ "from BatchModel left join BatchModel.technologyModel "
//						+ "on (BatchModel.technologyId = BatchModel.technologyModel.id) where "
//						+ "((upper(BatchModel.technologyModel.name) like upper('%" + technology + "%') or "
//						+ "upper(BatchModel.name) like upper('%" + name + "%')) and "
//						+ "BatchModel.isDelete != 1)";
//		batchModelList = session.createQuery(query).list();
//		
//		return batchModelList;
//	}
	
//	@Override
//	public List<BatchModel> searchTechnologyOrName(String technology, String name) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<BatchModel> batchModelList = new ArrayList<BatchModel>(); // instance return
//		String query = "Select BatchModel.id, BatchModel.name, BatchModel.technologyModel.name, BatchModel.trainerModel.name "
//						+ "from BatchModel inner join BatchModel.technologyModel "
//						+ "on BatchModel.technologyId = BatchModel.technologyModel.id "
//						+ "inner join BatchModel.trainerModel on BatchModel.trainerId = BatchModel.trainerModel.id "
//						+ " where (upper(BatchModel.technologyModel.name) like '%" + technology.toUpperCase() + "%' or "
//						+ "upper(BatchModel.name) like '%" + name.toUpperCase() + "%') and "
//						+ "BatchModel.isDelete != 1";
//		batchModelList = session.createQuery(query).list();
//		
//		return batchModelList;
//	}
	
//	@Override
//	public List<BatchModel> searchTechnologyOrName(String technology, String name) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<BatchModel> batchModelList = new ArrayList<BatchModel>(); // instance return
//		String query = "Select BatchModel, BatchModel.technologyModel, BatchModel.trainerModel "
//						+ "from BatchModel left join BatchModel.technologyModel "
//						+ "on BatchModel.technologyId = BatchModel.technologyModel.id "
//						+ "left join BatchModel.trainerModel on BatchModel.trainerId = BatchModel.trainerModel.id "
//						+ " where (upper(BatchModel.technologyModel.name) like '%" + technology.toUpperCase() + "%' or "
//						+ "upper(BatchModel.name) like '%" + name.toUpperCase() + "%') and "
//						+ "BatchModel.isDelete != 1";
//		batchModelList = session.createQuery(query).list();
//		
//		return batchModelList;
//	}
	
	@Override
	public List<BatchModel> searchTechnologyOrName(String technology, String name) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
		
		List<BatchModel> batchModelList = new ArrayList<BatchModel>(); // instance return
//		String query = " from BatchModel left join BatchModel.technologyModel "
//						+ "on BatchModel.technologyId = BatchModel.technologyModel.id "
//						+ "left join BatchModel.trainerModel on BatchModel.trainerId = BatchModel.trainerModel.id "
//						+ " where (upper(BatchModel.technologyModel.name) like '%" + technology.toUpperCase() + "%' or "
//						+ "upper(BatchModel.name) like '%" + name.toUpperCase() + "%') and "
//						+ "BatchModel.isDelete != 1";
		String query = "Select b from BatchModel b join b.technologyModel t "
						+ "where (upper(t.name) like '%" + technology.toUpperCase() + "%' or "
						+ "upper(b.name) like '%" + name.toUpperCase() + "%') and "
						+ "b.isDelete != 1";
		batchModelList = session.createQuery(query).list();
		
		return batchModelList;
	}

//	@Override
//	public List<BatchModel> searchNama(String namaBatch) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<BatchModel> batchModelList = new ArrayList<BatchModel>(); // instance return
//		String query = " from BatchModel where "
//				+ "upper(namaBatch) like upper('%" + namaBatch + "%')";
//		batchModelList = session.createQuery(query).list();
//		
//		return batchModelList;
//	}
//
//	@Override
//	public List<BatchModel> searchKode(String kodeBatch) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<BatchModel> batchModelList = new ArrayList<BatchModel>(); // instance return
//		String query = " from BatchModel where "
//				+ "upper(kodeBatch) like upper('%" + kodeBatch + "%')";
//		batchModelList = session.createQuery(query).list();
//		
//		return batchModelList;
//	}
//	
//	@Override
//	public List<BatchModel> searchKodeOrNama(String kodeBatch, String namaBatch) {
//		// // TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<BatchModel> batchModelList = new ArrayList<BatchModel>(); // instance return
//		String query = " from BatchModel "
//				+ "where upper(kodeBatch) like upper('%" + kodeBatch + "%') "
//				+ "or upper(namaBatch) like upper('%" + namaBatch + "%')";
//		batchModelList = session.createQuery(query).list();
//		
//		return batchModelList;
//	}
//
//	@Override
//	public List<BatchModel> searchKodeEqual(String kodeBatch) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<BatchModel> batchModelList = new ArrayList<BatchModel>(); // instance return
//		String query = " from BatchModel where "
//				+ "upper(kodeBatch) = upper('" + kodeBatch + "')";
//		batchModelList = session.createQuery(query).list();
//		
//		return batchModelList;
//	}

//	@Override
//	public List<BatchModel> searchNamaEqual(String namaBatch, Integer isDelete) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<BatchModel> batchModelList = new ArrayList<BatchModel>(); // instance return
//		String query = " from BatchModel where "
//					+ "upper(namaBatch) = upper('" + namaBatch + "')"
//					+ " and isDelete = " + isDelete + " ";
//		batchModelList = session.createQuery(query).list();
//		
//		return batchModelList;
//	}
}
