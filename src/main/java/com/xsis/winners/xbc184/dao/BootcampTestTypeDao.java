package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.BootcampTestTypeModel;

public interface BootcampTestTypeDao {

	public void create(BootcampTestTypeModel bootcampTestTypeModel);
	public void update(BootcampTestTypeModel bootcampTestTypeModel);
	public void delete(BootcampTestTypeModel bootcampTestTypeModel);
	public List<BootcampTestTypeModel> search();
	public BootcampTestTypeModel searchId(Integer idBootcampTestType);
	public List<BootcampTestTypeModel> searchNama(String namBootcampTestType);
}
