package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.BatchTestModel;
import com.xsis.winners.xbc184.model.TestModel;

public interface BatchTestDao {

	public void create(BatchTestModel batchTestModel);
//	public void update(BatchTestModel batchTestModel);
	public void delete(BatchTestModel batchTestModel);
//	public List<BatchTestModel> search();
//	public List<BatchTestModel> searchNama(String name);
//	public BatchTestModel searchId(Integer id);
	public List<TestModel> searchBootcampTest();
	public BatchTestModel searchSetupTest(Integer batchId, Integer testId);
}
