package com.xsis.winners.xbc184.dao;

import java.util.Date;
import java.util.List;

import com.xsis.winners.xbc184.model.AssignmentModel;

public interface AssignmentDao {

	public void saveAssignment(AssignmentModel assignmentModel);
	public void updateAssignment(AssignmentModel assignmentModel);
	public void deleteAssignment(AssignmentModel assignmentModel);
	public List<AssignmentModel> search();
	public List<AssignmentModel> search(Boolean isDelete);
	public List<AssignmentModel> search(Date searchDate, Boolean isDelete);
	public AssignmentModel searchById(Integer id);
}
