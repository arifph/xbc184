package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.BootcampTypeModel;

public interface BootcampTypeDao {

	public void create(BootcampTypeModel bootcampTypeModel);
	public void update(BootcampTypeModel bootcampTypeModel);
	public void delete(BootcampTypeModel bootcampTypeModel);
	public List<BootcampTypeModel> search(String kodeRole);
	public List<BootcampTypeModel> search();
	public BootcampTypeModel searchId(Integer idBootcampType);
	public List<BootcampTypeModel> searchNama(String nameBootcampType);
}
