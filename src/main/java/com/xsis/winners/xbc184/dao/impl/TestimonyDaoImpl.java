package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.TestimonyDao;
import com.xsis.winners.xbc184.model.OfficeModel;
import com.xsis.winners.xbc184.model.TestimonyModel;

@Repository
public class TestimonyDaoImpl implements TestimonyDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(TestimonyModel testimonyModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(testimonyModel);
	}

	@Override
	public List<TestimonyModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<TestimonyModel> testimonyModelList = new ArrayList<TestimonyModel>();
		String query = " from TestimonyModel ";
		String kondisi = " where isDelete !=1 ";
		
		testimonyModelList = session.createQuery(query + kondisi).list();
		return testimonyModelList;
	}

	@Override
	public void update(TestimonyModel testimonyModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(testimonyModel);
	}

	@Override
	public void delete(TestimonyModel testimonyModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(testimonyModel);
	}

	@Override
	public TestimonyModel searchId(Integer id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		TestimonyModel testimonyModel = new TestimonyModel();
		String query = " from TestimonyModel where id = "+ id+" ";
		testimonyModel = (TestimonyModel) session.createQuery(query).uniqueResult();
		
		return testimonyModel;
	}

	@Override
	public List<TestimonyModel> searchTitle(String title) {
		// TODO Auto-generated method stub
		
		Session session = this.sessionFactory.getCurrentSession(); //sintaks u/ memulai membuat query
		List<TestimonyModel> testimonyModelList = new ArrayList<TestimonyModel>(); //instance return
		String query=" from TestimonyModel where upper(title) like upper('%"+title+"%') "; //karena string pakai petik
		//pilih semua dari tabel Office dimana colom nama fakultasnya mengandung kata kunci (pake hql)
		
		//bikin hibernate
		testimonyModelList = session.createQuery(query).list();
		
		return testimonyModelList;
		
		
	}
	
	
	
	
}
