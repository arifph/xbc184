package com.xsis.winners.xbc184.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.CategoryDao;
import com.xsis.winners.xbc184.model.CategoryModel;

@Repository
public class CategoryDaoImpl implements CategoryDao {

	@Autowired
	private SessionFactory sessionFactory;

	private static Logger logger = LogManager.getLogger(CategoryDaoImpl.class);

	@Override
	public void saveCategory(CategoryModel categoryModel) {
		// TODO Auto-generated method stub
		logger.info("Saving category model to database");

		Session session = sessionFactory.getCurrentSession();
		session.save(categoryModel);

		logger.info("category saved");
	}

	@Override
	public void updateCategory(CategoryModel categoryModel) {
		// TODO Auto-generated method stub
		logger.info("Updating category model to database");

		Session session = sessionFactory.getCurrentSession();
		session.update(categoryModel);

		logger.info("category updated");
	}

	@Override
	public void deleteCategory(CategoryModel categoryModel) {
		// TODO Auto-generated method stub
		logger.info("Deleting category model to database");

		Session session = sessionFactory.getCurrentSession();
		session.delete(categoryModel);

		logger.info("category deleted");
	}

	@Override
	public List<CategoryModel> search() {
		// TODO Auto-generated method stub
		logger.info("Searching all category from database");

		Session session = sessionFactory.getCurrentSession();
		String query = "from CategoryModel";

		return session.createQuery(query, CategoryModel.class).getResultList();
	}

	@Override
	public CategoryModel searchById(Integer id) {
		// TODO Auto-generated method stub
		logger.info("Searching category with id : " + id + " from database");

		Session session = sessionFactory.getCurrentSession();
		String query = "from CategoryModel where id = " + id;

		return session.createQuery(query, CategoryModel.class).uniqueResult();
	}

	@Override
	public List<CategoryModel> search(Boolean isDelete) {
		// TODO Auto-generated method stub
		logger.info("Searching all category from database");

		Session session = sessionFactory.getCurrentSession();
		String query = "from CategoryModel where isDelete = " + isDelete;

		return session.createQuery(query, CategoryModel.class).getResultList();
	}

	@Override
	public List<CategoryModel> search(String code, String name, Boolean isDelete) {
		// TODO Auto-generated method stub
		logger.info("Searching code and name of category from database");

		Session session = sessionFactory.getCurrentSession();
		String query = "from CategoryModel where (lower(code) like '%" + code.toLowerCase()
				+ "%' or lower(name) like '%" + name.toLowerCase() + "%') and isDelete = " + isDelete;

		return session.createQuery(query, CategoryModel.class).getResultList();
	}

}
