package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.IdleNewsDao;
import com.xsis.winners.xbc184.model.IdleNewsModel;

@Repository
public class IdleNewsDaoImpl implements IdleNewsDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(IdleNewsModel idleNewsModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(idleNewsModel);
		
	}

	@Override
	public void update(IdleNewsModel idleNewsModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(idleNewsModel);
	}

	@Override
	public void delete(IdleNewsModel idleNewsModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(idleNewsModel);
	}

	@Override
	public List<IdleNewsModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<IdleNewsModel> idleNewsModelList = new ArrayList<IdleNewsModel>();
		String query = " from IdleNewsModel ";
		String kondisi = " where isDelete !=1 ";
		idleNewsModelList = session.createQuery(query+kondisi).list();
		
		return idleNewsModelList;
	}

	@Override
	public IdleNewsModel searchId(Integer ididleNews) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		IdleNewsModel idleNewsModel = new IdleNewsModel();
		String query = " from IdleNewsModel where idIdleNews = "+ididleNews+" ";
		idleNewsModel = (IdleNewsModel) session.createQuery(query).uniqueResult();
		return idleNewsModel;
	}

	@Override
	public List<IdleNewsModel> searchNama(String nameIdleNews) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<IdleNewsModel> idleNewsModelList = new ArrayList<IdleNewsModel>();
		
		String query = " from IdleNewsModel where upper(tittleIdleNews) like '%"+nameIdleNews.toUpperCase()+"%' ";
		idleNewsModelList = session.createQuery(query).list();
		
		return idleNewsModelList;
	}

	

}
