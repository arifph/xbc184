package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.OfficeDao;
import com.xsis.winners.xbc184.model.OfficeModel;

@Repository
public class OfficeDaoImpl implements OfficeDao{

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(OfficeModel officeModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(officeModel);
	}

	@Override
	public List<OfficeModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<OfficeModel> officeModelList = new ArrayList<OfficeModel>();
		String query = " from OfficeModel ";
		String kondisi = " where isDelete != 1 ";
		
		officeModelList = session.createQuery(query + kondisi).list();
		return officeModelList;
	}

	@Override
	public void update(OfficeModel officeModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(officeModel);
	}

	@Override
	public void delete(OfficeModel officeModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(officeModel);
	}

	@Override
	public OfficeModel searchId(Integer id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		OfficeModel officeModel = new OfficeModel();
		String query = " from OfficeModel where id = "+ id+" ";
		officeModel = (OfficeModel) session.createQuery(query).uniqueResult();
		
		return officeModel;
	}

	@Override
	public List<OfficeModel> searchNama(String name) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //sintaks u/ memulai membuat query
		List<OfficeModel> officeModelList = new ArrayList<OfficeModel>(); //instance return
		String query=" from OfficeModel where upper(name) like upper('%"+name+"%') "; //karena string pakai petik
		//pilih semua dari tabel Office dimana colom nama fakultasnya mengandung kata kunci (pake hql)
		
		//bikin hibernate
		officeModelList = session.createQuery(query).list();
		
		return officeModelList;
	}


	@Override
	public List<OfficeModel> searchNameSpace() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String spasi=" ";
		List<OfficeModel> officeModelList = new ArrayList<OfficeModel>();
		String query = " from OfficeModel where name='"+ spasi +"'";
		
		officeModelList = session.createQuery(query).list();
		return officeModelList;
		
	}
	
	
}
