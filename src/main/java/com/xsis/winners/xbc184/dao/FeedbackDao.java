package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.FeedbackModel;

public interface FeedbackDao {

	public List<FeedbackModel> search();
	public FeedbackModel searchIdTest(Integer idTest);
	
}
