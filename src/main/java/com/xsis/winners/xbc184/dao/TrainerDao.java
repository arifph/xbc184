package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.TrainerModel;

public interface TrainerDao {

	public void create(TrainerModel trainerModel);
	public void update(TrainerModel trainerModel);
	public void delete(TrainerModel trainerModel);
	public List<TrainerModel> search();
	public List<TrainerModel> search(String kodeRole);
	public TrainerModel searchId(Integer id);
	public List<TrainerModel> searchNama(String name);
}
