package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.IdleNewsModel;

public interface IdleNewsDao {

	public void create(IdleNewsModel idleNewsModel);
	public void update(IdleNewsModel idleNewsModel);
	public void delete(IdleNewsModel idleNewsModel);
	public List<IdleNewsModel> search();
	public IdleNewsModel searchId(Integer idIdleNewsModel);
	public List<IdleNewsModel> searchNama(String namaIdleNewsModel);
}
