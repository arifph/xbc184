package com.xsis.winners.xbc184.dao;

import com.xsis.winners.xbc184.model.AuditLogModel;

public interface AuditLogDao {

	public void save(AuditLogModel auditLogModel);
	public void update(AuditLogModel auditLogModel);
	public void delete(AuditLogModel auditLogModel);
	public AuditLogModel searchAuditLog(String jsonInsert);
}
