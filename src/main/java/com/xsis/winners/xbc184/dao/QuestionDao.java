package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.QuestionModel;

public interface QuestionDao {

	public void create(QuestionModel questionModel);
	public void update(QuestionModel questionModel);
	public List<QuestionModel> search();
	public QuestionModel searchId(Integer idQuestion);
	public List<QuestionModel> searchQuestion(String question);
	
}
