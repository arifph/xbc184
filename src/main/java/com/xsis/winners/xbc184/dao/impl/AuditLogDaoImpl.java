package com.xsis.winners.xbc184.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.AuditLogDao;
import com.xsis.winners.xbc184.model.AuditLogModel;

@Repository
public class AuditLogDaoImpl implements AuditLogDao {

	@Autowired
	private SessionFactory SessionFactory;

	private static final Logger logger = LogManager.getFormatterLogger();

	@Override
	public void save(AuditLogModel auditLogModel) {
		// TODO Auto-generated method stub
		logger.info("Saving audit log model");
		Session session = SessionFactory.getCurrentSession();
		session.save(auditLogModel);
	}

	@Override
	public void update(AuditLogModel auditLogModel) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("This method isn't implemented yet");
	}

	@Override
	public void delete(AuditLogModel auditLogModel) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("This method isn't implemented yet");
	}

	@Override
	public AuditLogModel searchAuditLog(String json) {
		// TODO Auto-generated method stub
		Session session = SessionFactory.getCurrentSession();
		String query = "from AuditLogModel where jsonInsert = '" + json + "' or jsonUpdate = '" + json
				+ "' order by id desc";
		return session.createQuery(query, AuditLogModel.class).getResultList().get(0);
	}

}
