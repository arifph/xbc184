package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.RoleDao;
import com.xsis.winners.xbc184.model.RoleModel;

@Repository
public class RoleDaoImpl implements RoleDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(RoleModel roleModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(roleModel);
	}
	
	@Override
	public void update(RoleModel roleModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(roleModel);
	}
	
	@Override
	public List<RoleModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		String query = " from RoleModel ";
		String kondisi= " where isDelete != 1";
		
		roleModelList = session.createQuery(query + kondisi).list();
		return roleModelList;
	}

	@Override
	public RoleModel searchId(Integer idRole) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		RoleModel roleModel = new RoleModel();
		String query = " from RoleModel where idRole= "+idRole+" ";
		
		roleModel = (RoleModel) session.createQuery(query).uniqueResult();
		return roleModel;
	}

	@Override
	public List<RoleModel> searchNama(String namaRole) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		String query = " from RoleModel where upper(namaRole) like '%"+namaRole.toUpperCase()+"%' ";
		String kondisi = " and isDelete != 1";
		
		roleModelList = session.createQuery(query + kondisi).list(); // Hibernate
		
		return roleModelList;
	}

	
	@Override
	public List<RoleModel> searchNamaEqual(String namaRole) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		String query = " from RoleModel where namaRole = '"+namaRole+"' ";
		
		roleModelList = session.createQuery(query).list();
		return roleModelList;
	}

}
