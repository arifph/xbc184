package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.RoomModel;

public interface RoomDao {

	public void create(RoomModel roomModel);
	public void update(RoomModel roomModel);
	public void delete(RoomModel roomModel);
	
	public RoomModel searchId(Integer id);
	//kode diatas nampilin data berdasarkan id
	public List<RoomModel> search();
	public List<RoomModel> searchRoomInOffice(Integer id);

}
