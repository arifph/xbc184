package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.MonitoringModel;

public interface MonitoringDao {

	public void create(MonitoringModel monitoringModel);

	public void update(MonitoringModel monitoringModel);

	public void delete(MonitoringModel monitoringModel);

	public List<MonitoringModel> search();

	public List<MonitoringModel> searchNamaBiodata(String name);
	
	public MonitoringModel searchId(Integer idMonitoring);
	
	public List<MonitoringModel> searchIdEqual(Integer idBiodata);
}

