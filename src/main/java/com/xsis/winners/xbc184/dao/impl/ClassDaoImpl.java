package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.ClassDao;
import com.xsis.winners.xbc184.model.BiodataModel;
import com.xsis.winners.xbc184.model.ClassModel;

@Repository
public class ClassDaoImpl implements ClassDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(ClassModel classModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(classModel); // .save itu fungsi untuk insert into
//		session.saveOrUpdate(classModel);
	}

	@Override
	public void update(ClassModel classModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(classModel);
	}
	
//	@Override
//	public void updateByNama(String namaClass) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession();
//		//session.update(classModel);
//	}

	@Override
	public void delete(ClassModel classModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(classModel);
	}

	@Override
	public List<ClassModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<ClassModel> classModelList = new ArrayList<ClassModel>();
		String query = " from ClassModel ";
//		String kondisi = " where isDelete != 1";
//		classModelList = session.createQuery(query+kondisi).list();
		classModelList = session.createQuery(query).list();
		
		return classModelList;
	}
	
//	@Override
//	public List<ClassModel> search(String kodeRole) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession();
//		
//		List<ClassModel> classModelList = new ArrayList<ClassModel>();
//		String query = " from ClassModel";
//		String kondisi = " where isDelete != 1";
//		
//		if (kodeRole.equals("ROLE_ADMIN")) {
//			kondisi = " ";
//		} else {
//			kondisi = " where isDelete != 1";
//		}
//		
//		classModelList = session.createQuery(query+kondisi).list();
//		
//		return classModelList;
//	}

	@Override
	public ClassModel searchId(Integer id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		ClassModel classModel = new ClassModel();
		String query = " from ClassModel where id =" + id + " ";
		classModel = (ClassModel) session.createQuery(query).uniqueResult();
		
		return classModel;
	}
	
	@Override
	public ClassModel searchBiodata(Integer batchId, Integer biodataId) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		ClassModel classModel = new ClassModel();
		String query = " from ClassModel where biodataId =" + biodataId + " and batchId = " + batchId + " ";
		classModel = (ClassModel) session.createQuery(query).uniqueResult();
		
		return classModel;
	}

	@Override
	public List<ClassModel> searchBatchOrTechnology(String batch, String technology) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
		
		List<ClassModel> classModelList = new ArrayList<ClassModel>(); // instance return
//		String query = " from ClassModel where "
//						+ "(upper(batch) like upper('%" + batch + "%') or "
//						+ "upper(technology) like upper('%" + technology + "%'))";
		
		String query = "Select c from ClassModel c "
						+ "join c.batchModel b "
						+ "join b.technologyModel t "
						+ "where "
						+ "(upper(b.name) like '%" + batch.toUpperCase() + "%' "
						+ "or upper(t.name) like '%" + technology.toUpperCase() + "%') "
						+ "and "
						+ "(b.isDelete != 1 or t.isDelete != 1)";
		
//		String query = " from ClassModel c "
//				+ "join c.batchModel b "
//				+ "where "
//				+ "upper(b.name) like '%" + batch.toUpperCase() + "%' "
//				+ "and "
//				+ "b.isDelete != 1";
		classModelList = session.createQuery(query).list();
		
		return classModelList;
	}

//	@Override
//	public List<ClassModel> searchNama(String namaClass) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<ClassModel> classModelList = new ArrayList<ClassModel>(); // instance return
//		String query = " from ClassModel where "
//				+ "upper(namaClass) like upper('%" + namaClass + "%')";
//		classModelList = session.createQuery(query).list();
//		
//		return classModelList;
//	}
//
//	@Override
//	public List<ClassModel> searchKode(String kodeClass) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<ClassModel> classModelList = new ArrayList<ClassModel>(); // instance return
//		String query = " from ClassModel where "
//				+ "upper(kodeClass) like upper('%" + kodeClass + "%')";
//		classModelList = session.createQuery(query).list();
//		
//		return classModelList;
//	}
//	
//	@Override
//	public List<ClassModel> searchKodeOrNama(String kodeClass, String namaClass) {
//		// // TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<ClassModel> classModelList = new ArrayList<ClassModel>(); // instance return
//		String query = " from ClassModel "
//				+ "where upper(kodeClass) like upper('%" + kodeClass + "%') "
//				+ "or upper(namaClass) like upper('%" + namaClass + "%')";
//		classModelList = session.createQuery(query).list();
//		
//		return classModelList;
//	}
//
//	@Override
//	public List<ClassModel> searchKodeEqual(String kodeClass) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<ClassModel> classModelList = new ArrayList<ClassModel>(); // instance return
//		String query = " from ClassModel where "
//				+ "upper(kodeClass) = upper('" + kodeClass + "')";
//		classModelList = session.createQuery(query).list();
//		
//		return classModelList;
//	}

//	@Override
//	public List<ClassModel> searchNamaEqual(String namaClass, Integer isDelete) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
//		
//		List<ClassModel> classModelList = new ArrayList<ClassModel>(); // instance return
//		String query = " from ClassModel where "
//					+ "upper(namaClass) = upper('" + namaClass + "')"
//					+ " and isDelete = " + isDelete + " ";
//		classModelList = session.createQuery(query).list();
//		
//		return classModelList;
//	}
	
	// Mencoba menampilkan biodata yang belum dipilih di 'Add Participant' dan biodata yang sudah terpilih dari periode yang berbeda
	@Override
	public List<BiodataModel> unchosenPeople(Integer batchId) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<BiodataModel> unchosenList = new ArrayList<BiodataModel>();
		String query = "Select bio from BiodataModel bio where bio.id not in ("
						+ " Select distinct c.biodataId from ClassModel c where c.batchId in ("
							+ " Select bc.id from BatchModel bc "
							+ " where ("
								+ " bc.periodFrom >= (Select bc.periodFrom from BatchModel bc where bc.id = " + batchId + ") "
								+ " and bc.periodFrom <= (Select bc.periodTo from BatchModel bc where bc.id = " + batchId + ")) "
							+ " or "
								+ " (bc.periodTo >= (Select bc.periodFrom from BatchModel bc where bc.id = " + batchId + ") "
								+ " and bc.periodTo <= (Select bc.periodTo from BatchModel bc where bc.id = " + batchId + ")))) "
					+ " and bio.isDelete != 1";
		unchosenList = session.createQuery(query).list();
//		unchosenList = session.createNativeQuery(query).list();
		
		return unchosenList;
	}
}
