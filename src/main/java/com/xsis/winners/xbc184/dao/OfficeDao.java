package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.OfficeModel;

public interface OfficeDao {

	public void create(OfficeModel officeModel);
	public void update(OfficeModel officeModel);
	public void delete(OfficeModel officeModel);
	
	public OfficeModel searchId(Integer id);
	//kode diatas nampilin data berdasarkan id

	public List<OfficeModel> search();
	public List<OfficeModel> searchNama(String name);
	
	public List<OfficeModel> searchNameSpace();
	
}
