package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.TestDao;
import com.xsis.winners.xbc184.model.BootcampTypeModel;
import com.xsis.winners.xbc184.model.TestModel;

@Repository
public class TestDaoImpl implements TestDao{

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(TestModel testModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(testModel);
	}

	@Override
	public void update(TestModel testModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(testModel);
	}

	@Override
	public void delete(TestModel testModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(testModel);
	}

	@Override
	public List<TestModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<TestModel> testModelList = new ArrayList<TestModel>();
		String query = " from TestModel ";
		String kondisi = " where isDelete !=1 ";
		testModelList = session.createQuery(query+kondisi).list();
		
		return testModelList;
	}

	@Override
	public List<TestModel> searchNama(String nameTest) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<TestModel> testModelList = new ArrayList<TestModel>();
		String query = " from TestModel where upper(nameTest) like '%"+nameTest.toUpperCase()+"%' ";
		testModelList = session.createQuery(query).list();
		
		return testModelList;
	}

	@Override
	public TestModel searchId(Integer idTest) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		TestModel testModel = new TestModel();
		String query = " from TestModel where idTest = "+idTest+" ";
		testModel = (TestModel) session.createQuery(query).uniqueResult();
		
		return testModel;
	}

	
	
}
