package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.FeedbackDao;
import com.xsis.winners.xbc184.model.FeedbackModel;

@Repository
public class FeedbackDaoImpl implements FeedbackDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<FeedbackModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<FeedbackModel> feedbackModelList = new ArrayList<FeedbackModel>();
		String query = " SELECT feedback from FeedbackModel feedback "
				+ " JOIN feedback.testModel test"
				+ " WHERE test.isDelete != 1 AND test.isBootcampTest != 1";
		
		feedbackModelList = session.createQuery(query).list();
		return feedbackModelList;
	}

	@Override
	public FeedbackModel searchIdTest(Integer idTest) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		FeedbackModel feedbackModel = new FeedbackModel();
		String query = " SELECT feedback FROM FeedbackModel feedback "
				+ " JOIN feedback.testModel test"
				+ " WHERE test.idTest = "+idTest+" ";
		
		feedbackModel = (FeedbackModel) session.createQuery(query).list();
		return feedbackModel;
	}

}
