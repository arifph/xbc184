package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.MonitoringDao;
import com.xsis.winners.xbc184.model.MonitoringModel;

@Repository
public class MonitoringDaoImpl implements MonitoringDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(MonitoringModel monitoringModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(monitoringModel);
	}

	@Override
	public void update(MonitoringModel monitoringModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(monitoringModel);
	}

	@Override
	public void delete(MonitoringModel monitoringModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(monitoringModel);
	}

	@Override
	public List<MonitoringModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<MonitoringModel> monitoringModelList = new ArrayList<MonitoringModel>();
		String query = " from MonitoringModel ";
		String kondisi = " where isDelete !=1 ";
		monitoringModelList = session.createQuery(query+kondisi).list();
		
		return monitoringModelList;
	}

	@Override
	public List<MonitoringModel> searchNamaBiodata(String name) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<MonitoringModel> monitoringModelList = new ArrayList<MonitoringModel>();
		String query = " SELECT monitor FROM MonitoringModel monitor "
					 + " JOIN monitor.biodataModel biodata"
					 + " WHERE UPPER(biodata.name) like '%"+name.toUpperCase()+"%' ";
		monitoringModelList = session.createQuery(query).list();
		
		return monitoringModelList;
	}

	@Override
	public MonitoringModel searchId(Integer idMonitoring) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		MonitoringModel monitoringModel = new MonitoringModel();
		String query = " from MonitoringModel where idMonitoring = "+idMonitoring+" ";
		monitoringModel = (MonitoringModel) session.createQuery(query).uniqueResult();
		return monitoringModel;
	}

	@Override
	public List<MonitoringModel> searchIdEqual(Integer idBiodata) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<MonitoringModel> monitoringModelList = new ArrayList<MonitoringModel>();
		String query = " from MonitoringModel where idBiodata = '"+idBiodata+"' ";
		
		monitoringModelList = session.createQuery(query).list();
		return monitoringModelList;
	}


}
