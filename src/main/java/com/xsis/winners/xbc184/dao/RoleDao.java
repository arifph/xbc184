package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.RoleModel;

public interface RoleDao {

	public void create(RoleModel roleModel);
	public void update(RoleModel roleModel);
	public List<RoleModel> search();
	public RoleModel searchId(Integer idRole);
	public List<RoleModel> searchNama(String namaRole);
	public List<RoleModel> searchNamaEqual(String namaRole);
	
}
