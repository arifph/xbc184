package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.CategoryModel;

public interface CategoryDao {

	public void saveCategory(CategoryModel categoryModel);
	public void updateCategory(CategoryModel categoryModel);
	public void deleteCategory(CategoryModel categoryModel);
	public List<CategoryModel> search();
	public List<CategoryModel> search(Boolean isDelete);
	public List<CategoryModel> search(String code, String name, Boolean isDelete);
	public CategoryModel searchById(Integer id);
}
