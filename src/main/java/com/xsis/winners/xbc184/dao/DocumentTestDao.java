package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.DocumentTestModel;

public interface DocumentTestDao {

	public void save(DocumentTestModel documentTest);
	public void update(DocumentTestModel documentTest);
	public void delete(DocumentTestModel documentTest);
	public DocumentTestModel searchById(Integer id);
	public List<DocumentTestModel> searchAll();
	public List<DocumentTestModel> searchByTestTypeId(Integer testTypeId);
	public List<DocumentTestModel> searchByTestTypeNameOrVersion(String keyword);
	public Boolean isAvailable(String token);
}
