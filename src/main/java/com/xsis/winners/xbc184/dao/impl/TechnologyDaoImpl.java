package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.TechnologyDao;
import com.xsis.winners.xbc184.model.TechnologyModel;
import com.xsis.winners.xbc184.model.TrainerModel;

@Repository
public class TechnologyDaoImpl implements TechnologyDao{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void create(TechnologyModel technologyModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(technologyModel);
	}

	@Override
	public void update(TechnologyModel technologyModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(technologyModel);
	}

	@Override
	public void delete(TechnologyModel technologyModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(technologyModel);
	}

	@Override
	public List<TechnologyModel> search(String kodeRole) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<TechnologyModel> technologyModelList = new ArrayList<TechnologyModel>();
		String query = " from TechnologyModel";
		String kondisi = "";
		
		if(kodeRole.equals("ROLE_ADMIN")) {
			kondisi = "";
		}
		else {
			kondisi = " where isDelete != 1";
		}
		technologyModelList = session.createQuery(query+kondisi).list();
		return technologyModelList;
	}

	@Override
	public TechnologyModel searchId(Integer id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		TechnologyModel technologyModel = new TechnologyModel();
		String query = " from TechnologyModel where id ='"+ id +"'";
		technologyModel = (TechnologyModel) session.createQuery(query).uniqueResult();
		return technologyModel;
	}

	@Override
	public List<TechnologyModel> searchNama(String name) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String query = " from TechnologyModel where lower(name) like lower('%"+ name +"%') and isDelete !=1";
		List<TechnologyModel> technologyModelList = new ArrayList<TechnologyModel>();
		technologyModelList = session.createQuery(query).list();
		return technologyModelList;
	}

	@Override
	public Integer findId(String name) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		String query = "id from TechnologyModel where name ='"+ name +"'";
		Integer id = (Integer) session.createQuery(query).uniqueResult();
		return id;
	}

	@Override
	public List<TechnologyModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<TechnologyModel> technologyModelList = new ArrayList<TechnologyModel>();
		String query = " from TechnologyModel where isDelete != 1";
		
		technologyModelList = session.createQuery(query).list();
		return technologyModelList;
	}
}
