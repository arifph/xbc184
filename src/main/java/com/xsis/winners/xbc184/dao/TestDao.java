package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.TestModel;

public interface TestDao {

	public void create(TestModel testModel);
	public void update(TestModel testModel);
	public void delete(TestModel testModel);
	public List<TestModel> search();
	public List<TestModel> searchNama(String nameTest);
	public TestModel searchId(Integer idTest);
		
}
