package com.xsis.winners.xbc184.dao;

import java.util.List;

import com.xsis.winners.xbc184.model.MenuModel;

public interface MenuDao {

	public void create(MenuModel menuModel);
	public void update(MenuModel menuModel);
	public void delete(MenuModel menuModel);
	public MenuModel searchId(Integer id);
	public void deleteTemp(MenuModel menuModel);
	public List<MenuModel> searchByTitle(String title);
	public List<MenuModel> search();
	public List<MenuModel> searchMenuMaster();
	public List<MenuModel> searchMenuParent();
}
