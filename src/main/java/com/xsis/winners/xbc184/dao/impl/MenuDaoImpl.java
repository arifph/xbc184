package com.xsis.winners.xbc184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.winners.xbc184.dao.MenuDao;
import com.xsis.winners.xbc184.model.MenuModel;

@Repository
public class MenuDaoImpl implements MenuDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(MenuModel menuModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(menuModel);
	}

	@Override
	public void update(MenuModel menuModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(menuModel);
	}

	@Override
	public void delete(MenuModel menuModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(menuModel);
	}

	@Override
	public MenuModel searchId(Integer id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		MenuModel menuModel = new MenuModel();
		
		String query = " from MenuModel where id="+id+" ";
		
		menuModel = (MenuModel) session.createQuery(query).uniqueResult();
		return menuModel;
	}

	@Override
	public void deleteTemp(MenuModel menuModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(menuModel);
	}

	@Override
	public List<MenuModel> searchByTitle(String title) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();
		
		String query = " from MenuModel where upper(title) like upper('%"+title+"%') ";
		
		menuModelList = session.createQuery(query).list();
		return menuModelList;
	}

	@Override
	public List<MenuModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();
		
		String query = " from MenuModel where isDelete !=1 ";
		
		menuModelList = session.createQuery(query).list();
		return menuModelList;
	}

	@Override
	public List<MenuModel> searchMenuMaster() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();
		
		String query = " from MenuModel where menuParent=null"
					+ " and isDelete !=1 ";
		
		menuModelList = session.createQuery(query).list();
		return menuModelList;
	}

	@Override
	public List<MenuModel> searchMenuParent() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();
		
		String query = " from MenuModel where menuParent !=0"
					+ " and isDelete !=1 ";
		
		menuModelList = session.createQuery(query).list();
		return menuModelList;
	}

	
}
