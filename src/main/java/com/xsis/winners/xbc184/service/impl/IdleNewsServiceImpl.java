package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.winners.xbc184.dao.IdleNewsDao;
import com.xsis.winners.xbc184.model.IdleNewsModel;
import com.xsis.winners.xbc184.service.IdleNewsService;

@Service
@Transactional
public class IdleNewsServiceImpl implements IdleNewsService {

	@Autowired
	private IdleNewsDao idleNewsDao;
	
	@Override
	public void create(IdleNewsModel idleNewsModel) {
		// TODO Auto-generated method stub
		this.idleNewsDao.create(idleNewsModel);
	}

	@Override
	public void update(IdleNewsModel idleNewsModel) {
		// TODO Auto-generated method stub
		this.idleNewsDao.update(idleNewsModel);
	}

	@Override
	public void delete(IdleNewsModel idleNewsModel) {
		// TODO Auto-generated method stub
		this.idleNewsDao.delete(idleNewsModel);
	}

	@Override
	public List<IdleNewsModel> search() {
		// TODO Auto-generated method stub
		return this.idleNewsDao.search();
	}

	@Override
	public IdleNewsModel searchId(Integer ididleNews) {
		// TODO Auto-generated method stub
		return this.idleNewsDao.searchId(ididleNews);
	}

	@Override
	public List<IdleNewsModel> searchNama(String nameIdleNews) {
		// TODO Auto-generated method stub
		return this.idleNewsDao.searchNama(nameIdleNews);
	}

	@Override
	public void deleteTemporary(IdleNewsModel idleNewsModel) {
		// TODO Auto-generated method stub
		this.idleNewsDao.update(idleNewsModel);
	}

}
