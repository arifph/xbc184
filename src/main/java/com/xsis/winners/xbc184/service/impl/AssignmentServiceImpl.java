package com.xsis.winners.xbc184.service.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.winners.xbc184.dao.AssignmentDao;
import com.xsis.winners.xbc184.model.AssignmentModel;
import com.xsis.winners.xbc184.service.AssignmentService;

@Service
@Transactional
public class AssignmentServiceImpl implements AssignmentService {

	@Autowired
	private AssignmentDao assignmentDao;
	
	private static Logger logger = LogManager.getLogger(AssignmentServiceImpl.class);
	
	@Override
	public void saveAssignment(AssignmentModel assignmentModel) {
		// TODO Auto-generated method stub
		logger.info("Delegate service to save Dao");
		this.assignmentDao.saveAssignment(assignmentModel);
	}

	@Override
	public void updateAssignment(AssignmentModel assignmentModel) {
		// TODO Auto-generated method stub
		logger.info("Delegate service to update Dao");
		this.assignmentDao.updateAssignment(assignmentModel);
	}

	@Override
	public void deleteAssignment(AssignmentModel assignmentModel) {
		// TODO Auto-generated method stub
		logger.info("Delegate service to delete Dao");
		this.assignmentDao.updateAssignment(assignmentModel);
	}

	@Override
	public List<AssignmentModel> search() {
		// TODO Auto-generated method stub
		return this.assignmentDao.search();
	}

	@Override
	public AssignmentModel searchById(Integer id) {
		// TODO Auto-generated method stub
		return this.assignmentDao.searchById(id);
	}

	@Override
	public List<AssignmentModel> search(Boolean isDelete) {
		// TODO Auto-generated method stub
		return this.assignmentDao.search(isDelete);
	}

	@Override
	public List<AssignmentModel> search(Date searchDate, Boolean isDelete) {
		// TODO Auto-generated method stub
		return this.assignmentDao.search(searchDate, isDelete);
	}

}
