package com.xsis.winners.xbc184.service.impl;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.winners.xbc184.dao.OfficeDao;
import com.xsis.winners.xbc184.model.OfficeModel;
import com.xsis.winners.xbc184.service.OfficeService;

@Service
@Transactional
public class OfficeServiceImpl implements OfficeService{

	@Autowired
	private OfficeDao officeDao;

	@Override
	public void create(OfficeModel officeModel) {
		// TODO Auto-generated method stub
		this.officeDao.create(officeModel);
		
	}

	@Override
	public List<OfficeModel> search() {
		// TODO Auto-generated method stub
		return this.officeDao.search();
	}

	@Override
	public void update(OfficeModel officeModel) {
		// TODO Auto-generated method stub
		this.officeDao.update(officeModel);
	}

	@Override
	public void delete(OfficeModel officeModel) {
		// TODO Auto-generated method stub
		this.officeDao.delete(officeModel);
	}

	@Override
	public OfficeModel searchId(Integer id) {
		// TODO Auto-generated method stub
		return this.officeDao.searchId(id);
	}

	@Override
	public List<OfficeModel> searchNama(String name) {
		// TODO Auto-generated method stub
		return this.officeDao.searchNama(name);
	}

	@Override
	public List<OfficeModel> searchNameSpace() {
		// TODO Auto-generated method stub
		return this.officeDao.searchNameSpace();
	}

	@Override
	public void deleteTemporary(OfficeModel officeModel) {
		// TODO Auto-generated method stub
		this.officeDao.update(officeModel);
	}

	
	
}
