package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.winners.xbc184.dao.MenuDao;
import com.xsis.winners.xbc184.model.MenuModel;
import com.xsis.winners.xbc184.service.MenuService;


@Service
@Transactional
public class MenuServiceImpl implements MenuService{

	@Autowired
	private MenuDao menuDao;
	
	@Override
	public void create(MenuModel menuModel) {
		// TODO Auto-generated method stub
		this.menuDao.create(menuModel);
	}

	@Override
	public void update(MenuModel menuModel) {
		// TODO Auto-generated method stub
		this.menuDao.update(menuModel);
	}

	@Override
	public void delete(MenuModel menuModel) {
		// TODO Auto-generated method stub
		this.menuDao.delete(menuModel);
	}

	@Override
	public MenuModel searchId(Integer id) {
		// TODO Auto-generated method stub
		return this.menuDao.searchId(id);
	}

	@Override
	public void deleteTemp(MenuModel menuModel) {
		// TODO Auto-generated method stub
		this.menuDao.deleteTemp(menuModel);
	}

	@Override
	public List<MenuModel> searchByTitle(String title) {
		// TODO Auto-generated method stub
		return this.menuDao.searchByTitle(title);
	}

	@Override
	public List<MenuModel> search() {
		// TODO Auto-generated method stub
		return this.menuDao.search();
	}

	@Override
	public List<MenuModel> searchMenuMaster() {
		// TODO Auto-generated method stub
		return this.menuDao.searchMenuMaster();
	}

	@Override
	public List<MenuModel> searchMenuParent() {
		// TODO Auto-generated method stub
		return this.menuDao.searchMenuParent();
	}
	
	
}
