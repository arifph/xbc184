package com.xsis.winners.xbc184.service;

import java.util.List;

import com.xsis.winners.xbc184.model.TestModel;
import com.xsis.winners.xbc184.model.TestTypeModel;

public interface TestTypeService {

	public List<TestTypeModel> search();
}
