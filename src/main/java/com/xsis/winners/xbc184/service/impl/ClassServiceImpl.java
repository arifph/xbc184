package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.winners.xbc184.dao.ClassDao;
import com.xsis.winners.xbc184.model.BiodataModel;
import com.xsis.winners.xbc184.model.ClassModel;
import com.xsis.winners.xbc184.service.ClassService;

@Service
@Transactional
public class ClassServiceImpl implements ClassService{

	@Autowired
	private ClassDao classDao;
	
	@Override
	public void create(ClassModel classModel) {
		// TODO Auto-generated method stub
		this.classDao.create(classModel);
	}

	@Override
	public void update(ClassModel classModel) {
		// TODO Auto-generated method stub
		this.classDao.update(classModel);
	}

	@Override
	public void delete(ClassModel classModel) {
		// TODO Auto-generated method stub
		this.classDao.delete(classModel);
	}

	@Override
	public List<ClassModel> search() {
		// TODO Auto-generated method stub
		return this.classDao.search();
	}

	@Override
	public ClassModel searchId(Integer id) {
		// TODO Auto-generated method stub
		return this.classDao.searchId(id);
	}
	
	@Override
	public ClassModel searchBiodata(Integer batchId, Integer biodataId) {
		// TODO Auto-generated method stub
		return this.classDao.searchBiodata(batchId, biodataId);
	}

//	@Override
//	public List<ClassModel> searchNama(String namaClass) {
//		// TODO Auto-generated method stub
//		return this.classDao.searchNama(namaClass);
//	}
//
//	@Override
//	public List<ClassModel> searchKode(String kodeClass) {
//		// TODO Auto-generated method stub
//		return this.classDao.searchKode(kodeClass);
//	}
//	
//	@Override
//	public List<ClassModel> searchKodeOrNama(String kodeClass, String namaClass) {
//		// TODO Auto-generated method stub
//		return this.classDao.searchKodeOrNama(kodeClass, namaClass);
//	}
//
//	@Override
//	public List<ClassModel> searchKodeEqual(String kodeClass) {
//		// TODO Auto-generated method stub
//		return this.classDao.searchKodeEqual(kodeClass);
//	}
//
//	@Override
//	public List<ClassModel> searchNamaEqual(String namaClass, Integer isDelete) {
//		// TODO Auto-generated method stub
//		return this.classDao.searchNamaEqual(namaClass, isDelete);
//	}

//	@Override
//	public void deleteTemporary(ClassModel classModel) {
//		// TODO Auto-generated method stub
//		this.classDao.update(classModel);
//	}

	@Override
	public List<ClassModel> searchBatchOrTechnology(String batch, String technology) {
		// TODO Auto-generated method stub
		return this.classDao.searchBatchOrTechnology(batch, technology);
	}

	@Override
	public List<BiodataModel> unchosenPeople(Integer batchId) {
		// TODO Auto-generated method stub
		return this.classDao.unchosenPeople(batchId);
	}
}
