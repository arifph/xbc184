package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.winners.xbc184.dao.BootcampTestTypeDao;
import com.xsis.winners.xbc184.model.BootcampTestTypeModel;
import com.xsis.winners.xbc184.service.BootcampTestTypeService;

@Service
@Transactional
public class BootcampTestTypeServiceImpl implements BootcampTestTypeService {

	@Autowired
	private BootcampTestTypeDao bootcampTestTypeDao;
	
	@Override
	public void create(BootcampTestTypeModel bootcampTestTypeModel) {
		// TODO Auto-generated method stub
		this.bootcampTestTypeDao.create(bootcampTestTypeModel);
	}

	@Override
	public void update(BootcampTestTypeModel bootcampTestTypeModel) {
		// TODO Auto-generated method stub
		this.bootcampTestTypeDao.update(bootcampTestTypeModel);
	}

	@Override
	public void delete(BootcampTestTypeModel bootcampTestTypeModel) {
		// TODO Auto-generated method stub
		this.bootcampTestTypeDao.delete(bootcampTestTypeModel);
	}

	@Override
	public List<BootcampTestTypeModel> search() {
		// TODO Auto-generated method stub
		return this.bootcampTestTypeDao.search();
	}

	@Override
	public BootcampTestTypeModel searchId(Integer idBootcampTestType) {
		// TODO Auto-generated method stub
		return this.bootcampTestTypeDao.searchId(idBootcampTestType);
	}

	@Override
	public List<BootcampTestTypeModel> searchNama(String nameBootcampTestType) {
		// TODO Auto-generated method stub
		return this.bootcampTestTypeDao.searchNama(nameBootcampTestType);
	}

	@Override
	public void deleteTemporary(BootcampTestTypeModel bootcampTestTypeModel) {
		// TODO Auto-generated method stub
		this.bootcampTestTypeDao.update(bootcampTestTypeModel);
	}

}
