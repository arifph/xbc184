package com.xsis.winners.xbc184.service;

import java.util.List;

import com.xsis.winners.xbc184.model.RoleModel;

public interface RoleService {

	public void create(RoleModel roleModel);
	public void update(RoleModel roleModel);
	public void deleteTemp(RoleModel roleModel);
	public List<RoleModel> search();
	public RoleModel searchId(Integer idRole);
	public List<RoleModel> searchNama(String namaRole);
	public List<RoleModel> searchNamaEqual(String namaRole);
	
}
