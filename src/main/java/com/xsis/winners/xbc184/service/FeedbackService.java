package com.xsis.winners.xbc184.service;

import java.util.List;

import com.xsis.winners.xbc184.model.FeedbackModel;

public interface FeedbackService {

	public List<FeedbackModel> search();
	public FeedbackModel searchIdTest(Integer idTest);
	
}
