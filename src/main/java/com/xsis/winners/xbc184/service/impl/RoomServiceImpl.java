package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.winners.xbc184.dao.RoomDao;
import com.xsis.winners.xbc184.model.RoomModel;
import com.xsis.winners.xbc184.service.RoomService;

@Service
@Transactional
public class RoomServiceImpl implements RoomService{
	
	@Autowired
	private RoomDao roomDao;

	@Override
	public void create(RoomModel roomModel) {
		// TODO Auto-generated method stub
		this.roomDao.create(roomModel);
	}

	@Override
	public void update(RoomModel roomModel) {
		// TODO Auto-generated method stub
		this.roomDao.update(roomModel);
	}

	@Override
	public void delete(RoomModel roomModel) {
		// TODO Auto-generated method stub
		this.roomDao.delete(roomModel);
	}

	@Override
	public RoomModel searchId(Integer id) {
		// TODO Auto-generated method stub
		return this.roomDao.searchId(id);
	}

	@Override
	public List<RoomModel> search() {
		// TODO Auto-generated method stub
		return this.roomDao.search();
	}

	@Override
	public List<RoomModel> searchRoomInOffice(Integer id) {
		// TODO Auto-generated method stub
		return this.roomDao.searchRoomInOffice(id);
	}

	@Override
	public void deleteTemporary(RoomModel roomModel) {
		// TODO Auto-generated method stub
		this.roomDao.update(roomModel);
	}

	
	
}
