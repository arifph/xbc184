package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.winners.xbc184.dao.TestimonyDao;
import com.xsis.winners.xbc184.model.TestimonyModel;
import com.xsis.winners.xbc184.service.TestimonyService;

@Service
@Transactional
public class TestimonyServiceImpl implements TestimonyService {

	@Autowired
	private TestimonyDao testimonyDao;
	
	@Override
	public void create(TestimonyModel testimonyModel) {
		// TODO Auto-generated method stub
		this.testimonyDao.create(testimonyModel);
	}

	@Override
	public List<TestimonyModel> search() {
		// TODO Auto-generated method stub
		return this.testimonyDao.search();
	}

	@Override
	public void update(TestimonyModel testimonyModel) {
		// TODO Auto-generated method stub
		this.testimonyDao.update(testimonyModel);
	}

	@Override
	public void delete(TestimonyModel testimonyModel) {
		// TODO Auto-generated method stub
		this.testimonyDao.delete(testimonyModel);
	}

	@Override
	public TestimonyModel searchId(Integer id) {
		// TODO Auto-generated method stub
		return this.testimonyDao.searchId(id);
	}

	@Override
	public List<TestimonyModel> searchTitle(String title) {
		// TODO Auto-generated method stub
		return this.testimonyDao.searchTitle(title);
	}

	@Override
	public void deleteTemporary(TestimonyModel testimonyModel) {
		// TODO Auto-generated method stub
		this.testimonyDao.update(testimonyModel);
	}

	
	
}
