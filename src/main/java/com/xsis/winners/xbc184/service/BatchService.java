package com.xsis.winners.xbc184.service;

import java.util.List;

import com.xsis.winners.xbc184.model.BatchModel;

public interface BatchService {
	
	public void create(BatchModel batchModel);
	public void update(BatchModel batchModel);
	public void delete(BatchModel batchModel);
	public List<BatchModel> search();
	public BatchModel searchId(Integer id);
	public List<BatchModel> searchTechnologyOrName(String technology, String name);
//	public List<BatchModel> searchNama(String namaBatch);
//	public List<BatchModel> searchKode(String kodeBatch);
//	public List<BatchModel> searchKodeOrNama(String kodeBatch, String namaBatch);
//	public List<BatchModel> searchKodeEqual(String kodeBatch);
//	public List<BatchModel> searchNamaEqual(String namaBatch, Integer isDelete);
	public void deleteTemporary(BatchModel batchModel);
	
	
	/*public output nama(type input)*/
	/*syntax input tanpa output*/
	/*public void simpan(String kodeBatch);
	public void ubahId(Integer idBatch);
	public void ubahNama(String namaBatch);
	public void cari(String namaBatch);
	
	public void createDua(Integer idBatch, String kodeBatch, String namaBatch);*/ 
	// sama saja dengan method yg create (di atas), hanya saja yg atas lebih OOP. Dan yang ini harus masukkan inputan satu per satu.
	
	/*syntax tanpa input dengan output*/
	/*SELECT * FROM M_FAKULTAS*/ /*= public BatchModel selectAll();*/
	
	/*syntax input dan output*/
	
	/*SELECT * FROM M_FAKULTAS WHERE NAMA_FAKULTAS = '';*/ 
	/*= public BatchModel searchNama(String namaBatch);*/
	
	/*SELECT * FROM M_FAKULTAS WHERE NAMA_FAKULTAS = '' AND KODE_FAKULTAS='';*/ 
	/*= public BatchModel searchNamaAndKode(String namaBatch, String kodeBatch);*/
	
	/*SELECT * FROM M_FAKULTAS WHERE ID_FAKULTAS = '' OR KODE_FAKULTAS='';*/ 
	/*= public BatchModel searchIdOrKode(Integer idBatch, String kodeBatch);*/
	
}
