package com.xsis.winners.xbc184.service;

import java.util.List;

import com.xsis.winners.xbc184.model.TestimonyModel;

public interface TestimonyService {
	public void create(TestimonyModel testimonyModel);
	public void update(TestimonyModel testimonyModel);
	public void delete(TestimonyModel testimonyModel);

	public TestimonyModel searchId(Integer id);//kode disamping nampilin data berdasarkan id
	public List<TestimonyModel> search();
	
	public List<TestimonyModel> searchTitle(String title);//kode di sampingnampilin data atau list

	public void deleteTemporary(TestimonyModel testimonyModel);
}
