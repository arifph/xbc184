package com.xsis.winners.xbc184.service;

	import java.util.List;

import com.xsis.winners.xbc184.model.IdleNewsModel;


	public interface IdleNewsService {

		public void create(IdleNewsModel IdleNewsModel);
		public void update(IdleNewsModel IdleNewsModel);
		public void delete(IdleNewsModel IdleNewsModel);
		public List<IdleNewsModel> search();
		public IdleNewsModel searchId(Integer idIdleNewsModel);
		public List<IdleNewsModel> searchNama(String nameIdleNewsModel);
		public void deleteTemporary(IdleNewsModel IdleNewsModel);
	}
