package com.xsis.winners.xbc184.service;

import java.util.List;

import com.xsis.winners.xbc184.model.MonitoringModel;

public interface MonitoringService {

	public void create(MonitoringModel monitoringModel);

	public void update(MonitoringModel monitoringModel);

	public void delete(MonitoringModel monitoringModel);

	public List<MonitoringModel> search();

	public List<MonitoringModel> searchNamaBiodata(String name);
	
	public MonitoringModel searchId(Integer idMonitoring);
	
	public void deleteTemporary(MonitoringModel monitoringModel);
	
	public List<MonitoringModel> searchIdEqual(Integer idBiodata);
	
	public void createPlacement(MonitoringModel monitoringModel);
}
