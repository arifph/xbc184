package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.winners.xbc184.dao.BootcampTypeDao;
import com.xsis.winners.xbc184.model.BootcampTypeModel;
import com.xsis.winners.xbc184.service.BootcampTypeService;

@Service
@Transactional
public class BootcampTypeServiceImpl implements BootcampTypeService {

	@Autowired
	private BootcampTypeDao bootcampTypeDao;
	
	@Override
	public void create(BootcampTypeModel bootcampTypeModel) {
		// TODO Auto-generated method stub
		this.bootcampTypeDao.create(bootcampTypeModel);
	}

	@Override
	public void update(BootcampTypeModel bootcampTypeModel) {
		// TODO Auto-generated method stub
		this.bootcampTypeDao.update(bootcampTypeModel);
	}

	@Override
	public void delete(BootcampTypeModel bootcampTypeModel) {
		// TODO Auto-generated method stub
		this.bootcampTypeDao.delete(bootcampTypeModel);
	}

	@Override
	public List<BootcampTypeModel> search(String kodeRole) {
		// TODO Auto-generated method stub
		return this.bootcampTypeDao.search(kodeRole);
	}

	@Override
	public BootcampTypeModel searchId(Integer idBootcampType) {
		// TODO Auto-generated method stub
		return this.bootcampTypeDao.searchId(idBootcampType);
	}

	@Override
	public List<BootcampTypeModel> searchNama(String nameBootcampType) {
		// TODO Auto-generated method stub
		return this.bootcampTypeDao.searchNama(nameBootcampType);
	}

	@Override
	public void deleteTemporary(BootcampTypeModel bootcampTypeModel) {
		// TODO Auto-generated method stub
		this.bootcampTypeDao.update(bootcampTypeModel);
	}

	@Override
	public List<BootcampTypeModel> search() {
		// TODO Auto-generated method stub
		return this.bootcampTypeDao.search();
	}

}
