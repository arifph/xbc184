package com.xsis.winners.xbc184.service;

import java.util.List;

import com.xsis.winners.xbc184.model.BiodataModel;

public interface BiodataService {
	
	public void create(BiodataModel biodataModel);
	public void update(BiodataModel biodataModel);
	public void delete(BiodataModel biodataModel);
	public List<BiodataModel> search();
	public BiodataModel searchId(Integer id);
	public List<BiodataModel> searchNameOrMajors(String name, String majors);
//	public List<BiodataModel> searchNama(String namaBiodata);
//	public List<BiodataModel> searchKode(String kodeBiodata);
//	public List<BiodataModel> searchKodeOrNama(String kodeBiodata, String namaBiodata);
//	public List<BiodataModel> searchKodeEqual(String kodeBiodata);
//	public List<BiodataModel> searchNamaEqual(String namaBiodata, Integer isDelete);
	public void deleteTemporary(BiodataModel biodataModel);
	public List<BiodataModel> searchBiodataNotInMonitoring();
	
	
	/*public output nama(type input)*/
	/*syntax input tanpa output*/
	/*public void simpan(String kodeBiodata);
	public void ubahId(Integer idBiodata);
	public void ubahNama(String namaBiodata);
	public void cari(String namaBiodata);
	
	public void createDua(Integer idBiodata, String kodeBiodata, String namaBiodata);*/ 
	// sama saja dengan method yg create (di atas), hanya saja yg atas lebih OOP. Dan yang ini harus masukkan inputan satu per satu.
	
	/*syntax tanpa input dengan output*/
	/*SELECT * FROM M_FAKULTAS*/ /*= public BiodataModel selectAll();*/
	
	/*syntax input dan output*/
	
	/*SELECT * FROM M_FAKULTAS WHERE NAMA_FAKULTAS = '';*/ 
	/*= public BiodataModel searchNama(String namaBiodata);*/
	
	/*SELECT * FROM M_FAKULTAS WHERE NAMA_FAKULTAS = '' AND KODE_FAKULTAS='';*/ 
	/*= public BiodataModel searchNamaAndKode(String namaBiodata, String kodeBiodata);*/
	
	/*SELECT * FROM M_FAKULTAS WHERE ID_FAKULTAS = '' OR KODE_FAKULTAS='';*/ 
	/*= public BiodataModel searchIdOrKode(Integer idBiodata, String kodeBiodata);*/
	
}
