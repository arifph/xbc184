package com.xsis.winners.xbc184.service;

import java.util.List;

import com.xsis.winners.xbc184.model.TechnologyModel;

public interface TechnologyService {
	
	public void create(TechnologyModel technologyModel);
	public void update(TechnologyModel technologyModel);
	public void delete(TechnologyModel technologyModel);
	public List<TechnologyModel> search();
	public List<TechnologyModel> search(String kodeRole);
	public TechnologyModel searchId(Integer id);
	public void deleteTemporary(TechnologyModel technologyModel);
	public List<TechnologyModel> searchNama(String name);
	public Integer findId(String name);
}
