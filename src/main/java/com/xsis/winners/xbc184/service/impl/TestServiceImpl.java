package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.winners.xbc184.dao.TestDao;
import com.xsis.winners.xbc184.model.TestModel;
import com.xsis.winners.xbc184.service.TestService;

@Service
@Transactional
public class TestServiceImpl implements TestService{

	@Autowired
	private TestDao testDao;
	
	@Override
	public void create(TestModel testModel) {
		// TODO Auto-generated method stub
		this.testDao.create(testModel);
	}

	@Override
	public void update(TestModel testModel) {
		// TODO Auto-generated method stub
		this.testDao.update(testModel);
	}

	@Override
	public void delete(TestModel testModel) {
		// TODO Auto-generated method stub
		this.testDao.delete(testModel);
	}

	@Override
	public List<TestModel> search() {
		// TODO Auto-generated method stub
		return this.testDao.search();
	}

	@Override
	public List<TestModel> searchNama(String nameTest) {
		// TODO Auto-generated method stub
		return this.testDao.searchNama(nameTest);
	}

	@Override
	public TestModel searchId(Integer idTest) {
		// TODO Auto-generated method stub
		return this.testDao.searchId(idTest);
	}

	@Override
	public void deleteTemporary(TestModel testModel) {
		// TODO Auto-generated method stub
		this.testDao.update(testModel);
	}

	
}
