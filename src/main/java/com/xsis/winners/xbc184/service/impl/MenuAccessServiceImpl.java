package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.winners.xbc184.dao.MenuAccessDao;
import com.xsis.winners.xbc184.model.MenuAccessModel;
import com.xsis.winners.xbc184.service.MenuAccessService;

@Service
@Transactional
public class MenuAccessServiceImpl implements MenuAccessService {

	@Autowired
	private MenuAccessDao menuAccessDao;

	@Override
	public void create(MenuAccessModel menuAccessModel) {
		// TODO Auto-generated method stub
		this.menuAccessDao.create(menuAccessModel);
	}

	@Override
	public void delete(MenuAccessModel menuAccessModel) {
		// TODO Auto-generated method stub
		this.menuAccessDao.delete(menuAccessModel);
	}

	@Override
	public MenuAccessModel searchId(Integer id) {
		// TODO Auto-generated method stub
		return this.menuAccessDao.searchId(id);
	}

	@Override
	public List<MenuAccessModel> searchByRoleId(Integer roleId) {
		// TODO Auto-generated method stub
		return this.menuAccessDao.searchByRoleId(roleId);
	}

	@Override
	public List<MenuAccessModel> search() {
		// TODO Auto-generated method stub
		return this.menuAccessDao.search();
	}
	
	
}
