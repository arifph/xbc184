package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.winners.xbc184.dao.TechnologyDao;
import com.xsis.winners.xbc184.model.TechnologyModel;
import com.xsis.winners.xbc184.service.TechnologyService;

@Service
@Transactional
public class TechnologyServiceImpl implements TechnologyService{
	
	@Autowired
	private TechnologyDao technologyDao;
	
	@Override
	public void create(TechnologyModel technologyModel) {
		// TODO Auto-generated method stub
		this.technologyDao.create(technologyModel);
	}

	@Override
	public void update(TechnologyModel technologyModel) {
		// TODO Auto-generated method stub
		this.technologyDao.update(technologyModel);
	}

	@Override
	public void delete(TechnologyModel technologyModel) {
		// TODO Auto-generated method stub
		this.technologyDao.delete(technologyModel);
	}

	@Override
	public List<TechnologyModel> search(String kodeRole) {
		// TODO Auto-generated method stub
		return this.technologyDao.search(kodeRole);
	}

	@Override
	public TechnologyModel searchId(Integer id) {
		// TODO Auto-generated method stub
		return this.technologyDao.searchId(id);
	}

	@Override
	public void deleteTemporary(TechnologyModel technologyModel) {
		// TODO Auto-generated method stub
		this.technologyDao.update(technologyModel);
	}

	@Override
	public List<TechnologyModel> searchNama(String name) {
		// TODO Auto-generated method stub
		return this.technologyDao.searchNama(name);
	}

	@Override
	public Integer findId(String name) {
		// TODO Auto-generated method stub
		return this.technologyDao.findId(name);
	}

	@Override
	public List<TechnologyModel> search() {
		// TODO Auto-generated method stub
		return this.technologyDao.search();
	}

}
