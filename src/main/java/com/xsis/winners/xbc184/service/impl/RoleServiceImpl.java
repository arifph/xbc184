package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.winners.xbc184.dao.RoleDao;
import com.xsis.winners.xbc184.model.RoleModel;
import com.xsis.winners.xbc184.service.RoleService;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDao roleDao;

	@Override
	public void create(RoleModel roleModel) {
		// TODO Auto-generated method stub
		this.roleDao.create(roleModel);
	}
	
	@Override
	public void update(RoleModel roleModel) {
		// TODO Auto-generated method stub
		this.roleDao.update(roleModel);
	}
	
	@Override
	public void deleteTemp(RoleModel roleModel) {
		// TODO Auto-generated method stub
		this.roleDao.update(roleModel);
	}
	
	@Override
	public List<RoleModel> search() {
		// TODO Auto-generated method stub
		return this.roleDao.search();
	}

	@Override
	public RoleModel searchId(Integer idRole) {
		// TODO Auto-generated method stub
		return this.roleDao.searchId(idRole);
	}

	@Override
	public List<RoleModel> searchNama(String namaRole) {
		// TODO Auto-generated method stub
		return this.roleDao.searchNama(namaRole);
	}
	
	@Override
	public List<RoleModel> searchNamaEqual(String namaRole) {
		// TODO Auto-generated method stub
		return this.roleDao.searchNamaEqual(namaRole);
	}

}
