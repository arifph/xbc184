package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.winners.xbc184.dao.BatchTestDao;
import com.xsis.winners.xbc184.model.BatchTestModel;
import com.xsis.winners.xbc184.model.TestModel;
import com.xsis.winners.xbc184.service.BatchTestService;
//import com.xsis.winners.xbc184.service.TestService;

@Service
@Transactional
public class BatchTestServiceImpl implements BatchTestService{

	@Autowired
	private BatchTestDao batchTestDao;
	
	@Override
	public void create(BatchTestModel batchTestModel) {
		// TODO Auto-generated method stub
		this.batchTestDao.create(batchTestModel);
	}

//	@Override
//	public void update(BatchTestModel batchTestModel) {
//		// TODO Auto-generated method stub
//		this.batchTestDao.update(batchTestModel);
//	}

	@Override
	public void delete(BatchTestModel batchTestModel) {
		// TODO Auto-generated method stub
		this.batchTestDao.delete(batchTestModel);
	}

	@Override
	public List<TestModel> searchBootcampTest() {
		// TODO Auto-generated method stub
		return this.batchTestDao.searchBootcampTest();
	}

//	@Override
//	public List<BatchTestModel> search() {
//		// TODO Auto-generated method stub
//		return this.batchTestDao.search();
//	}
//
//	@Override
//	public List<BatchTestModel> searchNama(String nameTest) {
//		// TODO Auto-generated method stub
//		return this.batchTestDao.searchNama(nameTest);
//	}
//
//	@Override
//	public BatchTestModel searchId(Integer idTest) {
//		// TODO Auto-generated method stub
//		return this.batchTestDao.searchId(idTest);
//	}
//
//	@Override
//	public void deleteTemporary(BatchTestModel batchTestModel) {
//		// TODO Auto-generated method stub
//		this.batchTestDao.update(batchTestModel);
//	}
	
	@Override
	public BatchTestModel searchSetupTest(Integer batchId, Integer testId) {
		// TODO Auto-generated method stub
		return this.batchTestDao.searchSetupTest(batchId, testId);
	}

	@Override
	public BatchTestModel searchIdBySetupTest(Integer batchId, Integer testId) {
		// TODO Auto-generated method stub
		return this.batchTestDao.searchSetupTest(batchId, testId); // mencari berdasarkan batchId dan testId
	}
}
