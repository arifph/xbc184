package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.winners.xbc184.dao.FeedbackDao;
import com.xsis.winners.xbc184.model.FeedbackModel;
import com.xsis.winners.xbc184.service.FeedbackService;

@Service
@Transactional
public class FeedbackServiceImpl implements FeedbackService {
	
	@Autowired
	private FeedbackDao feedbackDao;

	@Override
	public List<FeedbackModel> search() {
		// TODO Auto-generated method stub
		return this.feedbackDao.search();
	}

	@Override
	public FeedbackModel searchIdTest(Integer idTest) {
		// TODO Auto-generated method stub
		return this.feedbackDao.searchIdTest(idTest);
	}

}
