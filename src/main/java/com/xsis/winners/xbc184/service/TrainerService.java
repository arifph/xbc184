package com.xsis.winners.xbc184.service;

import java.util.List;

import com.xsis.winners.xbc184.model.TrainerModel;

public interface TrainerService {
	
	public void create(TrainerModel trainerModel);
	public void update(TrainerModel trainerModel);
	public void delete(TrainerModel trainerModel);
	public List<TrainerModel> search();
	public List<TrainerModel> search(String kodeRole);
	public TrainerModel searchId(Integer id);
	public void deleteTemporary(TrainerModel trainerModel);
	public List<TrainerModel> searchNama(String name);

}
