package com.xsis.winners.xbc184.service;

import java.util.List;

import com.xsis.winners.xbc184.model.BootcampTypeModel;

public interface BootcampTypeService {

	public void create(BootcampTypeModel bootcampTypeModel);

	public void update(BootcampTypeModel bootcampTypeModel);

	public void delete(BootcampTypeModel bootcampTypeModel);

	public List<BootcampTypeModel> search(String kodeRole);

	public BootcampTypeModel searchId(Integer idBootcampType);

	public List<BootcampTypeModel> searchNama(String nameBootcampType);

	public void deleteTemporary(BootcampTypeModel bootcampTypeModel);
	
	public List<BootcampTypeModel> search();
}
