package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.winners.xbc184.dao.MonitoringDao;
import com.xsis.winners.xbc184.model.MonitoringModel;
import com.xsis.winners.xbc184.service.MonitoringService;

@Service
@Transactional
public class MonitoringServiceImpl implements MonitoringService {

	@Autowired
	private MonitoringDao monitoringDao;

	@Override
	public void create(MonitoringModel monitoringModel) {
		// TODO Auto-generated method stub
		this.monitoringDao.create(monitoringModel);
	}

	@Override
	public void update(MonitoringModel monitoringModel) {
		// TODO Auto-generated method stub
		this.monitoringDao.update(monitoringModel);
	}

	@Override
	public void delete(MonitoringModel monitoringModel) {
		// TODO Auto-generated method stub
		this.monitoringDao.delete(monitoringModel);
	}

	@Override
	public List<MonitoringModel> search() {
		// TODO Auto-generated method stub
		return this.monitoringDao.search();
	}

	@Override
	public List<MonitoringModel> searchNamaBiodata(String name) {
		// TODO Auto-generated method stub
		return this.monitoringDao.searchNamaBiodata(name);
	}

	@Override
	public MonitoringModel searchId(Integer idMonitoring) {
		// TODO Auto-generated method stub
		return this.monitoringDao.searchId(idMonitoring);
	}

	@Override
	public void deleteTemporary(MonitoringModel monitoringModel) {
		// TODO Auto-generated method stub
		this.monitoringDao.update(monitoringModel);
	}

	@Override
	public List<MonitoringModel> searchIdEqual(Integer idBiodata) {
		// TODO Auto-generated method stub
		return this.monitoringDao.searchIdEqual(idBiodata);
	}

	@Override
	public void createPlacement(MonitoringModel monitoringModel) {
		// TODO Auto-generated method stub
		this.monitoringDao.update(monitoringModel);
	}
}
