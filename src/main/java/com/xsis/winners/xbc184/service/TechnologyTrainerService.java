package com.xsis.winners.xbc184.service;

import java.util.List;

import com.xsis.winners.xbc184.model.TechnologyTrainerModel;

public interface TechnologyTrainerService {
	
	public void create(TechnologyTrainerModel technologyTrainerModel);
	public void delete(TechnologyTrainerModel technologyTrainerModel);
	public void deleteList(List<TechnologyTrainerModel> deleteTechnologyTrainerModelList);
	public List<TechnologyTrainerModel> search();
	
	public List<TechnologyTrainerModel> searchIdTrainer(Integer id);
	
	public TechnologyTrainerModel searchId(Integer id);
	
	public void update(TechnologyTrainerModel technologyTrainerModel);
}
