package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.winners.xbc184.dao.CategoryDao;
import com.xsis.winners.xbc184.model.CategoryModel;
import com.xsis.winners.xbc184.service.CategoryService;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryDao categoryDao;
	
	private static Logger logger = LogManager.getLogger(CategoryServiceImpl.class);
	
	@Override
	public void saveCategory(CategoryModel categoryModel) {
		// TODO Auto-generated method stub
		logger.info("Delegate service to save Dao");
		this.categoryDao.saveCategory(categoryModel);
	}

	@Override
	public void updateCategory(CategoryModel categoryModel) {
		// TODO Auto-generated method stub
		logger.info("Delegate service to update Dao");
		this.categoryDao.updateCategory(categoryModel);
	}

	@Override
	public void deleteCategory(CategoryModel categoryModel) {
		// TODO Auto-generated method stub
		logger.info("Delegate service to delete Dao");
		this.categoryDao.updateCategory(categoryModel);
	}

	@Override
	public List<CategoryModel> search() {
		// TODO Auto-generated method stub
		return this.categoryDao.search();
	}

	@Override
	public CategoryModel searchById(Integer id) {
		// TODO Auto-generated method stub
		return this.categoryDao.searchById(id);
	}

	@Override
	public List<CategoryModel> search(Boolean isDelete) {
		// TODO Auto-generated method stub
		return this.categoryDao.search(isDelete);
	}

	@Override
	public List<CategoryModel> search(String code, String name, Boolean isDelete) {
		// TODO Auto-generated method stub
		return this.categoryDao.search(code, name, isDelete);
	}

	
}
