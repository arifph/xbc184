package com.xsis.winners.xbc184.service;

import java.util.List;

import com.xsis.winners.xbc184.model.BiodataModel;
import com.xsis.winners.xbc184.model.ClassModel;

public interface ClassService {
	
	public void create(ClassModel classModel);
	public void update(ClassModel classModel);
	public void delete(ClassModel classModel);
	public List<ClassModel> search();
	public ClassModel searchId(Integer id);
	public ClassModel searchBiodata(Integer batchId, Integer biodataId);
	public List<ClassModel> searchBatchOrTechnology(String batch, String technology);
//	public List<ClassModel> searchNama(String namaClass);
//	public List<ClassModel> searchKode(String kodeClass);
//	public List<ClassModel> searchKodeOrNama(String kodeClass, String namaClass);
//	public List<ClassModel> searchKodeEqual(String kodeClass);
//	public List<ClassModel> searchNamaEqual(String namaClass, Integer isDelete);
//	public void deleteTemporary(ClassModel classModel);
	public List<BiodataModel> unchosenPeople(Integer batchId);
	
	
	/*public output nama(type input)*/
	/*syntax input tanpa output*/
	/*public void simpan(String kodeClass);
	public void ubahId(Integer idClass);
	public void ubahNama(String namaClass);
	public void cari(String namaClass);
	
	public void createDua(Integer idClass, String kodeClass, String namaClass);*/ 
	// sama saja dengan method yg create (di atas), hanya saja yg atas lebih OOP. Dan yang ini harus masukkan inputan satu per satu.
	
	/*syntax tanpa input dengan output*/
	/*SELECT * FROM M_FAKULTAS*/ /*= public ClassModel selectAll();*/
	
	/*syntax input dan output*/
	
	/*SELECT * FROM M_FAKULTAS WHERE NAMA_FAKULTAS = '';*/ 
	/*= public ClassModel searchNama(String namaClass);*/
	
	/*SELECT * FROM M_FAKULTAS WHERE NAMA_FAKULTAS = '' AND KODE_FAKULTAS='';*/ 
	/*= public ClassModel searchNamaAndKode(String namaClass, String kodeClass);*/
	
	/*SELECT * FROM M_FAKULTAS WHERE ID_FAKULTAS = '' OR KODE_FAKULTAS='';*/ 
	/*= public ClassModel searchIdOrKode(Integer idClass, String kodeClass);*/
	
}
