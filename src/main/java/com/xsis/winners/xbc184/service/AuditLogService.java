package com.xsis.winners.xbc184.service;

import com.xsis.winners.xbc184.model.AuditLogModel;

public interface AuditLogService {

	public void save(AuditLogModel auditLogModel);
	public void update(AuditLogModel auditLogModel);
	public void delete(AuditLogModel auditLogModel);
	public AuditLogModel searchAuditLog(String jsonInsert);
}
