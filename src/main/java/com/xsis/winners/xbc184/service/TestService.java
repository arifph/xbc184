package com.xsis.winners.xbc184.service;

import java.util.List;


import com.xsis.winners.xbc184.model.TestModel;

public interface TestService {
	
	public void create(TestModel testModel);
	public void update(TestModel testModel);
	public void delete(TestModel testModel);
	public List<TestModel> search();
	public List<TestModel> searchNama(String nameTest);
	public TestModel searchId(Integer idTest);
	public void deleteTemporary(TestModel testModel);
}
