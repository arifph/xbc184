package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.winners.xbc184.dao.BiodataDao;
import com.xsis.winners.xbc184.model.BiodataModel;
import com.xsis.winners.xbc184.service.BiodataService;

@Service
@Transactional
public class BiodataServiceImpl implements BiodataService{

	@Autowired
	private BiodataDao biodataDao;
	
	@Override
	public void create(BiodataModel biodataModel) {
		// TODO Auto-generated method stub
		this.biodataDao.create(biodataModel);
	}

	@Override
	public void update(BiodataModel biodataModel) {
		// TODO Auto-generated method stub
		this.biodataDao.update(biodataModel);
	}

	@Override
	public void delete(BiodataModel biodataModel) {
		// TODO Auto-generated method stub
		this.biodataDao.delete(biodataModel);
	}

	@Override
	public List<BiodataModel> search() {
		// TODO Auto-generated method stub
		return this.biodataDao.search();
	}

	@Override
	public BiodataModel searchId(Integer id) {
		// TODO Auto-generated method stub
		return this.biodataDao.searchId(id);
	}

//	@Override
//	public List<BiodataModel> searchNama(String namaBiodata) {
//		// TODO Auto-generated method stub
//		return this.biodataDao.searchNama(namaBiodata);
//	}
//
//	@Override
//	public List<BiodataModel> searchKode(String kodeBiodata) {
//		// TODO Auto-generated method stub
//		return this.biodataDao.searchKode(kodeBiodata);
//	}
//	
//	@Override
//	public List<BiodataModel> searchKodeOrNama(String kodeBiodata, String namaBiodata) {
//		// TODO Auto-generated method stub
//		return this.biodataDao.searchKodeOrNama(kodeBiodata, namaBiodata);
//	}
//
//	@Override
//	public List<BiodataModel> searchKodeEqual(String kodeBiodata) {
//		// TODO Auto-generated method stub
//		return this.biodataDao.searchKodeEqual(kodeBiodata);
//	}
//
//	@Override
//	public List<BiodataModel> searchNamaEqual(String namaBiodata, Integer isDelete) {
//		// TODO Auto-generated method stub
//		return this.biodataDao.searchNamaEqual(namaBiodata, isDelete);
//	}

	@Override
	public void deleteTemporary(BiodataModel biodataModel) {
		// TODO Auto-generated method stub
		this.biodataDao.update(biodataModel);
	}

	@Override
	public List<BiodataModel> searchNameOrMajors(String name, String majors) {
		// TODO Auto-generated method stub
		return this.biodataDao.searchNameOrMajors(name, majors);
	}

	@Override
	public List<BiodataModel> searchBiodataNotInMonitoring() {
		// TODO Auto-generated method stub
		return this.biodataDao.searchBiodataNotInMonitoring();
	}
}
