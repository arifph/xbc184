package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.winners.xbc184.dao.TestDao;
import com.xsis.winners.xbc184.dao.TestTypeDao;
import com.xsis.winners.xbc184.model.TestModel;
import com.xsis.winners.xbc184.model.TestTypeModel;
import com.xsis.winners.xbc184.service.TestService;
import com.xsis.winners.xbc184.service.TestTypeService;

@Service
@Transactional
public class TestTypeServiceImpl implements TestTypeService {

	@Autowired
	private TestTypeDao testTypeDao;

	@Override
	public List<TestTypeModel> search() {
		// TODO Auto-generated method stub
		return this.testTypeDao.searchAll();
	}	
	
}
