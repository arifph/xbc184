package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.winners.xbc184.dao.QuestionDao;
import com.xsis.winners.xbc184.model.QuestionModel;
import com.xsis.winners.xbc184.service.QuestionService;

@Service
@Transactional
public class QuestionServiceImpl implements QuestionService {
	
	@Autowired
	private QuestionDao questionDao;

	@Override
	public void create(QuestionModel questionModel) {
		// TODO Auto-generated method stub
		this.questionDao.create(questionModel);
	}

	@Override
	public void update(QuestionModel questionModel) {
		// TODO Auto-generated method stub
		this.questionDao.update(questionModel);
	}

	@Override
	public void deleteTemp(QuestionModel questionModel) {
		// TODO Auto-generated method stub
		this.questionDao.update(questionModel);
	}

	@Override
	public List<QuestionModel> search() {
		// TODO Auto-generated method stub
		return this.questionDao.search();
	}

	@Override
	public QuestionModel searchId(Integer idQuestion) {
		// TODO Auto-generated method stub
		return this.questionDao.searchId(idQuestion);
	}

	@Override
	public List<QuestionModel> searchQuestion(String question) {
		// TODO Auto-generated method stub
		return this.questionDao.searchQuestion(question);
	}

}
