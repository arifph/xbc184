package com.xsis.winners.xbc184.service;

import java.util.List;


import com.xsis.winners.xbc184.model.BatchTestModel;
import com.xsis.winners.xbc184.model.TestModel;

public interface BatchTestService {
	
	public void create(BatchTestModel batchTestModel);
//	public void update(BatchTestModel batchTestModel);
	public void delete(BatchTestModel batchTestModel);
//	public List<BatchTestModel> search();
//	public List<BatchTestModel> searchNama(String nameTest);
//	public BatchTestModel searchId(Integer idTest);
//	public void deleteTemporary(BatchTestModel batchTestModel);
	public List<TestModel> searchBootcampTest();
	public BatchTestModel searchSetupTest(Integer batchId, Integer testId);
	public BatchTestModel searchIdBySetupTest(Integer batchId, Integer testId); // mencari ID BatchTest
}
