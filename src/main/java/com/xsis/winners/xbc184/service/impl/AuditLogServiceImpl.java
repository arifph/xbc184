package com.xsis.winners.xbc184.service.impl;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.winners.xbc184.dao.AuditLogDao;
import com.xsis.winners.xbc184.model.AuditLogModel;
import com.xsis.winners.xbc184.service.AuditLogService;

@Service
@Transactional
public class AuditLogServiceImpl implements AuditLogService {
	
	private static final Logger logger = LogManager.getFormatterLogger();

	@Autowired
	private AuditLogDao auditLogDao;
	
	@Override
	public void save(AuditLogModel auditLogModel) {
		// TODO Auto-generated method stub
		this.auditLogDao.save(auditLogModel);
	}

	@Override
	public void update(AuditLogModel auditLogModel) {
		// TODO Auto-generated method stub
		this.auditLogDao.update(auditLogModel);
	}

	@Override
	public void delete(AuditLogModel auditLogModel) {
		// TODO Auto-generated method stub
		this.auditLogDao.delete(auditLogModel);
	}

	@Override
	public AuditLogModel searchAuditLog(String jsonInsert) {
		// TODO Auto-generated method stub
		logger.info("Searching Audit Log");
		return this.auditLogDao.searchAuditLog(jsonInsert);
	}

}
