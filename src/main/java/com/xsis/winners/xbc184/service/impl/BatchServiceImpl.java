package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.winners.xbc184.dao.BatchDao;
import com.xsis.winners.xbc184.model.BatchModel;
import com.xsis.winners.xbc184.service.BatchService;

@Service
@Transactional
public class BatchServiceImpl implements BatchService{

	@Autowired
	private BatchDao batchDao;
	
	@Override
	public void create(BatchModel batchModel) {
		// TODO Auto-generated method stub
		this.batchDao.create(batchModel);
	}

	@Override
	public void update(BatchModel batchModel) {
		// TODO Auto-generated method stub
		this.batchDao.update(batchModel);
	}

	@Override
	public void delete(BatchModel batchModel) {
		// TODO Auto-generated method stub
		this.batchDao.delete(batchModel);
	}

	@Override
	public List<BatchModel> search() {
		// TODO Auto-generated method stub
		return this.batchDao.search();
	}

	@Override
	public BatchModel searchId(Integer id) {
		// TODO Auto-generated method stub
		return this.batchDao.searchId(id);
	}

//	@Override
//	public List<BatchModel> searchNama(String namaBatch) {
//		// TODO Auto-generated method stub
//		return this.batchDao.searchNama(namaBatch);
//	}
//
//	@Override
//	public List<BatchModel> searchKode(String kodeBatch) {
//		// TODO Auto-generated method stub
//		return this.batchDao.searchKode(kodeBatch);
//	}
//	
//	@Override
//	public List<BatchModel> searchKodeOrNama(String kodeBatch, String namaBatch) {
//		// TODO Auto-generated method stub
//		return this.batchDao.searchKodeOrNama(kodeBatch, namaBatch);
//	}
//
//	@Override
//	public List<BatchModel> searchKodeEqual(String kodeBatch) {
//		// TODO Auto-generated method stub
//		return this.batchDao.searchKodeEqual(kodeBatch);
//	}
//
//	@Override
//	public List<BatchModel> searchNamaEqual(String namaBatch, Integer isDelete) {
//		// TODO Auto-generated method stub
//		return this.batchDao.searchNamaEqual(namaBatch, isDelete);
//	}

	@Override
	public void deleteTemporary(BatchModel batchModel) {
		// TODO Auto-generated method stub
		this.batchDao.update(batchModel);
	}

	@Override
	public List<BatchModel> searchTechnologyOrName(String technology, String name) {
		// TODO Auto-generated method stub
		return this.batchDao.searchTechnologyOrName(technology, name);
	}
}
