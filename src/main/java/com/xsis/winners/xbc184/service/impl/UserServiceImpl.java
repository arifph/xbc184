package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.winners.xbc184.dao.UserDao;
import com.xsis.winners.xbc184.model.UserModel;
import com.xsis.winners.xbc184.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserDao userDao;

	@Override
	public UserModel searchUsernamePassword(String username, String password) {
		// TODO Auto-generated method stub
		return this.userDao.searchUsernamePassword(username, password);
	}

	@Override
	public void create(UserModel userModel) {
		// TODO Auto-generated method stub
		this.userDao.create(userModel);
	}

	@Override
	public void update(UserModel userModel) {
		// TODO Auto-generated method stub
		this.userDao.update(userModel);
	}

	@Override
	public void delete(UserModel userModel) {
		// TODO Auto-generated method stub
		this.userDao.delete(userModel);
	}

	@Override
	public List<UserModel> search() {
		// TODO Auto-generated method stub
		return this.userDao.search();
	}

	@Override
	public UserModel searchId(Integer id) {
		// TODO Auto-generated method stub
		return this.userDao.searchId(id);
	}

	@Override
	public List<UserModel> searchUsernameOrEmail(String username, String email) {
		// TODO Auto-generated method stub
		return this.userDao.searchUsernameOrEmail(username, email);
	}

	@Override
	public void deleteTemp(UserModel userModel) {
		// TODO Auto-generated method stub
		this.userDao.deleteTemp(userModel);
	}

	@Override
	public List<UserModel> searchUsername(String username) {
		// TODO Auto-generated method stub
		return userDao.searchUsername(username);
	}

	@Override
	public List<UserModel> searchEmail(String email) {
		// TODO Auto-generated method stub
		return userDao.searchEmail(email);
	}

	@Override
	public void update(List<UserModel> userModelList) {
		// TODO Auto-generated method stub
		this.userDao.update(userModelList);
	}

	@Override
	public UserModel searchEmailX(String email) {
		// TODO Auto-generated method stub
		return userDao.searchEmailX(email);
	}

	@Override
	public void forgetPass(String email) {
		// TODO Auto-generated method stub
		this.userDao.forgetPass(email);
	}

	
	

}
