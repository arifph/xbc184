package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.winners.xbc184.dao.TrainerDao;
import com.xsis.winners.xbc184.model.TrainerModel;
import com.xsis.winners.xbc184.service.TrainerService;

@Service
@Transactional
public class TrainerServiceImpl implements TrainerService{

	@Autowired
	private TrainerDao trainerDao;
	
	@Override
	public void create(TrainerModel trainerModel) {
		// TODO Auto-generated method stub
		this.trainerDao.create(trainerModel);
	}

	@Override
	public void update(TrainerModel trainerModel) {
		// TODO Auto-generated method stub
		this.trainerDao.update(trainerModel);
	}

	@Override
	public void delete(TrainerModel trainerModel) {
		// TODO Auto-generated method stub
		this.trainerDao.delete(trainerModel);
	}

	@Override
	public List<TrainerModel> search(String kodeRole) {
		// TODO Auto-generated method stub
		return this.trainerDao.search(kodeRole);
	}

	@Override
	public TrainerModel searchId(Integer id) {
		// TODO Auto-generated method stub
		return this.trainerDao.searchId(id);
	}

	@Override
	public void deleteTemporary(TrainerModel trainerModel) {
		// TODO Auto-generated method stub
		this.trainerDao.update(trainerModel);
	}

	@Override
	public List<TrainerModel> searchNama(String name) {
		// TODO Auto-generated method stub
		return this.trainerDao.searchNama(name);
	}

	@Override
	public List<TrainerModel> search() {
		// TODO Auto-generated method stub
		return this.trainerDao.search();
	}

}
