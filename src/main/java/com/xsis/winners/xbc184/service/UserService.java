package com.xsis.winners.xbc184.service;

import java.util.List;

import com.xsis.winners.xbc184.model.UserModel;

public interface UserService {

	public UserModel searchUsernamePassword(String username, String password);
	public void create(UserModel userModel);
	public void update(UserModel userModel);
	public void delete(UserModel userModel);
	public List<UserModel> search();
	public UserModel searchId(Integer id);
	public List<UserModel> searchUsernameOrEmail(String username, String email);
	public void deleteTemp(UserModel userModel);
	public List<UserModel> searchUsername(String username);
	public List<UserModel> searchEmail(String email);
	public UserModel searchEmailX(String email);
	public void update(List<UserModel> userModelList);
	public void forgetPass(String email);
	
}
