package com.xsis.winners.xbc184.service;

import java.util.List;

import com.xsis.winners.xbc184.model.OfficeModel;

public interface OfficeService {

	public void create(OfficeModel officeModel);
	public void update(OfficeModel officeModel);
	public void delete(OfficeModel officeModel);
	
	
	public OfficeModel searchId(Integer id);//kode disamping nampilin data berdasarkan id
	public List<OfficeModel> search();
	public List<OfficeModel> searchNama(String name);//kode di sampingnampilin data atau list
		
	public List<OfficeModel> searchNameSpace();
	
	public void deleteTemporary(OfficeModel officeModel);
	
}
