package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.winners.xbc184.dao.DocumentTestDao;
import com.xsis.winners.xbc184.model.DocumentTestModel;
import com.xsis.winners.xbc184.service.DocumentTestService;

@Service
@Transactional
public class DocumentTestServiceImpl implements DocumentTestService {

	@Autowired
	private DocumentTestDao documentTestDao;
	
	@Override
	public void save(DocumentTestModel documentTest) {
		// TODO Auto-generated method stub
		this.documentTestDao.save(documentTest);
	}

	@Override
	public void update(DocumentTestModel documentTest) {
		// TODO Auto-generated method stub
		this.documentTestDao.update(documentTest);
	}

	@Override
	public void delete(DocumentTestModel documentTest) {
		// TODO Auto-generated method stub
		this.documentTestDao.delete(documentTest);
	}

	@Override
	public DocumentTestModel searchById(Integer id) {
		// TODO Auto-generated method stub
		return this.documentTestDao.searchById(id);
	}

	@Override
	public List<DocumentTestModel> searchAll() {
		// TODO Auto-generated method stub
		return this.documentTestDao.searchAll();
	}

	@Override
	public List<DocumentTestModel> searchByTestTypeId(Integer testTypeId) {
		// TODO Auto-generated method stub
		return this.documentTestDao.searchByTestTypeId(testTypeId);
	}

	@Override
	public List<DocumentTestModel> searchByTestTypeNameOrVersion(String keyword) {
		// TODO Auto-generated method stub
		return this.documentTestDao.searchByTestTypeNameOrVersion(keyword);
	}

	@Override
	public Boolean isAvailable(String token) {
		// TODO Auto-generated method stub
		return this.documentTestDao.isAvailable(token);
	}

}
