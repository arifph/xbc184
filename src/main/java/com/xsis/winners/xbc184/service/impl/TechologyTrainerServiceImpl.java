package com.xsis.winners.xbc184.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.winners.xbc184.dao.TechnologyTrainerDao;
import com.xsis.winners.xbc184.model.TechnologyTrainerModel;
import com.xsis.winners.xbc184.service.TechnologyTrainerService;

@Service
@Transactional
public class TechologyTrainerServiceImpl implements TechnologyTrainerService{
	
	@Autowired
	private TechnologyTrainerDao technologyTrainerDao;
	
	@Override
	public void create(TechnologyTrainerModel technologyTrainerModel) {
		// TODO Auto-generated method stub
		this.technologyTrainerDao.create(technologyTrainerModel);
	}
	
	@Override
	public void delete(TechnologyTrainerModel technologyTrainerModel) {
		// TODO Auto-generated method stub
		this.technologyTrainerDao.delete(technologyTrainerModel);
	}

	@Override
	public void deleteList(List<TechnologyTrainerModel> deleteTechnologyTrainerModelList) {
		// TODO Auto-generated method stub
		this.technologyTrainerDao.deleteList(deleteTechnologyTrainerModelList);
	}

	@Override
	public List<TechnologyTrainerModel> search() {
		// TODO Auto-generated method stub
		return this.technologyTrainerDao.search();
	}

	@Override
	public List<TechnologyTrainerModel> searchIdTrainer(Integer id) {
		// TODO Auto-generated method stub
		return this.technologyTrainerDao.searchIdTrainer(id);
	}

	@Override
	public TechnologyTrainerModel searchId(Integer id) {
		// TODO Auto-generated method stub
		return this.technologyTrainerDao.searchId(id);
	}

	@Override
	public void update(TechnologyTrainerModel technologyTrainerModel) {
		// TODO Auto-generated method stub
		this.technologyTrainerDao.update(technologyTrainerModel);
	}
}
