package com.xsis.winners.xbc184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "T_DOCUMENT_TEST_DETAIL")
public class DocumentTestDetailModel {

	private Integer id;
	private QuestionModel questionId;
	private DocumentTestModel documentTestId;
	private Integer createdBy;
	private Date createdOn;
	
//	private QuestionModel question;
//	private DocumentTestModel documentTestModel;
	private UserModel createdUser;
	
	@Id
	@Column(name="ID", length=11)
	@GeneratedValue(strategy=GenerationType.TABLE, generator="T_DOCUMENT_TEST_DETAIL")
	@TableGenerator(name="T_DOCUMENT_TEST_DETAIL", table="T_SEQUENCE", pkColumnName="SEQUENCE_NAME", pkColumnValue="DOCUMENT_TEST_DETAIL_ID", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name="QUESTION_ID", nullable=false)
	public QuestionModel getQuestionId() {
		return questionId;
	}
	public void setQuestionId(QuestionModel questionId) {
		this.questionId = questionId;
	}
	
	@ManyToOne()
	@JoinColumn(name="DOCUMENT_TEST_ID", nullable=false)
	@JsonBackReference
	public DocumentTestModel getDocumentTestId() {
		return documentTestId;
	}
	public void setDocumentTestId(DocumentTestModel documentTestId) {
		this.documentTestId = documentTestId;
	}
	
	@Column(name="CREATED_BY", length=11, nullable=false)
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name="CREATED_ON", nullable=false)
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
//	@ManyToOne
//	@JoinColumn(name="DOCUMENT_TEST_ID", nullable = false)
//	public DocumentTestModel getDocumentTestModel() {
//		return documentTestModel;
//	}
//	public void setDocumentTestModel(DocumentTestModel documentTestModel) {
//		this.documentTestModel = documentTestModel;
//	}
	
	@ManyToOne
	@JoinColumn(name="CREATED_BY", insertable=false, nullable=true, updatable=false)
	public UserModel getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(UserModel createdUser) {
		this.createdUser = createdUser;
	}
	
//	@ManyToOne
//	@JoinColumn(name="QUESTION_ID", insertable=false, nullable=false, updatable=false)
//	public QuestionModel getQuestion() {
//		return question;
//	}
//	public void setQuestion(QuestionModel question) {
//		this.question = question;
//	}
}
