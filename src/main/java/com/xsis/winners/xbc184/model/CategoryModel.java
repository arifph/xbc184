package com.xsis.winners.xbc184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "T_CATEGORY")
public class CategoryModel {

	private Integer id;
	private String code;
	private String name;
	private String description;
	private Integer createdBy;
	private Date createdOn;
	private Integer modifiedBy;
	private Date modifiedOn;
	private Integer deletedBy;
	private Date deletedOn;
	private Boolean isDelete;

	private UserModel createdUser;
	private UserModel modifiedUser;
	private UserModel deletedUser;

	@Id
	@Column(name = "ID", length = 11, nullable = false)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "T_CATEGORY")
	@TableGenerator(name = "T_CATEGORY", table = "T_SEQUENCE", pkColumnName = "SEQUENCE_NAME", pkColumnValue = "CATEGORY_ID", valueColumnName = "SEQUENCE_VALUE", allocationSize = 1, initialValue = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "CODE", length = 50, nullable = false)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "NAME", length = 50, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "DESCRIPTION", length = 255, nullable = false)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "CREATED_BY", length = 11, nullable = false)
	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "CREATED_ON", nullable = false)
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(name = "MODIFIED_BY", length = 11)
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Column(name = "MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	@Column(name = "DELETED_BY", length = 11)
	public Integer getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Integer deletedBy) {
		this.deletedBy = deletedBy;
	}

	@Column(name = "DELETED_ON")
	public Date getDeletedDate() {
		return deletedOn;
	}

	public void setDeletedDate(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	@Column(name = "IS_DELETE", nullable = false)
	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	@ManyToOne
	@JoinColumn(name = "CREATED_BY", insertable = false, nullable = false, updatable = false)
	public UserModel getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(UserModel createdUser) {
		this.createdUser = createdUser;
	}

	@ManyToOne
	@JoinColumn(name = "MODIFIED_BY", insertable = false, nullable = true, updatable = false)
	public UserModel getModifiedUser() {
		return modifiedUser;
	}

	public void setModifiedUser(UserModel modifiedUser) {
		this.modifiedUser = modifiedUser;
	}

	@ManyToOne
	@JoinColumn(name = "DELETED_BY", insertable = false, nullable = true, updatable = false)
	public UserModel getDeletedUser() {
		return deletedUser;
	}

	public void setDeletedUser(UserModel deletedUser) {
		this.deletedUser = deletedUser;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String string = "Category Code :" + getCode() + "\n" + "Category Name : " + getName() + "\n"
				+ "Category Description : " + getDescription() + "\n" + "Category isDelete : " + getIsDelete() + "\n"
				+ "Category Created By : " + getCreatedBy() + "\n" + "Category Created On : " + getCreatedOn() + "\n"
				+ "Category Modified By : " + getModifiedBy() + "\n" + "Category Modified On : " + getModifiedOn()
				+ "\n" + "Category Deleted By : " + getDeletedBy() + "\n" + "Category Deleted On : " + getDeletedDate();
		return string;
	}
}
