package com.xsis.winners.xbc184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="T_FEEDBACK")
public class FeedbackModel {

	private Integer idFeedback;
	
	// Join TestModel
	private Integer idTest;
	private TestModel testModel;
	
	// Join DocumentTestModel
	private Integer idDocumentTest;
	private DocumentTestModel documentTestModel;
	
	private String jsonFeedback;
	
	// Audit Trail
	private Integer xIdCreatedBy;
	private UserModel xCreatedBy;
	private Date xCreatedDate;
	
	private Integer xIdUpdatedBy;
	private UserModel xUpdatedBy;
	private Date xUpdatedDate;
	
	private Integer xIdDeletedBy;
	private UserModel xDeletedBy;
	private Date xDeletedDate;
	// Audit Trail
	
	private Boolean isDelete;
	
	@Id
	@Column(name= "ID_FEEDBACK", length= 11, nullable= false)
	@GeneratedValue(strategy=GenerationType.TABLE, generator="T_FEEDBACK") // utk buat nilai sequential
	@TableGenerator(name="T_FEEDBACK", table="T_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_FEEDBACK",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getIdFeedback() {
		return idFeedback;
	}
	public void setIdFeedback(Integer idFeedback) {
		this.idFeedback = idFeedback;
	}
	
	@Column(name= "ID_TEST", length= 11, nullable= false)
	public Integer getIdTest() {
		return idTest;
	}
	public void setIdTest(Integer idTest) {
		this.idTest = idTest;
	}
	
	@ManyToOne
	@JoinColumn(name= "ID_TEST", nullable= true, updatable= false, insertable= false)
	public TestModel getTestModel() {
		return testModel;
	}
	public void setTestModel(TestModel testModel) {
		this.testModel = testModel;
	}
	
	@Column(name= "ID_DOCUMENT_TEST", length= 11, nullable= false)
	public Integer getIdDocumentTest() {
		return idDocumentTest;
	}
	public void setIdDocumentTest(Integer idDocumentTest) {
		this.idDocumentTest = idDocumentTest;
	}
	
	@ManyToOne
	@JoinColumn(name= "ID_DOCUMENT_TEST", nullable= true, updatable= false, insertable= false)
	public DocumentTestModel getDocumentTestModel() {
		return documentTestModel;
	}
	public void setDocumentTestModel(DocumentTestModel documentTestModel) {
		this.documentTestModel = documentTestModel;
	}
	
	@Column(name="JSON_FEEDBACK", length= 5000)
	public String getJsonFeedback() {
		return jsonFeedback;
	}
	public void setJsonFeedback(String jsonFeedback) {
		this.jsonFeedback = jsonFeedback;
	}
	
	@Column(name= "IS_DELETE", nullable= false)
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	@Column(name= "X_ID_CREATED", length= 11, nullable= false)
	public Integer getxIdCreatedBy() {
		return xIdCreatedBy;
	}
	public void setxIdCreatedBy(Integer xIdCreatedBy) {
		this.xIdCreatedBy = xIdCreatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name= "X_ID_CREATED", nullable= true, updatable= false, insertable= false)
	public UserModel getxCreatedBy() {
		return xCreatedBy;
	}
	public void setxCreatedBy(UserModel xCreatedBy) {
		this.xCreatedBy = xCreatedBy;
	}
	
	@Column(name= "X_CREATED_DATE")
	public Date getxCreatedDate() {
		return xCreatedDate;
	}
	public void setxCreatedDate(Date xCreatedDate) {
		this.xCreatedDate = xCreatedDate;
	}
	
	@Column(name= "X_ID_UPDATED", length= 11)
	public Integer getxIdUpdatedBy() {
		return xIdUpdatedBy;
	}
	public void setxIdUpdatedBy(Integer xIdUpdatedBy) {
		this.xIdUpdatedBy = xIdUpdatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name= "X_ID_UPDATED", nullable= true, updatable= false, insertable= false)
	public UserModel getxUpdatedBy() {
		return xUpdatedBy;
	}
	public void setxUpdatedBy(UserModel xUpdatedBy) {
		this.xUpdatedBy = xUpdatedBy;
	}
	
	@Column(name= "X_UPDATED_DATE")
	public Date getxUpdatedDate() {
		return xUpdatedDate;
	}
	public void setxUpdatedDate(Date xUpdatedDate) {
		this.xUpdatedDate = xUpdatedDate;
	}
	
	@Column(name= "X_ID_DELETED", length= 11)
	public Integer getxIdDeletedBy() {
		return xIdDeletedBy;
	}
	public void setxIdDeletedBy(Integer xIdDeletedBy) {
		this.xIdDeletedBy = xIdDeletedBy;
	}
	
	@ManyToOne
	@JoinColumn(name= "X_ID_DELETED", nullable= true, updatable= false, insertable= false)
	public UserModel getxDeletedBy() {
		return xDeletedBy;
	}
	public void setxDeletedBy(UserModel xDeletedBy) {
		this.xDeletedBy = xDeletedBy;
	}
	
	@Column(name= "X_DELETED_DATE")
	public Date getxDeletedDate() {
		return xDeletedDate;
	}
	public void setxDeletedDate(Date xDeletedDate) {
		this.xDeletedDate = xDeletedDate;
	}
	
}
