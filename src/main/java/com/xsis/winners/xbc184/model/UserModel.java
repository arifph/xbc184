package com.xsis.winners.xbc184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="T_USER")
public class UserModel {

	private Integer id;
	private String username;
	private String password;
	private String email;
	private Integer roleId;
	private RoleModel roleModel;
	private Boolean mobileFlag;
	private Integer mobileToken;
	
	private Integer createdBy;
	private UserModel created;
	private Date createdOn;
	private Integer modifiedBy;
	private UserModel modified;
	private Date modifiedOn;
	private Integer deletedBy;
	private UserModel deleted;
	private Date deletedOn;
	
	private Boolean isDelete;
	
	
	@Id // menandakan primary key
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="T_USER") // utk buat nilai sequential
	@TableGenerator(name="T_USER", table="T_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_USER",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="USERNAME")
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@Column(name="PASSWORD")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Column(name="EMAIL")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Column(name="ROLE_ID")
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	@ManyToOne
	@JoinColumn(name="ROLE_ID", nullable=true, updatable=false, insertable=false)
	public RoleModel getRoleModel() {
		return roleModel;
	}
	public void setRoleModel(RoleModel roleModel) {
		this.roleModel = roleModel;
	}
	@Column(name="MOBILE_FLAG")
	public Boolean getMobileFlag() {
		return mobileFlag;
	}
	public void setMobileFlag(Boolean mobileFlag) {
		this.mobileFlag = mobileFlag;
	}
	@Column(name="MOBILE_TOKEN", length=9)
	public Integer getMobileToken() {
		return mobileToken;
	}
	public void setMobileToken(Integer mobileToken) {
		this.mobileToken = mobileToken;
	}
	
	@Column(name="CREATED_BY")
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	@ManyToOne
	@JoinColumn(name="CREATED_BY", nullable=true, updatable=false, insertable=false)
	public UserModel getCreated() {
		return created;
	}
	public void setCreated(UserModel created) {
		this.created = created;
	}
	
	@Column(name="CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(name="MODIFIED_BY")
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	@ManyToOne
	@JoinColumn(name="MODIFIED_BY", nullable=true, updatable=false, insertable=false)
	public UserModel getModified() {
		return modified;
	}
	public void setModified(UserModel modified) {
		this.modified = modified;
	}
	
	@Column(name="MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	@Column(name="DELETED_BY")
	public Integer getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(Integer deletedBy) {
		this.deletedBy = deletedBy;
	}
	@ManyToOne
	@JoinColumn(name="DELETED_BY", nullable=true, updatable=false, insertable=false)
	public UserModel getDeleted() {
		return deleted;
	}
	public void setDeleted(UserModel deleted) {
		this.deleted = deleted;
	}
	
	@Column(name="DELETED_ON")
	public Date getDeletedOn() {
		return deletedOn;
	}
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}
	//@org.hibernate.annotations.Type(type="true_false")
	@Column(name="IS_DELETE")
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	
}
