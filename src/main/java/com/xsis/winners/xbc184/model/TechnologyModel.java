package com.xsis.winners.xbc184.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="t_technology")
public class TechnologyModel {
	
	private Integer id;
	private String name;
	private String notes;
	
	private Integer createdBy;
	private Date createdOn;
	
	private Integer modifiedBy;
	private Date modifiedOn;
	
	private Integer deletedBy;
	private Date deletedOn;
	
	private Boolean isDelete;
	
	private List<TechnologyTrainerModel> technologyTrainerModelList;
	
	private UserModel xCreatedBy;

	@Id
	@Column(name="id", nullable=false)
	@GeneratedValue(strategy=GenerationType.TABLE, generator="t_trainer")
	@TableGenerator(name="t_trainer", table="T_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="TECHNOLOGY_ID",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="name", nullable=false)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Column(name="notes", nullable=true)
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Column(name="created_by", nullable=false)
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name="created_on", nullable=false)
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date date) {
		this.createdOn = date;
	}

	@Column(name="modified_by", nullable=true)
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Column(name="modified_on", nullable=true)
	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	@Column(name="deleted_by", nullable=true)
	public Integer getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(Integer deletedBy) {
		this.deletedBy = deletedBy;
	}

	@Column(name="deleted_on", nullable=true)
	public Date getDeletedOn() {
		return deletedOn;
	}
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	@Column(name="is_deleted", nullable=false)
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	@OneToMany(mappedBy="technologyModel", cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JsonManagedReference
	public List<TechnologyTrainerModel> getTechnologyTrainerModelList() {
		return technologyTrainerModelList;
	}

	public void setTechnologyTrainerModelList(List<TechnologyTrainerModel> technologyTrainerModelList) {
		this.technologyTrainerModelList = technologyTrainerModelList;
	}
	
	@ManyToOne
	@JoinColumn(name="created_by", nullable=false, updatable=false, insertable=false)
	public UserModel getxCreatedBy() {
		return xCreatedBy;
	}

	public void setxCreatedBy(UserModel xCreatedBy) {
		this.xCreatedBy = xCreatedBy;
	}
}
