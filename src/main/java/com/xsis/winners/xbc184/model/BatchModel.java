package com.xsis.winners.xbc184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="T_BATCH")
public class BatchModel {
	
	private Integer id; // PK
	private String name;
	private Date periodFrom;
	private Date periodTo;
	private String notes;
	private Boolean isDelete;
	
	//join table TechnologyModel
	private Integer technologyId; // FK dari TechnologyModel
	private TechnologyModel technologyModel;
	
	//join table TrainerModel
	private Integer trainerId; // FK dari TrainerModel
	private TrainerModel trainerModel;
	
	//join table RoomModel
	private Integer roomId; // FK dari RoomModel
	private RoomModel roomModel;
	
	//join table BootcampTypeModel
	private Integer bootcampTypeId; // FK dari BootcampTypeModel
	private BootcampTypeModel bootcampTypeModel;
	
	// Audit trail
	private Integer createdBy; // FK T_USER
	private UserModel xCreatedBy; // datetime
	private Date createdOn;
	
	private Integer modifiedBy; // FK T_USER
	private UserModel xModifiedBy; // datetime
	private Date modifiedOn;
	
	private Integer deletedBy; // FK T_USER
	private UserModel xDeletedBy; // datetime
	private Date deletedOn;
	// Audit trail
	
	@Id //untuk primary key
	@Column(name="ID", length=11)
	@GeneratedValue(strategy=GenerationType.TABLE, generator="T_BATCH") // utk autoincrement
	@TableGenerator(name="T_BATCH", table="T_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="BATCH_ID",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="TECHNOLOGY_ID", length=11, nullable=false) // FK
	public Integer getTechnologyId() {
		return technologyId;
	}
	public void setTechnologyId(Integer technologyId) {
		this.technologyId = technologyId;
	}
	
	@ManyToOne
	@JoinColumn(name="TECHNOLOGY_ID", nullable=false, updatable=false, insertable=false)
	public TechnologyModel getTechnologyModel() {
		return technologyModel;
	}
	public void setTechnologyModel(TechnologyModel technologyModel) {
		this.technologyModel = technologyModel;
	}
	
	@Column(name="TRAINER_ID", length=11, nullable=false) // FK
	public Integer getTrainerId() {
		return trainerId;
	}
	public void setTrainerId(Integer trainerId) {
		this.trainerId = trainerId;
	}
	
	@ManyToOne
	@JoinColumn(name="TRAINER_ID", nullable=true, updatable=false, insertable=false)
	public TrainerModel getTrainerModel() {
		return trainerModel;
	}
	public void setTrainerModel(TrainerModel trainerModel) {
		this.trainerModel = trainerModel;
	}
	
	@Column(name="NAME", length=255, nullable=false)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Column(name="PERIOD_FROM")
	public Date getPeriodFrom() {
		return periodFrom;
	}
	public void setPeriodFrom(Date periodFrom) {
		this.periodFrom = periodFrom;
	}
	
	@Column(name="PERIOD_TO")
	public Date getPeriodTo() {
		return periodTo;
	}
	public void setPeriodTo(Date periodTo) {
		this.periodTo = periodTo;
	}
	
	@Column(name="ROOM_ID", length=11)
	public Integer getRoomId() {
		return roomId;
	}
	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}
	
	@ManyToOne
	@JoinColumn(name="ROOM_ID", nullable=true, updatable=false, insertable=false)
	public RoomModel getRoomModel() {
		return roomModel;
	}
	public void setRoomModel(RoomModel roomModel) {
		this.roomModel = roomModel;
	}
	
	@Column(name="BOOTCAMP_TYPE_ID", length=11)
	public Integer getBootcampTypeId() {
		return bootcampTypeId;
	}
	public void setBootcampTypeId(Integer bootcampTypeId) {
		this.bootcampTypeId = bootcampTypeId;
	}
	
	@ManyToOne
	@JoinColumn(name="BOOTCAMP_TYPE_ID", nullable=true, updatable=false, insertable=false)
	public BootcampTypeModel getBootcampTypeModel() {
		return bootcampTypeModel;
	}
	public void setBootcampTypeModel(BootcampTypeModel bootcampTypeModel) {
		this.bootcampTypeModel = bootcampTypeModel;
	}
	
	@Column(name="NOTES", length=255)
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@Column(name="CREATED_BY", length=11)
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	@ManyToOne
	@JoinColumn(name="CREATED_BY", nullable=true, updatable=false, insertable=false)
	public UserModel getxCreatedBy() {
		return xCreatedBy;
	}
	public void setxCreatedBy(UserModel xCreatedBy) {
		this.xCreatedBy = xCreatedBy;
	}
	
	@Column(name="CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(name="MODIFIED_BY", length=11)
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="MODIFIED_BY", nullable=true, updatable=false, insertable=false)
	public UserModel getxModifiedBy() {
		return xModifiedBy;
	}
	public void setxModifiedBy(UserModel xModifiedBy) {
		this.xModifiedBy = xModifiedBy;
	}
	
	@Column(name="MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	@Column(name="DELETED_BY", length=11)
	public Integer getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(Integer deletedBy) {
		this.deletedBy = deletedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="DELETED_BY", nullable=true, updatable=false, insertable=false)
	public UserModel getxDeletedBy() {
		return xDeletedBy;
	}
	public void setxDeletedBy(UserModel xDeletedBy) {
		this.xDeletedBy = xDeletedBy;
	}
	
	@Column(name="DELETED_ON")
	public Date getDeletedOn() {
		return deletedOn;
	}
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}
	
	@Column(name="IS_DELETE", nullable=false)
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
}
