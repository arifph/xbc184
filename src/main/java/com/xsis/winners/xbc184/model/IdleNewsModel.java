package com.xsis.winners.xbc184.model;
import java.util.Date;

	import javax.persistence.Column;
	import javax.persistence.Entity;
	import javax.persistence.GeneratedValue;
	import javax.persistence.GenerationType;
	import javax.persistence.Id;
	import javax.persistence.JoinColumn;
	import javax.persistence.ManyToOne;
	import javax.persistence.Table;
	import javax.persistence.TableGenerator;

	@Entity
	@Table(name="T_IDLE_NEWS")
	public class IdleNewsModel {
		
		private Integer idIdleNews;
		private String tittleIdleNews;
		private String contentIdleNews;
		
		private Boolean isDelete;
		
		//audit trail
		private Integer xIdCreatedBy;
		private UserModel xCreatedBy;
		private Date xCreatedDate;
		private Integer xIdModifiedBy;
		private UserModel xModifiedBy;
		private Date xModifiedDate;
		private Integer xIdDeletedBy;
		private UserModel xDeletedBy;
		private Date xDeletedDate;
		private Integer categoryId;
		private CategoryModel categoryModel;
		
		@Id
		@Column(name="ID")
		@GeneratedValue(strategy=GenerationType.TABLE, generator="T_IDLE_NEWS")
		@TableGenerator(name="T_IDLE_NEWS", table="T_SEQUENCE",
						pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_IDLE_NEWS",
						valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
		public Integer getIdIdleNews() {
			return idIdleNews;
		}
		public void setIdIdleNews(Integer idIdleNews) {
			this.idIdleNews = idIdleNews;
		}
		
		
		@Column(name="IS_DELETE")
		public Boolean getIsDelete() {
			return isDelete;
		}
		public void setIsDelete(Boolean isDelete) {
			this.isDelete = isDelete;
		}
		
		@Column(name="X_ID_CREATED")
		public Integer getxIdCreatedBy() {
			return xIdCreatedBy;
		}
		public void setxIdCreatedBy(Integer xIdCreatedBy) {
			this.xIdCreatedBy = xIdCreatedBy;
		}
		
		@ManyToOne
		@JoinColumn(name="X_ID_CREATED", nullable=true, updatable=false, insertable=false)
		public UserModel getxCreatedBy() {
			return xCreatedBy;
		}
		public void setxCreatedBy(UserModel xCreatedBy) {
			this.xCreatedBy = xCreatedBy;
		}
		
		@Column(name="X_CREATED_DATE")
		public Date getxCreatedDate() {
			return xCreatedDate;
		}
		public void setxCreatedDate(Date xCreatedDate) {
			this.xCreatedDate = xCreatedDate;
		}
		
		@Column(name="X_ID_MODIFIED")
		public Integer getxIdModifiedBy() {
			return xIdModifiedBy;
		}
		public void setxIdModifiedBy(Integer xIdModifiedBy) {
			this.xIdModifiedBy = xIdModifiedBy;
		}
		
		@ManyToOne
		@JoinColumn(name="X_ID_MODIFIED", nullable=true, updatable=false, insertable=false)
		public UserModel getxModifiedBy() {
			return xModifiedBy;
		}
		public void setxModifiedBy(UserModel xModifiedBy) {
			this.xModifiedBy = xModifiedBy;
		}
		
		@Column(name="X_MODIFIED_DATE")
		public Date getxModifiedDate() {
			return xModifiedDate;
		}
		public void setxModifiedDate(Date xModifiedDate) {
			this.xModifiedDate = xModifiedDate;
		}
		
		@Column(name="X_ID_DELETED")
		public Integer getxIdDeletedBy() {
			return xIdDeletedBy;
		}
		public void setxIdDeletedBy(Integer xIdDeletedBy) {
			this.xIdDeletedBy = xIdDeletedBy;
		}
		
		@ManyToOne
		@JoinColumn(name="X_ID_DELETED", nullable=true, updatable=false, insertable=false)
		public UserModel getxDeletedBy() {
			return xDeletedBy;
		}
		public void setxDeletedBy(UserModel xDeletedBy) {
			this.xDeletedBy = xDeletedBy;
		}
		
		@Column(name="X_DELETED_DATE")
		public Date getxDeletedDate() {
			return xDeletedDate;
		}
		public void setxDeletedDate(Date xDeletedDate) {
			this.xDeletedDate = xDeletedDate;
		}
		@Column(name="TITTLE")
		public String getTittleIdleNews() {
			return tittleIdleNews;
		}
		public void setTittleIdleNews(String tittleIdleNews) {
			this.tittleIdleNews = tittleIdleNews;
		}
		
	
		
		@Column(name="CONTENT")
		public String getContentIdleNews() {
			return contentIdleNews;
		}
		public void setContentIdleNews(String contentIdleNews) {
			this.contentIdleNews = contentIdleNews;
		}
		@Column(name="CATEGORY_ID")
		public Integer getCategoryId() {
			return categoryId;
		}
		public void setCategoryId(Integer categoryId) {
			this.categoryId = categoryId;
		}
		@ManyToOne
		@JoinColumn(name="CATEGORY_ID	", nullable=true, updatable=false, insertable=false)
		public CategoryModel getCategoryModel() {
			return categoryModel;
		}
		public void setCategoryModel(CategoryModel categoryModel) {
			this.categoryModel = categoryModel;
		}
		public void setCategoryIdleNews(String categoryIdleNews) {
			// TODO Auto-generated method stub
			
		}
		
		
		

	}


