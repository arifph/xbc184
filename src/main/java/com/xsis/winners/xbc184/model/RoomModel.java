package com.xsis.winners.xbc184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
@Table(name="T_ROOM")
public class RoomModel {

	private Integer id;
	private String code;
	private String name;
	private Integer capacity;
	private Boolean anyProjector;
	private String notes;
	
	//join table Office
	private Integer officeId;
	private OfficeModel officeModel;
	
	private Integer createdBy;
	private Date createdOn;
	
	private Integer modifiedBy;
	private Date modifiedOn;
	
	private Integer deletedBy;
	private Date deletedOn;
	
	private Boolean isDelete;
	
	
	@Id
	@Column(name="ID_ROOM", length=11)
	@GeneratedValue(strategy=GenerationType.TABLE, generator="T_ROOM") // utk buat nilai sequential
	@TableGenerator(name="T_ROOM", table="T_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_ROOM",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="CODE", length=11, nullable=false)
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	@Column(name="NAME", length=50, nullable=false)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="CAPACITY", length=5, nullable=false)
	public Integer getCapacity() {
		return capacity;
	}
	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}
	
	@Column(name="NOTES", length=500, nullable=true)
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@Column(name="OFFICE_ID")
	public Integer getOfficeId() {
		return officeId;
	}
	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}
	
	@ManyToOne
	@JoinColumn(name="OFFICE_ID", nullable=true, updatable=false, insertable=false)
	@JsonBackReference
	public OfficeModel getOfficeModel() {
		return officeModel;
	}
	public void setOfficeModel(OfficeModel officeModel) {
		this.officeModel = officeModel;
	}
	
	@Column(name="CREATED_BY",length=11)
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name="CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(name="MODIFIED_BY",length=11)
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	@Column(name="MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	@Column(name="DELETED_BY",length=11)
	public Integer getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(Integer deletedBy) {
		this.deletedBy = deletedBy;
	}
	
	@Column(name="DELETED_ON")
	public Date getDeletedOn() {
		return deletedOn;
	}
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}
	
	@Column(name="IS_DELETE")
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	@Column(name="ANY_PROJECTOR", nullable=false)
	public Boolean getAnyProjector() {
		return anyProjector;
	}
	public void setAnyProjector(Boolean anyProjector) {
		this.anyProjector = anyProjector;
	}
	
}
