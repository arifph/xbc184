package com.xsis.winners.xbc184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="T_MENU")
public class MenuModel {

	private Integer id;
	private String code;
	private String title;
	private String description;
	private String imageUrl;
	private Integer menuOrder;
	private Integer menuParent;
	private MenuModel menuParentBy;
	private String menuUrl;
	
	private Integer createdBy;
	private UserModel created;
	private Date createdOn;
	private Integer modifiedBy;
	private UserModel modified;
	private Date modifiedOn;
	private Integer deletedBy;
	private UserModel deleted;
	private Date deletedOn;
	
	private Boolean isDelete;

	
		
	@Id // menandakan primary key
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="T_MENU") // utk buat nilai sequential
	@TableGenerator(name="T_MENU", table="T_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_MENU",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="CODE", length=50, nullable=false)
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	@Column(name="TITLE", length=50, nullable=true)
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Column(name="DESCRIPTION", length=255, nullable=true)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name="IMAGE_URL", length=100)
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	@Column(name="MENU_ORDER", nullable=false)
	public Integer getMenuOrder() {
		return menuOrder;
	}
	public void setMenuOrder(Integer menuOrder) {
		this.menuOrder = menuOrder;
	}
	
	@Column(name="MENU_PARENT", length=11)
	public Integer getMenuParent() {
		return menuParent;
	}
	public void setMenuParent(Integer menuParent) {
		this.menuParent = menuParent;
	}
	
	@ManyToOne
	@JoinColumn(name="MENU_PARENT", nullable=true, updatable=false, insertable=false)
	public MenuModel getMenuParentBy() {
		return menuParentBy;
	}
	public void setMenuParentBy(MenuModel menuParentBy) {
		this.menuParentBy = menuParentBy;
	}
	@Column(name="MENU_URL", length=100, nullable=false)
	public String getMenuUrl() {
		return menuUrl;
	}
	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}
	@Column(name="CREATED_BY")
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	@ManyToOne
	@JoinColumn(name="CREATED_BY", nullable=true, updatable=false, insertable=false)
	public UserModel getCreated() {
		return created;
	}
	public void setCreated(UserModel created) {
		this.created = created;
	}
	
	@Column(name="CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(name="MODIFIED_BY")
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	@ManyToOne
	@JoinColumn(name="MODIFIED_BY", nullable=true, updatable=false, insertable=false)
	public UserModel getModified() {
		return modified;
	}
	public void setModified(UserModel modified) {
		this.modified = modified;
	}
	
	@Column(name="MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	@Column(name="DELETED_BY")
	public Integer getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(Integer deletedBy) {
		this.deletedBy = deletedBy;
	}
	@ManyToOne
	@JoinColumn(name="DELETED_BY", nullable=true, updatable=false, insertable=false)
	public UserModel getDeleted() {
		return deleted;
	}
	public void setDeleted(UserModel deleted) {
		this.deleted = deleted;
	}
	@Column(name="DELETED_ON")
	public Date getDeletedOn() {
		return deletedOn;
	}
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}
	//@org.hibernate.annotations.Type(type="true_false")
	@Column(name="IS_DELETE")
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	
}
