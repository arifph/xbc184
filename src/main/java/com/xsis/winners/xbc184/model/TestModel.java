package com.xsis.winners.xbc184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="T_TEST")
public class TestModel {
	
	private Integer idTest;
	private String nameTest;
	private String notesTest;
	
	private Boolean isDelete;
	private Boolean isBootcampTest;
	
	private Integer xIdCreatedBy;
	private UserModel xCreatedBy;
	private Date xCreatedDate;
	
	private Integer xIdModifiedBy;
	private UserModel xModifiedBy;
	private Date xModifiedDate;
	
	private Integer xIdDeletedBy;
	private UserModel xDeletedBy;
	private Date xDeletedDate;
	
	@Id
	@Column(name="ID_TEST")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="T_TEST")
	@TableGenerator(name="T_TEST", table="T_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_TEST",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getIdTest() {
		return idTest;
	}
	public void setIdTest(Integer idTest) {
		this.idTest = idTest;
	}
	
	@Column(name="NAME_TEST")
	public String getNameTest() {
		return nameTest;
	}
	public void setNameTest(String nameTest) {
		this.nameTest = nameTest;
	}
	
	@Column(name="NOTES_TEST")
	public String getNotesTest() {
		return notesTest;
	}
	public void setNotesTest(String notesTest) {
		this.notesTest = notesTest;
	}
	
	@Column(name="IS_DELETE")
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	@Column(name="IS_BOOTCAMP_TEST")
	public Boolean getIsBootcampTest() {
		return isBootcampTest;
	}
	public void setIsBootcampTest(Boolean isBootcampTest) {
		this.isBootcampTest = isBootcampTest;
	}
	
	@Column(name="X_ID_CREATED")
	public Integer getxIdCreatedBy() {
		return xIdCreatedBy;
	}
	public void setxIdCreatedBy(Integer xIdCreatedBy) {
		this.xIdCreatedBy = xIdCreatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_CREATED", nullable=true , updatable=false , insertable=false)
	public UserModel getxCreatedBy() {
		return xCreatedBy;
	}
	public void setxCreatedBy(UserModel xCreatedBy) {
		this.xCreatedBy = xCreatedBy;
	}
	
	@Column(name="X_CREATED_DATE")
	public Date getxCreatedDate() {
		return xCreatedDate;
	}
	public void setxCreatedDate(Date xCreatedDate) {
		this.xCreatedDate = xCreatedDate;
	}
	
	@Column(name="X_ID_MODIFIED")
	public Integer getxIdModifiedBy() {
		return xIdModifiedBy;
	}
	public void setxIdModifiedBy(Integer xIdModifiedBy) {
		this.xIdModifiedBy = xIdModifiedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_MODIFIED", nullable=true, updatable=false, insertable=false)
	public UserModel getxModifiedBy() {
		return xModifiedBy;
	}
	public void setxModifiedBy(UserModel xModifiedBy) {
		this.xModifiedBy = xModifiedBy;
	}
	
	@Column(name="X_MODIFIED_DATE")
	public Date getxModifiedDate() {
		return xModifiedDate;
	}
	public void setxModifiedDate(Date xModifiedDate) {
		this.xModifiedDate = xModifiedDate;
	}
	
	@Column(name="X_ID_DELETED")
	public Integer getxIdDeletedBy() {
		return xIdDeletedBy;
	}
	public void setxIdDeletedBy(Integer xIdDeletedBy) {
		this.xIdDeletedBy = xIdDeletedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_DELETED", nullable=true, updatable=false, insertable=false)
	public UserModel getxDeletedBy() {
		return xDeletedBy;
	}
	public void setxDeletedBy(UserModel xDeletedBy) {
		this.xDeletedBy = xDeletedBy;
	}
	
	@Column(name="X_DELETED_DATE")
	public Date getxDeletedDate() {
		return xDeletedDate;
	}
	public void setxDeletedDate(Date xDeletedDate) {
		this.xDeletedDate = xDeletedDate;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idTest == null) ? 0 : idTest.hashCode());
		result = prime * result + ((isBootcampTest == null) ? 0 : isBootcampTest.hashCode());
		result = prime * result + ((isDelete == null) ? 0 : isDelete.hashCode());
		result = prime * result + ((nameTest == null) ? 0 : nameTest.hashCode());
		result = prime * result + ((notesTest == null) ? 0 : notesTest.hashCode());
		result = prime * result + ((xCreatedBy == null) ? 0 : xCreatedBy.hashCode());
		result = prime * result + ((xCreatedDate == null) ? 0 : xCreatedDate.hashCode());
		result = prime * result + ((xDeletedBy == null) ? 0 : xDeletedBy.hashCode());
		result = prime * result + ((xDeletedDate == null) ? 0 : xDeletedDate.hashCode());
		result = prime * result + ((xIdCreatedBy == null) ? 0 : xIdCreatedBy.hashCode());
		result = prime * result + ((xIdDeletedBy == null) ? 0 : xIdDeletedBy.hashCode());
		result = prime * result + ((xIdModifiedBy == null) ? 0 : xIdModifiedBy.hashCode());
		result = prime * result + ((xModifiedBy == null) ? 0 : xModifiedBy.hashCode());
		result = prime * result + ((xModifiedDate == null) ? 0 : xModifiedDate.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TestModel other = (TestModel) obj;
		if (idTest == null) {
			if (other.idTest != null)
				return false;
		} else if (!idTest.equals(other.idTest))
			return false;
		if (isBootcampTest == null) {
			if (other.isBootcampTest != null)
				return false;
		} else if (!isBootcampTest.equals(other.isBootcampTest))
			return false;
		if (isDelete == null) {
			if (other.isDelete != null)
				return false;
		} else if (!isDelete.equals(other.isDelete))
			return false;
		if (nameTest == null) {
			if (other.nameTest != null)
				return false;
		} else if (!nameTest.equals(other.nameTest))
			return false;
		if (notesTest == null) {
			if (other.notesTest != null)
				return false;
		} else if (!notesTest.equals(other.notesTest))
			return false;
		if (xCreatedBy == null) {
			if (other.xCreatedBy != null)
				return false;
		} else if (!xCreatedBy.equals(other.xCreatedBy))
			return false;
		if (xCreatedDate == null) {
			if (other.xCreatedDate != null)
				return false;
		} else if (!xCreatedDate.equals(other.xCreatedDate))
			return false;
		if (xDeletedBy == null) {
			if (other.xDeletedBy != null)
				return false;
		} else if (!xDeletedBy.equals(other.xDeletedBy))
			return false;
		if (xDeletedDate == null) {
			if (other.xDeletedDate != null)
				return false;
		} else if (!xDeletedDate.equals(other.xDeletedDate))
			return false;
		if (xIdCreatedBy == null) {
			if (other.xIdCreatedBy != null)
				return false;
		} else if (!xIdCreatedBy.equals(other.xIdCreatedBy))
			return false;
		if (xIdDeletedBy == null) {
			if (other.xIdDeletedBy != null)
				return false;
		} else if (!xIdDeletedBy.equals(other.xIdDeletedBy))
			return false;
		if (xIdModifiedBy == null) {
			if (other.xIdModifiedBy != null)
				return false;
		} else if (!xIdModifiedBy.equals(other.xIdModifiedBy))
			return false;
		if (xModifiedBy == null) {
			if (other.xModifiedBy != null)
				return false;
		} else if (!xModifiedBy.equals(other.xModifiedBy))
			return false;
		if (xModifiedDate == null) {
			if (other.xModifiedDate != null)
				return false;
		} else if (!xModifiedDate.equals(other.xModifiedDate))
			return false;
		return true;
	}
	
	
}
