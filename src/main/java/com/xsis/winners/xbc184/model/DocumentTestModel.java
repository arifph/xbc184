package com.xsis.winners.xbc184.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "T_DOCUMENT_TEST")
public class DocumentTestModel {

	private Integer id;
	private Integer testId;
	private Integer testTypeId;
	private Integer version;
	private String token;
	private Integer createdBy;
	private Date createdOn;
	private Integer modifiedBy;
	private Date modifiedOn;
	private Integer deletedBy;
	private Date deletedOn;
	private Boolean isDelete;
	private TestModel test;
	private TestTypeModel testType;
	private UserModel createdUser;
	private UserModel modifiedUser;
	private UserModel deletedUser;
	private List<DocumentTestDetailModel> documentTestDetailModel;

	@Id
	@Column(name = "ID", length = 11)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "T_DOCUMENT_TEST")
	@TableGenerator(name = "T_DOCUMENT_TEST", table = "T_SEQUENCE", pkColumnName = "SEQUENCE_NAME", pkColumnValue = "DOCUMENT_TEST_ID", valueColumnName = "SEQUENCE_VALUE", allocationSize = 1, initialValue = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="TEST_ID", nullable=false)
	public Integer getTestId() {
		return testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	@Column(name = "TEST_TYPE_ID", nullable = false)
	public Integer getTestTypeId() {
		return testTypeId;
	}

	public void setTestTypeId(Integer testTypeId) {
		this.testTypeId = testTypeId;
	}

	@Column(name = "VERSION", length = 11, nullable = false)
	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Column(name = "TOKEN", length = 10, nullable = false)
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Column(name = "CREATED_BY", length = 11, nullable = false)
	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "CREATED_ON", nullable = false)
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(name = "MODIFIED_BY", length = 11)
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Column(name = "MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	@Column(name = "DELETED_BY", length = 11)
	public Integer getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Integer deletedBy) {
		this.deletedBy = deletedBy;
	}

	@Column(name = "DELETED_ON")
	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	@Column(name = "IS_DELETE", nullable = false)
	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	@ManyToOne
	@JoinColumn(name="TEST_ID", nullable=false, insertable = false, updatable = false)
	public TestModel getTest() {
		return test;
	}

	public void setTest(TestModel test) {
		this.test = test;
	}

	@ManyToOne
	@JoinColumn(name="TEST_TYPE_ID", nullable=false, insertable = false, updatable = false)
	public TestTypeModel getTestType() {
		return testType;
	}

	public void setTestType(TestTypeModel testType) {
		this.testType = testType;
	}

	@ManyToOne
	@JoinColumn(name = "CREATED_BY", nullable = true, insertable = false, updatable = false)
	public UserModel getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(UserModel createdUser) {
		this.createdUser = createdUser;
	}

	@ManyToOne
	@JoinColumn(name = "MODIFY_BY", nullable = true, insertable = false, updatable = false)
	public UserModel getModifiedUser() {
		return modifiedUser;
	}

	public void setModifiedUser(UserModel modifiedUser) {
		this.modifiedUser = modifiedUser;
	}

	@ManyToOne
	@JoinColumn(name = "DELETED_BY", nullable = true, insertable = false, updatable = false)
	public UserModel getDeletedUser() {
		return deletedUser;
	}

	public void setDeletedUser(UserModel deletedUser) {
		this.deletedUser = deletedUser;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "documentTestId", cascade = CascadeType.ALL, orphanRemoval=true)
	@JsonManagedReference
	public List<DocumentTestDetailModel> getDocumentTestDetailModel() {
		return this.documentTestDetailModel;
	}

	public void setDocumentTestDetailModel(List<DocumentTestDetailModel> documentTestDetailModel) {
		this.documentTestDetailModel = documentTestDetailModel;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String documentTest = "ID : " + getId() + "\n" + "test ID : " + getTestId() + "\n" + "test Type ID : "
				+ getTestTypeId() + "\n" + "version : " + getVersion() + "\n" + "token : " + getToken() + "\n"
				+ "created by : " + getCreatedBy() + "\n" + "created on : " + getCreatedOn() + "\n" + "modified by : "
				+ getModifiedBy() + "\n" + "modified on : " + getModifiedOn() + "\n" + "deleted by : " + getDeletedBy()
				+ "\n" + "deleted on : " + getDeletedOn() + "\n";
		return documentTest;
	}
}
