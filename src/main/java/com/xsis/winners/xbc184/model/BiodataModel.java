package com.xsis.winners.xbc184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="T_BIODATA")
public class BiodataModel {
	
	private Integer id; // PK
	private String name;
	private String gender;
	private String lastEducation;
	private String graduationYear;
	private String educationalLevel;
	private String majors;
	private String gpa;
	private Integer iq;
	private String du;
	private Integer arithmetic;
	private Integer nestedLogic;
	private Integer joinTable;
	private String tro;
	private String notes;
	private String interviewer;
	private Boolean isDelete;
	
	//join table BootcampTestTypeModel
	private Integer bootcampTestType; // FK dari BootcampTestTypeModel
	private BootcampTestTypeModel bootcampTestTypeModel;
	
	// Audit trail
	private Integer createdBy; // FK T_USER
	private UserModel xCreatedBy; // datetime
	private Date createdOn;
	
	private Integer modifiedBy; // FK T_USER
	private UserModel xModifiedBy; // datetime
	private Date modifiedOn;
	
	private Integer deletedBy; // FK T_USER
	private UserModel xDeletedBy; // datetime
	private Date deletedOn;
	// Audit trail
	
	@Id //untuk primary key
	@Column(name="ID", length=11)
	@GeneratedValue(strategy=GenerationType.TABLE, generator="T_BIODATA") // utk autoincrement
	@TableGenerator(name="T_BIODATA", table="T_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="BIODATA_ID",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="NAME", length=255, nullable=false)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="GENDER", length=5)
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	@Column(name="LAST_EDUCATION", length=100, nullable=false)
	public String getLastEducation() {
		return lastEducation;
	}
	public void setLastEducation(String lastEducation) {
		this.lastEducation = lastEducation;
	}
	
	@Column(name="GRADUATION_YEAR", length=5, nullable=false)
	public String getGraduationYear() {
		return graduationYear;
	}
	public void setGraduationYear(String graduationYear) {
		this.graduationYear = graduationYear;
	}
	
	@Column(name="EDUCATIONAL_LEVEL", length=5, nullable=false)
	public String getEducationalLevel() {
		return educationalLevel;
	}
	public void setEducationalLevel(String educationalLevel) {
		this.educationalLevel = educationalLevel;
	}
	
	@Column(name="MAJORS", length=100, nullable=false)
	public String getMajors() {
		return majors;
	}
	public void setMajors(String majors) {
		this.majors = majors;
	}
	
	@Column(name="GPA", length=5, nullable=false)
	public String getGpa() {
		return gpa;
	}
	public void setGpa(String gpa) {
		this.gpa = gpa;
	}
	
	@Column(name="BOOTCAMP_TEST_TYPE", length=11)
	public Integer getBootcampTestType() {
		return bootcampTestType;
	}
	public void setBootcampTestType(Integer bootcampTestType) {
		this.bootcampTestType = bootcampTestType;
	}
	
	@ManyToOne
	@JoinColumn(name="BOOTCAMP_TEST_TYPE", nullable=true, updatable=false, insertable=false)
	public BootcampTestTypeModel getBootcampTestTypeModel() {
		return bootcampTestTypeModel;
	}
	public void setBootcampTestTypeModel(BootcampTestTypeModel bootcampTestTypeModel) {
		this.bootcampTestTypeModel = bootcampTestTypeModel;
	}
	
	@Column(name="IQ", length=4)
	public Integer getIq() {
		return iq;
	}
	public void setIq(Integer iq) {
		this.iq = iq;
	}
	
	@Column(name="DU", length=10)
	public String getDu() {
		return du;
	}
	public void setDu(String du) {
		this.du = du;
	}
	
	@Column(name="ARITHMETIC", length=5)
	public Integer getArithmetic() {
		return arithmetic;
	}
	public void setArithmetic(Integer arithmetic) {
		this.arithmetic = arithmetic;
	}
	
	@Column(name="NESTED_LOGIC", length=5)
	public Integer getNestedLogic() {
		return nestedLogic;
	}
	public void setNestedLogic(Integer nestedLogic) {
		this.nestedLogic = nestedLogic;
	}
	
	@Column(name="JOIN_TABLE", length=5)
	public Integer getJoinTable() {
		return joinTable;
	}
	public void setJoinTable(Integer joinTable) {
		this.joinTable = joinTable;
	}
	
	@Column(name="TRO", length=50)
	public String getTro() {
		return tro;
	}
	public void setTro(String tro) {
		this.tro = tro;
	}
	
	@Column(name="NOTES", length=100)
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@Column(name="INTERVIEWER", length=100)
	public String getInterviewer() {
		return interviewer;
	}
	public void setInterviewer(String interviewer) {
		this.interviewer = interviewer;
	}
	
	@Column(name="CREATED_BY", length=11)
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	@ManyToOne
	@JoinColumn(name="CREATED_BY", nullable=true, updatable=false, insertable=false)
	public UserModel getxCreatedBy() {
		return xCreatedBy;
	}
	public void setxCreatedBy(UserModel xCreatedBy) {
		this.xCreatedBy = xCreatedBy;
	}
	
	@Column(name="CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(name="MODIFIED_BY", length=11)
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="MODIFIED_BY", nullable=true, updatable=false, insertable=false)
	public UserModel getxModifiedBy() {
		return xModifiedBy;
	}
	public void setxModifiedBy(UserModel xModifiedBy) {
		this.xModifiedBy = xModifiedBy;
	}
	
	@Column(name="MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	@Column(name="DELETED_BY", length=11)
	public Integer getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(Integer deletedBy) {
		this.deletedBy = deletedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="DELETED_BY", nullable=true, updatable=false, insertable=false)
	public UserModel getxDeletedBy() {
		return xDeletedBy;
	}
	public void setxDeletedBy(UserModel xDeletedBy) {
		this.xDeletedBy = xDeletedBy;
	}
	
	@Column(name="DELETED_ON")
	public Date getDeletedOn() {
		return deletedOn;
	}
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}
	
	@Column(name="IS_DELETE", nullable=false)
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
}
