package com.xsis.winners.xbc184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "T_AUDIT_LOG")
public class AuditLogModel {

	private Integer id;
	private String type;
	private String jsonInsert;
	private String jsonBefore;
	private String jsonAfter;
	private String jsonDelete;
	private Integer createdBy;
	private Date createdOn;
	private UserModel createdUser;

	@Id
	@Column(name = "ID", length = 11)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "T_AUDIT_LOG")
	@TableGenerator(name = "T_AUDIT_LOG", table = "T_SEQUENCE", pkColumnName = "SEQUENCE_NAME", pkColumnValue = "AUDIT_LOG_ID", valueColumnName = "SEQUENCE_VALUE", allocationSize = 1, initialValue = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "TYPE", length = 10, nullable = false)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "JSON_INSERT", length = 4000)
	public String getJsonInsert() {
		return jsonInsert;
	}

	public void setJsonInsert(String jsonInsert) {
		this.jsonInsert = jsonInsert;
	}

	@Column(name = "JSON_BEFORE", length = 4000)
	public String getJsonBefore() {
		return jsonBefore;
	}

	public void setJsonBefore(String jsonBefore) {
		this.jsonBefore = jsonBefore;
	}

	@Column(name = "JSON_AFTER", length = 4000)
	public String getJsonAfter() {
		return jsonAfter;
	}

	public void setJsonAfter(String jsonAfter) {
		this.jsonAfter = jsonAfter;
	}

	@Column(name = "JSON_DELETE", length = 4000)
	public String getJsonDelete() {
		return jsonDelete;
	}

	public void setJsonDelete(String jsonDelete) {
		this.jsonDelete = jsonDelete;
	}

	@Column(name = "CREATED_BY", length = 11, nullable = false)
	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "CREATED_ON", nullable = false)
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@ManyToOne
	@JoinColumn(name = "CREATED_BY", insertable = false, nullable = true, updatable = false)
	public UserModel getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(UserModel createdUser) {
		this.createdUser = createdUser;
	}

}
