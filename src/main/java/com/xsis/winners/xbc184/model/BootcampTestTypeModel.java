package com.xsis.winners.xbc184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="T_BOOTCAMP_Test_TYPE")
public class BootcampTestTypeModel {
	
	private Integer idBootcampTestType;
	private String nameBootcampTestType;
	private String notesBootcampTestType;
	
	private Boolean isDelete;
	
	//audit trail
	private Integer xIdCreatedBy;
	private UserModel xCreatedBy;
	private Date xCreatedDate;
	private Integer xIdModifiedBy;
	private UserModel xModifiedBy;
	private Date xModifiedDate;
	private Integer xIdDeletedBy;
	private UserModel xDeletedBy;
	private Date xDeletedDate;
	
	@Id
	@Column(name="ID_BOOTCAMP_TEST_TYPE")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="T_BOOTCAMP_TEST_TYPE")
	@TableGenerator(name="T_BOOTCAMP_TEST_TYPE", table="T_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_BOOTCAMP_TEST_TYPE",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getIdBootcampTestType() {
		return idBootcampTestType;
	}
	public void setIdBootcampTestType(Integer idBootcampTestType) {
		this.idBootcampTestType = idBootcampTestType;
	}
	
	@Column(name="NAMA_BOOTCAMP_TEST_TYPE")
	public String getNameBootcampTestType() {
		return nameBootcampTestType;
	}
	public void setNameBootcampTestType(String nameBootcampTestType) {
		this.nameBootcampTestType = nameBootcampTestType;
	}
	
	@Column(name="NOTES_BOOTCAMP_TEST_TYPE")
	public String getNotesBootcampTestType() {
		return notesBootcampTestType;
	}
	public void setNotesBootcampTestType(String notesBootcampTestType) {
		this.notesBootcampTestType = notesBootcampTestType;
	}
	
	@Column(name="IS_DELETE")
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	@Column(name="X_ID_CREATED")
	public Integer getxIdCreatedBy() {
		return xIdCreatedBy;
	}
	public void setxIdCreatedBy(Integer xIdCreatedBy) {
		this.xIdCreatedBy = xIdCreatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_CREATED", nullable=true, updatable=false, insertable=false)
	public UserModel getxCreatedBy() {
		return xCreatedBy;
	}
	public void setxCreatedBy(UserModel xCreatedBy) {
		this.xCreatedBy = xCreatedBy;
	}
	
	@Column(name="X_CREATED_DATE")
	public Date getxCreatedDate() {
		return xCreatedDate;
	}
	public void setxCreatedDate(Date xCreatedDate) {
		this.xCreatedDate = xCreatedDate;
	}
	
	@Column(name="X_ID_MODIFIED")
	public Integer getxIdModifiedBy() {
		return xIdModifiedBy;
	}
	public void setxIdModifiedBy(Integer xIdModifiedBy) {
		this.xIdModifiedBy = xIdModifiedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_MODIFIED", nullable=true, updatable=false, insertable=false)
	public UserModel getxModifiedBy() {
		return xModifiedBy;
	}
	public void setxModifiedBy(UserModel xModifiedBy) {
		this.xModifiedBy = xModifiedBy;
	}
	
	@Column(name="X_MODIFIED_DATE")
	public Date getxModifiedDate() {
		return xModifiedDate;
	}
	public void setxModifiedDate(Date xModifiedDate) {
		this.xModifiedDate = xModifiedDate;
	}
	
	@Column(name="X_ID_DELETED")
	public Integer getxIdDeletedBy() {
		return xIdDeletedBy;
	}
	public void setxIdDeletedBy(Integer xIdDeletedBy) {
		this.xIdDeletedBy = xIdDeletedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_DELETED", nullable=true, updatable=false, insertable=false)
	public UserModel getxDeletedBy() {
		return xDeletedBy;
	}
	public void setxDeletedBy(UserModel xDeletedBy) {
		this.xDeletedBy = xDeletedBy;
	}
	
	@Column(name="X_DELETED_DATE")
	public Date getxDeletedDate() {
		return xDeletedDate;
	}
	public void setxDeletedDate(Date xDeletedDate) {
		this.xDeletedDate = xDeletedDate;
	}
	
	
	

}
