package com.xsis.winners.xbc184.model;


import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="T_OFFICE")
public class OfficeModel {
	
	private Integer id;
	private String name;
	private String phone;
	private String email;
	private String address;
	private String notes;
	
	private Integer createdBy;
	private Date createdOn;
	
	private Integer modifiedBy;
	private Date modifiedOn;
	
	private Integer deletedBy;
	private Date deletedOn;
	
	private Boolean isDelete;
	
	private List<RoomModel> roomModelList;
	
	@Id
	@Column(name="ID_OFFICE", length=11)
	@GeneratedValue(strategy=GenerationType.TABLE, generator="T_OFFICE") // utk buat nilai sequential
	@TableGenerator(name="T_OFFICE", table="T_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_OFFICE",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="NAME", length=50, nullable=false)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="PHONE", length=50, nullable=true)
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Column(name="EMAIL", length=255, nullable=true)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="ADDRESS", length=500, nullable=true)
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name="NOTES",length=500, nullable=true)
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@OneToMany(mappedBy="officeModel", cascade=CascadeType.ALL)
	@JsonManagedReference
	public List<RoomModel> getRoomModelList() {
		return roomModelList;
	}
	public void setRoomModelList(List<RoomModel> roomModelList) {
		this.roomModelList = roomModelList;
	}
	
	@Column(name="CREATED_BY",length=11)
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name="CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(name="MODIFIED_BY",length=11)
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	@Column(name="MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	@Column(name="DELETED_BY",length=11)
	public Integer getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(Integer deletedBy) {
		this.deletedBy = deletedBy;
	}
	
	@Column(name="DELETED_ON")
	public Date getDeletedOn() {
		return deletedOn;
	}
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}
	
	@Column(name="IS_DELETE")
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	
}
