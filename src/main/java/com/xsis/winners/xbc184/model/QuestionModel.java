package com.xsis.winners.xbc184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="T_QUESTION")
public class QuestionModel {

	private Integer idQuestion;
	private String question;
	private String questionType;
	private String optionA;
	private String optionB;
	private String optionC;
	private String optionD;
	private String optionE;
	private String imageUrl;
	private String imageA;
	private String imageB;
	private String imageC;
	private String imageD;
	private String imageE;
	private Boolean isDelete;
	
	//Audit Trail
	private Integer xIdCreatedBy;
	private UserModel xCreatedBy;
	private Date xCreatedDate;
	
	private Integer xIdUpdatedBy;
	private UserModel xUpdatedBy;
	private Date xUpdatedDate;
	
	private Integer xIdDeletedBy;
	private UserModel xDeletedBy;
	private Date xDeletedDate;
	//Akhir Audit Trail
	
	@Id // menandakan primary key
	@Column(name="ID_QUESTION", length= 11, nullable= false)
	@GeneratedValue(strategy=GenerationType.TABLE, generator="T_QUESTION") // utk buat nilai sequential
	@TableGenerator(name="T_QUESTION", table="T_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_QUESTION",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getIdQuestion() {
		return idQuestion;
	}
	public void setIdQuestion(Integer idQuestion) {
		this.idQuestion = idQuestion;
	}
	
	@Column(name="QUESTION", length= 255, nullable= false)
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	
	@Column(name="QUESTION_TYPE", length= 5, nullable= false)
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	
	@Column(name="OPTION_A", length= 255)
	public String getOptionA() {
		return optionA;
	}
	public void setOptionA(String optionA) {
		this.optionA = optionA;
	}
	
	@Column(name="OPTION_B", length= 255)
	public String getOptionB() {
		return optionB;
	}
	public void setOptionB(String optionB) {
		this.optionB = optionB;
	}
	
	@Column(name="OPTION_C", length= 255)
	public String getOptionC() {
		return optionC;
	}
	public void setOptionC(String optionC) {
		this.optionC = optionC;
	}
	
	@Column(name="OPTION_D", length= 255)
	public String getOptionD() {
		return optionD;
	}
	public void setOptionD(String optionD) {
		this.optionD = optionD;
	}
	
	@Column(name="OPTION_E", length= 255)
	public String getOptionE() {
		return optionE;
	}
	public void setOptionE(String optionE) {
		this.optionE = optionE;
	}
	
	@Column(name="IMAGE_URL", length= 255)
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	@Column(name="IMAGE_A", length= 255)
	public String getImageA() {
		return imageA;
	}
	public void setImageA(String imageA) {
		this.imageA = imageA;
	}
	
	@Column(name="IMAGE_B", length= 255)
	public String getImageB() {
		return imageB;
	}
	public void setImageB(String imageB) {
		this.imageB = imageB;
	}
	
	@Column(name="IMAGE_C", length= 255)
	public String getImageC() {
		return imageC;
	}
	public void setImageC(String imageC) {
		this.imageC = imageC;
	}
	
	@Column(name="IMAGE_D", length= 255)
	public String getImageD() {
		return imageD;
	}
	public void setImageD(String imageD) {
		this.imageD = imageD;
	}
	
	@Column(name="IMAGE_E", length= 255)
	public String getImageE() {
		return imageE;
	}
	public void setImageE(String imageE) {
		this.imageE = imageE;
	}
	
	@Column(name= "IS_DELETE", nullable= false)
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	@Column(name= "X_ID_CREATED", length= 11, nullable= false)
	public Integer getxIdCreatedBy() {
		return xIdCreatedBy;
	}
	public void setxIdCreatedBy(Integer xIdCreatedBy) {
		this.xIdCreatedBy = xIdCreatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name= "X_ID_CREATED", nullable= true, updatable= false, insertable= false)
	public UserModel getxCreatedBy() {
		return xCreatedBy;
	}
	public void setxCreatedBy(UserModel xCreatedBy) {
		this.xCreatedBy = xCreatedBy;
	}
	
	@Column(name= "X_CREATED_DATE")
	public Date getxCreatedDate() {
		return xCreatedDate;
	}
	public void setxCreatedDate(Date xCreatedDate) {
		this.xCreatedDate = xCreatedDate;
	}
	
	@Column(name= "X_ID_UPDATED", length= 11)
	public Integer getxIdUpdatedBy() {
		return xIdUpdatedBy;
	}
	public void setxIdUpdatedBy(Integer xIdUpdatedBy) {
		this.xIdUpdatedBy = xIdUpdatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name= "X_ID_UPDATED", nullable= true, updatable= false, insertable= false)
	public UserModel getxUpdatedBy() {
		return xUpdatedBy;
	}
	public void setxUpdatedBy(UserModel xUpdatedBy) {
		this.xUpdatedBy = xUpdatedBy;
	}
	
	@Column(name= "X_UPDATED_DATE")
	public Date getxUpdatedDate() {
		return xUpdatedDate;
	}
	public void setxUpdatedDate(Date xUpdatedDate) {
		this.xUpdatedDate = xUpdatedDate;
	}
	
	@Column(name= "X_ID_DELETED", length= 11)
	public Integer getxIdDeletedBy() {
		return xIdDeletedBy;
	}
	public void setxIdDeletedBy(Integer xIdDeletedBy) {
		this.xIdDeletedBy = xIdDeletedBy;
	}
	
	@ManyToOne
	@JoinColumn(name= "X_ID_DELETED", nullable= true, updatable= false, insertable= false)
	public UserModel getxDeletedBy() {
		return xDeletedBy;
	}
	public void setxDeletedBy(UserModel xDeletedBy) {
		this.xDeletedBy = xDeletedBy;
	}
	
	@Column(name= "X_DELETED_DATE")
	public Date getxDeletedDate() {
		return xDeletedDate;
	}
	public void setxDeletedDate(Date xDeletedDate) {
		this.xDeletedDate = xDeletedDate;
	}
	
}
