package com.xsis.winners.xbc184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="T_BOOTCAMP_TYPE")
public class BootcampTypeModel {
	
	private Integer idBootcampType;
	private String nameBootcampType;
	private String notesBootcampType;
	
	private Boolean isDelete;
	
	//audit trail
	private Integer xIdCreatedBy;
	private UserModel xCreatedBy;
	private Date xCreatedDate;
	private Integer xIdModifiedBy;
	private UserModel xModifiedBy;
	private Date xModifiedDate;
	private Integer xIdDeletedBy;
	private UserModel xDeletedBy;
	private Date xDeletedDate;
	
	@Id
	@Column(name="ID_BOOTCAMP_TYPE")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="T_BOOTCAMP_TYPE")
	@TableGenerator(name="T_BOOTCAMP_TYPE", table="T_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_BOOTCAMP_TYPE",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getIdBootcampType() {
		return idBootcampType;
	}
	public void setIdBootcampType(Integer idBootcampType) {
		this.idBootcampType = idBootcampType;
	}
	
	@Column(name="NAMA_BOOTCAMP_TYPE")
	public String getNameBootcampType() {
		return nameBootcampType;
	}
	public void setNameBootcampType(String nameBootcampType) {
		this.nameBootcampType = nameBootcampType;
	}
	
	@Column(name="NOTES_BOOTCAMP_TYPE")
	public String getNotesBootcampType() {
		return notesBootcampType;
	}
	public void setNotesBootcampType(String notesBootcampType) {
		this.notesBootcampType = notesBootcampType;
	}
	
	@Column(name="IS_DELETE")
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	@Column(name="X_ID_CREATED")
	public Integer getxIdCreatedBy() {
		return xIdCreatedBy;
	}
	public void setxIdCreatedBy(Integer xIdCreatedBy) {
		this.xIdCreatedBy = xIdCreatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_CREATED", nullable=true, updatable=false, insertable=false)
	public UserModel getxCreatedBy() {
		return xCreatedBy;
	}
	public void setxCreatedBy(UserModel xCreatedBy) {
		this.xCreatedBy = xCreatedBy;
	}
	
	@Column(name="X_CREATED_DATE")
	public Date getxCreatedDate() {
		return xCreatedDate;
	}
	public void setxCreatedDate(Date xCreatedDate) {
		this.xCreatedDate = xCreatedDate;
	}
	
	@Column(name="X_ID_MODIFIED")
	public Integer getxIdModifiedBy() {
		return xIdModifiedBy;
	}
	public void setxIdModifiedBy(Integer xIdModifiedBy) {
		this.xIdModifiedBy = xIdModifiedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_MODIFIED", nullable=true, updatable=false, insertable=false)
	public UserModel getxModifiedBy() {
		return xModifiedBy;
	}
	public void setxModifiedBy(UserModel xModifiedBy) {
		this.xModifiedBy = xModifiedBy;
	}
	
	@Column(name="X_MODIFIED_DATE")
	public Date getxModifiedDate() {
		return xModifiedDate;
	}
	public void setxModifiedDate(Date xModifiedDate) {
		this.xModifiedDate = xModifiedDate;
	}
	
	@Column(name="X_ID_DELETED")
	public Integer getxIdDeletedBy() {
		return xIdDeletedBy;
	}
	public void setxIdDeletedBy(Integer xIdDeletedBy) {
		this.xIdDeletedBy = xIdDeletedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_DELETED", nullable=true, updatable=false, insertable=false)
	public UserModel getxDeletedBy() {
		return xDeletedBy;
	}
	public void setxDeletedBy(UserModel xDeletedBy) {
		this.xDeletedBy = xDeletedBy;
	}
	
	@Column(name="X_DELETED_DATE")
	public Date getxDeletedDate() {
		return xDeletedDate;
	}
	public void setxDeletedDate(Date xDeletedDate) {
		this.xDeletedDate = xDeletedDate;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idBootcampType == null) ? 0 : idBootcampType.hashCode());
		result = prime * result + ((isDelete == null) ? 0 : isDelete.hashCode());
		result = prime * result + ((nameBootcampType == null) ? 0 : nameBootcampType.hashCode());
		result = prime * result + ((notesBootcampType == null) ? 0 : notesBootcampType.hashCode());
		result = prime * result + ((xCreatedBy == null) ? 0 : xCreatedBy.hashCode());
		result = prime * result + ((xCreatedDate == null) ? 0 : xCreatedDate.hashCode());
		result = prime * result + ((xDeletedBy == null) ? 0 : xDeletedBy.hashCode());
		result = prime * result + ((xDeletedDate == null) ? 0 : xDeletedDate.hashCode());
		result = prime * result + ((xIdCreatedBy == null) ? 0 : xIdCreatedBy.hashCode());
		result = prime * result + ((xIdDeletedBy == null) ? 0 : xIdDeletedBy.hashCode());
		result = prime * result + ((xIdModifiedBy == null) ? 0 : xIdModifiedBy.hashCode());
		result = prime * result + ((xModifiedBy == null) ? 0 : xModifiedBy.hashCode());
		result = prime * result + ((xModifiedDate == null) ? 0 : xModifiedDate.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BootcampTypeModel other = (BootcampTypeModel) obj;
		if (idBootcampType == null) {
			if (other.idBootcampType != null)
				return false;
		} else if (!idBootcampType.equals(other.idBootcampType))
			return false;
		if (isDelete == null) {
			if (other.isDelete != null)
				return false;
		} else if (!isDelete.equals(other.isDelete))
			return false;
		if (nameBootcampType == null) {
			if (other.nameBootcampType != null)
				return false;
		} else if (!nameBootcampType.equals(other.nameBootcampType))
			return false;
		if (notesBootcampType == null) {
			if (other.notesBootcampType != null)
				return false;
		} else if (!notesBootcampType.equals(other.notesBootcampType))
			return false;
		if (xCreatedBy == null) {
			if (other.xCreatedBy != null)
				return false;
		} else if (!xCreatedBy.equals(other.xCreatedBy))
			return false;
		if (xCreatedDate == null) {
			if (other.xCreatedDate != null)
				return false;
		} else if (!xCreatedDate.equals(other.xCreatedDate))
			return false;
		if (xDeletedBy == null) {
			if (other.xDeletedBy != null)
				return false;
		} else if (!xDeletedBy.equals(other.xDeletedBy))
			return false;
		if (xDeletedDate == null) {
			if (other.xDeletedDate != null)
				return false;
		} else if (!xDeletedDate.equals(other.xDeletedDate))
			return false;
		if (xIdCreatedBy == null) {
			if (other.xIdCreatedBy != null)
				return false;
		} else if (!xIdCreatedBy.equals(other.xIdCreatedBy))
			return false;
		if (xIdDeletedBy == null) {
			if (other.xIdDeletedBy != null)
				return false;
		} else if (!xIdDeletedBy.equals(other.xIdDeletedBy))
			return false;
		if (xIdModifiedBy == null) {
			if (other.xIdModifiedBy != null)
				return false;
		} else if (!xIdModifiedBy.equals(other.xIdModifiedBy))
			return false;
		if (xModifiedBy == null) {
			if (other.xModifiedBy != null)
				return false;
		} else if (!xModifiedBy.equals(other.xModifiedBy))
			return false;
		if (xModifiedDate == null) {
			if (other.xModifiedDate != null)
				return false;
		} else if (!xModifiedDate.equals(other.xModifiedDate))
			return false;
		return true;
	}

}
