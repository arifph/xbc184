package com.xsis.winners.xbc184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="T_BATCH_TEST")
public class BatchTestModel {
	private Integer id;
	private Integer batchId;
	private Integer testId;
	private Integer createdBy;
	private UserModel xCreatedBy;
	private Date createdOn;
	
	// join ke batch dan test
	private BatchModel batchModel;
	private BiodataModel testModel;
	
	@Id //untuk primary key
	@Column(name="ID", length=11)
	@GeneratedValue(strategy=GenerationType.TABLE, generator="T_BATCH_TEST") // utk autoincrement
	@TableGenerator(name="T_BATCH_TEST", table="T_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="BATCH_TEST_ID",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="BATCH_ID", length=11, nullable=false)
	public Integer getBatchId() {
		return batchId;
	}
	public void setBatchId(Integer batchId) {
		this.batchId = batchId;
	}
	
	@ManyToOne
	@JoinColumn(name="BATCH_ID", nullable=false, updatable=false, insertable=false)
	public BatchModel getBatchModel() {
		return batchModel;
	}
	public void setBatchModel(BatchModel batchModel) {
		this.batchModel = batchModel;
	}
	
	@Column(name="TEST_ID", length=11, nullable=false)
	public Integer getTestId() {
		return testId;
	}
	public void setTestId(Integer testId) {
		this.testId = testId;
	}
	
	@ManyToOne
	@JoinColumn(name="TEST_ID", nullable=false, updatable=false, insertable=false)
	public BiodataModel getTestModel() {
		return testModel;
	}
	public void setTestModel(BiodataModel testModel) {
		this.testModel = testModel;
	}
	
	@Column(name="CREATED_BY", length=11, nullable=false)
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	@ManyToOne
	@JoinColumn(name="CREATED_BY", nullable=false, updatable=false, insertable=false)
	public UserModel getxCreatedBy() {
		return xCreatedBy;
	}
	public void setxCreatedBy(UserModel xCreatedBy) {
		this.xCreatedBy = xCreatedBy;
	}
	
	@Column(name="CREATED_ON", nullable=false)
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
}
