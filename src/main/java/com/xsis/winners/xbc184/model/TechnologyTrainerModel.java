package com.xsis.winners.xbc184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="t_technology_trainer")
public class TechnologyTrainerModel {
		
	private Integer id;
	
	private Integer createdBy;
	private Date createdOn;
	
	//join tech
	/* private Integer technologyId; */
	private TechnologyModel technologyModel;
	
	/* private Integer trainerId; */
	private TrainerModel trainerModel;
	
	@Id
	@Column(name="id", nullable=false)
	@GeneratedValue(strategy=GenerationType.TABLE, generator="t_technology_trainer")
	@TableGenerator(name="t_technology_trainer", table="T_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="TECHNOLOGY_TRAINER_ID",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="created_by", nullable=false)
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name="created_on", nullable=false)
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	/*
	 * @Column(name="technology_id", nullable=false, updatable=true,
	 * insertable=true) public Integer getTechnologyId() { return technologyId; }
	 * public void setTechnologyId(Integer technologyId) { this.technologyId =
	 * technologyId; }
	 */
	
	@ManyToOne(optional=false)
	@JoinColumn(name="technology_id")
	@JsonBackReference
	public TechnologyModel getTechnologyModel() {
		return technologyModel;
	}
	public void setTechnologyModel(TechnologyModel technologyModel) {
		this.technologyModel = technologyModel;
	}
	
	/*
	 * @Column(name="trainer_id", nullable=false) public Integer getTrainerId() {
	 * return trainerId; } public void setTrainerId(Integer trainerId) {
	 * this.trainerId = trainerId; }
	 */
	
	@ManyToOne(optional=false)
	@JoinColumn(name="trainer_id")
	@JsonBackReference
	public TrainerModel getTrainerModel() {
		return trainerModel;
	}
	public void setTrainerModel(TrainerModel trainerModel) {
		this.trainerModel = trainerModel;
	}
	
	
	
	/*
	 * @Column(name="technology_id") public Integer getTechnologyId() { return
	 * technologyId; } public void setTechnologyId(Integer technologyId) {
	 * this.technologyId = technologyId; }
	 * 
	 * @ManyToOne
	 * 
	 * @JoinColumn(name="technology	_id", nullable=true, updatable=false,
	 * insertable=false) public TechnologyModel getTechnologyModel() { return
	 * technologyModel; } public void setTechnologyModel(TechnologyModel
	 * technologyModel) { this.technologyModel = technologyModel; }
	 * 
	 * @Column(name="trainer_id") public Integer getTrainerId() { return trainerId;
	 * } public void setTrainerId(Integer trainerId) { this.trainerId = trainerId; }
	 */
	
	
	/*
	 * @ManyToOne
	 * 
	 * @JoinColumn(name="trainer_id", nullable=true, updatable=false,
	 * insertable=false) public List<TrainerModel> getTrainerModel() { return
	 * trainerModel; } public void setTrainerModel(List<TrainerModel> trainerModel)
	 * { this.trainerModel = trainerModel; }
	 */
	
	
}
