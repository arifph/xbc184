package com.xsis.winners.xbc184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "T_TEST_TYPE")
public class TestTypeModel {

	private Integer id;
	private String name;
	private String notes;
	private Integer typeOfAnswer;
	private UserModel createdBy;
	private Date createdOn;
	private UserModel modifiedBy;
	private Date modifiedOn;
	private UserModel deletedBy;
	private Date deletedOn;
	private Boolean isDelete;
	private GlobalParameterModel globalParameterModel;

	@Id
	@Column(name = "ID", length = 11)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "T_TEST_TYPE")
	@TableGenerator(name = "T_TEST_TYPE", table = "T_SEQUENCE", pkColumnName = "SEQUENCE_NAME", pkColumnValue = "TEST_TYPE_ID", valueColumnName = "SEQUENCE_VALUE", allocationSize = 1, initialValue = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "NAME")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "TYPE_OF_ANSWER")
	public Integer getTypeOfAnswer() {
		return typeOfAnswer;
	}

	public void setTypeOfAnswer(Integer typeOfAnswer) {
		this.typeOfAnswer = typeOfAnswer;
	}

	@ManyToOne
	@JoinColumn(name = "TYPE_OF_ANSWER", insertable = false, nullable = true, updatable = false)
	public GlobalParameterModel getGlobalParameterModel() {
		return globalParameterModel;
	}

	public void setGlobalParameterModel(GlobalParameterModel globalParameterModel) {
		this.globalParameterModel = globalParameterModel;
	}

	@Column(name = "NOTES", length = 255)
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@ManyToOne
	@JoinColumn(name="CREATED_BY", nullable=false)
	public UserModel getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(UserModel createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name="CREATED_ON", nullable = false)
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@ManyToOne
	@JoinColumn(name="MODIFIED_BY")
	public UserModel getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UserModel modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Column(name="MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	@ManyToOne
	@JoinColumn(name="DELETED_BY")
	public UserModel getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(UserModel deletedBy) {
		this.deletedBy = deletedBy;
	}

	@Column(name="DELETED_ON")
	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	@Column(name="IS_DELETE", nullable=false)
	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

}
