package com.xsis.winners.xbc184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="T_MONITORING")
public class MonitoringModel {
	
	private Integer idMonitoring;
	
	private Integer idBiodata;
	private BiodataModel biodataModel;

	private Date idleDate;
	private String lastProject;
	private String idleReason;
	
	private Date placementDate;
	private String placementAt;
	private String notes;

	//audit trail
	
	private Integer xIdCreatedBy;
	private UserModel xCreatedBy;
	private Date xCreatedDate;
	
	private Integer xIdModifiedBy;
	private UserModel xModifiedBy;
	private Date xModifiedDate;
	
	private Integer xIdDeletedBy;
	private UserModel xDeletedBy;
	private Date xDeletedDate;
	
	private Boolean isDelete;
	
	@Id
	@Column(name="ID_MONITORING")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="T_MONITORING")
	@TableGenerator(name="T_MONITORING", table="T_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_MONITORING",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getIdMonitoring() {
		return idMonitoring;
	}
	public void setIdMonitoring(Integer idMonitoring) {
		this.idMonitoring = idMonitoring;
	}
	
	@Column(name="ID_BIODATA")
	public Integer getIdBiodata() {
		return idBiodata;
	}
	public void setIdBiodata(Integer idBiodata) {
		this.idBiodata = idBiodata;
	}
	
	@ManyToOne
	@JoinColumn(name="ID_BIODATA", nullable=true, updatable=false, insertable=false)
	public BiodataModel getBiodataModel() {
		return biodataModel;
	}
	public void setBiodataModel(BiodataModel biodataModel) {
		this.biodataModel = biodataModel;
	}
	
	@Column(name="IDLE_DATE")
	public Date getIdleDate() {
		return idleDate;
	}
	public void setIdleDate(Date idleDate) {
		this.idleDate = idleDate;
	}
	
	@Column(name="LAST_PROJECT")
	public String getLastProject() {
		return lastProject;
	}
	public void setLastProject(String lastProject) {
		this.lastProject = lastProject;
	}
	
	@Column(name="IDLE_REASON")
	public String getIdleReason() {
		return idleReason;
	}
	public void setIdleReason(String idleReason) {
		this.idleReason = idleReason;
	}
	
	@Column(name="PLACEMENT_DATE")
	public Date getPlacementDate() {
		return placementDate;
	}
	public void setPlacementDate(Date placementDate) {
		this.placementDate = placementDate;
	}
	
	@Column(name="PLACEMENT_AT")
	public String getPlacementAt() {
		return placementAt;
	}
	public void setPlacementAt(String placementAt) {
		this.placementAt = placementAt;
	}
	
	@Column(name="NOTES")
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@Column(name="X_ID_CREATED")
	public Integer getxIdCreatedBy() {
		return xIdCreatedBy;
	}
	public void setxIdCreatedBy(Integer xIdCreatedBy) {
		this.xIdCreatedBy = xIdCreatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_CREATED", nullable=true, updatable=false, insertable=false)
	public UserModel getxCreatedBy() {
		return xCreatedBy;
	}
	public void setxCreatedBy(UserModel xCreatedBy) {
		this.xCreatedBy = xCreatedBy;
	}
	
	@Column(name="X_CREATED_DATE")
	public Date getxCreatedDate() {
		return xCreatedDate;
	}
	public void setxCreatedDate(Date xCreatedDate) {
		this.xCreatedDate = xCreatedDate;
	}
	
	@Column(name="X_ID_MODIFIED")
	public Integer getxIdModifiedBy() {
		return xIdModifiedBy;
	}
	public void setxIdModifiedBy(Integer xIdModifiedBy) {
		this.xIdModifiedBy = xIdModifiedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_MODIFIED", nullable=true, updatable=false, insertable=false)
	public UserModel getxModifiedBy() {
		return xModifiedBy;
	}
	public void setxModifiedBy(UserModel xModifiedBy) {
		this.xModifiedBy = xModifiedBy;
	}
	
	@Column(name="X_MODIFIED_DATE")
	public Date getxModifiedDate() {
		return xModifiedDate;
	}
	public void setxModifiedDate(Date xModifiedDate) {
		this.xModifiedDate = xModifiedDate;
	}
	
	@Column(name="X_ID_DELETED")
	public Integer getxIdDeletedBy() {
		return xIdDeletedBy;
	}
	public void setxIdDeletedBy(Integer xIdDeletedBy) {
		this.xIdDeletedBy = xIdDeletedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_DELETED", nullable=true, updatable=false, insertable=false)
	public UserModel getxDeletedBy() {
		return xDeletedBy;
	}
	public void setxDeletedBy(UserModel xDeletedBy) {
		this.xDeletedBy = xDeletedBy;
	}
	
	@Column(name="X_DELETED_DATE")
	public Date getxDeletedDate() {
		return xDeletedDate;
	}
	public void setxDeletedDate(Date xDeletedDate) {
		this.xDeletedDate = xDeletedDate;
	}
	
	@Column(name="IS_DELETE")
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((biodataModel == null) ? 0 : biodataModel.hashCode());
		result = prime * result + ((idBiodata == null) ? 0 : idBiodata.hashCode());
		result = prime * result + ((idMonitoring == null) ? 0 : idMonitoring.hashCode());
		result = prime * result + ((idleDate == null) ? 0 : idleDate.hashCode());
		result = prime * result + ((idleReason == null) ? 0 : idleReason.hashCode());
		result = prime * result + ((isDelete == null) ? 0 : isDelete.hashCode());
		result = prime * result + ((lastProject == null) ? 0 : lastProject.hashCode());
		result = prime * result + ((notes == null) ? 0 : notes.hashCode());
		result = prime * result + ((placementAt == null) ? 0 : placementAt.hashCode());
		result = prime * result + ((placementDate == null) ? 0 : placementDate.hashCode());
		result = prime * result + ((xCreatedBy == null) ? 0 : xCreatedBy.hashCode());
		result = prime * result + ((xCreatedDate == null) ? 0 : xCreatedDate.hashCode());
		result = prime * result + ((xDeletedBy == null) ? 0 : xDeletedBy.hashCode());
		result = prime * result + ((xDeletedDate == null) ? 0 : xDeletedDate.hashCode());
		result = prime * result + ((xIdCreatedBy == null) ? 0 : xIdCreatedBy.hashCode());
		result = prime * result + ((xIdDeletedBy == null) ? 0 : xIdDeletedBy.hashCode());
		result = prime * result + ((xIdModifiedBy == null) ? 0 : xIdModifiedBy.hashCode());
		result = prime * result + ((xModifiedBy == null) ? 0 : xModifiedBy.hashCode());
		result = prime * result + ((xModifiedDate == null) ? 0 : xModifiedDate.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonitoringModel other = (MonitoringModel) obj;
		if (biodataModel == null) {
			if (other.biodataModel != null)
				return false;
		} else if (!biodataModel.equals(other.biodataModel))
			return false;
		if (idBiodata == null) {
			if (other.idBiodata != null)
				return false;
		} else if (!idBiodata.equals(other.idBiodata))
			return false;
		if (idMonitoring == null) {
			if (other.idMonitoring != null)
				return false;
		} else if (!idMonitoring.equals(other.idMonitoring))
			return false;
		if (idleDate == null) {
			if (other.idleDate != null)
				return false;
		} else if (!idleDate.equals(other.idleDate))
			return false;
		if (idleReason == null) {
			if (other.idleReason != null)
				return false;
		} else if (!idleReason.equals(other.idleReason))
			return false;
		if (isDelete == null) {
			if (other.isDelete != null)
				return false;
		} else if (!isDelete.equals(other.isDelete))
			return false;
		if (lastProject == null) {
			if (other.lastProject != null)
				return false;
		} else if (!lastProject.equals(other.lastProject))
			return false;
		if (notes == null) {
			if (other.notes != null)
				return false;
		} else if (!notes.equals(other.notes))
			return false;
		if (placementAt == null) {
			if (other.placementAt != null)
				return false;
		} else if (!placementAt.equals(other.placementAt))
			return false;
		if (placementDate == null) {
			if (other.placementDate != null)
				return false;
		} else if (!placementDate.equals(other.placementDate))
			return false;
		if (xCreatedBy == null) {
			if (other.xCreatedBy != null)
				return false;
		} else if (!xCreatedBy.equals(other.xCreatedBy))
			return false;
		if (xCreatedDate == null) {
			if (other.xCreatedDate != null)
				return false;
		} else if (!xCreatedDate.equals(other.xCreatedDate))
			return false;
		if (xDeletedBy == null) {
			if (other.xDeletedBy != null)
				return false;
		} else if (!xDeletedBy.equals(other.xDeletedBy))
			return false;
		if (xDeletedDate == null) {
			if (other.xDeletedDate != null)
				return false;
		} else if (!xDeletedDate.equals(other.xDeletedDate))
			return false;
		if (xIdCreatedBy == null) {
			if (other.xIdCreatedBy != null)
				return false;
		} else if (!xIdCreatedBy.equals(other.xIdCreatedBy))
			return false;
		if (xIdDeletedBy == null) {
			if (other.xIdDeletedBy != null)
				return false;
		} else if (!xIdDeletedBy.equals(other.xIdDeletedBy))
			return false;
		if (xIdModifiedBy == null) {
			if (other.xIdModifiedBy != null)
				return false;
		} else if (!xIdModifiedBy.equals(other.xIdModifiedBy))
			return false;
		if (xModifiedBy == null) {
			if (other.xModifiedBy != null)
				return false;
		} else if (!xModifiedBy.equals(other.xModifiedBy))
			return false;
		if (xModifiedDate == null) {
			if (other.xModifiedDate != null)
				return false;
		} else if (!xModifiedDate.equals(other.xModifiedDate))
			return false;
		return true;
	}
	
	
	
}
