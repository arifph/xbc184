
<nav class="navbar" style="height: 100%; position: fixed;"
	role="navigation">
	<aside class="main-sidebar">

		<section class="sidebar">
			<ul class="sidebar-menu">
				<li class="header">MAIN NAVIGATION</li>
					
				<li>
					<a href="#">
						<i class="fa fa-save"></i><span>Master</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i> 
						</span>
					</a>
					<ul class="treeview-menu">
						<li>
							<a href="${contextName}/biodata.html" class="menu-item"><i class="fa fa-list-alt"></i>Biodata</a>
						</li>
						<li>
							<a href="${contextName}/bootcampType.html" class="menu-item"><i class="fa fa-list-alt"></i>Bootcamp Type</a>
						</li>
						<li>
							<a href="${contextName}/bootcampTestType.html" class="menu-item"><i class="fa fa-list-alt"></i>Bootcamp Test Type</a>
						</li>
						<li>
							<a href="${contextName}/category.html" class="menu-item"><i class="fa fa-list-alt"></i>Category</a>
						</li>
						<li>
							<a href="${contextName}/class.html" class="menu-item"><i class="fa fa-list-alt"></i>Class</a>
						</li>
						<li>
							<a href="${contextName}/menu.html" class="menu-item"><i class="fa fa-list-alt"></i>Menu</a>
						</li>
						<li>
							<a href="${contextName}/menuAccess.html" class="menu-item"><i class="fa fa-list-alt"></i>Menu Access</a>
						</li>
						<li>
							<a href="${contextName}/office.html" class="menu-item"><i class="fa fa-list-alt"></i>Office</a>
						</li>
						<li>
							<a href="${contextName}/question.html" class="menu-item"><i class="fa fa-list-alt"></i>Question</a>
						</li>
						<li>
							<a href="${contextName}/role.html" class="menu-item"><i class="fa fa-list-alt"></i>Role</a>
						</li>
						<li>
							<a href="${contextName}/technology.html" class="menu-item"><i class="fa fa-list-alt"></i>Technology</a>
						</li>
						<li>
							<a href="${contextName}/test.html" class="menu-item"><i class="fa fa-list-alt"></i>Test</a>
						</li>
						<li>
							<a href="${contextName}/trainer.html" class="menu-item"><i class="fa fa-list-alt"></i>Trainer</a>
						</li>
						<li>
							<a href="${contextName}/user.html" class="menu-item"><i class="fa fa-list-alt"></i>User</a>
						</li>
					</ul>
				</li>	
				
				<li>
					<a href="#">
						<i class="fa fa-money"></i> <span>Transaction</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
												
						<li>
							<a href="${contextName}/batch.html" class="menu-item"><i class="fa fa-cube"></i>Batch</a>
						</li>
						<li>
							<a href="${contextName}/class.html" class="menu-item"><i class="fa fa-building-o"></i>Class</a>
						</li>
						<li>
							<a href="${contextName}/document-test.html" class="menu-item"><i class="fa fa-building-o"></i>Document Test</a>
						</li>
						<li>
							<a href="${contextName}/feedback.html" class="menu-item"><i class="fa fa-building-o"></i>Feedback</a>
						</li>
						<li>
							<a href="${contextName}/idleNews.html" class="menu-item"><i class="fa fa-building-o"></i>Idle News</a>
						</li>
						<li>
							<a href="${contextName}/monitoring.html" class="menu-item"><i class="fa fa-building-o"></i>Monitoring</a>
						</li>
						<li>
							<a href="${contextName}/testimony.html" class="menu-item"><i class="fa fa-building-o"></i>Testimony</a>
						</li>
					</ul>
				</li>	
			</ul>
		</section>
		<!-- /.sidebar -->
	</aside>
</nav>