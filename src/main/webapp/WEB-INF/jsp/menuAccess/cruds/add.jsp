<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<i class="fa fa-close"></i>
	</button>
	<h3>ADD MENU</h3><br/>
</div>
	
	<form action="#" method="get" id="form-menu-add"> 		
		<div class="box-body">
			<div class="col-md-6">
        	<div class="form-group">
                <input type="text" class="form-control" placeholder="Title" name="title" required="required">
			</div>
			<div class="form-group">
                <textarea rows="6" cols="5" style="width:100%; height: 100%" placeholder="Description" name="description"></textarea>
			</div>
			</div>
			<div class="col-md-6">
			<div class="form-group">
                <input type="text" class="form-control" placeholder="Image URL" name="imageUrl" id="imageUrl">
			</div>
			<div class="form-group">
                <input type="text" class="form-control" placeholder="Menu Order" name="menuOrder" id="menuOrder">
			</div>
			<div class="form-group">       
            	<select class="form-control" id="menuParent" name="menuParent" >
                    <option>--Choose Menu Parent--</option>
                    <option value="1">Master</option>
                    <option value="2">Parent</option>
                 </select>
            </div>
			<div class="form-group">
                <input type="text" class="form-control" placeholder="Menu URL" name="menuUrl" id="menuUrl">
			</div>
			
			<div class="box-footer">
                <button type="reset" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" class="btn btn-info pull-right">SAVE</button>
            </div>
		</div>
		</div>
		
	
	</form>

<!--  <script type="text/javascript">
function ceksama(){
	var password = document.getElementById("password");
	var repassword = document.getElementById("repassword");
	var divRepassword = document.getElementById("divRepassword");
	var spanRepassword = document.getElementById("spanRepassword");
	
	 if(password.value != repassword.value) {
		alert('Password beda');
		
	} 
	 return false;
	
	
}
</script>  -->