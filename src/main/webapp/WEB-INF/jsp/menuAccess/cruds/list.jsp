<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${menuAccessModelList}" var="menuAccessModel" varStatus="number">
	<tr>
		<td>${menuAccessModel.roleModel.name}</td>
		<td>${menuAccessModel.menuModel.title}</td>
		<td>
        	<div class="btn-group-vertical">
            	

                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-fw fa-database"></i>
                  </button>
                 	<ul class="dropdown-menu" role="menu">
                        <li><a id="btnDelete" data-value="${menuAccessModel.id}" >Delete</a></li>
                    </ul>
                 </div>
             </div>
                  
			<%-- <button type="button" class="btn btn-warning" value="${jurusanModel.idJurusan}" id="btn-detail">Lihat</button>
			<button type="button" class="btn btn-success" value="${jurusanModel.idJurusan}" id="btn-edit">Ubah</button>
			<button type="button" class="btn btn-danger" value="${jurusanModel.idJurusan}" id="btn-delete">Hapus</button> --%>
		</td>
	</tr>
</c:forEach>