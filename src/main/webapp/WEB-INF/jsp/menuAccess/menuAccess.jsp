<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<!-- <div class="panel" style="background: white; margin-top: 50px; min-height: 500px"> -->
<div class="panel" style="background: white; margin-top: 50px">
<!-- 	<h1>MENU ACCESS</h1> -->

	<div>
		<div class="box box-primary">
        	<div class="box-header with-border">
          	<h2 class="box-title"><b>MENU ACCESS</b></h2>

        		<div class="box-tools pull-right">
            		<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
           <!--  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
          		</div>
        	</div>
        	<br/>
        <!-- /.box-header -->
			
				<form action="#" method="get" id="mAccess">
				<div class="box-body">
					<div class="col-md-6">
						<div class="form-group">       
		            	<select class="form-control" id="role" name="role" >
		                    <option>--Choose Role--</option>
		                    <c:forEach items="${roleModelList}" var="roleModel">
		                    <option value="${roleModel.id}">${roleModel.name}</option>
		                    </c:forEach>
		                </select>
		           	 	</div>
		           	 </div>
		           	 <div class="col-md-6">
		           	 	<div class="form-group">       
		            	<select class="form-control" id="menu" name="menu" >
		                    <option>--Choose Menu Menu--</option>
		                    <c:forEach items="${menuModelList}" var="menuModel">
		                    	<option value="${menuModel.id}">${menuModel.title}</option>
		                    </c:forEach>
		                </select>
		           	 	</div>
		           	 	<div class="box-footer">
		                	<button type="reset" class="btn btn-default" data-dismiss="modal">CANCEL</button>
		                	<button type="submit" class="btn btn-info pull-right">SAVE</button>
		           		</div>
		           	</div>
		        </div>
        		</form>
			
		</div>
	</div>
</div>
		
<div class="row">
	<div class="col-xs-12">
    	<div class="box box-primary">
            <div class="box-header">
              <!-- <h3 class="box-title">Responsive Hover Table</h3>
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div> -->
              <form action="#" method="get" id="form-search">
              	<div class="input-group input-group-sm" style="width:20%;">
            		<select class="form-control pull-left" id="roleId" name="roleId">
                 	   <option>--Choose Role--</option>
                  		<c:forEach items="${roleModelList}" var="roleModel">
                   		<option value="${roleModel.id}">${roleModel.name}</option>
                    	</c:forEach>
                	</select>
               <!-- <input type="text" name="table_search" class="form-control pull-left" placeholder="Search"> -->
                <div class="input-group-btn">
					<button type="submit" class="btn btn-primary">Cari</button>
				</div>
				</div>
			  </form>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover" id="tbl-menu">
				<tr>
					<td><b>ROLE</b></td>
					<td><b>MENU</b></td>
					<td><b>#</b></td>
				</tr>
				
				<tbody id="list-menuAccess">
				<!-- isi Listnya -->
				</tbody>
				
              </table>
            </div>
            <!-- /.box-body -->
		</div>
         <!-- /.box -->
	</div>
</div>
<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form PopUp User</h4>
			</div> -->
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>


<script>

	loadListMenuAccess(); /* untuk manggil otomatis krn tidak ada ready */
	
	function loadListMenuAccess(){
		$.ajax({
			url:'menuAccess/cruds/list.html',
			type:'get',
			dataType:'html',
			success: function(result){
				$('#list-menuAccess').html(result);
			}
		});
	}
		
	//document ready itu setelah halaman dipanggil
	$(document).ready(function(){
		
		//fungsi ajax untuk popup tambah
		$('#btn-add').on('click',function(){
			$.ajax({
				url:'menu/cruds/add.html',
				type:'get',
				dataType:'html',
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup tambah
		
		
		//fungsi ajax untuk create tambah
		$('#mAccess').on('submit',function(){
			$.ajax({
				url:'menuAccess/cruds/create.json',
				type:'get',
				dataType:'json',
				data: $(this).serialize(),
				success: function(result){
						//$('#modal-input').modal('hide');
						alert("Data successfully saved");
						loadListMenuAccess();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk create tambah
		
		
		//fungsi ajax untuk hapus
		$('#list-menuAccess').on('click','#btnDelete',function(){
			 var cmt = confirm('Are you sure to delete this data?');
			 if (cmt == true) {
				var id = $(this).attr('data-value');
					console.log("id : " + id);
     			$.ajax({
     				url:'menuAccess/cruds/delete.json',
     				type:'get',
     				dataType:'json',
     				data:{id : id},
     				success: function(result){
     					$('#modal-input').modal('hide');
    					alert("Data successfully deleted !");
    					loadListMenuAccess();
     				}
     			});
             }
             else {
                 return false;
             }
		});
		//akhir fungsi ajax untuk hapus
		
		/* //fungsi ajax untuk delete
		$('#modal-input').on('submit','#form-jurusan-delete',function(){
			
			$.ajax({
				url:'jurusan/cruds/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah dihapus");
					loadListJurusan();
				}
			});
			return false;
		}); */
		//akhir fungsi ajax untuk delete
		
		
		//fungsi ajax untuk search Username dan password
		$('#form-search').on('submit',function(){
			$.ajax({
				url:'menuaccess/cruds/search/search.html',
				type:'get',
				dataType:'html',
				data: $(this).serialize(),
				success: function(result){
					
					$('#list-menuAccess').html(result); //reload list jurusan
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search kodeJurusan
		
		/* //fungsi ajax untuk search kodeJurusan /namaJurusan
		$('#form-jurusan-search').on('submit',function(){
			$.ajax({
				url:'jurusan/cruds/search.html',
				type:'get',
				dataType:'html',
				data: $(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan kode/ nama jurusan");
					$('#list-jurusan').html(result); //reload list jurusan
				}
			});
			return false;
		}); */
		//akhir fungsi ajax untuk search kodeJurusan/namaJurusan
		
	});
	//akhir dokumen ready
</script>