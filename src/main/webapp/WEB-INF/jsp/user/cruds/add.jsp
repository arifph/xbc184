<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<div class="modal-header">
		<h3>ADD USER</h3>
	</div>
	
	<form action="#" method="get" id="form-user-add" onsubmit="ceksama()"> 		
		<div class="box-body">
			<div class="form-group">       
            	<select class="form-control" id="idRole" name="idRole" onclick="pilihRole();">
                    <option>--Choose Role--</option>
                   		<c:forEach items="${roleModelList}" var="roleModel">
							<option value="${roleModel.id}">${roleModel.name}</option>
						</c:forEach>
                 </select>
            </div>
        	<div class="form-group">
                <input type="text" class="form-control" placeholder="Email" name="email" required="required">
			</div>
			<div class="form-group">
                <input type="text" class="form-control" placeholder="Username" name="username" required pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[a-z]).*$">
			</div>
			<div class="form-group">
                <input type="password" class="form-control" placeholder="Password" name="password" id="password" required pattern="(?=^.{4,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$">
			</div>
			<div id="divRepassword" class="form-group">
                <input type="password" class="form-control" placeholder="Re-type Password" id="repassword" name="repassword" required="required">
                <span id="spanRepassword" class="help-block" style="display: none">Password Beda</span>
               
			</div>
			
			
			<div class="box-footer">
                <button type="reset" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" class="btn btn-info pull-right">SAVE</button>
            </div>
		</div>
		
	
	</form>
</div>
<!--  <script type="text/javascript">
function ceksama(){
	var password = document.getElementById("password");
	var repassword = document.getElementById("repassword");
	var divRepassword = document.getElementById("divRepassword");
	var spanRepassword = document.getElementById("spanRepassword");
	
	 if(password.value != repassword.value) {
		alert('Password beda');
		
	} 
	 return false;
	
	
}
</script>  -->