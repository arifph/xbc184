<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<i class="fa fa-close"></i>
	</button>
	<h3>EDIT USER</h3><br/>
</div>
<div class="form-horizontal">
	
	
	<form action="#" method="get" id="form-user-edit"> 		
		<div class="box-body">
			<div class="form-group">       
            	<select class="form-control" id="idRole" name="idRole" onclick="pilihRole();">
                    <option>--Choose Role--</option>
                   		<c:forEach items="${roleModelList}" var="roleModel">
							<option value="${roleModel.id}" ${roleModel.id==userModel.roleId ? 'selected="true"':''}>${roleModel.name}</option>
						</c:forEach>
                 </select>
            </div>
        	<div class="form-group">
                <input type="text" class="form-control" placeholder="Email" name="email" value="${userModel.email}" >
                <input type="hidden" class="form-control" placeholder="Email" name="id" value="${userModel.id}" >
			</div>
			<div class="form-group">
                <input type="text" class="form-control" placeholder="Username" name="username" value="${userModel.username}" required pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[a-z]).*$">
			</div>
			<div class="form-group">
                   <label for="inputEmail3" class="col-sm-3 control-label">Mobile Flag</label>

                  <div class="radio">
                    <label>
                      <input type="radio" name="mobileFlag" id="optionsRadios1" value="true" ${userModel.mobileFlag=='true' ? 'checked':'' } >
                      True
                    </label>
                    <label>
                      <input type="radio" name="mobileFlag" id="optionsRadios2" value="false" ${userModel.mobileFlag=='false' ? 'checked':'' }>
                      False
                    </label>
                  </div>
            </div>
			<div class="form-group">
                <input type="number" class="form-control" placeholder="Mobile Token" name="mobileToken" id="mobileToken" onclick="myFunction()" value="${userModel.mobileToken}" maxlength="10" />
			</div>
			
			
			<div class="box-footer">
                <button type="reset" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" class="btn btn-info pull-right">SAVE</button>
            </div>
		</div>
		
	
	</form>
</div>
<!--  <script type="text/javascript">
function ceksama(){
	var password = document.getElementById("password");
	var repassword = document.getElementById("repassword");
	var divRepassword = document.getElementById("divRepassword");
	var spanRepassword = document.getElementById("spanRepassword");
	
	 if(password.value != repassword.value) {
		alert('Password beda');
		
	} 
	 return false;	
}

</script>  -->
<script>
function myFunction() {
	  document.getElementById("mobileToken").value = Math.floor((Math.random() * 10000000000) + 1);
	}
</script>