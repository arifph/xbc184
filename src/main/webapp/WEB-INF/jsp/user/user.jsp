<div class="panel" style="background: white; margin-top: 50px; min-height: 500px">
	
	<div>
		<div class="row">
			<div class="col-xs-12">
		    	<div class="box">
		    		<div class="box-header">
		    		<h1 class="box-title"><b>MENU ACCESS</b></h1>
		    		</div> <br/>
		            <div class="box-header">
     					
		              <form action="#" method="get" id="form-search">
		              	<div class="col-md-6">
			              	<div class="input-group input-group-sm" style="width:20%;">
			            		<input type="text" id="usernameEmailSearch" name="usernameEmailSearch" size="27" placeholder="Search by username/email">
			               <!-- <input type="text" name="table_search" class="form-control pull-left" placeholder="Search"> -->
			                	<div class="input-group-btn">
									<button type="submit" class="btn btn-primary">Cari</button>
								</div>
							</div>
						  </div>
						  <div class="col-md-2">
						  </div>
						   <div class="col-md-2">
						  </div>
						   <div class="col-md-2">
						  <button type="button" class="btn btn-primary" id="btn-add" style="align-items: right">Tambah</button>
						  </div>
					  </form>
            		</div>
           		 <!-- /.box-header -->
	            <div class="box-body table-responsive no-padding">
	              <table class="table table-hover" id="tbl-user">
					<tr>
						<td><b>USERNAME</b></td>
						<td><b>ROLE</b></td>
						<td><b>EMAIL</b></td>
						<td><b>#</b></td>
					</tr>
					
					<tbody id="list-user">
					<!-- isi Listnya -->
					</tbody>
					
	              </table>
	            </div>
            <!-- /.box-body -->
		</div>
         <!-- /.box -->
	</div>
</div>
	
	

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form PopUp User</h4>
			</div> -->
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script>

	loadListUser(); /* untuk manggil otomatis krn tidak ada ready */
	
	function loadListUser(){
		$.ajax({
			url:'user/cruds/list.html',
			type:'get',
			dataType:'html',
			success: function(result){
				$('#list-user').html(result);
			}
		});
	}
	
	function cek(){
		var password = document.getElementId("password");
		var repassword = document.getElementId("repassword");
		
		if(password.value != repassword.value){
			alert("beda coy");
		}
		return false;
	}
		
	//document ready itu setelah halaman dipanggil
	$(document).ready(function(){
		
		//fungsi ajax untuk popup tambah
		$('#btn-add').on('click',function(){
			$.ajax({
				url:'user/cruds/add.html',
				type:'get',
				dataType:'html',
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup tambah
		
		
		//fungsi ajax untuk create tambah
		$('#modal-input').on('submit','#form-user-add',function(){
			$.ajax({
				url:'user/cruds/create.json',
				type:'get',
				dataType:'json',
				data: $(this).serialize(),
				success: function(result){
					if (result.jumlahDataUsername > 0) {
						alert("Username sudah ada "+result.password+" dan "+result.repassword+"");
					} else if(result.password != result.repassword){
						alert("Please input match password..");
					} else {
						$('#modal-input').modal('hide');
						alert("Data successfully saved");
						loadListUser();
					}
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk create tambah
		
		/* //fungsi ajax untuk lihat
		$('#list-jurusan').on('click','#btn-detail',function(){
			var idJurusan = $(this).val();
			$.ajax({
				url:'jurusan/cruds/detail.html',
				type:'get',
				dataType:'html',
				data:{idJurusan:idJurusan},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk lihat */
		
		//fungsi ajax untuk ubah (EDIT)
		$('#list-user').on('click','#btnEdit',function(){
			var id = $(this).attr('data-value');
			console.log("id : " + id);
			$.ajax({
				url:'user/cruds/edit.html',
				type:'get',
				dataType:'html',
				data:{id : id},
				success: function(result){ 
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk ubah
		
		//fungsi ajax untuk update
		$('#modal-input').on('submit','#form-user-edit',function(){
			
			$.ajax({
				url:'user/cruds/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data Successfully updated ! ");
					loadListUser();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update
		
		//fungsi untuk tampil form reset password
		$('#list-user').on('click','#btnReset',function(){
			var id = $(this).attr('data-value');
			console.log("id : " + id);
			$.ajax({
				url:'user/cruds/resetPassword.html',
				type:'get',
				dataType:'html',
				data:{id : id},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk reset password
		
		 //fungsi ajax untuk action reset password
		$('#modal-input').on('submit','#form-reset-password',function(){
			$.ajax({
				url:'user/cruds/reset.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					
					if (result.password != result.repassword) {
						alert("Please input match password..");
					} else {
						$('#modal-input').modal('hide');
						alert("Password has been reset ! ");
						loadListUser();
					}
					
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update 
		
		//fungsi ajax untuk hapus
		$('#list-user').on('click','#btnDelete',function(){
			 var cmt = confirm('Are you sure to delete this data?');
			 if (cmt == true) {
				var id = $(this).attr('data-value');
					console.log("id : " + id);
     			$.ajax({
     				url:'user/cruds/delete.json',
     				type:'get',
     				dataType:'json',
     				data:{id : id},
     				success: function(result){
     					$('#modal-input').modal('hide');
    					alert("Data successfully deleted !");
    					loadListUser();
     				}
     			});
             }
             else {
                
                 return false;
             }
			
		});
		//akhir fungsi ajax untuk hapus
		
		/* //fungsi ajax untuk delete
		$('#modal-input').on('submit','#form-jurusan-delete',function(){
			
			$.ajax({
				url:'jurusan/cruds/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah dihapus");
					loadListJurusan();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete
		
		//fungsi ajax untuk search namaJurusan
		$('#form-jurusan-search-nama').on('submit',function(){
			$.ajax({
				url:'jurusan/cruds/search/nama.html',
				type:'get',
				dataType:'html',
				data: $(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan nama");
					$('#list-jurusan').html(result); //reload list jurusan
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search namaJurusan */
		
		//fungsi ajax untuk search Username dan password
		$('#form-search').on('submit',function(){
			$.ajax({
				url:'user/cruds/search/usernamepass.html',
				type:'get',
				dataType:'html',
				data: $(this).serialize(),
				success: function(result){
					
					$('#list-user').html(result); //reload list jurusan
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search kodeJurusan
		
		/* //fungsi ajax untuk search kodeJurusan /namaJurusan
		$('#form-jurusan-search').on('submit',function(){
			$.ajax({
				url:'jurusan/cruds/search.html',
				type:'get',
				dataType:'html',
				data: $(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan kode/ nama jurusan");
					$('#list-jurusan').html(result); //reload list jurusan
				}
			});
			return false;
		}); */
		//akhir fungsi ajax untuk search kodeJurusan/namaJurusan
		
	});
	//akhir dokumen ready
</script>