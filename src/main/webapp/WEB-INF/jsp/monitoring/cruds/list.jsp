<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${monitoringModelList}" var="monitoringModel">
	<tr>
		<td>${monitoringModel.biodataModel.name}</td>
		<fmt:formatDate value="${monitoringModel.idleDate}" pattern="dd-MM-yyyy" var="idleDate"/>
		<td>${idleDate}</td>
		<fmt:formatDate value="${monitoringModel.placementDate}" pattern="dd-MM-yyyy" var="placementDate"/>
		<td>${placementDate}</td>
		<td>
			<div class="dropdown" align="right">
				<div class="button-group" align="left">
					<button type="button" class="btn-box-tool dropdown-toggler" data-toggle= "dropdown">
						<i class="fa fa-navicon"></i>
					</button>
					<ul id="menu" class="dropdown-menu" role="menu">
						<li><a id="btn-edit" data-value="${monitoringModel.idMonitoring}">Edit</a></li>
						<c:choose >
							<c:when test="${monitoringModel.placementDate == null }">
								<li><a id="btn-placement" data-value="${monitoringModel.idMonitoring}">Placement</a></li>
							</c:when>
							<c:when test="${monitoringModel.placementDate != null }">
								<li><a id="btn-placementEdit" data-value="${monitoringModel.idMonitoring}">Placement</a></li>
							</c:when>
						</c:choose>
						<li><a id="btn-delete" data-value="${monitoringModel.idMonitoring}">Delete</a></li>
					</ul>
				</div>
			</div>
		</td>
	</tr>
	
</c:forEach>