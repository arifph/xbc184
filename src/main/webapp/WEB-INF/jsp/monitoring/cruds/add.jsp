<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div>
	<div>
		<table class="table" id="tbl-header-idleMonitoring">
			<tr bgcolor="orange">
				<td><h3><b><font color="white">INPUT IDLE</font></b></h3></td>
			</tr>
		</table>
	</div>
	
	<form action="#" method="get" id="form-monitoring-add">
		<table style="width: 80%; margin-left: 10%; margin-right: 10%; border-collapse: separate; border-spacing: 15px; ">
			<tr>
				<td>
					<select id="idBiodata" name="idBiodata" style="width: 95%">
							<option value="" disabled selected >-Choose Biodata Name-</option>
						<c:forEach items="${biodataModelList}" var="biodataModel" >
							<option value="${biodataModel.id}">
								${biodataModel.name}
							</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<input type="date" id="idleDate" name="idleDate" size="63%">
				</td>
			</tr>
			<tr>
				<td>
					<input type="text" placeholder="Last Project at" id="lastProject" name="lastProject" size="63%">
				</td>
			</tr>
			<tr>
				<td>
					<input type="text" placeholder="Idle Reason" id="idleReason" name="idleReason" size="63%">
				</td>
			</tr>
		</table>
		<button type="submit" style="float: right; margin-right: 10%; margin-bottom: 15%;">SAVE</button>
		<button type="button" data-dismiss="modal" style="float: right; margin-right: 5%; margin-bottom: 15%;">CANCEL</button>
	</form>
	<hr>
</div>