<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<div>
	<div>
		<table class="table" id="tbl-header-idleMonitoring">
			<tr bgcolor="orange">
				<td><h3><b><font color="white">EDIT IDLE</font></b></h3></td>
			</tr>
		</table>
	</div>
	
	<form action="#" method="get" id="form-monitoring-edit">
	<input type="hidden" id="idMonitoring" name="idMonitoring" value ="${monitoringModel.idMonitoring}"/>
	<input type="hidden" id="idBiodata" name="idBiodata" value ="${monitoringModel.idBiodata}"/>
		<table style="width: 80%; margin-left: 10%; margin-right: 10%; border-collapse: separate; border-spacing: 15px; ">
			<tr>
				<td>
					<input type="text" id="idBiodata" name="idBiodata" size="63%"
					placeholder="${monitoringModel.biodataModel.name}"  ${biodataModel.id==monitoringModel.idBiodata ? 'selected="true"':''} disabled>
					<%-- <select id="idBiodata" name="idBiodata" style="width: 95%">
						<c:forEach items="${biodataModelList}" var="biodataModel" >
							<option value="${biodataModel.id}" ${biodataModel.id==monitoringModel.idBiodata ? 'selected="true"':''}>
								${biodataModel.name}
							</option>
						</c:forEach>
					</select> --%>
				</td>
			</tr>
			<tr>
				<td>
					
					<%-- <fmt:parseDate value="${date}" type="BOTH" pattern="mm/dd/yyyy" var="idleDate"/> --%>
					<%-- <p>${idleDate}</p> --%>
					<input type="date" id="idleDate" name="idleDate" size="63%" value="${idleDate}">
				</td>
			</tr>
			<tr>
				<td>
					<input type="text" placeholder="Last Project at" id="lastProject" name="lastProject" size="63%" value="${monitoringModel.lastProject}">
				</td>
			</tr>
			<tr>
				<td>
					<input type="text" placeholder="Idle Reason" id="idleReason" name="idleReason" size="63%" value="${monitoringModel.idleReason}">
				</td>
			</tr>
		</table>
		<button type="submit" style="float: right; margin-right: 10%; margin-bottom: 15%;">SAVE</button>
		<button type="button" data-dismiss="modal" style="float: right; margin-right: 5%; margin-bottom: 15%;">CANCEL</button>
	</form>
	<hr>
</div>