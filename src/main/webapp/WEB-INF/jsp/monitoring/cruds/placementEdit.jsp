<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div>
	<div>
		<table class="table" id="tbl-header-placement">
			<tr bgcolor="#ff9933">
				<td><h3><b><font color="white">EDIT PLACEMENT</font></b></h3></td>
			</tr>		
		</table>
	</div>
	
	<form action="#" method="get" id="form-monitoring-placementEdit">
	<input type="hidden" id="idMonitoring" name="idMonitoring" value ="${monitoringModel.idMonitoring}"/>
		<table style="width: 80%; margin-left: 10%; margin-right: 10%; border-collapse: separate; border-spacing: 15px; ">
			<tr>
				<td>
					<input type="date" id="placementDate" name="placementDate" style="width: 60%" value="${placementDate}">
				</td>
			</tr>
			<tr>
				<td>
					<input type="text" id="placementAt" name="placementAt" size="63%" placeholder="Placement At" value="${monitoringModel.placementAt}">
				</td>
			</tr>
			<tr>
				<td>
					<textarea rows="3" cols="65%" name="notes" id="notes" placeholder="Notes" >${monitoringModel.notes}</textarea>
				</td>
			</tr>
		</table>
		<button type="submit" style="float: right; margin-right: 10%; margin-bottom: 15%;">SAVE</button>
		<button type="button" data-dismiss="modal" style="float: right; margin-right: 5%; margin-bottom: 15%;">CANCEL</button>
	</form>
	<hr>
</div>