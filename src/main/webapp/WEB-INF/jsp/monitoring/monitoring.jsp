<div class="panel" style="background : white; margin-top: 50px; min-height: 500px;">
	<div>
		<table class="table" id="tbl-header-monitoring">
			<tr bgcolor="orange">
				<td><h3><b><font color="white">IDLE MONITORING</font></b></h3></td>
		</table>
	</div>
		<table class="table" id="tbl-monitoring" style="width: 70%; margin-left: 15%; margin-right: 15%;">
			<tr>
				<td>
					<form action="#" method="get" id="form-monitoring-search-name">
						<input placeholder="Search by name" type="text" id="nameKey" name="nameKey" size="20">
						<button class="btn btn-warning" type="submit">O</button>
					</form>
				</td>
				<td></td>
				<td></td>
				<td>
					<button type="button" class="btn btn-warning" id="btn-add" >+</button>
				</td>
			</tr>
			<tr bgcolor="orange">
				<td>NAME</td>
				<td>IDLE DATE</td>
				<td>PLACEMENT DATE</td>
				<td>#</td>
			</tr>
			<tbody id="list-monitoring">
				<!-- listnya -->
			</tbody>
		</table>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script>

	loadListMonitoring();
	
	function loadListMonitoring() {
		$.ajax({
			url: 'monitoring/cruds/list.html',
			type: 'get',
			dataType: 'html',
			success: function(result) {
				$('#list-monitoring').html(result);
			}
		});
	}
	
	//document ready setelah halaman dipanggil
	$(document).ready(function() {
		
		//fungsi ajax untuk pop up tambah
		$('#btn-add').on('click', function(){
			$.ajax({
				url:'monitoring/cruds/add.html',
				type:'get',
				dataType:'html',
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		}); // akhir fungsi ajax untuk popup tambah
		
		
		//fungsi ajax untuk tambah
		$('#modal-input').on('submit', '#form-monitoring-add', function(){
			$.ajax({
				url:'monitoring/cruds/create.json',
				type:'get',
				dataType:'json',
				data: $(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully saved");
					loadListMonitoring();
				}
			});
			return false;
		}); // akhir fungsi ajax  tambah
		
		
		//fungsi search nama Idle Monitoring
		$('#form-monitoring-search-name').on('submit',function(){
			$.ajax({
				url:'monitoring/cruds/search/name.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("Data successfully submitted");
					$('#list-monitoring').html(result);
				}
			});
			return false;
		});
		//akhir fungsi search nama Idle Monitoring
		
		//fungsi ajax untuk pop up tambah placement
		$('#list-monitoring').on('click','#btn-placement', function(){
			var idMonitoring = $(this).attr("data-value");
			$.ajax({
				url:'monitoring/cruds/placementAdd.html',
				type:'get',
				dataType:'html',
				data:{idMonitoring : idMonitoring},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		}); // akhir fungsi ajax untuk popup tambah placememt
		
		 //fungsi ajax untuk update tambah placement
		$('#modal-input').on('submit','#form-monitoring-placementAdd',function(){
			$.ajax({
				url:'monitoring/cruds/createPlacement.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					if(result.dateResult == 0){
						alert("Idle Date isn't allowed more than Placement Date");
					} else {
						$('#modal-input').modal('hide');
						alert("Data successfully added");
						loadListMonitoring();	
					}
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update  tambah placement
		
		//fungsi ajax untuk pop up hapus monitoring
		$('#list-monitoring').on('click','#btn-delete', function(){
			var idMonitoring = $(this).attr("data-value");
			$.ajax({
				url:'monitoring/cruds/remove.html',
				type:'get',
				dataType:'html',
				data:{idMonitoring : idMonitoring},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		}); // akhir fungsi ajax untuk hapus monitoring
		
		//fungsi ajax untuk delete monitoring
		$('#modal-input').on('submit','#form-monitoring-delete',function(){
			$.ajax({
				url:'monitoring/cruds/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully deleted");
					loadListMonitoring();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete monitoring
		
		//fungsi ajax untuk ubah idle
		$('#list-monitoring').on('click','#btn-edit',function(){
			var idMonitoring = $(this).attr("data-value");
			$.ajax({
				url:'monitoring/cruds/edit.html',
				type:'get',
				dataType:'html',
				data:{idMonitoring : idMonitoring},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk ubah idle
		
		 //fungsi ajax untuk update
		$('#modal-input').on('submit','#form-monitoring-edit',function(){
			$.ajax({
				url:'monitoring/cruds/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully updated");
					loadListMonitoring();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update 
		
		//fungsi ajax untuk pop up edit placement
		$('#list-monitoring').on('click','#btn-placementEdit', function(){
			var idMonitoring = $(this).attr("data-value");
			$.ajax({
				url:'monitoring/cruds/placementEdit.html',
				type:'get',
				dataType:'html',
				data:{idMonitoring : idMonitoring},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		}); // akhir fungsi ajax untuk popup edit placememt
		
		//fungsi ajax untuk update edit placement
		$('#modal-input').on('submit','#form-monitoring-placementEdit',function(){
			$.ajax({
				url:'monitoring/cruds/updatePlacement.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					if(result.dateResult == 0){
						alert("Idle Date isn't allowed more than Placement Date");
					} else {
						$('#modal-input').modal('hide');
						alert("Data successfully added");
						loadListMonitoring();	
					}
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update edit placement
		
	}); // akhir dokumen ready
</script>