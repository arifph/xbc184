<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<div>
		<table class="table" id="tbl-header-idleNews">
			<tr bgcolor="orange">
				<td><h3><b><font color="white">IDLE NEWS</font></b></h3></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
	
	
	<form action="#" method="get" id="form-idleNews-add">
		<table style="width: 80%; margin-left: 10%; margin-right: 10%; border-collapse: separate; border-spacing: 15px; "> 
			<tr>
				<td><input type="text" placeholder="Tittle" id="tittleIdleNewsType" name="tittleIdleNews" size="63%"> </td>
			</tr>
			<tr>
				<td>
					<select id="idCategory" name="idCategory">
						<option value="">Choose Category</option>
						<c:forEach items="${categoryModelList}" var="categoryModel">
							<option value="${categoryModel.id}">
								${categoryModel.name}
							</option>
						</c:forEach>						
					</select>
					</td>
					<tr>
			<td>
				<textarea rows="3" cols="65%" name="contentIdleNews" id="contentIdleNews" placeholder="Content" ></textarea>
				</td>
					</tr>
				</table>
		<button type="submit" style="float: right; margin-right: 10%; margin-bottom: 15%;">SAVE</button>
		<button type="button" data-dismiss="modal" style="float: right; margin-right: 5%; margin-bottom: 15%;">CANCEL</button>
	</form>
	<hr>
</div>