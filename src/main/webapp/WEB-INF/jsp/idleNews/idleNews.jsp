<div class="panel" style="background : white; margin-top: 50px; min-height: 500px;">
	<div>
		<table class="table" id="tbl-header-idleNews">
			<tr bgcolor="orange">
				<td><h3><b><font color="white">IDLE NEWS</font></b></h3></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
		<table class="table" id ="tbl-idleNews" style="width: 70%; margin-left: 15%; margin-right: 15%;">
			<tr>
				<td>
					<form action="#" method="get" id="form-idleNews-search-name">
						<input placeholder="Search by name" type="text" id="nameIdleNewsKey" name="nameIdleNewsTypeKey" size="20">
						<button class="btn btn-warning" type="submit">O</button>
					</form>
				</td>
				<td></td>
				<td >
					<button type="button" class="btn btn-warning" id="btn-add" >+</button>
				</td>
			</tr>
			<tr bgcolor="orange">
				<td>TITTLE</td>
				 <td>CATEGORY</td> 
				<td>#</td>
			</tr>
			<tbody id="list-idleNews">
			<!-- isi listya -->
			</tbody>
		</table>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script>

	loadListIdleNews();
	
	function loadListIdleNews() {
		$.ajax({
			url: 'idleNews/cruds/list.html',
			type: 'get',
			dataType:'html',
			success: function(result) {
				$('#list-idleNews').html(result);
			}
		});
	}
	//document ready itu setelah halaman dipanggil
	$(document).ready(function(){
		
		//fungsi ajax untuk pop up tambah
		$('#btn-add').on('click', function(){
			$.ajax({
				url:'idleNews/cruds/add.html',
				type:'get',
				dataType:'html',
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		}); // akhir fungsi ajax untuk popup tambah
		
		//fungsi ajax untuk tambah
		$('#modal-input').on('submit', '#form-idleNews-add', function(){
			console.log($(this).serialize());
			$.ajax({
				url:'idleNews/cruds/create.json',
				type:'get',
				dataType:'json',
				data: $(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully saved");
					loadListIdleNews();
				}
			});
			return false;
		}); // akhir fungsi ajax  tambah
		
		//fungsi ajax untuk ubah
		$('#list-idleNews').on('click','#btn-edit',function(){
			var idIdleNews = $(this).attr("data-value");
			console.log("ididleNews: " + idIdleNews);
			$.ajax({
				url:'idleNews/cruds/edit.html',
				type:'get',
				dataType:'html',
				data:{idIdleNews : idIdleNews},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk ubah
		
		//fungsi ajax untuk update
		$('#modal-input').on('submit','#form-idleNews-edit',function(){
			$.ajax({
				url:'idleNews/cruds/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully updated");
					loadListIdleNews();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update
	
		//fungsi search nama Bootcamp Type
		$('#form-idleNews-search-name').on('submit',function(){
			console.log($(this).serialize());
			$.ajax({
				url:'idleNews/cruds/search/name.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("Data successfully submitted");
					$('#list-idleNews').html(result);
				}
			});
			return false;
		});
		//akhir fungsi search nama Bootcamp Type
		
		//fungsi ajax untuk hapus
		$('#list-idleNews').on('click','#btn-delete',function(){
			var idIdleNews = $(this).attr("data-value");
			console.log("ididleNews: " + idIdleNews);
			
			/* var idIdleNews =$(this).val();
			 var ididleNews = $(this).val();  */
			$.ajax({
				url:'idleNews/cruds/remove.html',
				type:'get',
				dataType:'html',
				data:{idIdleNews:idIdleNews},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk hapus
		
		$('#list-idleNews').on('click','#btn-publish',function(){
			var idIdleNews = $(this).attr("data-value");
			console.log("ididleNews: " + idIdleNews);
			
			/* var idIdleNews =$(this).val();
			 var ididleNews = $(this).val();  */
			$.ajax({
				url:'idleNews/cruds/publish.html',
				type:'get',
				dataType:'html',
				data:{idIdleNews:idIdleNews},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		
		//fungsi ajax untuk delete
		$('#modal-input').on('submit','#form-idleNews-delete',function(){
			var idIdleNews = $(this).attr("data-value");
			console.log("ididleNews: " + idIdleNews);
			$.ajax({
				url:'idleNews/cruds/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully deleted");
					loadListIdleNews();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete
		
		$('#modal-input').on('submit','#form-idleNews-publish',function(){
			var idIdleNews = $(this).attr("data-value");
			console.log("ididleNews: " + idIdleNews);
			$.ajax({
				url:'idleNews/cruds/published.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully published");
					loadListIdleNews();
				}
			});
			return false;
		});
		
	}); 
	//akhir dokumen ready
</script>