<%@ taglib uri= "http://java.sun.com/jstl/core_rt" prefix= "c" %>
<div class= "form-horizontal">
	<div>
		<table class= "table" id= "tbl-header-role">
			<tr bgcolor= "#0066ff">
				<td><h4><b><font color="white">SET CHOICES</font></b></h4></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
	
	<form action= "#" method= "get" id= "form-question-edit">
	<input type= "hidden" id= "idQuestion" name= "idQuestion" value= "${questionModel.idQuestion}" />
		<table style= "width: 80%; margin-left: 10%; margin-right: 10%; border-collapse: separate; border-spacing: 15px;">
			<tr>
				<td><input type= "text" id= "optionA" name= "optionA" value= "${questionModel.optionA}" placeholder= "A" size= 53%/></td>
			</tr>
			
			<tr>
				<td><input type= "text" id= "optionB" name= "optionB" value= "${questionModel.optionB}" placeholder= "B" size= 53%/></td>
			</tr>
			
			<tr>
				<td><input type= "text" id= "optionC" name= "optionC" value= "${questionModel.optionC}" placeholder= "C" size= 53%/></td>
			</tr>
			
			<tr>
				<td><input type= "text" id= "optionD" name= "optionD" value= "${questionModel.optionD}" placeholder= "D" size= 53%/></td>
			</tr>
			
			<tr>
				<td><input type= "text" id= "optionE" name= "optionE" value= "${questionModel.optionE}" placeholder= "E" size= 53%/></td>
			</tr>
			
		</table>
		
		<button type= "submit" style="float: right; margin-right: 10%; margin-bottom: 15%;">SAVE</button>
		<button type= "button" data-dismiss= "modal" style="float: right; margin-right: 5%; margin-bottom: 15%;">CANCEL</button>

	</form>
	
	<hr>
	
</div>