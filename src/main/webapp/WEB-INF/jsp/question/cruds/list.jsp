<%@ taglib uri= "http://java.sun.com/jstl/core_rt" prefix= "c" %>
<%@ taglib uri= "http://java.sun.com/jsp/jstl/fmt" prefix= "fmt" %>

<c:forEach items= "${questionModelList}" var= "questionModel" varStatus= "number">
	<tr>
		<td width= "45%" style= "white-space: nowrap; overflow: hidden; display: block; text-overflow: ellipsis">${questionModel.question}</td>
		<td>
			<c:choose>
				<c:when test="${questionModel.questionType == 'MC'}">
					MULTIPLE CHOICE
				</c:when>
				<c:when test="${questionModel.questionType == 'ES'}">
					ESSAY
				</c:when>
			</c:choose>
		</td>
		<td>
			<div class= "dropdown" align= "center">
				<div class= "button-group" align= "center">
					<button type= "button" class= "btn-box-tool dropdown-toggler" data-toggle= "dropdown">
						<i class= "fa fa-navicon"></i>
					</button>
					<ul id= "menu" class= "dropdown-menu" role= "menu">
						<c:choose>
							<c:when test="${questionModel.questionType == 'MC'}">
								<li><a id= "btn-edit" data-value= "${questionModel.idQuestion}">Set Choices</a></li>
							</c:when>
						</c:choose>
						<li><a id= "btn-view" data-value= "${questionModel.idQuestion}">View Question</a></li>
						<li><a id= "btn-delete" data-value= "${questionModel.idQuestion}">Delete</a></li>
					</ul>
				</div>
			</div>
		</td>
	</tr>
</c:forEach>