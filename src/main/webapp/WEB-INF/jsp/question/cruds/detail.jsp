<%@ taglib uri= "http://java.sun.com/jstl/core_rt" prefix= "c" %>
<div class= "form-horizontal">
	<div>
		<table class= "table" id= "tbl-header-question">
			<tr bgcolor= "#0066ff">
				<td><h4><b><font color="white">VIEW QUESTION</font></b></h4></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
	
	<form action= "#" method= "get" id= "form-question-detail">
		<table style= "width: 80%; margin-left: 10%; margin-right: 10%; border-collapse: separate; border-spacing: 15px;">
			<tr>
				<td>
					<c:choose>
						<c:when test="${questionModel.questionType == 'MC'}">
							<input type= "text" placeholder= "Multiple Choice" size= 53% disabled/>
						</c:when>
						<c:when test="${questionModel.questionType == 'ES'}">
							<input type= "text" placeholder= "Essay" size= 53% disabled/>
						</c:when>
					</c:choose>
				</td>
			</tr>
			
			<tr>
				<td><textarea rows= "4" cols= "55%" placeholder= "${questionModel.question}" disabled style= "resize: none"></textarea>
			</tr>
			
			<%-- <tr>
				<td><textarea rows= "4" cols= "65%"placeholder= "${questionModel.imageUrl}" disabled></textarea>
			</tr> --%>
		</table>
		
		<button type= "button" data-dismiss= "modal" style="float: right; margin-right: 5%; margin-bottom: 15%;">CANCEL</button>
		
	</form>
	
	<hr>
</div>