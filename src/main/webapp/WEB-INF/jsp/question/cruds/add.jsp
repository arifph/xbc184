<%@ taglib uri= "http://java.sun.com/jstl/core_rt" prefix= "c" %>
<div class= "form-horizontal">
	<div>
		<table class= "table" id= "tbl-header-role">
			<tr bgcolor= "#0066ff">
				<td><h4><b><font color="white">ADD QUESTION</font></b></h4></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
	
	<form action= "#" method= "get" id= "form-question-add">
		<table style= "width: 80%; margin-left: 10%; margin-right: 10%; border-collapse: separate; border-spacing: 15px;">
			<tr>
				<td>
					<select id= "questionType" name= "questionType" style= "width: 95%">
						<option value= null disabled selected>- Choose Question Type-</option>
						<option value= "MC">Multiple Choice</option>
						<option value= "ES">Essay</option>
					</select>
				</td>
			</tr>
			
			<tr>
				<td><textarea rows= "4" cols= "55%" name= "question" placeholder= "Question" style= "resize: none"></textarea>
			</tr>
			
<!-- 		<tr>
				<td><input type= "file" name= "imageUrl" size= "50"/></td>
			</tr>  -->
			
			<!-- <tr>
				<td><input type= "text" id= "imageUrl" name= "imageUrl" placeholder= "Upload Image -placeholder-" size= 55%/></td>
				<td><button type= "button">Browse</button></td>
			</tr> -->
			
		</table>
		
		<button type= "submit" style="float: right; margin-right: 10%; margin-bottom: 15%;">SAVE</button>
		<button type= "button" data-dismiss= "modal" style="float: right; margin-right: 5%; margin-bottom: 15%;">CANCEL</button>

	</form>
	
	<hr>
	
</div>