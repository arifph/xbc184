<%@ taglib uri= "http://java.sun.com/jstl/core_rt" prefix= "c" %>
<div class= "form-horizontal">
	<div>
		<table class= "table" id= "tbl-header-question">
			<tr bgcolor= "#0066ff">
				<td><h4><b><font color="white">DELETE</font></b></h4></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
	
	<form action= "#" method= "get" id= "form-question-delete">
		<input type= "hidden" id= "idQuestion" name= "idQuestion" value= "${questionModel.idQuestion}" />
		<div style= "margin: auto; width: 60%;">
			<h3><b>Are you sure to delete this data ?</b></h3>
		</div>
		
		<button type= "button" class= "btn btn-danger" data-dismiss= "modal" style="float: left; margin-left: 20%;">No</button>
		<button type= "submit" class= "btn btn-success" style="float: right; margin-right: 20%;">Yes</button>
	</form>
	
	<hr>
	<hr>
</div>