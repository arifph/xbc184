<div class= "panel" style = "background: white; margin-top: 50px; min-height: 500px">
	<div>
		<table class= "table" id= "tbl-header-role">
			<tr bgcolor= "#0066ff">
				<td><h4><b><font color="white">QUESTION</font></b></h4></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
	
	<div>
		<table class= "table" id ="tbl-question" style= "width: 70%; margin-left: 15%; margin-right: 15%; table-layout:fixed">
			<tr>
				<td>
					<form action= "#" method= "get" id= "form-question-search-question">
						<input type= "text" id= "questionQuestionKey" name= "questionQuestionKey" size= "15" placeholder="Search by question"/>
						<button class= "btn btn-primary" type= "submit">Search</button>
					</form>
				</td>
				<td></td>
				<td><button type = "button" class= "btn btn-primary" id= "btn-add">Add</button></td>
			</tr>
			<tr></tr>
			<tr bgcolor="#3399ff">
				<td>QUESTION</td>
				<td>TYPE</td>
				<td align= "center">#</td>
			</tr>
			<tbody id= "list-question">
			</tbody>
		</table>
	</div>
</div>

<div id= "modal-input" class= "modal fade">
	<div class= "modal-dialog">
		<div class= "modal-content">
			<div class= "modal-header">
			</div>
			<div class= "modal-body">
				<!-- Tempat jsp PopUp -->
			</div>
		</div>
	</div>
</div>

<script>
	
	loadListQuestion();
	
	function loadListQuestion() {
		$.ajax({
			url: 'question/cruds/list.html',
			type: 'get',
			dataType: 'html',
			success: function(result) {
				$('#list-question').html(result);
			}
		})
	}
	
	//Document ready setelah halaman dipanggil
	$(document).ready(function() {
		
		//Fungsi ajax untuk popup tambah
		$('#btn-add').on('click', function() {
			$.ajax({
				url: 'question/cruds/add.html',
				type: 'get',
				dataType: 'html',
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		})
		//Akhir fungsi ajax untuk popup tambah
		
		//Fungsi ajax untuk create tambah
		$('#modal-input').on('submit', '#form-question-add', function() {
			$.ajax({
				url: 'question/cruds/create.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully saved !");
					loadListQuestion();
				}
			});
			return false;
		});
		//Akhir fungsi ajax untuk create tambah
		
		//Fungsi ajax untuk ubah
		$('#list-question').on('click', '#btn-edit', function() {
			var idQuestion = $(this).attr("data-value");
			$.ajax({
				url: 'question/cruds/edit.html',
				type: 'get',
				dataType: 'html',
				data: {idQuestion: idQuestion},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		})
		//Akhir fungsi ajax untuk ubah
		
		//Fungsi ajax untuk update
		$('#modal-input').on('submit', '#form-question-edit', function() {
			$.ajax({
				url: 'question/cruds/update.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully Updated !");
					loadListQuestion();
				}
			});
			return false;
		})
		//Akhir fungsi ajax untuk update
		
		//Fungsi ajax untuk lihat
		$('#list-question').on('click', '#btn-view', function() {
			var idQuestion = $(this).attr("data-value");
			//var idQuestion = $(this).val();
			$.ajax({
				url: 'question/cruds/detail.html',
				type: 'get',
				dataType: 'html',
				data: {idQuestion: idQuestion},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		})
		//Akhir fungsi ajax untuk lihat
		
		//Fungsi ajax untuk hapus
		$('#list-question').on('click', '#btn-delete', function() {
			//var idRole = $(this).val();
			var idQuestion = $(this).attr("data-value");
			$.ajax({
				url: 'question/cruds/remove.html',
				type: 'get',
				dataType: 'html',
				data: {idQuestion: idQuestion},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		})
		//Akhir fungsi ajax untuk hapus
		
		//Fungsi ajax untuk delete
		$('#modal-input').on('submit', '#form-question-delete', function() {
			$.ajax({
				url: 'question/cruds/delete.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data successfully deleted !");
					loadListQuestion();
				}
			});
			return false;
		})
		//Akhir fungsi ajax untuk delete
		
		//Fungsi ajax untuk search question
		$('#form-question-search-question').on('submit', function() {
			$.ajax({
				url: 'question/cruds/search/question.html',
				type: 'get',
				dataType: 'html',
				data: $(this).serialize(),
				success: function(result){
					$('#list-question').html(result);
				}
			});
			return false;
		})
		//Akhir fungsi ajax untuk search question
		
	});
</script>	