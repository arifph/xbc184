<div class="panel" style="background : white; margin-top: 50px; min-height: 500px;">
	<div>
		<table class="table" id="tbl-header-bootcampType">
			<tr bgcolor="orange">
				<td><h3><b><font color="white">BOOTCAMP TYPE</font></b></h3></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
		<table class="table" id ="tbl-bootcampType" style="width: 70%; margin-left: 15%; margin-right: 15%; table-layout: fixed;">
			<tr>
				<td>
					<form action="#" method="get" id="form-bootcampType-search-name">
						<input placeholder="Search by name" type="text" id="nameBootcampTypeKey" name="nameBootcampTypeKey" size="20">
						<button class="btn btn-warning" type="submit">O</button>
					</form>
				</td>
				<td></td>
				<td >
					<button type="button" class="btn btn-warning" id="btn-add" >+</button>
				</td>
			</tr>
			<tr bgcolor="orange">
				<td>NAME</td>
				<td>CREATE BY</td>
				<td>#</td>
			</tr>
			<tbody id="list-bootcampType">
			<!-- isi listya -->
			</tbody>
		</table>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script>

	loadListBootcampType();
	
	function loadListBootcampType() {
		$.ajax({
			url: 'bootcampType/cruds/list.html',
			type: 'get',
			dataType:'html',
			success: function(result) {
				$('#list-bootcampType').html(result);
			}
		});
	}
	//document ready itu setelah halaman dipanggil
	$(document).ready(function(){
		
		//fungsi ajax untuk pop up tambah
		$('#btn-add').on('click', function(){
			$.ajax({
				url:'bootcampType/cruds/add.html',
				type:'get',
				dataType:'html',
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		}); // akhir fungsi ajax untuk popup tambah
		
		//fungsi ajax untuk tambah
		$('#modal-input').on('submit', '#form-bootcampType-add', function(){
			$.ajax({
				url:'bootcampType/cruds/create.json',
				type:'get',
				dataType:'json',
				data: $(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully saved");
					loadListBootcampType();
				}
			});
			return false;
		}); // akhir fungsi ajax  tambah
		
		//fungsi ajax untuk ubah
		$('#list-bootcampType').on('click','#btn-edit',function(){
			var idBootcampType = $(this).attr("data-value");
			/* console.log("idBootcampType: " + idBootcampType) */
			$.ajax({
				url:'bootcampType/cruds/edit.html',
				type:'get',
				dataType:'html',
				data:{idBootcampType : idBootcampType},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk ubah
		
		//fungsi ajax untuk update
		$('#modal-input').on('submit','#form-bootcampType-edit',function(){
			$.ajax({
				url:'bootcampType/cruds/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully updated");
					loadListBootcampType();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update
	
		//fungsi search nama Bootcamp Type
		$('#form-bootcampType-search-name').on('submit',function(){
			$.ajax({
				url:'bootcampType/cruds/search/name.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("Data successfully submitted");
					$('#list-bootcampType').html(result);
				}
			});
			return false;
		});
		//akhir fungsi search nama Bootcamp Type
		
		//fungsi ajax untuk hapus
		$('#list-bootcampType').on('click','#btn-delete',function(){
			var idBootcampType = $(this).attr("data-value");
			/* var idBootcampType = $(this).val(); */
			$.ajax({
				url:'bootcampType/cruds/remove.html',
				type:'get',
				dataType:'html',
				data:{idBootcampType:idBootcampType},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk hapus
		
		
		//fungsi ajax untuk delete
		$('#modal-input').on('submit','#form-bootcampType-delete',function(){
			$.ajax({
				url:'bootcampType/cruds/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully deleted");
					loadListBootcampType();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete
		
	}); 
	//akhir dokumen ready
</script>