<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<div>
		<table class="table" id="tbl-header-bootcampType">
			<tr bgcolor="orange">
				<td><h3><b><font color="white">ADD BOOTCAMP TYPE</font></b></h3></td>
			</tr>
		</table>
	</div>
	
	<form action="#" method="get" id="form-bootcampType-add">
		<table style="width: 80%; margin-left: 10%; margin-right: 10%; border-collapse: separate; border-spacing: 15px; ">
			<tr>
				<td><input type="text" placeholder="Name" id="nameBootcampType" name="nameBootcampType" size="63%"> </td>
			</tr>
			<tr>
				<td>
					<textarea rows="3" cols="65%" name="notesBootcampType" id="notesBootcampType" placeholder="Notes" ></textarea>
				</td>
			</tr>
		</table>
		
		<button type="submit" style="float: right; margin-right: 10%; margin-bottom: 15%;">SAVE</button>
		<button type="button" data-dismiss="modal" style="float: right; margin-right: 5%; margin-bottom: 15%;">CANCEL</button>
	</form>
	<hr>
</div>