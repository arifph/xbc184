<div class="container-fluid">
	<div class="panel panel-primary"
		style="background: white; margin-top: 50px; min-height: 500px;">
		<div class="panel-heading">
			<div class="panel-title">
				<h3>Category</h3>
			</div>
		</div>
		<div class="panel-content">
			<div class="row" style="margin: 2rem 1.5rem">
				<div class="col-sm-8">
					<form action="#" id="form-category-search" class="form-inline">
						<div class="form-group">
							<div class="input-group">
								<input type="text" id="form-category-search"
									class="form-control" name="keyword"
									placeholder="Search by name or code"><span
									class="input-group-btn">
									<button type="submit" class="btn btn-primary">Search</button>
								</span>
							</div>
						</div>
					</form>
				</div>
				<div class="col-sm-4" align="right">
					<button type="button" class="btn btn-primary" id="btnAdd">
						<span class="glyphicon glyphicon-plus"></span>
					</button>
				</div>
			</div>
			<div id="alert" class="alert alert-success alert-dismissible"
				role="alert" style="display: none;">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
			<div class="row" style="margin: 0.5rem 1.5rem">
				<div class="col-sm-12">
					<table class="table tabel-bordered" id="tbl-category">
						<tr>
							<th style="size: 1rem;">Code</th>
							<th style="size: 1.5rem;">Name</th>
							<th style="size: 0.5rem;">#</th>
						</tr>

						<tbody id="list-category">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
</div>

<script>
	/* 	$(document).ready(function() {
	 $('.alert').show();
	 setTimeout(function() {
	 $('.alert').hide();
	 }, 5000);
	 }) */

	/* $('#alert').on('closed.bs.alert', function() {
		
		
	}); */

	function loadListCategory() {
		$.ajax({
			url : 'category/cruds/list',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-category').html(result);
			}
		});
	}

	$(document).ready(function() {
		loadListCategory();
	});

	$('#btnAdd').on('click', function() {
		$.ajax({
			url : 'category/cruds/add.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#modal-input').find('.modal-content').html(result);
				$('#modal-input').modal('show');
			}
		});
	});

	$('#modal-input')
			.on(
					'submit',
					'#form-category-add',
					function() {
						console.log("executing add submit event");
						var name = $(this).find('#category-name').val();
						var description = $(this).find('#category-description')
								.val();
						var category = {
							"name" : name,
							"description" : description
						};
						console.log(category);
						$
								.ajax({
									url : 'category/cruds/create.json',
									type : 'GET',
									dataType : 'json',
									data : category,
									success : function(result) {
										console.log(result);
										$('#modal-input').modal('hide');
										loadListCategory();

										$('#alert')
												.append(
														'<div id="message">Data successfully saved!<div>');
										$(document)
												.ready(
														function() {
															$('.alert').show();
															setTimeout(
																	function() {
																		$(
																				'.alert')
																				.hide();
																		console
																				.log("removing message");
																		$(
																				'.alert')
																				.find(
																						'#message')
																				.remove();
																	}, 5000);
														});
									}
								});
						return false;
					});

	$('#list-category').on('click', '#btnEdit', function() {
		var id = $(this).attr('data-value');
		console.log("id : " + id);
		$.ajax({
			url : 'category/cruds/edit.html',
			type : 'get',
			dataType : 'html',
			data : {
				id : id
			},
			success : function(result) {
				$('#modal-input').find('.modal-content').html(result);
				$('#modal-input').modal('show');
			}
		});
	});

	$('#modal-input').on('submit', '#form-category-edit', function() {
		console.log("executing edit submit event");
		var id = $(this).find('#category-id').val();
		var code = $(this).find('#category-code').val();
		var name = $(this).find('#category-name').val();
		var description = $(this).find('#category-description').val();
		var category = {
			"id" : id,
			"code" : code,
			"name" : name,
			"description" : description
		};
		console.log(category);
		$.ajax({
			url : 'category/cruds/update.json',
			type : 'get',
			dataType : 'json',
			data : category,
			success : function(result) {
				$('#modal-input').modal('hide');
				loadListCategory();

				$('#alert').append('Data successfully updated!');
				$(document).ready(function() {
					$('.alert').show();
					setTimeout(function() {
						$('.alert').hide();
					}, 5000);
				})
			}
		});
		return false;
	});

	$('#list-category').on('click', '#btnDelete', function() {
		var id = $(this).attr('data-value');
		console.log("id : " + id);
		$.ajax({
			url : 'category/cruds/confirmation.html',
			type : 'get',
			dataType : 'html',
			data : {
				id : id
			},
			success : function(result) {
				$('#modal-input').find('.modal-content').html(result);
				$('#modal-input').modal('show');
			}
		});
	});

	$('#modal-input').on('click', '#btnYes', function() {
		console.log("deleting data");
		var id = $(this).val();
		console.log("id : " + id);
		$.ajax({
			url : 'category/cruds/delete.json',
			type : 'get',
			dataType : 'json',
			data : {
				id : id
			},
			success : function(result) {
				$('#modal-input').modal('hide');
				loadListCategory();

				$('#alert').append('Data successfully deleted');
				$(document).ready(function() {
					$('.alert').show();
					setTimeout(function() {
						$('.alert').hide();
					}, 5000);
				})
			}
		});
		return false;
	});

	$('#modal-input').on('click', '#btnNo', function() {
		console.log("canceling delete");
		$('#modal-input').modal('hide');
	});

	$('#form-category-search').on('submit', function() {
		console.log("Searching data");
		$.ajax({
			url : 'category/search/code-and-name',
			type : 'get',
			dataType : 'html',
			data : $(this).serialize(),
			success : function(result) {
				$('#list-category').html(result);
			}
		});
		return false;
	});
</script>