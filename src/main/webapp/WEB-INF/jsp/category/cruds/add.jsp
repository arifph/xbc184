<form action="#" method="GET" id="form-category-add"
	class="form-horizontal">
	<div class="modal-header  bg-primary">
		<h4 class="modal-title">Add Category</h4>
	</div>
	<div class="modal-body" style="margin-left: 5px; margin-right: 5px;">

		<input type="text" id="category-name" name="category-name"
			class="form-control" maxlength="50" placeholder="Name"
			style="margin-top: 10px;" required="required">

		<textarea id="category-description" name="category-description"
			class="form-control" maxlength="255" placeholder="Description"
			style="margin-top: 10px;" required="required"></textarea>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
		<button type="submit" class="btn btn-primary">Save</button>
	</div>
</form>
