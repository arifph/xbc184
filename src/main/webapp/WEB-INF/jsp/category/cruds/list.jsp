<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:forEach items="${categoryModelList}" var="categoryModel">
	<tr>
		<td>${categoryModel.code}</td>
		<td>${categoryModel.name}</td>
		<td>
			<div class="dropdown" align="right">
				<div class="button-group" align="left">
					<button type="button" class="btn btn-box-tool dropdown-toggler" data-toggle="dropdown">
						<i class="fa fa-navicon"></i>
					</button>
					<ul id="menu" class="dropdown-menu" role="button">
						<li><a id="btnEdit" data-value="${categoryModel.id}">Edit</a></li>
						<li><a id="btnDelete" data-value="${categoryModel.id}">Delete</a></li>
					</ul>
				</div>
			</div>
		</td>
	</tr>
</c:forEach>