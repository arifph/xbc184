
<form action="#" method="GET" id="form-category-edit"
	class="form-horizontal" role="form">
	<!-- <div class="container-fluid"> -->
		<div class="modal-header  bg-primary">
			<h4 class="modal-title">Edit Category</h4>
		</div>

		<div class="modal-body" style="margin-left: 5px; margin-right: 5px;">
			<input type="hidden" name="category-id" id="category-id"
				class="form-control" value="${categoryModel.id}">
			
				<input type="text" class="form-control" id="category-code"
					name="category-name" maxlength="50" placeholder="Name"
					required="required" disabled="disabled" style="margin-top: 10px;"
					value="${categoryModel.code}">
			
				<input type="text" class="form-control" id="category-name"
					name="category-name" maxlength="255" placeholder="Category" style="margin-top: 5px;"
					required="required" value="${categoryModel.name}">
			
			
				<textarea id="category-description" class="form-control"
					name="category-description" maxlength="255"
					placeholder="Description" required="required" style="margin-top: 5px;">${categoryModel.description}</textarea>
			
			<%-- <input type="hidden" name="category-id" id="category-id"
				value="${categoryModel.id}">
			<table>
				<tr>
					<td colspan="2"><input type="text" id="category-code"
						name="category-name" maxlength="50" placeholder="Name"
						required="required" disabled="disabled"
						value="${categoryModel.code}"></td>
				</tr>
				<tr>
					<td colspan="2"><input type="text" id="category-name"
						name="category-name" maxlength="50" placeholder="Name"
						required="required" value="${categoryModel.name}"></td>
				</tr>
				<tr>
					<td colspan="2"><input type="text" id="category-description"
						name="category-description" maxlength="255"
						placeholder="Description" required="required"
						value="${categoryModel.description}"></td>
				</tr>
				<tr>
					<td></td>
					<td align="right"><button type="submit" id="save">Save</button>
						<button type="button" id="cancel" data-dismiss="modal">Cancel</button></td>
				</tr>
			</table> --%>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
			<button type="submit" class="btn btn-primary">Save</button>
		</div>
	<!-- </div> -->
</form>


