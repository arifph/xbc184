<div class="container-fluid">
	<div class="panel panel-primary"
		style="background-color: white; margin-top: 50px; min-height: 500px">
		<div id="alert" class="alert alert-success alert-dismissible"
			role="alert" style="display: none;">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<div class="panel-heading">
			<div class="panel-title">
				<h3>Document Test</h3>
			</div>
		</div>
		<div class="panel-content">
			<div class="row" style="margin: 2rem 1.5rem">
				<div class="col-sm-8">
					<form action="#" id="form-document-test-search" class="form-inline">
						<div class="input-group">
							<input type="text" id="keyword" name="keyword"
								class="form-control" placeholder="Search by name or version">
							<span class="input-group-btn">
								<button type="submit" class="btn btn-primary">
									<span class="glyphicon glyphicon-search"></span>
								</button>
							</span>
						</div>
					</form>
				</div>
				<div class="col-sm-4" align="right">
					<button type="button" class="btn btn-primary" id="btnAdd"
						name="btnAdd">
						<span class="glyphicon glyphicon-plus"></span>
					</button>
				</div>
			</div>
			<div class="row" style="margin: 0.5rem 1.5rem">
				<div class="col-sm-12">
					<table class="table table-bordered" id="tbl-document-test">
						<tr>
							<th style="size: 1em;">TEST TYPE</th>
							<th style="size: 0.5em;">VERSION</th>
							<th style="size: 2em;">TEST</th>
							<th style="size: 0.5em;">#</th>
						</tr>
						<tbody id="list-document-test"></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog" role="document">
		<div class="modal-content"></div>
	</div>
</div>

<div id="modal-input-question" class="modal fade">
	<div class="modal-dialog" role="document">
		<div class="modal-content"></div>
	</div>
</div>

<script>
	function loadListDocumentTest() {
		$.ajax({
			url : 'document-test/cruds/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-document-test').html(result);
			}
		});
	}

	function loadListDocumentTestDetail() {
		$.ajax({
			url : 'document-test/cruds/edit/detail-list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#modal-input').find('#detail-list').html(result);
			}
		});
	}

	$(document).ready(function() {
		console.log("Showing document test list");
		loadListDocumentTest();
	});

	$('#btnAdd').click(function() {
		console.log("Showing add modal window");
		$.ajax({
			url : 'document-test/cruds/add.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#modal-input').find('.modal-content').html(result);
				$('#modal-input').modal('show');
			}
		});
	});

	$('#modal-input').on('change', '#select-test-type', function() {
		console.log("Setting version");
		var testTypeId = $(this).val();
		var documentTestId = $('#modal-input').find('#idDocumentTest').val();
		console.log(testTypeId)
		console.log(testTypeId != "-");
		if (testTypeId != "-") {
			$.ajax({
				url : 'document-test/cruds/add/set-version.json',
				type : 'get',
				dataType : 'json',
				data : {
					testTypeId : testTypeId,
					documentTestId : documentTestId
				},
				success : function(result) {
					console.log("Result: " + result);
					$('#modal-input').find('#version').val(result);
				}
			});
		}
	});

	$('#modal-input').on('click', '#generate-token-button', function() {
		console.log("Generating Token");
		$.ajax({
			url : 'document-test/generate-token.json',
			type : 'get',
			dataType : 'json',
			success : function(result) {
				console.log("The generated code : " + result);
				$('#modal-input').find('#token').val(result);
			}
		});
	});

	$('#modal-input')
			.on('submit',
					'#add-document-test-form',
					function() {
						if ($(this).find('#select-test').val() == "-"
								|| $(this).find('#select-test-type').val() == "-"
								|| $(this).find('#token').val() == "") {
							alert("Please complete the form input");
						} else {
							console.log("Saving document test");
							$
									.ajax({
										url : 'document-test/cruds/save.json',
										type : 'get',
										dataType : 'json',
										data : $(this).serialize(),
										success : function(result) {
											$('#modal-input').modal('hide');
											loadListDocumentTest();

											$('#alert')
													.append(
															'<div id="message">Data successfully saved!<div>');
											$(document)
													.ready(
															function() {
																$('.alert')
																		.show();
																setTimeout(
																		function() {
																			$('.alert'.hide());
																			console.log("removing message");
																			$('.alert').find('#message').remove();
																		}, 5000);
															});
										}
									});
						}
						return false;
					});

	$('#list-document-test').on('click', '#btnEdit', function() {
		console.log("Showing edit dialog");
		let id = $(this).attr('data-value');
		$.ajax({
			url : 'document-test/cruds/edit.html',
			type : 'get',
			dataType : 'html',
			data : {
				id : id
			},
			success : function(result) {
				$('.modal-content').html(result);
				loadListDocumentTestDetail();
				$('#modal-input').modal('show');
			}
		});
	});

	$('#modal-input').on('click', '#add-question', function() {
		console.log("Showing add question dialog");
		$.ajax({
			url : 'document-test/cruds/edit/add-question.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#modal-input-question').find('.modal-content').html(result);
				$('#modal-input-question').modal('show');
			}
		});
	});

	$('#modal-input-question').on('click', '#cancel-button', function() {
		console.log("Canceling add question");
		$('#modal-input-question').modal('hide');
	});

	$('#modal-input-question').on('submit', '#add-question-form', function() {
		console.log("Adding question");
		if($(this).find('#select-question').val() == "-") {
			alert("Please choose a question");
		}
		else {
			$.ajax({
				url : 'document-test/cruds/edit/save-question.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function() {
					$('#modal-input-question').modal('hide');
					loadListDocumentTestDetail();
				}
			});
		}
		return false;
	});

	$('#modal-input')
			.on(
					'submit',
					'#edit-document-test-form',
					function() {
						console.log("Updating document test");
						$
								.ajax({
									url : 'document-test/cruds/update.json',
									type : 'get',
									dataType : 'json',
									data : $(this).serialize(),
									success : function() {
										$('#modal-input').modal('hide');
										loadListDocumentTest();

										$('#alert')
												.append(
														'<div id="message">Data successfully updated!<div>');
										$(document)
												.ready(
														function() {
															$('.alert').show();
															setTimeout(
																	function() {
																		$(
																				'.alert')
																				.hide();
																		console
																				.log("removing message");
																		$(
																				'.alert')
																				.find(
																						'#message')
																				.remove();
																	}, 5000);
														});
									}
								});
						return false;
					});

	$('#modal-input').on('click', '#btnDelete', function() {
		console.log("Showing confirmation dialog");
		let index = $(this).attr('data-value');
		$.ajax({
			url : 'document-test/cruds/edit/confirm.html',
			type : 'get',
			dataType : 'html',
			data : {
				index : index
			},
			success : function(result) {
				$('#modal-input-question').find('.modal-content').html(result);
				$('#modal-input-question').modal('show');
			}
		});
		return false;
	});

	$('#modal-input-question').on('click', '#btnYes', function() {
		console.log("Deleting question");
		let index = $(this).val();
		$.ajax({
			url : 'document-test/cruds/edit/delete-question.json',
			type : 'get',
			dataType : 'json',
			data : {
				index : index
			},
			success : function() {
				$('#modal-input-question').modal('hide');
				loadListDocumentTestDetail();
			}
		});
		return false;
	}).on('click', '#btnNo', function() {
		$('#modal-input-question').modal('hide');
	});

	$('#list-document-test').on('click', '#btnCopy', function() {
		console.log("Showing copy document dialog");
		let documentTestId = $(this).attr('data-value');
		console.log("documentTestId = " + documentTestId);
		$.ajax({
			url : 'document-test/cruds/copy-document.html',
			type : 'get',
			dataType : 'html',
			data : {
				documentTestId : documentTestId
			},
			success : function(result) {
				$('.modal-content').html(result);
				$('#modal-input').modal('show');
			}
		});

		return false;
	});

	$('#modal-input')
			.on(
					'submit',
					'#copy-document-test-form',
					function() {
						console.log("Saving copy document");
						if ($(this).find('#select-test').val() == "-"
								|| $(this).find('#select-test-type').val() == "-"
								|| $(this).find('#token').val() == "") {
							alert("Please complete the form input");
						} else {
							console.log("Saving copy document test");
							$
									.ajax({
										url : 'document-test/cruds/copy-document/save.json',
										type : 'get',
										dataType : 'json',
										data : $(this).serialize(),
										success : function(result) {
											$('#modal-input').modal('hide');
											loadListDocumentTest();

											$('#alert')
													.append(
															'<div id="message">Data successfully saved!<div>');
											$(document)
													.ready(
															function() {
																$('.alert')
																		.show();
																setTimeout(
																		function() {
																			$(
																					'.alert')
																					.hide();
																			console
																					.log("removing message");
																			$(
																					'.alert')
																					.find(
																							'#message')
																					.remove();
																		}, 5000);
															});
										}
									});
						}
						return false;
					});

	$('#form-document-test-search').on('submit', function() {
		console.log("Searching data");
		$.ajax({
			url : 'document-test/cruds/search.html',
			type : 'get',
			dataType : 'html',
			data : $(this).serialize(),
			success : function(result) {
				$('#list-document-test').html(result);
			}
		});
		return false;
	});

	$('#list-document-test').on('click', '#btnDelete', function() {
		var id = $(this).attr('data-value');
		$.ajax({
			url : 'document-test/cruds/confirm',
			type : 'get',
			dataType : 'html',
			data : {
				id : id
			},
			success : function(result) {
				$('#modal-input').find('.modal-content').html(result);
				$('#modal-input').modal('show');
			}
		});
	});

	$('#modal-input').on('click', '#btnYes', function() {
		console.log("Deleting document-test");
		let id = $(this).val();
		$.ajax({
			url : 'document-test/cruds/delete.json',
			type : 'get',
			dataType : 'json',
			data : {
				id : id
			},
			success : function() {
				$('#modal-input').modal('hide');
				loadListDocumentTest();
			}
		});
		return false;
	}).on('click', '#btnNo', function() {
		$('#modal-input').modal('hide');
	});
	
	$('#list-document-test').on('click', '#btnView', function() {
		console.log("Viewing document-test");
		let id = $(this).attr('data-value');
		$.ajax({
			url : 'document-test/cruds/view.html',
			type : 'get',
			dataType : 'html',
			data : {
				id : id
			},
			success : function(result) {
				$('#modal-input').find('.modal-content').html(result);
				$('#modal-input').modal('show');
			}
		});
	});
</script>