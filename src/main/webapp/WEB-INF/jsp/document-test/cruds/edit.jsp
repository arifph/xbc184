<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form action="#" id="edit-document-test-form" class="form-horizontal">
	<div class="modal-header bg-primary">
		<h4 class="modal-title">Add Document Test</h4>
	</div>

	<div class="modal-body" style="margin-left: 5px; margin-right: 5px;">
		<input type="hidden" id="idDocumentTest" value="${documentTest.id}">
		<div class="form-group">
			<div class="col-sm-6">
				<select id="select-test" name="testId" class="form-control">
					<option value="-">-Choose Test-</option>
					<c:forEach items="${testModelList}" var="testModel">
						<option value="${testModel.idTest}"
							${testModel.idTest == documentTestModel.testId ? 'selected="selected"' : ''}>${testModel.nameTest}</option>
					</c:forEach>
				</select>
			</div>
			<div class="col-sm-6">
				<select id="select-test-type" name="testTypeId" class="form-control">
					<option value="-">-Choose Test Type-</option>
					<c:forEach items="${testTypeModelList}" var="testTypeModel">
						<option value="${testTypeModel.id}"
							${testTypeModel.id == documentTestModel.testTypeId ? 'selected="selected"' : ''}>${testTypeModel.name}</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-6">
				<input type="text" id="version" class="form-control" name="version"
					readonly="readonly" value="${documentTestModel.version}">
			</div>
			<div class="col-sm-6">
				<div class="input-group">
					<input type="text" id="token" class="form-control" name="token"
						readonly="readonly" maxlength="10"
						value="${documentTestModel.token}"> <span
						class="input-group-btn">
						<button type="button" name="generate-token-button"
							id="generate-token-button" class="btn btn-primary">Generate</button>
					</span>
				</div>
			</div>
		</div>
		<hr>
		<div class="form-group">
			<div class="col-sm-12" align="right">
				<button type="button" id="add-question" name="add-question"
					class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span>
				</button>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-12">
				<table class="table" style="margin: 1rem;">
					<tr>
						<th>Question</th>
						<th style="size: 1.5rem;">#</th>
					</tr>
					<tbody id="detail-list"></tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="modal-footer" style="margin-left: 5px; margin-right: 5px;">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
		<button type="submit" class="btn btn-primary">Save</button>
	</div>
</form>


<%-- <div class="modal-header">
	<h4 class="title">Edit Document Test</h4>
</div>

<div class="modal-body">
	<form action="#" id="edit-document-test-form">
		<input type="hidden" id="idDocumentTest" value="${documentTest.id}">
		<table>
			<tr>
				<td><select id="select-test" name="testId">
						<option value="-">-Choose Test-</option>
						<c:forEach items="${testModelList}" var="testModel">
							<option value="${testModel.id}"
								${testModel.id == documentTestModel.testId ? 'selected="selected"' : ''}>${testModel.name}</option>
						</c:forEach>
				</select></td>
				<td><select id="select-test-type" name="testTypeId">
						<option value="-">-Choose Test Type-</option>
						<c:forEach items="${testTypeModelList}" var="testTypeModel">
							<option value="${testTypeModel.id}"
								${testTypeModel.id == documentTestModel.testTypeId ? 'selected="selected"' : ''}>${testTypeModel.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td><input type="text" id="version" name="version"
					readonly="readonly" value="${documentTestModel.version}"></td>
				<td><input type="text" id="token" name="token"
					readonly="readonly" maxlength="10"
					value="${documentTestModel.token}">
					<button type="button" name="generate-token-button"
						id="generate-token-button">Generate</button></td>
			</tr>
			<tr>
				<td></td>
				<td><button type="submit">Save</button>
					<button type="button" id="cancel-button" data-dismiss="modal">Cancel</button></td>
			</tr>
		</table>
	</form>

	<hr>
	<div id="modal-detail">
		<div align="right">
			<button type="button" id="add-question" name="add-question"
				class="btn btn-primary"></button>
		</div>
		<table class="table">
			<thead>
				<tr>
					<td>Question
					<td>
					<td>#</td>
				</tr>
			</thead>
			<tbody id="detail-list"></tbody>
		</table>
	</div>
</div> --%>
