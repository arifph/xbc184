<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form action="#" id="add-document-test-form" class="form-horizontal">
	<div class="modal-header bg-primary">
		<h4 class="modal-title">Add Document Test</h4>
	</div>

	<div class="modal-body" style="margin-left: 5px; margin-right: 5px;">
		<div class="form-group">
			<div class="col-sm-6">
				<select id="select-test" name="testId" class="form-control">
					<option value="-">-Choose Test-</option>
					<c:forEach items="${testModelList}" var="testModel">
						<option value="${testModel.idTest}">${testModel.nameTest}</option>
					</c:forEach>
				</select>
			</div>
			<div class="col-sm-6">
				<select id="select-test-type" name="testTypeId" class="form-control">
					<option value="-">-Choose Test Type-</option>
					<c:forEach items="${testTypeModelList}" var="testTypeModel">
						<option value="${testTypeModel.id}">${testTypeModel.name}</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-6">
				<input type="text" id="version" class="form-control" name="version"
					readonly="readonly">
			</div>
			<div class="col-sm-6">
				<div class="input-group">
					<input type="text" id="token" class="form-control" name="token"
						readonly="readonly" maxlength="10">
						<span class="input-group-btn">
							<button type="button"
								name="generate-token-button" id="generate-token-button"
								class="btn btn-primary">Generate</button>
						</span>
				</div>
			</div>
		</div>
		<%-- <table>
			<tr>
				<td><select id="select-test" name="testId">
						<option value="-">-Choose Test-</option>
						<c:forEach items="${testModelList}" var="testModel">
							<option value="${testModel.id}">${testModel.name}</option>
						</c:forEach>
				</select></td>
				<td><select id="select-test-type" name="testTypeId">
						<option value="-">-Choose Test Type-</option>
						<c:forEach items="${testTypeModelList}" var="testTypeModel">
							<option value="${testTypeModel.id}">${testTypeModel.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td><input type="text" id="version" name="version"
					readonly="readonly"></td>
				<td><input type="text" id="token" name="token"
					readonly="readonly" maxlength="10">
					<button type="button" name="generate-token-button"
						id="generate-token-button">Generate</button></td>
			</tr>
			<tr>
				<td></td>
				<td><button type="submit">Save</button>
					<button type="button" id="cancel-button" data-dismiss="modal">Cancel</button></td>
			</tr>
		</table> --%>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
		<button type="submit" class="btn btn-primary">Save</button>
	</div>
</form>