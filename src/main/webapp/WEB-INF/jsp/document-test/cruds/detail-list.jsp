<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${documentTestDetailList}" var="documentTestDetailModel" varStatus="status">
	<tr>
		<td>${documentTestDetailModel.questionId.question}</td>
		<td>
			<div class="dropdown" align="right">
				<div class="button-group" align="center">
					<button type="button" class="btn btn-box-tool dropdown-toggler"
						data-toggle="dropdown">
						<i class="fa fa-navicon"></i>
					</button>
					<ul id="menu" class="dropdown-menu" role="button">
						<li><a id="btnDelete" data-value="${status.count - 1}">Delete</a></li>
					</ul>
				</div>
			</div>
	</tr>
</c:forEach>