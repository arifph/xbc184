<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${documentTestModelList}" var="documentTestModel">
	<tr>
		<td>${documentTestModel.testType.name}</td>
		<td>${documentTestModel.version}</td>
		<td>${documentTestModel.test.nameTest}</td>
		<td>
			<div class="dropdown" align="right">
				<div class="button-group" align="center">
					<button type="button" class="btn btn-box-tool dropdown-toggler"
						data-toggle="dropdown">
						<i class="fa fa-navicon"></i>
					</button>
					<ul id="menu" class="dropdown-menu" role="button">
						<li><a id="btnEdit" data-value="${documentTestModel.id}">Edit</a></li>
						<li><a id="btnCopy" data-value="${documentTestModel.id}">Copy
								Document</a>
						<li><a id="btnView" data-value="${documentTestModel.id}">View
								Test</a>
						<li><a id="btnDelete" data-value="${documentTestModel.id}">Delete</a></li>
					</ul>
				</div>
			</div>
	</tr>
</c:forEach>