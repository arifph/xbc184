<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form action="#" id="add-question-form">
	<div class="modal-header bg-primary">
		<h4 class="modal-title">Add Question</h4>
	</div>
	<div id="question-modal-body" class="modal-body">
		<div class="form-group">
			<select id="select-question" name="questionId" class="form-control">
				<option value="-" selected disabled>-Choose Question-</option>
				<c:forEach items="${questionModelList}" var="questionModel">
					<option value="${questionModel.idQuestion}">${questionModel.question}</option>
				</c:forEach>
			</select>
		</div>
		<%-- <table>
			<tr>
				<td colspan="2"><select id="select-question" name="questionId">
						<option value="-">-Choose Question-</option>
						<c:forEach items="${questionModelList}" var="questionModel">
							<option value="${questionModel.idQuestion}">${questionModel.question}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td></td>
				<td><button type="submit">Save</button>
					<button type="button" id="cancel-button">Cancel</button></td>
			</tr>
		</table> --%>
	</div>
	<div class="modal-footer">
		<button type="button" id="cancel-button" class="btn btn-secondary">Cancel</button>
		<button type="submit" class="btn btn-primary">Save</button>
	</div>
</form>