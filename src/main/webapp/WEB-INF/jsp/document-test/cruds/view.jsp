<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<div class="modal-header bg-primary">
	<button type="button" class="close" data-dismiss="modal">
		<i class="fa fa-close"></i>
	</button>
	<h4 class="modal-title">Test</h4>
</div>
<div class="modal-body">
	<c:forEach items="${questionModelList}" var="questionModel"
		varStatus="num">
		<div class="row">
			<div class="col-sm-12">
				<p>Soal ${num.count}, 	${questionModel.question}</p>
			</div>
		</div>
		<c:choose>
			<c:when test="${not empty questionModel.imageUrl}">
				<div class="row">
					<div class="col-sm-12">
						<img class="img-responsive" alt="Question Image"
							src="${questionModel.imageUrl}">
					</div>
				</div>
			</c:when>
		</c:choose>
		<c:choose>
			<c:when test="${questionModel.questionType eq 'MC'}">
				<div class="row">
					<div class="col-sm-2">
						<c:choose>
							<c:when test="${not empty questionModel.optionA}">
								A. ${questionModel.optionA}
							</c:when>
							<c:otherwise>
								A. <img class="img-responsive" alt="Image A"
									src="${questionModel.imageA}">
							</c:otherwise>
						</c:choose>
					</div>
					<div class="col-sm-2">
						<c:choose>
							<c:when test="${not empty questionModel.optionB}">
								B. ${questionModel.optionB}
							</c:when>
							<c:otherwise>
								B. <img class="img-responsive" alt="Image B"
									src="${questionModel.imageB}">
							</c:otherwise>
						</c:choose>
					</div>
					<div class="col-sm-2">
						<c:choose>
							<c:when test="${not empty questionModel.optionC}">
								C. ${questionModel.optionC}
							</c:when>
							<c:otherwise>
								C. <img class="img-responsive" alt="Image C"
									src="${questionModel.imageC}">
							</c:otherwise>
						</c:choose>
					</div>
					<div class="col-sm-2">
						<c:choose>
							<c:when test="${not empty questionModel.optionD}">
								D. ${questionModel.optionD}
							</c:when>
							<c:otherwise>
								D. <img class="img-responsive" alt="Image D"
									src="${questionModel.imageD}">
							</c:otherwise>
						</c:choose>
					</div>
					<div class="col-sm-2">
						<c:choose>
							<c:when test="${not empty questionModel.optionE}">
								E. ${questionModel.optionE}
							</c:when>
							<c:otherwise>
								E. <img class="img-responsive" alt="Image E"
									src="${questionModel.imageE}">
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</c:when>
		</c:choose>
		<br>
	</c:forEach>
</div>