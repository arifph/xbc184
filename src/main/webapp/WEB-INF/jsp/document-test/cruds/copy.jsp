<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="modal-header">
	<h4 class="title">Copy Document</h4>
</div>

<div class="modal-body">
	<form action="#" id="copy-document-test-form">
		<table>
			<tr>
				<td><select id="select-test" name="testId">
						<option value="-">-Choose Test-</option>
						<c:forEach items="${testModelList}" var="testModel">
							<option value="${testModel.id}"
								${testModel.id == documentTestModel.testId ? 'selected="selected"' : ''}>${testModel.name}</option>
						</c:forEach>
				</select></td>
				<td><select id="select-test-type" name="testTypeId">
						<option value="-">-Choose Test Type-</option>
						<c:forEach items="${testTypeModelList}" var="testTypeModel">
							<option value="${testTypeModel.id}"
								${testTypeModel.id == documentTestModel.testTypeId ? 'selected="selected"' : ''}>${testTypeModel.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td><input type="text" id="version" name="version"
					readonly="readonly" value="${newVersion}"></td>
				<td><input type="text" id="token" name="token"
					readonly="readonly" maxlength="10" value="${newToken}">
					<button type="button" name="generate-token-button"
						id="generate-token-button">Generate</button></td>
			</tr>
			<tr>
				<td></td>
				<td><button type="submit">Save</button>
					<button type="button" id="cancel-button" data-dismiss="modal">Cancel</button></td>
			</tr>
		</table>
	</form>
</div>