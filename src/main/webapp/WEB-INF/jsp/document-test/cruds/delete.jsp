
<div class="modal-header bg-primary">
	<h4 class="title">Delete Category</h4>
</div>

<div class="modal-body">
	<p>Are you sure to delete this data?</p>
	<%-- <table>
		<tr>
			<td><button type="button" class="btn" id="btnNo" name="btnNo">No</button></td>
			<td><button type="button" class="btn btn-danger" id="btnYes" name="btnYes"
					value="${id}">Yes</button></td>
		</tr>
	</table> --%>
</div>

<div class="modal-footer">
	<button type="button" class="btn" id="btnNo" name="btnNo">No</button>
	<button type="button" class="btn btn-danger" id="btnYes" name="btnYes"
		value="${id}">Yes</button>
</div>
