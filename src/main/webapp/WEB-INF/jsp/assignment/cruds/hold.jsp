<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>   
<div>
	<div>
		<table class="table" id="tbl-header-assignmentId">
			<tr bgcolor="orange">
				<td><h3><b><font color="white"> HOLD </font></b></h3></td>
			</tr>
		</table>
	</div>
	
	<form action="#" method="get" id="form-assignment-hold">
		<input type="hidden" id="idAssignment" name="idAssignment" value="${assignmentModel.id}"/>
		<div style="margin: auto; width: 60%;">
			<h3><b>Are you sure hold this data ?</b></h3>
		</div>
		<button type="button" class="btn btn-danger" data-dismiss="modal" style="float:left; margin-left: 20%;">NO</button>
		<button type="submit" class="btn btn-success" style="float: right; margin-right: 20%;">YES</button>
	</form>
	<hr>
	<hr>
</div>