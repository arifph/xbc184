<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div>
	<div>
		<table class="table" id="tbl-header-assignment">
			<tr bgcolor="orange">
				<td><h3>
						<b><font color="white">EDIT ASSIGNMENT</font></b>
					</h3></td>
			</tr>
		</table>
	</div>

	<form action="#" method="get" id="form-assignment-edit">
		<input type="hidden" id="idAssignment" name="idAssignment"
			value="${assignmentModel.id}" /> <input type="hidden" id="idBiodata"
			name="idBiodata" value="${assignmentModel.biodataId}" />
		<table
			style="width: 80%; margin-left: 10%; margin-right: 10%; border-collapse: separate; border-spacing: 15px;">
			<tr>
				<td><select id="biodataId" name="biodataId" style="width: 95%">
						<option value="" disabled selected>-Choose Biodata Name-</option>
						<c:forEach items="${biodataModelList}" var="biodataModel">
							<option value="${biodataModel.id}">${biodataModel.name}
							</option>
						</c:forEach>
				</select></td>

			</tr>
			<tr>
				<td><input type="text" placeholder="Title" id="title"
					name="title" size="63%" value="${assignmentModel.title}"></td>
			<tr>
				<td><input type="date" id="startDate" name="startDate"
					size="63%" value="${startDate}"></td>
			</tr>
			<tr>
			<td><input type="date" id="endDate" name="endDate" size="63%"
				value="${endDate}"></td>
				</tr>
			<tr>
				<td><input type="text" placeholder="Description"
					id="Description" name="Description" size="63%"
					value="${assignmentModel.description}"></td>
			</tr>
			<tr>


			</tr>
		</table>
		<button type="submit"
			style="float: right; margin-right: 10%; margin-bottom: 15%;">SAVE</button>
		<button type="button" data-dismiss="modal"
			style="float: right; margin-right: 5%; margin-bottom: 15%;">CANCEL</button>
	</form>
	<hr>
</div>