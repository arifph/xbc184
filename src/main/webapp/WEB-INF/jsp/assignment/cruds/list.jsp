<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${assignmentModelList}" var="assignmentModel" varStatus="number">	
		<tr>
		<td>${assignmentModel.biodataModel.name}</td>
		<td>${assignmentModel.startDate}</td>
		<td>${assignmentModel.endDate}</td>
		<td>
			<div class="dropdown" align="right">
				<div class="button-group" align="left">
					<button type="button" class="btn-box-tool dropdown-toggler" data-toggle= "dropdown">
						<i class="fa fa-navicon"></i>
					</button>
					<ul id="menu" class="dropdown-menu" role="menu">
						<li><a id="btn-edit" data-value="${assignmentModel.id}">Edit</a></li>
						<li><a id="btn-delete" data-value="${assignmentModel.id}">Delete</a></li>
						<li><a id="btn-hold" data-value="${assignmentModel.id}">Hold</a></li>
						<li><a id="btn-done" data-value="${assignmentModel.id}">Mark as Done</a></li>
					</ul>
				</div>
			</div>
		</td>
	</tr>
</c:forEach>