<div class="panel" style="background : white; margin-top: 50px; min-height: 500px;">
	<div>
		<table class="table" id="tbl-header-assignment">
			<tr bgcolor="orange">
				<td><h3><b><font color="white">ASSIGNMENT</font></b></h3></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
		<table class="table" id ="tbl-assignment" style="width: 70%; margin-left: 15%; margin-right: 15%;">
			<tr>
				<td>
					<form action="#" method="get" id="form-assignment-search-date">
						<input placeholder="Search by start date" type="text" id="nameAssignmentKey" name="dateAssignment" size="20">
						<button class="btn btn-warning" type="submit">O</button>
					</form>
				</td>
				<td></td>
				<td >
					<button type="button" class="btn btn-warning" id="btn-add" >+</button>
				</td>
			</tr>
			<tr bgcolor="orange">
				<td>NAME</td>
				 <td>START DATE</td>
				 <td>END DATE</td> 
				<td>#</td>
			</tr>
			<tbody id="list-assignment">
			<!-- isi listya -->
			</tbody>
		</table>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script>
loadListAssignment();
	// fungsi untuk list data Fakultas
	function loadListAssignment(){
		console.log("loading list");
		$.ajax({
			url:'assignment/cruds/list.html',
			type:'get',
			dataType:'html',
			success: function(result){
				console.log(result);
				$('#list-assignment').html(result);
			}
		});
	}

	//document ready itu setelah halaman dipanggil
	$(document).ready(function(){
		loadListAssignment();
		//fungsi ajax untuk popup tambah
		$('#btn-add').on('click',function(){
			$.ajax({
				url:'assignment/cruds/add.html',
				type:'get',
				dataType:'html',
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup tambah
		
		
		//fungsi ajax untuk create tambah
		$('#modal-input').on('submit','#form-assignment-add',function(){
			$.ajax({
				url:'assignment/cruds/create.json',
				type:'get',
				dataType:'json',
				data: $(this).serialize(),
				success: function(result){
					
					if (result.jumlahDataAssignment > 0) {
						alert("Kode "+result.namaAssignment+" sudah ada di DB")
					} else {
						$('#modal-input').modal('hide');
						alert("data telah ditambah");
						loadListAssignment();
					}
				}
			});
			return false; // tanpa load ulang atau refresh
		});
		//akhir fungsi ajax untuk create tambah
		
		//fungsi ajax untuk lihat
		$('#list-assignment').on('click','#btn-detail',function(){
			var idAssignment = $(this).val();
			alert(idAssignment);
			$.ajax({
				url:'assignment/cruds/detail.html',
				type:'get',
				dataType:'html',
				data:{idAssignment:idAssignment},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk lihat
		
		//fungsi ajax untuk ubah
		$('#list-assignment').on('click','#btn-edit',function(){
			var idAssignment = $(this).attr('data-value');
			$.ajax({
				url:'assignment/cruds/edit.html',
				type:'get',
				dataType:'html',
				data:{idAssignment:idAssignment},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk ubah
		
		//fungsi ajax untuk update
		$('#modal-input').on('submit','#form-assignment-edit',function(){
			
			$.ajax({
				url:'assignment/cruds/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah diubah");
					loadListAssignment();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update
		
		//fungsi ajax untuk hapus
		$('#list-assignment').on('click','#btn-delete',function(){
			console.log("popping up delete dialog");
			var idAssignment= $(this).attr('data-value');
			$.ajax({
				url:'assignment/cruds/confirmation.html',
				type:'get',
				dataType:'html',
				data:{idAssignment:idAssignment},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk hapus
		
		//fungsi ajax untuk delete
		$('#modal-input').on('submit','#form-assignment-delete',function(){
			/* var idAssignment= $(this).val(); */
			console.log($(this).serialize());
			$.ajax({
				url:'assignment/cruds/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah dihapus");
					loadListAssignment();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete
		
			//fungsi ajax untuk hold
		$('#list-assignment').on('click','#btn-hold',function(){
			console.log("popping up hold dialog");
			var idAssignment= $(this).attr('data-value');
			$.ajax({
				url:'assignment/cruds/confirmation2.html',
				type:'get',
				dataType:'html',
				data:{idAssignment:idAssignment},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk hold
		
			//fungsi ajax untuk popup hold
		$('#modal-input').on('submit','#form-assignment-hold',function(){
			/* var idAssignment= $(this).val(); */
			console.log($(this).serialize());
			$.ajax({
				url:'assignment/cruds/hold.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah dihold");
					loadListAssignment();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk popup hold
		
		//fungsi ajax untuk search namaFakultas
		$('#form-assignment-search-date').on('submit',function(){
			
			$.ajax({
				url:'assignment/cruds/search/startDate.html',
				type:'get',
				dataType:'html',
				data: $(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan Start");
					$('#list-assignment').html(result); //reload list bootcamp
				}
			});
			return false;
		});
		
		//fungsi ajax untuk mark
		$('#list-assignment').on('click','#btn-done',function(){
			console.log("popping up hold dialog");
			var idAssignment= $(this).attr('data-value');
			$.ajax({
				url:'assignment/cruds/done2.html',
				type:'get',
				dataType:'html',
				data:{idAssignment:idAssignment},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk mark
		
		//fungsi ajax untuk popup Mark As Done
		$('#modal-input').on('submit','#form-assignment-done',function(){
			/* var idAssignment= $(this).val(); */
			console.log("This serialize = " + $(this).serialize());
			$.ajax({
				url:'assignment/cruds/done.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah dihold");
					loadListAssignment();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk Mark As Done
			
	});
	//akhir dokumen ready

</script>