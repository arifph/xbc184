<div class="panel"
	style="background: white; margin-top: 50px; min-height: 550px">
	<div>
		<table class="table" id="tbl-trainer">
			<tr>
				<td><h2>Trainer</h2></td>
			</tr>
			<tr>
				<td>
					<form action="#" method="get" id="form-trainer-search">
						<input type="text" id="keyword" name="keyword" size="30" placeholder="Search by Name"/>
						<button class="fa fa-search btn btn-primary" type="submit" id="btn-search"></button>
					</form>
				</td>
				<td align="left" ><button class="btn btn-primary" type="button" id="btn-add">+</button></td>
			</tr>
			<tr>
				<td>Nama</td>
				<td align="left">#</td>
			</tr>
			<tbody id="list-trainer" >
			</tbody>
		</table>
	<!-- 	<table class="table table-bordered">
		<tbody id="list-trainer" class="table-bordered">
			</tbody>
		</table> -->
	</div>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-light-blue">
				<h2>Add Trainer</h2>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popup -->
			</div>
		</div>
	</div>
</div>

<div id="modal-edit" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-light-blue">
				<h2>Edit Trainer</h2>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popup -->
			</div>
		</div>
	</div>
</div>

<div id="modal-delete" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-light-blue">
				<h2>Delete</h2>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popup -->
			</div>
		</div>
	</div>
</div>

<script>
	
	loadListTrainer();
	
	function loadListTrainer(){
		$.ajax({
			url:'trainer/cruds/list.html',
			type:'get',
			dataType:'html',
			success: function(result){
				$('#list-trainer').html(result);
			}
		});
	}
	
	$(document).ready(function(){

		$('#btn-add').on('click',function(){
			$.ajax({
				url:'trainer/cruds/add.html',
				type:'get',
				dataType:'html',
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');	
				}
			});
		});
		
		$('#modal-input').on('submit','#form-trainer-add',function(){
			var name = $('#name').val();
			if(name != ""){
				$.ajax({
					url : 'trainer/cruds/create.json',
					type : 'get',
					dataType : 'json',
					data : $(this).serialize(),
					success : function(result){
						alert("Data successfully saved");
						$('#modal-input').modal('hide');
						loadListTrainer();
					}
				});
				return false;
			}
			else{
				alert("Name field can't be empty.");
				return false;
			}
		});
		
		$('#list-trainer').on('click','#btn-edit',function(){
			var id = $(this).attr('data-value');
			$.ajax({
				url : 'trainer/cruds/edit.html',
				type : 'get',
				dataType : 'html',
				data : {id:id},
				success : function(result){
					$('#modal-edit').find('.modal-body').html(result);
					$('#modal-edit').modal('show');
				}
			});
			return false;
		});
		
		$('#modal-edit').on('submit','#form-trainer-edit',function(){
			var name = $('#name').val();
			if (name != ""){
				$.ajax({
					url : 'trainer/cruds/update.json',
					type : 'get',
					dataType : 'json',
					data : $(this).serialize(),
					success : function(result){
						alert("Data successfully updated");
						$('#modal-edit').modal('hide');
						loadListTrainer();
					}
				});
				return false;
			}
			else{
				alert("Trainer name can't be empty.");
				/* loadListTrainer(); */
				return false;
			}
		});
		
		$('#list-trainer').on('click','#btn-delete',function(){
			var id = $(this).attr('data-value');
			$.ajax({
				url : 'trainer/cruds/delete.html',
				type : 'get',
				dataType : 'html',
				data : {id:id},
				success : function(result){
					$('#modal-delete').find('.modal-body').html(result);
					$('#modal-delete').modal('show');
				}
			});
			return false;
		});
		
		$('#modal-delete').on('submit','#form-trainer-delete',function(){
			var id = $('#id').val();
			$.ajax({
				url : 'trainer/cruds/remove.json',
				dataType : 'json',
				data : {id:id},
				success : function(result){
					alert("Data successfully deleted");
					$('#modal-delete').modal('hide');
					loadListTrainer();
				}
			});
			return false;
		});
		
		$('#form-trainer-search').on('submit',function(){
			var name = $('#keyword').val();
			if(name != ""){
				$.ajax({
					url : 'trainer/cruds/search.html',
					dataType : 'html',
					data : {name:name},
					success : function(result){
						alert("Your data search result");
						$('#list-trainer').html(result);
					}
				});
				return false;
			}
			else{
				alert("Search field can't be empty.");
				return false;
			}
		});
	});
	
</script>