<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<c:forEach items="${trainerModelList}" var="trainerModel" varStatus="number">
	<tr>
		<td>${trainerModel.name}</td>
		<td>
			<div class="box-tools pull-left" align="center">
                <div class="btn-group" align="center">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa  fa-list"></i>
                    </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#" id="btn-edit" data-value="${trainerModel.id}">Edit</a></li>
                    <li><a href="#" id="btn-delete" data-value="${trainerModel.id}">Delete</a></li>
                  </ul>
                </div>
              </div>
		</td>
	</tr>
</c:forEach>
	