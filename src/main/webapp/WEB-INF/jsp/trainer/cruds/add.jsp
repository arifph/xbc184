<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal" align="center">
	<form action="#" method="get" id="form-trainer-add" >
		<table>
			<col width="260">
			<tr>
				<td align="center"><input type="text" id="name" name="name" 
					size="40" placeholder="Name"  
					style="font-style:italic"></td>
			</tr>
			<tr>
				<td><br/></td>
			</tr>
			<tr>
				<td align="center"><textarea id="notes" name="notes" style="font-style:italic; width:68mm; height: 120px;"
					placeholder="Notes"></textarea></td>
			</tr>
			<tr>
				<td><br/></td>
			</tr>
		</table>
		<table>
			<col width="130">
			<col width="130">
			<tr>
				<td align="center">
					<button type="button" class="btn btn-danger" data-dismiss="modal">
						Cancel
					</button>
				</td>
				<td align="center"><button type="submit" class="btn btn-success" id="btn-add">Create</button></td>	
			</tr>
		</table>
	</form>
</div>