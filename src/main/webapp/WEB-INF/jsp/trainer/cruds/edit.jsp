<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal" align="center">
	<form action="#" method="get" id="form-trainer-edit" >
		<input type="hidden" id="id" name="id" value="${trainerModel.id}"/>
		
		<input type="text" id="name" name="name"
					value="${trainerModel.name}" size="40" placeholder="Name"  
					style="font-style:italic">
		<br/>
		<br/>
		<textarea id="notes" name="notes" 
					style="height: 120px; font-style:italic; width:68mm;"
					placeholder="Notes">${trainerModel.notes}
		</textarea>
		<br/>
		<table>
			<col width="130">
			<col width="130">
			<tr>
				<td align="center">
					<button type="button" class="btn btn-danger" data-dismiss="modal">
						Cancel
					</button>
				</td>
				<td align="center"><button type="submit" class="btn btn-success">Save</button></td>	
			</tr>
		</table>
	</form>
</div>