<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal" align="center">
	<h3>Are you sure delete this data ?</h3>
	<form action="#" method="get" id="form-trainer-delete">
		<input type="hidden" id="id" name="id" value="${trainerModel.id}"/>
		<br/>
		<table>
			<col width="130">
			<col width="130">
			<tr>
				<td align="center">
					<button type="button" class="btn btn-danger" data-dismiss="modal">
						No
					</button>
				</td>
				<td align="center"><button type="submit" class="btn btn-success">Yes</button></td>	
			</tr>
		</table>
	</form>
</div>