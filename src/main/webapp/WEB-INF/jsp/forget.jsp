<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
  <div class="register-box-body" id="body-box">
    <!-- <p class="login-box-msg">Register a new membership</p> --> 

    <form action="#" method="get" id="forgetPass">
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" name="email" id="email" required="required">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password" id="password" required pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Retype password" name="repassword" id="repassword" required="required">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div class="row" align="center">
        <div class="col-xs-4" align="right">
            <button type="button" onClick="history.go(-1);" class="btn btn-primary btn-block btn-flat">Back</button>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">SUBMIT</button>
        </div>
        <!-- /.col -->
      </div>
    </form>


  </div>
  <!-- /.form-box -->

<!-- /.register-box -->



