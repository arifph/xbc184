<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<!-- <h1>EDIT BIODATA</h1> -->
	<form action="#" method="get" id="form-biodata-edit">
	<input type="hidden" id="id" name="id" value="${biodataModel.id}" />
	<%-- <table>
		<tr>
			<td><input class="form-control" type="text" id="name" name="name" value="${biodataModel.name}"/></td>
			<td>
				<table class="table bg-aqua">
					<tr>
						<td>Gender</td>
						<td><input type="radio" id="gender" name="gender" value="M" ${biodataModel.gender=='M' ? 'checked="checked"' : ''}/>M</td>
						<td><input type="radio" id="gender" name="gender" value="F" ${biodataModel.gender=='F' ? 'checked="checked"' : ''}/>F</td>
					</tr>
				</table>
			</td>
			
			<!-- <td></td>
			<td>
			Gender
				<div class="form-group">
                  <div class="radio">
                    <label>
                      <input type="radio" name="gender" id="gender" value="M">
                      M
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="gender" id="gender" value="F">
                      F
                    </label>
                  </div>
                </div>
			</td> -->
		</tr>
		<tr>
			<td><input class="form-control" type="text" id="lastEducation" name="lastEducation" value="${biodataModel.lastEducation}"/></td>
			<td>
				<select class="form-control" id="bootcampTestType" name="bootcampTestType">
					<option value="">- Choose Bootcamp Test Type -</option>
					<c:forEach items="${bootcampTestTypeModelList}" var="bootcampTestTypeModel">
						<option value="${bootcampTestTypeModel.id}" 
						${biodataModel.bootcampTestType == bootcampTestTypeModel.id ? 'selected="true"' : ''}>
							${bootcampTestTypeModel.name}
						</option>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<td><input class="form-control" type="text" id="educationalLevel" name="educationalLevel" value="${biodataModel.educationalLevel}"/></td>
			<td>
				<div>
					<table>
						<tr>
							<td><input class="form-control" type="text" id="iq" name="iq" value="" placeholder="IQ"/></td>
							<td><input class="form-control" type="text" id="du" name="du" value="" placeholder="DU"/></td>
							<td><input class="form-control" type="text" id="nestedLogic" name="nestedLogic" value="" placeholder="NL"/></td>
							<td><input class="form-control" type="text" id="joinTable" name="joinTable" value="" placeholder="JT"/></td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td><input class="form-control" type="text" id="graduationYear" name="graduationYear" value="${biodataModel.graduationYear}"/></td>
			<td><input class="form-control" type="text" id="arithmetic" name="arithmetic" value="" placeholder="Arithmetic"/></td>
		</tr>
		<tr>
			<td><input class="form-control" type="text" id="majors" name="majors" value="${biodataModel.majors}"/></td>
			<td><input class="form-control" type="text" id="tro" name="tro" value="" placeholder="TRO"/></td>
		</tr>
		<tr>
			<td><input class="form-control" type="text" id="gpa" name="gpa" value="${biodataModel.gpa}"/></td>
			<td><input class="form-control" type="text" id="interviewer" name="interviewer" value="" placeholder="Interviewer"/></td>
		</tr>
		<tr>
			<td>
				<div class="form-group">
					<textarea class="form-control" rows="3" id="notes" name="notes" placeholder="Notes"></textarea>
				</div>
			</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td>
				<button type="button" class="btn btn-default" data-dismiss="modal">
					CANCEL
				</button>
			</td>
			<td>
				<!-- <div class="form-horizontal">
					<button type="button" data-dismiss="modal">
						CANCEL
					</button> -->
					<button type="submit" class="btn btn-primary">SAVE</button>
				<!-- </div> -->
			</td>
		</tr>
	</table> --%>
	
	<div class="box-body">
	    <div class="row">
	      <div class="col-xs-5">
	      	<input class="form-control" type="text" id="name" name="name" placeholder="Name" value="${biodataModel.name}"/>
	      	<!-- <input type="text" class="form-control" placeholder=".col-xs-5"> -->
	      </div>
	      <div class="col-xs-7">
	      	<table class="table bg-aqua">
				<tr>
					<td>Gender</td>
					<td><input type="radio" id="gender" name="gender" value="M" ${biodataModel.gender=='M' ? 'checked="checked"' : ''}/>M</td>
					<td><input type="radio" id="gender" name="gender" value="F" ${biodataModel.gender=='F' ? 'checked="checked"' : ''}/>F</td>
				</tr>
			</table>
	      </div>
	    </div>
	    
	    <div class="row">
	      <div class="col-xs-5">
			<input class="form-control" type="text" id="lastEducation" name="lastEducation" placeholder="Last Education" value="${biodataModel.lastEducation}"/>
	      </div>
	      <div class="col-xs-7">
	      	<select class="form-control" id="bootcampTestType" name="bootcampTestType">
					<option value="">- Choose Bootcamp Test Type -</option>
					<c:forEach items="${bootcampTestTypeModelList}" var="bootcampTestTypeModel">
						<option value="${bootcampTestTypeModel.id}" 
						${biodataModel.bootcampTestType == bootcampTestTypeModel.id ? 'selected="true"' : ''}>
							${bootcampTestTypeModel.name}
						</option>
					</c:forEach>
				</select>
	      </div>
	    </div>
	    <br/>
	    <div class="row">
	      <div class="col-xs-5">
			<input class="form-control" type="text" id="educationalLevel" name="educationalLevel" placeholder="Educational Level" value="${biodataModel.educationalLevel}"/>
	      </div>
	      <div class="col-xs-7">
	      	<div class="row">
	      		<div class="col-xs-3">
	      			<input class="form-control" type="text" id="iq" name="iq" placeholder="IQ" value="${biodataModel.iq}" />
	      		</div>
	      		<div class="col-xs-3">
	      			<input class="form-control" type="text" id="du" name="du" value="${biodataModel.du}" placeholder="DU"/>
	      		</div>
	      		<div class="col-xs-3">
	      			<input class="form-control" type="text" id="nestedLogic" name="nestedLogic" value="${biodataModel.nestedLogic}" placeholder="NL"/>
	      		</div>
	      		<div class="col-xs-3">
	      			<input class="form-control" type="text" id="joinTable" name="joinTable" value="${biodataModel.joinTable}" placeholder="JT"/>
	      		</div>
	      	</div>
	      </div>
	    </div>
	    <br/>
	    <div class="row">
	      <div class="col-xs-5">
			<input class="form-control" type="text" id="graduationYear" name="graduationYear" placeholder="Graduation Year" value="${biodataModel.graduationYear}"/>
	      </div>
	      <div class="col-xs-7">
	      	<input class="form-control" type="text" id="arithmetic" name="arithmetic" value="${biodataModel.arithmetic}" placeholder="Arithmetic"/>
	      </div>
	    </div>
	    <br/>
	    <div class="row">
	      <div class="col-xs-5">
			<input class="form-control" type="text" id="majors" name="majors" placeholder="Majors" value="${biodataModel.majors}"/>
	      </div>
	      <div class="col-xs-7">
	      	<input class="form-control" type="text" id="tro" name="tro" value="${biodataModel.tro}" placeholder="TRO"/>
	      </div>
	    </div>
	    <br/>
	    <div class="row">
	      <div class="col-xs-5">
			<input class="form-control" type="text" id="gpa" name="gpa" placeholder="GPA" value="${biodataModel.gpa}"/>
	      </div>
	      <div class="col-xs-7">
	      	<input class="form-control" type="text" id="interviewer" name="interviewer" value="${biodataModel.interviewer}" placeholder="Interviewer"/>
	      </div>
	    </div>
	    <br/>
	    <div class="row">
	    	<div class="col-xs-12">
	    		<textarea class="form-control" rows="3" id="notes" name="notes" placeholder="Notes">${biodataModel.notes}</textarea>
	    	</div>
	    </div>
	    
    </div>
    
    <div class="box-body">
	    <div class="row pull-right">
		    <button type="reset" class="btn btn-default" data-dismiss="modal">
				CANCEL
			</button>
			<button type="submit" class="btn btn-primary">SAVE</button>
	    </div>
    </div>
    
	</form>
</div>