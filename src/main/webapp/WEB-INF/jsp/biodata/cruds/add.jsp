<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<!-- <h1>Tambah Biodata</h1> -->
	<form action="#" method="get" id="form-biodata-add">
		<%-- <table>
			<tr>
				<td>Kode Biodata</td>
				<td><input type="text" id="kodeBiodata" name="kodeBiodata" value="${kodeBiodataAuto}"/></td>
			</tr>
			
			<tr>
				<td><input class="form-control" type="text" id="name" name="name" placeholder="Name" /></td>
			</tr>
			
			<tr>
				<td><input class="form-control" type="text" id="lastEducation" name="lastEducation" placeholder="Last Education" /></td>
			</tr>
			
			<tr>
				<td><input class="form-control" type="text" id="educationalLevel" name="educationalLevel" placeholder="Educational Level" /></td>
			</tr>
			
			<tr>
				<td><input class="form-control" type="text" id="graduationYear" name="graduationYear" placeholder="Graduation Year" /></td>
			</tr>
			
			<tr>
				<td><input class="form-control" type="text" id="majors" name="majors" placeholder="Majors" /></td>
			</tr>
			
			<tr>
				<td><input class="form-control" type="text" id="gpa" name="gpa" placeholder="GPA" /></td>
			</tr>
			
			<tr>
				<td>Lokasi Biodata</td>
				<td>
					<select id="idLokasi" name="idLokasi">
						<c:forEach items="${lokasiModelList}" var="lokasiModel">
							<option value="${lokasiModel.idLokasi}">
								${lokasiModel.namaLokasi}
							</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<div class="form-horizontal pull-right" align="right">
						<button type="button" class="btn btn-default" data-dismiss="modal">
							CANCEL
						</button>
						<button type="submit" class="btn btn-primary">SAVE</button>
					</div>
				</td>
			</tr>
		</table> --%>
		
		<div class="box-body">
		    <div class="row">
			    <input class="form-control" type="text" id="name" name="name" placeholder="Name" />
		    </div>
		    <br/>
		    <div class="row">
			    <input class="form-control" type="text" id="lastEducation" name="lastEducation" placeholder="Last Education" />
		    </div>
		    <br/>
		    <div class="row">
			    <input class="form-control" type="text" id="educationalLevel" name="educationalLevel" placeholder="Educational Level" />
		    </div>
		    <br/>
		    <div class="row">
			    <input class="form-control" type="text" id="graduationYear" name="graduationYear" placeholder="Graduation Year" />
		    </div>
		    <br/>
		    <div class="row">
			    <input class="form-control" type="text" id="majors" name="majors" placeholder="Majors" />
		    </div>
		    <br/>
		    <div class="row">
			    <input class="form-control" type="text" id="gpa" name="gpa" placeholder="GPA" />
		    </div>
	    </div>
		
		<div class="box-body">
		    <div class="row pull-right">
			    <button type="reset" class="btn btn-default" data-dismiss="modal">
					CANCEL
				</button>
				<button type="submit" class="btn btn-primary">SAVE</button>
		    </div>
	    </div>
	</form>
</div>