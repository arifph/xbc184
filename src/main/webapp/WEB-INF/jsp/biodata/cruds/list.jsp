<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${biodataModelList}" var="biodataModel" varStatus="number">
	<tr>
		<td>${biodataModel.name}</td>
		<td>${biodataModel.majors}</td>
		<td>${biodataModel.gpa}</td>
		<td>
			<%-- <button type="button" class="btn btn-warning" value="${biodataModel.id}" id="btn-detail">Lihat</button>
			<button type="button" class="btn btn-success" value="${biodataModel.id}" id="btn-edit">Ubah</button>
			<button type="button" class="btn btn-danger" value="${biodataModel.id}" id="btn-delete">Hapus</button> --%>
			<div class="btn-group">
              <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-list-ul"></i></button>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#" id="menu-edit" data-value="${biodataModel.id}">Edit</a></li>
                <li><a href="#" id="menu-delete" data-value="${biodataModel.id}">Delete</a></li>
                
                <%-- <li><button type="button" class="btn" value="${biodataModel.id}" id="menu-edit">Edit</button></li>
                <li><button type="button" class="btn" value="${biodataModel.id}" id="menu-delete">Delete</button></li> --%>
              </ul>
            </div>
		</td>
	</tr>
</c:forEach>