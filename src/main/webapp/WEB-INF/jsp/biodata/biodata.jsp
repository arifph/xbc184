<div class="panel" style="margin-top: 50px; min-height:500px;">
	<section class="content-header">
		<h1>BIODATA</h1>
	</section>
	
	<section class="content">
		<div class="box box-primary">
	<!-- <div class="panel" style="background: white; margin-top: 50px; min-height:500px;"> -->
		<!-- <h1>BIODATA</h1> -->
		<div>
			<table class="table">
				<tr>
					<td>
						<div class="input-group input-group-sm col-xs-5">
							<form action="#" method="get" id="form-biodata-search-name-majors">
								<!-- <div class="row">
									<input class="form-control" type="text" id="keyword" name="keyword" placeholder="Search by name / majors"/>
									<div class="input-group-btn">
										<button class="btn btn-primary btn-flat fa fa-search" type="submit"></button>
									</div>
								</div> -->
								
								<input type="text" id="keyword" name="keyword" placeholder="Search by name / majors"/>
								<button class="btn btn-primary btn-flat fa fa-search" type="submit"></button>
							</form>
						</div>
					</td>
					<td></td>
					<td></td>
					<td>
						<button type="button" class="btn btn-primary fa fa-plus" id="btn-add"></button>
					</td>
				</tr>	
			</table>
		</div>
		
		<div>
			<table class="table" id="tbl-biodata">
				<tr>
					<td>NAME</td>
					<td>MAJORS</td>
					<td>GPA</td>
					<td>#</td>
				</tr>
				<tbody id="list-biodata">
					
				</tbody>
			</table>
		</div>
	</div>
	
	<div id="modal-input" class="modal modal-primary fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<!-- tombol close di popupnya -->
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h4>ADD BIODATA</h4>
				</div>
				<div class="modal-body">
					<div class="modal fade">
						<!-- tempat jsp popupnya -->
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="modal-update" class="modal modal-primary fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<!-- tombol close di popupnya -->
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h4>EDIT BIODATA</h4>
				</div>
				<div class="modal-body">
					<!-- tempat jsp popupnya -->
				</div>
			</div>
		</div>
	</div>
	
	<div id="modal-delete" class="modal modal-warning fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<!-- tombol close di popupnya -->
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h4>DELETE</h4>
				</div>
				<div class="modal-body">
					<!-- tempat jsp popupnya -->
				</div>
			</div>
		</div>
	</div>
	</section>
</div>

<script>

	loadListBiodata(); // untuk memanggil otomatis

	function loadListBiodata(){ // fungsi ini tidak bisa jalan otomatis tanpa line di atas, karena di luar document.ready
		$.ajax({
			url:'biodata/cruds/list.html',
			type: 'get',
			dataType: 'html',
			success: function(result) {
				$('#list-biodata').html(result); // fungsi ini memanggil id list-biodata
			}
		});
	}

	//document ready itu setelah halamannya dipanggil
	$(document).ready(function(){
		
		// fungsi ajax untuk memunculkan POP UP tambah
		$('#btn-add').on('click',function(){
			$.ajax({
				url:'biodata/cruds/add.html',
				type:'get',
				dataType:'html',
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		// akhir fungsi ajax untuk memunculkan POP UP tambah
		
		// fungsi ajax untuk menambah biodata (tombol create pada popup tambah)
		$('#modal-input').on('submit', '#form-biodata-add',function(){
			$.ajax({
				url:'biodata/cruds/create.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully saved ! ");
					loadListBiodata();
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk menambah biodata
		
		// fungsi ajax untuk menambah biodata (tombol create pada popup tambah)
	//	$('#modal-input').on('submit', '#form-biodata-add',function(){
	//		$.ajax({
	//			url:'biodata/cruds/create.json',
	//			type:'get',
	//			dataType:'json',
	//			data:$(this).serialize(),
	//			success: function(result){
	//				/* if (result.jumlahDataBiodata > 0) {
	//					alert("ERROR: kode biodata " + result.kodeBiodata + " sudah ada di database!");
	//				}
	//				else */ /* if (result.jumlahDataBiodata2 > 0) {
	//					alert("ERROR: nama biodata " + result.namaBiodata + " sudah ada di database!");
	//				} */
	//				//else {
	//					$('#modal-input').modal('hide');
	//					alert("data telah ditambahkan");
	//					loadListBiodata();
	//				//}
	//			}
	//		});
	//		return false;
	//	});
		// akhir fungsi ajax untuk menambah biodata
		
		// fungsi ajax untuk memunculkan POP UP lihat
	//	$('#list-biodata').on('click', '#btn-detail', function(){
	//		var idBiodata = $(this).val();
	//		$.ajax({
	//			url:'biodata/cruds/detail.html',
	//			type:'get',
	//			dataType:'html',
	//			data:{idBiodata:idBiodata},
	//			success: function(result){
	//				$('#modal-input').find('.modal-body').html(result);
	//				$('#modal-input').modal('show');
	//			}
	//		});
	//	});
		// akhir fungsi ajax untuk lihat
		
		// fungsi ajax untuk memunculkan POP UP ubah
		$('#list-biodata').on('click', '#menu-edit', function(){
			var id = $(this).attr('data-value');
			$.ajax({
				url:'biodata/cruds/edit.html',
				type:'get',
				dataType:'html',
				data:{id:id},
				success: function(result){
					$('#modal-update').find('.modal-body').html(result);
					$('#modal-update').modal('show');
				}
			});
		});
		// akhir fungsi ajax untuk ubah
		
		// fungsi ajax untuk update biodata
		$('#modal-update').on('submit', '#form-biodata-edit', function(){
			
			$.ajax({
				url:'biodata/cruds/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-update').modal('hide');
					alert("Data successfully updated ! ");
					loadListBiodata();
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk update biodata
		
		// fungsi ajax untuk memunculkan POP UP hapus
		$('#list-biodata').on('click', '#menu-delete', function(){
			// var idBiodata = $(this).val();
			var id = $(this).attr('data-value');
			$.ajax({
				url:'biodata/cruds/remove.html',
				type:'get',
				dataType:'html',
				data:{id:id},
				success: function(result){
					$('#modal-delete').find('.modal-body').html(result);
					$('#modal-delete').modal('show');
				}
			});
		});
		// akhir fungsi ajax untuk hapus
		
		// fungsi ajax untuk delete biodata
		$('#modal-delete').on('submit', '#form-biodata-delete', function(){
			// var id = $(this);
			$.ajax({
				url:'biodata/cruds/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-delete').modal('hide');
					alert("Data successfully deleted ! ");
					loadListBiodata();
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk delete biodata
		
		// fungsi ajax untuk mencari biodata berdasarkan name atau majors
		$('#form-biodata-search-name-majors').on('submit',function(){
			$.ajax({
			url:'biodata/cruds/search/nameORmajors.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					//alert("data telah dicari berdasarkan nama");
					$('#list-biodata').html(result);
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk mencari biodata berdasarkan nama
		
		// fungsi ajax untuk mencari biodata berdasarkan kode
	//	$('#form-biodata-search-kode').on('submit',function(){
	//		$.ajax({
	//			url:'biodata/cruds/search/kode.html',
	//			type:'get',
	//			dataType:'html',
	//			data:$(this).serialize(),
	//			success: function(result){
	//				alert("data telah dicari berdasarkan kode");
	//				$('#list-biodata').html(result);
	//			}
	//		});
	//		return false;
	//	});
		// akhir fungsi ajax untuk mencari biodata berdasarkan kode
		
		// fungsi ajax untuk mencari berdasarkan kode / nama biodata
	//	$('#form-biodata-search').on('submit',function(){
	//		$.ajax({
	//			url:'biodata/cruds/search.html',
	//			type:'get',
	//			dataType:'html',
	//			data:$(this).serialize(),
	//			success: function(result){
	//				// alert("data telah dicari berdasarkan keyword");
	//				$('#list-biodata').html(result);
	//			}
	//		});
	//		return false;
	//	});
		// akhir fungsi ajax untuk mencari berdasarkan kode / nama biodata
	});
	// akhir dari dokumen ready
</script>