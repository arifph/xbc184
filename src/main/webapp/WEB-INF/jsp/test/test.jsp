<div class="panel" style="background: white; margin-top: 50px; min-height: 500px;">
	<div>
		<table class="table" id="tbl-header-test">
			<tr bgcolor="orange">
				<td><h3>TEST</h3></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
		<table class="table" id ="tbl-test" style="width: 70%; margin-left: 15%; margin-right: 15%; table-layout: fixed;">
			<tr>
				<td>
					<form action="#" method="get" id="form-test-search-name">
						<input placeholder="Search by name" type="text" id="nameTestKey" name="nameTestKey" size="20">
						<button class="btn btn-warning" type="submit">O</button>
					</form>
				</td>
				<td></td>
				<td>
					<button type="button" class="btn btn-warning" id="btn-add">+</button>
				</td>
			</tr>
			<tr>
				<td>NAME</td>
				<td>CREATE BY</td>
				<td>#</td>
			</tr>
			<tbody id="list-test">
			 <!-- listnya -->
			</tbody>
		</table>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script>
	
	loadListTest();
	
	function loadListTest(){
		$.ajax({
			url : 'test/cruds/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-test').html(result);
			}
		});
	}
	
	//dokumen ready itu setelah halaman dipanggil
	$(document).ready(function(){
		
		//fungsi ajax untuk add
		$('#btn-add').on('click', function(){
			$.ajax({
				url : 'test/cruds/add.html',
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		}); // akhir fungsi add
		
		//fungsi ajax untuk tambah
		$('#modal-input').on('submit', '#form-test-add', function(){
			$.ajax({
				url:'test/cruds/create.json',
				type:'get',
				dataType:'json',
				data: $(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully saved");
					loadListTest();
				}
			});
		}); // akhir fungsi ajax  tambah
		
		
		//fungsi search nama Test
		$('#form-test-search-name').on('submit',function(){
			$.ajax({
				url:'test/cruds/search/name.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("Data successfully submitted");
					$('#list-test').html(result);
				}
			});
			return false;
		});
		//akhir fungsi search nama Test
		
		//fungsi ajax untuk edit
		$('#list-test').on('click','#btn-edit', function(){
			var idTest = $(this).attr("data-value");
			$.ajax({
				url : 'test/cruds/edit.html',
				type : 'get',
				dataType : 'html',
				data:{idTest : idTest},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		}); // akhir fungsi edit
		
		
		//fungsi ajax untuk update
		$('#modal-input').on('submit', '#form-test-edit', function(){
			$.ajax({
				url:'test/cruds/update.json',
				type:'get',
				dataType:'json',
				data: $(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully updated");
					loadListTest();
				}
			});
		}); // akhir fungsi ajax  update
		
		//fungsi ajax untuk remove
		$('#list-test').on('click','#btn-delete', function(){
			var idTest = $(this).attr("data-value");
			$.ajax({
				url : 'test/cruds/remove.html',
				type : 'get',
				dataType : 'html',
				data:{idTest : idTest},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		}); // akhir fungsi edit
		
		
		//fungsi ajax untuk update
		$('#modal-input').on('submit', '#form-test-delete', function(){
			$.ajax({
				url:'test/cruds/delete.json',
				type:'get',
				dataType:'json',
				data: $(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully deleted");
					loadListTest();
				}
			});
		}); // akhir fungsi ajax  update
		
	});//akhir dokumen

</script>