<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${testModelList}" var="testModel">
	<tr>
		<td>${testModel.nameTest}</td>
		<td>${testModel.xCreatedBy.username}</td>
		<td>
			<div class="dropdown" align="right">
				<div class="button-group" align="left">
					<button type="button" class="btn-box-tool dropdown-toggler" data-toggle= "dropdown">
						<i class="fa fa-navicon"></i>
					</button>
					<ul id="menu" class="dropdown-menu" role="menu">
						<li><a id="btn-edit" data-value="${testModel.idTest}">Edit</a></li>
						<li><a id="btn-delete" data-value="${testModel.idTest}">Delete</a></li>
					</ul>
				</div>
			</div>
		</td>
	</tr>
	
</c:forEach>