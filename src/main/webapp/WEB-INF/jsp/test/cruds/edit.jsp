<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<div>
		<table class="table" id="tbl-header-test">
			<tr bgcolor="orange">
				<td><h3><b><font color="white">EDIT TEST</font></b></h3></td>
			</tr>
		</table>
	</div>
	
	<form action="#" method="get" id="form-test-edit">
	<input type="hidden" id="idTest" name="idTest" value="${testModel.idTest}">
		<table style="width: 80%; margin-left: 10%; margin-right: 10%; border-collapse: separate; border-spacing: 15px; ">
			<tr>
				<td><input type="text" placeholder="Name" id="nameTest" name="nameTest" size="63%" value="${testModel.nameTest}"> </td>
			</tr>
			<tr>
				<td>Is Bootcamp Test ? 
				    <input type="radio" name="isBootcampTest" value="true" ${testModel.isBootcampTest=='true' ? 'checked="checked"':''}/>Yes
					<input type="radio" name="isBootcampTest" value="false" ${testModel.isBootcampTest=='false' ? 'checked="checked"':''}>No <br>
				</td>
			</tr>
			<tr>
				<td>
					<textarea rows="3" cols="65%" name="notesTest" id="notesTest" placeholder="Notes" >${testModel.notesTest}</textarea>
				</td>
			</tr>
		</table>
		
		<button type="submit" style="float: right; margin-right: 10%; margin-bottom: 15%;">SAVE</button>
		<button type="button" data-dismiss="modal" style="float: right; margin-right: 5%; margin-bottom: 15%;">CANCEL</button>
	</form>
	<hr>
</div>