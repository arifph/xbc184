<div class= "panel" style = "background: white; margin-top: 50px; min-height: 500px">
	<div>
		<table class= "table" id= "tbl-header-feedback">
			<tr bgcolor= "#0066ff">
				<td><h4><b><font color="white">FEEDBACK</font></b></h4></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
	
	<h3 style= "margin-left: 15%"><b><u>TEST LIST</u></b></h3>
	<hr>
	
	<div>
		<table class= "table" id ="tbl-feedback" style= "width: 70%; margin-left: 15%; margin-right: 15%; table-layout:fixed">
			<tr></tr>
			<tr bgcolor="#3399ff">
				<td>NAME</td>
				<td align= "center">#</td>
			</tr>
			<tbody id= "list-feedback">
			</tbody>
		</table>
	</div>
</div>

<div id= "modal-input" class= "modal fade">
	<div class= "modal-dialog">
		<div class= "modal-content">
			<div class= "modal-header">
			</div>
			<div class= "modal-body">
				<!-- Tempat jsp PopUp -->
			</div>
		</div>
	</div>
</div>

<script>

	loadListFeedback();
	
	function loadListFeedback() {
		$.ajax({
			url: 'feedback/cruds/list.html',
			type: 'get',
			dataType: 'html',
			success: function(result) {
				$('#list-feedback').html(result);
			}
		})
	}
	
	/* //Document ready setelah halaman dipanggil
	$(document).ready(function() {
		
		//Fungsi ajax untuk popup tambah
		$('#btn-add').on('click', function() {
			$.ajax({
				url: 'feedback/cruds/add.html',
				type: 'get',
				dataType: 'html',
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		})
		//Akhir fungsi ajax untuk popup tambah
		
		//Fungsi ajax untuk create tambah
		$('#modal-input').on('submit', '#form-feedback-add', function() {
			$.ajax({
				url: 'feedback/cruds/create.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully saved !");
					loadListFeedback();
				}
			});
			return false;
		});
		//Akhir fungsi ajax untuk create tambah */
	
</script>