<%@ taglib uri= "http://java.sun.com/jstl/core_rt" prefix= "c" %>
<div class= "form-horizontal">
	<div>
		<table class= "table" id= "tbl-header-role">
			<tr bgcolor= "#0066ff">
				<td><h4><b><font color="white">CHOOSE DOCUMENT</font></b></h4></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
	
	<h3 style= "margin-left: 15%"><b><u>DOCUMENT TEST LIST</u></b></h3>
	<hr>
	
	<form action= "#" method= "get" id= "form-feedback-add">
		<table style= "width: 80%; margin-left: 10%; margin-right: 10%; border-collapse: separate; border-spacing: 15px;">
			<c:forEach items= "${feedbackModelList}" var= "feedbackModel" varStatus= "number">
				<tr>
					<td>Feedback ${feedbackModel.documentTestModel.testTypeModel.name}</td>
					<td><button type = "button" class= "btn btn-primary" id= "btn-open">Open</button></td>
				</tr>
			</c:forEach>
			
		</table>
		
		<button type= "button" data-dismiss= "modal" style="float: right; margin-right: 5%; margin-bottom: 15%;">CLOSE</button>

	</form>
	
	<hr>
	
</div>