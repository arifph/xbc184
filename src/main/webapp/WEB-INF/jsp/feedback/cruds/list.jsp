<%@ taglib uri= "http://java.sun.com/jstl/core_rt" prefix= "c" %>
<%@ taglib uri= "http://java.sun.com/jsp/jstl/fmt" prefix= "fmt" %>

<c:forEach items= "${feedbackModelList}" var= "feedbackModel" varStatus= "number">
	<tr>
		<td>Feedback ${feedbackModel.testModel.nameTest}</td>
		<td>
			<div class= "dropdown" align= "center">
				<div class= "button-group" align= "center">
					<button type= "button" class= "btn-box-tool dropdown-toggler" data-toggle= "dropdown">
						<i class= "fa fa-navicon"></i>
					</button>
					<ul id= "menu" class= "dropdown-menu" role= "menu">
						<li><a id= "btn-add" data-value= "${feedbackModel.idFeedback}">Choose Document</a></li>
					</ul>
				</div>
			</div>
		</td>
	</tr>
</c:forEach>