<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${classModelList}" var="classModel" varStatus="number">
	<tr>
		<%-- <c:forEach items="${technologyModelList}" var="technologyModel" varStatus="number">
			<c:if test="${batchModel.technologyId == technologyModel.id}">
				<td>${technologyModel.name}</td>
			</c:if>
		</c:forEach> --%>
		<td>${classModel.batchModel.technologyModel.name} ${classModel.batchModel.name}</td>
		<td>${classModel.biodataModel.name}</td>
		<%-- <td>${classModel.gpa}</td> --%>
		<td>
			<%-- <button type="button" class="btn btn-warning" value="${classModel.id}" id="btn-detail">Lihat</button>
			<button type="button" class="btn btn-success" value="${classModel.id}" id="btn-edit">Ubah</button>
			<button type="button" class="btn btn-danger" value="${classModel.id}" id="btn-delete">Hapus</button> --%>
			<div class="btn-group">
              <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-list-ul"></i></button>
              <ul class="dropdown-menu" role="menu">
                <%-- <li><a href="#" id="menu-edit" data-value="${classModel.id}">Edit</a></li> --%>
                <li><a href="#" id="menu-delete" data-value="${classModel.id}">Delete</a></li>
                
                <%-- <li><button type="button" class="btn" value="${classModel.id}" id="menu-edit">Edit</button></li>
                <li><button type="button" class="btn" value="${classModel.id}" id="menu-delete">Delete</button></li> --%>
              </ul>
            </div>
		</td>
	</tr>
</c:forEach>