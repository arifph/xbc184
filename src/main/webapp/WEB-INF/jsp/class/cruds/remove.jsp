<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<h3>Are you sure to delete this data?</h3>
	<form action="#" method="get" id="form-class-delete">
	<input type="hidden" id="id" name="id" value="${classModel.id}" />
		<!-- <table>
			<tr>
				<td>
					Are you sure to delete this data?
				</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>
					<button type="button" class="btn btn-default" data-dismiss="modal">
						NO
					</button>
				</td>
				<td>
					<button type="submit" class="btn btn-outline">YES</button>
				</td>
			</tr>
		</table> -->
		
		<div class="box-body">
		    <div class="row pull-right">
			    <button type="button" class="btn btn-default" data-dismiss="modal">
					NO
				</button>
				<button type="submit" class="btn btn-success">YES</button>
		    </div>
	    </div>
	</form>
</div>