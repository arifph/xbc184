<div class="panel" style="margin-top: 50px; min-height:500px;">
	<section class="content-header">
		<h1>CLASS</h1>
	</section>
	
	<section class="content">
		<div class="box box-primary">
	<!-- <div class="panel" style="background: white; margin-top: 50px; min-height:500px;"> -->
		<!-- <h1>CLASS</h1> -->
		<div>
			<table class="table">
				<tr>
					<td>
						<div class="input-group input-group-sm col-xs-5">
							<form action="#" method="get" id="form-class-search">
								<!-- <div class="row">
									<input class="form-control" type="text" id="keyword" name="keyword" placeholder="Search by name / majors"/>
									<div class="input-group-btn">
										<button class="btn btn-primary btn-flat fa fa-search" type="submit"></button>
									</div>
								</div> -->
								
								<input type="text" id="keyword" name="keyword" placeholder="Search by batch"/>
								<button class="btn btn-primary btn-flat fa fa-search" type="submit"></button>
							</form>
						</div>
					</td>
					<td></td>
					<td></td>
					<!-- <td>
						<button type="button" class="btn btn-primary fa fa-plus" id="btn-add"></button>
					</td> -->
				</tr>	
			</table>
		</div>
		
		<div>
			<table class="table" id="tbl-class">
				<tr>
					<td>BATCH</td>
					<td>NAME</td>
					<!-- <td>GPA</td> -->
					<td>#</td>
				</tr>
				<tbody id="list-class">
					
				</tbody>
			</table>
		</div>
	</div>
	
	<div id="modal-delete" class="modal modal-warning fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<!-- tombol close di popupnya -->
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h4>DELETE</h4>
				</div>
				<div class="modal-body">
					<!-- tempat jsp popupnya -->
				</div>
			</div>
		</div>
	</div>
	</section>
</div>

<script>

	loadListClass(); // untuk memanggil otomatis

	function loadListClass(){ // fungsi ini tidak bisa jalan otomatis tanpa line di atas, karena di luar document.ready
		$.ajax({
			url:'class/cruds/list.html',
			type: 'get',
			dataType: 'html',
			success: function(result) {
				$('#list-class').html(result); // fungsi ini memanggil id list-class
			}
		});
	}

	//document ready itu setelah halamannya dipanggil
	$(document).ready(function(){
		
		// fungsi ajax untuk memunculkan POP UP hapus
		$('#list-class').on('click', '#menu-delete', function(){
			// var idClass = $(this).val();
			var id = $(this).attr('data-value');
			$.ajax({
				url:'class/cruds/remove.html',
				type:'get',
				dataType:'html',
				data:{id:id},
				success: function(result){
					$('#modal-delete').find('.modal-body').html(result);
					$('#modal-delete').modal('show');
				}
			});
		});
		// akhir fungsi ajax untuk hapus
		
		// fungsi ajax untuk delete class
		$('#modal-delete').on('submit', '#form-class-delete', function(){
			// var id = $(this);
			$.ajax({
				url:'class/cruds/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-delete').modal('hide');
					alert("Data successfully deleted ! ");
					loadListClass();
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk delete class
		
		// fungsi ajax untuk mencari class berdasarkan batch
		$('#form-class-search').on('submit',function(){
			$.ajax({
			url:'class/cruds/search.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					//alert("data telah dicari berdasarkan nama");
					$('#list-class').html(result);
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk mencari class berdasarkan batch
	});
	// akhir dari dokumen ready
</script>