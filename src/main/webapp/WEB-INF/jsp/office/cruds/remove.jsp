<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div align="center" class="form-horizontal">
	<form action="#" method="get" id="form-office-delete">
	<input type="hidden" id="id" name="id" value="${officeModel.id}"/>
		Are you sure to delete this data ?
		<table class="table">
			<tr align="center">
				<td><button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button></td>
				<td><button type="submit" class="btn btn-success">Yes</button></td>
			</tr>
		</table>
	</form>
</div>