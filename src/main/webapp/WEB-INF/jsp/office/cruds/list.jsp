<%@ taglib uri = "http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<table class="table">
<c:forEach items="${officeModelList}" var="officeModel" varStatus="number">
	<tr>
		<td>${officeModel.name}</td>
		<td>${officeModel.phone} // ${officeModel.email}</td>
		<td>
		<div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-list"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#" data-value="${officeModel.id}" id="btn-edit">Edit</a></li>
                    <li><a href="#" data-value="${officeModel.id}" id="btn-delete">Delete</a></li>
                  </ul>
                </div>
		</td>
	</tr>
</c:forEach>
</table>
			<%-- <button type="button" class="btn btn-success" value="${officeModel.idOffice}" id="btn-edit">Ubah</button>
			<button type="button" class="btn btn-danger" value="${officeModel.idOffice}" id="btn-delete">Delete</button> --%>