<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<form action="#" method="get" id="form-office-add">
		<input type="hidden" id="idOffice" name="idOffice" value="${idOffice}"/>
		<table class="table">
			<tr>
			
			<!-- 		name untuk controller -->
				
				<td><input type="text" class="form-control" id="name" name="name" placeholder="Name"/></td>
								
				<td><input type="text" class="form-control" id="phone" name="phone" placeholder="Phone"/></td>
						
				<td><input type="text" class="form-control" id="email" name="email" placeholder="Email"/></td>
							
			</tr>
			
			</table>
			
			<table class = "table">
			<tr>
				<td>
					<textarea class="form-control" rows="4" id="address" name="address" placeholder="Address" style="resize: none"></textarea>
				</td>			
				<td>
					<textarea class="form-control" rows="4" id="notes" name="notes" placeholder="Description" style="resize: none"></textarea>
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right"><button type="button" class="btn btn-primary" id="btn-add">+ROOM</button></td>
			</tr>
		</table>
	
	 <div>
		<table class="table table-bordered dataTable" id="tbl-room">
			<tr>
				<td>CODE</td>
				<td>NAME</td>
				<td>CAPACITY</td>
				<td>#</td>
			</tr>

			<tbody id="list-room-body">

			</tbody>

			<tr>
				<td>
				</td>
				<td>
				</td>
			</tr>
		
		</table>
	</div>
	
				<button type="button" class="btn btn-primary" data-dismiss="modal" id="btn-cancel">Cancel</button>
				
				<button type="submit" class="btn btn-primary">Save</button> 
	
	</form>
</div>




