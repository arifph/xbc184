<%@ taglib uri = "http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
	<%-- ID OFFICE <input type="text" id="idOffice" name="idOffice" value="${idOffice}"/> --%>
	
<table class="table">
<c:forEach items="${roomModelList}" var="roomModel" varStatus="number">	
	<tr>
		<%-- <td>${roomModel.officeId}</td> --%>
		<td>${roomModel.code}</td>
		<td>${roomModel.name}</td>
		<td>${roomModel.capacity}</td>		
		<td>
		<div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-list"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#" data-value="${roomModel.id}" id="btn-edit">Edit</a></li>
                    <li><a href="#" data-value="${roomModel.id}" id="btn-delete">Delete</a></li>
                  </ul>
                </div>
		</td>
	</tr>
</c:forEach>
</table>