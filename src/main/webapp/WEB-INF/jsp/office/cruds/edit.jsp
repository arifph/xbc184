<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<form action="#" method="get" id="form-office-edit">
		<input type="hidden" id="id" name="id" value="${officeModel.id}"/>
		<table class="table">
			<tr>
			
			<!-- 		name untuk controller -->
			
				<td><input type="text" id="name" name="name" class="form-control" value="${officeModel.name}" placeholder="Name"/></td>
								
				<td><input type="text" id="phone" name="phone" class="form-control" value="${officeModel.phone}" placeholder="Phone"/></td>
						
				<td><input type="text" id="email" name="email" class="form-control" value="${officeModel.email}" placeholder="Email"/></td>
							
			</tr>
		</table>
			
			<table class="table">
			<tr>
				<td>
				<textarea class="form-control" rows="4" id="address" name="address" style="resize: none" placeholder="Address">${officeModel.address}</textarea>
				</td>				
				<td>
					<textarea class="form-control" rows="4" id="notes" name="notes" style="resize: none" placeholder="Description">${officeModel.notes}</textarea>
				</td>	
			</tr>
			<tr>
				<td></td>
				<td align="right"><button type="button" class="btn btn-primary" id="btn-add">+ROOM</button></td>
			</tr>
		</table>
	<div>
		<table class="table table-bordered dataTable" id="tbl-room">

			<tr>
			</tr>
			
			<tr>
				<td>CODE</td>
				<td>NAME</td>
				<td>CAPACITY</td>
				<td>#</td>
			</tr>

			<tbody id="list-room">

			</tbody>

			<tr>
				<td>
				</td>
				<td>
				</td>
			</tr>
		
		</table>
	</div>
				<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
				
				<button type="submit" class="btn btn-primary">Save</button>
	</form>
</div>