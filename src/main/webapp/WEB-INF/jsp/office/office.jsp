<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Office</h1>
	<div>
		<table class="table" id="tbl-office">

			<tr>	
				<td style="width: 40%">
					<form action="#" method="get" id="form-office-search-name">
					<div class="input-group">
						<input type="text" id="nameKey" name="nameKey" class="form-control" placeholder="Search by name"/>
						<span class="input-group-btn">
						<button type="submit" class="btn btn-flat btn btn-primary" >
							<i class="fa fa-search"></i>
						</button>
						</span>
					</div>
					</form>
				</td>
				<td style="width: 40%">
				</td>
				<td style="width: 20%">
					<button type="button" class="btn btn-primary" id="btn-add"><b>+</b></button>	
				</td>
			</tr>

			<tr>

			</tr>
			
			<tr>
				<td>NAME</td>
				<td>CONTACT</td>
				<td>#</td>
			</tr>

			<tbody id="list-office">

			</tbody>
		</table>
	</div>
</div>

<!-- Modal untuk add office -->
<div id="modal-input" class="modal fade" tabindex="-1" data-focus-on="input:first" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-light-blue">
				<h4><b>OFFICE</b></h4>
			</div>
			<div class="modal-body">
				tempat jsp popupnya
							
			</div>
			<div class="modal-footer">
   			</div>
		</div>
	</div>
</div>

<!-- Modal untuk add room -->
<div id="modal-room" class="modal fade" tabindex="-1" data-focus-on="input:first" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-light-blue">
				<h4>ROOM</h4>
			</div>
			<div class="modal-body-room">
				tempat jsp popupnya
							
  			</div>
			<div class="modal-footer">
    		</div>
		</div>
	</div>
</div>


<script>
//loadListOffice();
loadListOfficeDelete();

function loadListOffice() {
	$.ajax({
		url : 'office/cruds/list.html',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#list-office').html(result);
		}
	});
}

//delete name kosong
function loadListOfficeDelete() {
	$.ajax({
		url : 'office/cruds/list-delete.json',
		type : 'get',
		dataType : 'json',
		success : function(result) {
			loadListOffice();
			$('#list-office').html(result);
		}
	});
}




//roomz
/* loadListRoom();
 */
function loadListRoom() {
	 var id = $('#idOffice').val();
	$.ajax({
		url : 'office/cruds/list-room.html',
		type : 'get',
		dataType : 'html',
		data :{
			id:id
		},
		success : function(result) {
			$('#list-room-body').html(result);	
			$('#list-room').html(result);	
		}
	});
}


$(document).ready(
		function() {
			
			$('#modal-room').on('hide.bs.modal', function () {
			    $("#modal-input").css("overflow-y", "auto"); // 'auto' or 'scroll'
			});
			/*
			JQuery diatas untuk menghilangkan bug tabrakan modal 
			saat modal-room/modal kedua tampil
			on 'hide' event in modal 2
			*/			
			$('#btn-add').on('click', function() {
				$.ajax({
					url : 'office/cruds/add.html',
					type : 'get',
					dataType : 'html',
					success : function(result) {
						$('#modal-input').find('.modal-body').html(result);
						$('#modal-input').modal('show');
					}
				});
			})
			/*
			JQuery diatas, memunculkan modal OFFICE
			dengan nama modal-input dan untuk menampilkan
			jsp add pada folder office/cruds/add
			pada modal-body di dalam modal OFFICE
			saat button dengan id=btn-add diklik
			*/
			
			//fungsi ajax untuk memunculkan popup tambah room
			$('#modal-input').on('click', '#btn-add', function() {
				var id = $('#idOffice').val();
				$.ajax({
					url : 'office/room/add.html',
					type : 'get',
					dataType : 'html',
					data : {id:id},
					success : function(result) {
						$('#modal-room').find('.modal-body-room').html(result);
						$('#modal-room').modal('show');
					}
				});
			})
			/*
			JQuery diatas, memunculkan modal ROOM
			dengan nama modal-room di dalam modal-office
			dan untuk menampilkan
			jsp add pada folder office/room/add
			pada modal-body-room di dalam modal ROOM
			saat button dengan id=btn-add diklik
			*/
			
			
			
			
			
			//coba==============
			//fungsi ajax untuk memunculkan popup tambah room di edit office
/* 			$('#modal-input').on('click', '#btn-add', function() {
				var id = $('#idOffice').val();
				$.ajax({
					url : 'office/room/add.html',
					type : 'get',
					dataType : 'html',
					data : {id:id},
					success : function(result) {
						$('#modal-room').find('.modal-body-room').html(result);
						$('#modal-room').modal('show');
					}
				});
			})
 */			
			//fungsi ajax untuk menyimpan popup tambah office
			$('#modal-input').on('submit','#form-office-add',function() {
							$.ajax({
								url : 'office/cruds/create.json',
								type : 'get',
								dataType : 'json',
								data : $(this).serialize(),
								success : function(result) {
										$('#modal-input').modal('hide');
										alert("data telah ditambah");
										loadListOffice();
								}
							});
							return false; //supaya tidak merefresh ulang semua data, jadi data ditambah saja
						});
		
			//fungsi ajax untuk menyimpan popup tambah room
			$('#modal-room').on('submit','#form-room-add',function() {
				$.ajax({
					url : 'office/room/create.json',
					type : 'get',
					dataType : 'json',
					data :  $(this).serialize(),
					success : function(result) {
							$('#modal-room').modal('hide');
							alert("data telah ditambah");
							loadListRoom();
					}
				});
				return false; //supaya tidak merefresh ulang semua data, jadi data ditambah saja
			});
			
			
			
			//edit popup btn
			$('#list-office').on('click', '#btn-edit', function() {
				var id = $(this).attr('data-value');
				$.ajax({
					url : 'office/cruds/edit.html',
					type : 'get',
					dataType : 'html',
					data : {
						id : id
					},
					success : function(result) {
						$('#modal-input').find('.modal-body').html(result);
						$('#modal-input').modal('show');
						loadListRoom();
					}
				});
			});

			//fungsi ajax untuk update di dalam popup update
			$('#modal-input').on('submit', '#form-office-edit',
					function() {
						$.ajax({
							url : 'office/cruds/update.json',
							type : 'get',
							dataType : 'json',
							data : $(this).serialize(),
							success : function(result) {
								$('#modal-input').modal('hide');
								alert("data telah diubah");
								loadListOffice();
							}
						});
						return false;
					});
		
			//btn hapus
			$('#list-office').on('click', '#btn-delete', function() {
				var id = $(this).attr('data-value');
				$.ajax({
					url : 'office/cruds/remove.html',
					type : 'get',
					dataType : 'html',
					data : {
						id : id
					},
					success : function(result) {
						$('#modal-input').find('.modal-body').html(result);
						$('#modal-input').modal('show');
					}
				});
			});
			
			//fungsi untuk menjalakan hapus pada pop up
			$('#modal-input').on('submit', '#form-office-delete',
					function() {
						$.ajax({
							url : 'office/cruds/delete.json',
							type : 'get',
							dataType : 'json',
							data : $(this).serialize(),// ambil data dari backend secara kesuluruhan
							success : function(result) {
								$('#modal-input').modal('hide');
								alert("data telah dihapus");
								loadListOffice();
							}
						});
						return false;
					});

			//search nama office
			$('#form-office-search-name').on('submit', function() {
				$.ajax({
					url : 'office/cruds/search/name.html',
					type : 'get',
					dataType : 'html',
					data : $(this).serialize(),
					success : function(result) {
						alert("data telah dicari berdasarkan nama!");
						$('#list-office').html(result);
					}
				});
				return false;
			});			
			
			
			//edit popup btn room pada add room
			$('#modal-input').on('click', '#btn-edit', function() {
				var id = $(this).attr('data-value');
				$.ajax({
					url : 'office/room/edit.html',
					type : 'get',
					dataType : 'html',
					data : {
						id : id
					},
					success : function(result) {
						$('#modal-room').find('.modal-body-room').html(result);
						$('#modal-room').modal('show');
 						loadListRoom();
					}
				});
			});
			
			//fungsi ajax untuk update di dalam popup update ROOM
			$('#modal-room').on('submit', '#form-room-edit',
					function() {
						$.ajax({
							url : 'office/room/update.json',
							type : 'get',
							dataType : 'json',
							data : $(this).serialize(),
							success : function(result) {
								$('#modal-room').modal('hide');
								alert("data telah diubah");
								loadListRoom();
							}
						});
						return false;
					});
			
			
			//btn hapus room
			$('#modal-input').on('click', '#btn-delete', function() {
				var id = $(this).attr('data-value');
				$.ajax({
					url : 'office/room/remove.html',
					type : 'get',
					dataType : 'html',
					data : {
						id : id
					},
					success : function(result) {
						$('#modal-room').find('.modal-body-room').html(result);
						$('#modal-room').modal('show');
					}
				});
			});
			
			//fungsi untuk menjalakan hapus pada pop up
			$('#modal-room').on('submit', '#form-room-delete',
					function() {
						$.ajax({
							url : 'office/room/delete.json',
							type : 'get',
							dataType : 'json',
							data : $(this).serialize(),// ambil data dari backend secara kesuluruhan
							success : function(result) {
								$('#modal-room').modal('hide');
								alert("data telah dihapus");
								loadListRoom();
							}
						});
						return false;
					});
			
			$('#modal-input, #form-office-add').on('click', '#btn-cancel',
					function() {
						$.ajax({
							url : 'office/cruds/cancel.json',
							type : 'get',
							dataType : 'json',
							data : $(this).serialize(),// ambil data dari backend secara kesuluruhan
							success : function(result) {
								$('#modal-input').modal('hide');
								alert("data telah dihapus");
								loadListOffice();
							}
						});
						return false;
					});
			
		});
</script>