<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<form action="#" method="get" id="form-room-edit">
		<input type="hidden" id="id" name="id" value="${roomModel.id}" />
		<table class="table fixed">

			<!-- 		name untuk controller -->
			<tr>
				<td style="width: 100%"><input type="text" id="code"
					name="code" placeholder="Code" class="form-control"
					value="${roomModel.code}" /></td>
			</tr>
			<tr>
				<td><input type="text" id="name" name="name" placeholder="Name"
					class="form-control" value="${roomModel.name}" /></td>
			</tr>
			<tr>
				<td><input type="text" id="capacity" name="capacity"
					placeholder="Capacity" class="form-control"
					value="${roomModel.capacity}" /></td>
			</tr>

			<tr>
				<td><label>Any Projector ?</label> <label
					style="padding-left: 5em"><input type="radio"
						id="anyProjector" name="anyProjector" value=1 ${roomModel.anyProjector == 'true'? 'checked="checked"':'' }>Yes </label> <label
					style="padding-left: 3em"><input type="radio"
						id="anyProjector" name="anyProjector" value=0 ${roomModel.anyProjector == 'false' ? 'checked="checked"':'' }>No </label></td>
			</tr>

			<tr>
				<td><textarea class="form-control" rows="4" id="notes"
						name="notes" placeholder="Description" style="resize: none"> ${roomModel.notes} </textarea>
				</td>
			</tr>

			<tr>
				<td></td>
				<td><button type="button" class="btn btn-primary"
						data-dismiss="modal">CANCEL</button></td>

				<td><button type="submit" class="btn btn-primary">ADD</button></td>
			</tr>
		</table>
	</form>
</div>