<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<form action="#" method="get" id="form-room-add">
<%-- 		ID OFFICE <input type="text" id="officeId" name="officeId" value="${officeId}"/>	
 --%>		<table class="table fixed">

			<!-- 		name untuk controller -->
			<tr>
				<td style="width: 100%"><input type="text" id="code" name="code"
					placeholder="Code" class="form-control"/></td>
			</tr>
			<tr>
				<td><input type="text" id="name" name="name"
					placeholder="Name" class="form-control"/></td>
			</tr>
			<tr>
				<td><input type="text" id="capacity" name="capacity"
					placeholder="Capacity" class="form-control"/></td>
			</tr>

			<tr>
				<td><label>Any Projector ?</label>
				<label style="padding-left:5em"><input type="radio" id="anyProjector" name="anyProjector" value="true">Yes </label>
				<label style="padding-left:3em"><input type="radio" id="anyProjector"name="anyProjector" value="false">No </label></td>
			</tr>
			
			<tr>
				<td>
					<textarea class="form-control" rows="4" id="notes" name="notes" placeholder="Description" style="resize: none"></textarea>
				</td>
			</tr>
			
			
			 <tr>
				<td></td>
				<td><button type="button" class="btn btn-primary"
						data-dismiss="modal" id="btn-cancel">CANCEL</button></td>

				<td><button type="submit" class="btn btn-primary">ADD</button></td>
			</tr>
		</table>
	</form>
</div>