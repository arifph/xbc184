<div class="panel"
	style="background: white; margin-top: 50px; min-height: 550px">
	<div>
		<table class="table" id="tbl-technology">
			<tr>
				<td><h2>TECHNOLOGY</h2></td>
			</tr>
			<tr>
				<td>
					<form action="#" method="get" id="form-technology-search">
						<input type="text" id="keyword" name="keyword" size="30" placeholder="Search by Name"/>
						<button class="fa fa-search btn btn-primary" type="submit" id="btn-search"></button>
					</form>
				</td>
				<td align="left" ><button class="btn btn-primary" type="button" id="btn-add">+</button></td>
			</tr>
			<tr>
				<td>Technology</td>
				<td>Created By</td>
				<td align="left">#</td>
			</tr>
			<tbody id="list-technology">
			</tbody>
		</table>
	</div>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-light-blue">
				<h2>ADD TECHNOLOGY</h2>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popup -->
			</div>
		</div>
	</div>
</div>

<div id="modal-edit" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-light-blue">
				<h2>EDIT TECHNOLOGY</h2>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popup -->
			</div>
		</div>
	</div>
</div>

<div id="modal-delete" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-light-blue">
				<h2>Delete</h2>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popup -->
			</div>
		</div>
	</div>
</div>

<script>
	
	loadListTechnology();
/* 	loadListTechnologyTrainerRemove(); */
	
	function loadListTechnology(){
		$.ajax({
			url:'technology/cruds/list.html',
			type:'get',
			dataType:'html',
			success: function(result){
				$('#list-technology').html(result);
			}
		});
	}
	
	//list trainer pada modal add
	function loadListTechnologyTrainer(){
		$.ajax({
			url:'technologytrainer/cruds/addList.html',
			type:'get',
			dataType:'html',
			success: function(result){
				$('#list-add-trainer').html(result);
			}
		});
	} 
	
	//reset arraylist
	function loadListTechnologyTrainerRemove(){
		$.ajax({
			url:'technologytrainer/cruds/removeList.html',
			type:'get',
			dataType:'html',
			success: function(result){
				$('#list-add-trainer').html(result);
			}
		});
	}
	
	function loadListEditTechnologyTrainer(){
		var id = $('#idTechnology').val();
		$.ajax({
			url:'technology/cruds/edit/editList.html',
			type:'get',
			dataType:'html',
			data: {id:id},
			success: function(result){
				$('#list-edit-trainer').html(result);
			}
		});
	} 
	
	function loadListEditTechnologyTrainerUpdate(){
		$.ajax({
			url:'technology/cruds/edit/editListUpdate.html',
			type:'get',
			dataType:'html',
			success: function(result){
				$('#list-edit-trainer-update').html(result);
			}
		});
	}
	
	function loadListEditTechnologyTrainerRemove(){
		$.ajax({
			url:'technologytrainer/cruds/edit/removeList.html',
			type:'get',
			dataType:'html',
			success: function(result){
				$('#list-edit-trainer-update').html(result);
			}
		});
	}
	
	$(document).ready(function(){
		//ADD TECHNOLOGY
		$('#btn-add').on('click',function(){
			$.ajax({
				url:'technology/cruds/add.html',
				type:'get',
				dataType:'html',
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
					loadListTechnologyTrainerRemove();
				}
			});
		});
		
		//EDIT TECHNOLOGY
		$('#list-technology').on('click','#btn-edit',function(){
			var id = $(this).attr('data-value');
			$.ajax({
				url : 'technology/cruds/edit.html',
				type : 'get',
				dataType : 'html',
				data : {id:id},
				success : function(result){
					loadListEditTechnologyTrainerRemove(); 
					$('#modal-edit').find('.modal-body').html(result);
					$('#modal-edit').modal('show');
					loadListEditTechnologyTrainer();
				}
			});
			return false;
		});
		
		//pop up delete technology
		$('#list-technology').on('click','#btn-delete',function(){
			var id = $(this).attr('data-value');
			$.ajax({
				url : 'technology/cruds/delete.html',
				type : 'get',
				dataType : 'html',
				data : {id:id},
				success : function(result){
					$('#modal-delete').find('.modal-body').html(result);
					$('#modal-delete').modal('show');
				}
			});
			return false;
		});
		
		//eksekusi deleete technology
		$('#modal-delete').on('submit','#form-technology-delete',function(){
			var id = $('#id').val();
			$.ajax({
				url : 'technology/cruds/remove.json',
				dataType : 'json',
				data : {id:id},
				success : function(result){
					alert("Data has been deleted");
					$('#modal-delete').modal('hide');
					loadListTechnology();
				}
			});
			return false;
		});
		
		$('#form-technology-search').on('submit',function(){
			var name = $('#keyword').val();
			if (name != ""){
				$.ajax({
					url : 'technology/cruds/search.html',
					dataType : 'html',
					data : {name:name},
					success : function(result){
						alert("your search result");
						$('#list-technology').html(result);
					}
				});
				return false;
			}
			else {
				loadListTechnology();
				return false;
			}
		});	
		
		//JQUERY ADD
		$('#modal-input').on('click','#btn-choose-add',function(){
			$.ajax({
				url:'technology/cruds/trainer/add.html',
				type:'get',
				dataType:'html',
				success: function(result){
					$('#modal-add-choose').find('.modal-body').html(result);
					$('#modal-add-choose').modal('show');	
				}
			});
		});
		
		$('#modal-input, #modal-add-choose, #form-technology-choose-trainer').on('click','#btn-close',function(){
			$('#modal-add-choose').modal('hide');
		});
		
		$('#modal-input, #modal-add-choose').on('submit','#form-technology-choose-trainer',function(){
			var trainer = $('#trainerId').val();
			if(trainer !="-"){
				$.ajax({
					url:'technologytrainer/cruds/add.json',
					type:'get',
					dataType:'json',
					data: $(this).serialize(),
					success: function(result){
						$('#modal-add-choose').modal('hide');  
						alert("Trainer successfully added");
						loadListTechnologyTrainer();
						}
				});
				return false;
			}
			else{
				alert("Please choose trainer to add");
				return false;
			}
		});
		
		$('#modal-input, #form-technology-add').on('click', '#btn-cancel',function(){
			$('#modal-input').modal('hide');
		});
		
		$('#modal-input, #list-add-trainer').on('click','#btn-delete',function(){
			var index = $(this).attr('data-value');
			$.ajax({
				url:'technology/cruds/trainer/deleteAdd.html',
				type:'get',
				dataType:'html',
				data: {index:index},
				success: function(result){
					$('#modal-add-delete').find('.modal-body').html(result);
					$('#modal-add-delete').modal('show');
				}
			});
			return false;
		});
		
		$('#modal-input, #modal-add-delete').on('submit',function(){
			var index = $('#id').val();
			$.ajax({
				url:'technology/cruds/trainer/removeAdd.json',
				type:'get',
				dataType:'json',
				data: {index:index},
				success: function(result){
					$('#modal-add-delete').modal('hide');
					alert("Trainer successfully deleted");
					loadListTechnologyTrainer();
				}
			});
			return false;
		});
		
		$('#modal-input').on('submit','#form-technology-add',function(){
			var name = $('#name').val();
			if(name != ""){
				$.ajax({
					url:'technologytrainer/cruds/createTechnology.json',
					type:'get',
					dataType:'json',
					data: $(this).serialize(),
					success: function(result){
						alert("Data has been saved");
						$('#modal-input').modal('hide');
						loadListTechnology();
					}
				});
				return false;
			}
			else{
				alert("Name field can't be empty");
				return false;
			}
		});
		
		//JQUERY EDIT
		$('#modal-edit').on('click','#btn-choose-add-trainer',function(){
			$.ajax({
				url:'technology/cruds/trainer/edit.html',
				type:'get',
				dataType:'html',
				success: function(result){
					$('#modal-edit-add-trainer').find('.modal-body').html(result);
					$('#modal-edit-add-trainer').modal('show');	
				}
			});
			return false;
		});
		
		//tambah trainer baru ke list
		$('#modal-edit, #modal-edit-add-trainer, #form-technology-choose-edit-trainer').on('submit',function(){
			var id = $('#trainerId').val();
			if(id != "-"){
				$.ajax({
					url:'technologytrainer/cruds/edit/addTrainer.json',
					type:'get',
					dataType:'json',
					data: {id:id},
					success: function(result){
						$('#modal-edit-add-trainer').modal('hide');
						alert("Trainer successfully added");
						loadListEditTechnologyTrainerUpdate();
					}
				});
				return false;
			}
			else{
				alert("Please choose trainer to add");
				return false;
			}
		});
		
		$('#modal-edit, #modal-edit-add-trainer, #form-technology-choose-edit-trainer').on('click','#btn-close-choose-edit',function(){
			$('#modal-edit-add-trainer').modal('hide');
		});
		
		//memunculkan confirm delete trainer lama
		$('#modal-edit, #list-edit-trainer').on('click','#btn-delete-trainer-old',function(){
			var id = $(this).attr('data-value');
			$.ajax({
				url:'technologytrainer/cruds/edit/deleteTrainer.html',
				type:'get',
				dataType:'html',
				data: {id:id},
				success: function(result){
					$('#modal-delete-edit-trainer').find('.modal-body').html(result);
					$('#modal-delete-edit-trainer').modal('show');	
				}
			});
			return false;
		});
		//eksekusi trainer lama
		$('#modal-edit, #modal-delete-edit-trainer').on('submit','#form-technology-delete-edit-trainer',function(){
			var id = $(this).find('#id-technology-trainer').val();
			$.ajax({
				url : 'technologytrainer/cruds/edit/removeTrainer.json',
				type : 'get',
				dataType : 'json',
				data : {id:id},
				success : function(result){
					loadListEditTechnologyTrainer();
					alert("Trainer successfully deleted");
					$('#modal-delete-edit-trainer').modal('hide');
				}
			});
			return false;
		});
		
		//pop up konfirmasi delete trainer baru
		$('#modal-edit, #list-edit-trainer-update').on('click','#btn-delete-trainer-update',function(){
			var id = $(this).attr('data-value');
			$.ajax({
				url:'technologytrainer/cruds/edit/deleteTrainerUpdate.html',
				type:'get',
				dataType:'html',
				data: {id:id},
				success: function(result){
					$('#modal-delete-edit-trainer-update').find('.modal-body').html(result);
					$('#modal-delete-edit-trainer-update').modal('show');	
				}
			});
			return false;
		});
		//eksekusi delete trainer baru
		$('#modal-edit, #modal-delete-edit-trainer-update').on('submit','#form-technology-delete-edit-trainer-update',function(){
			var id = $(this).find('#id-technology-trainer').val();
			$.ajax({
				url : 'technologytrainer/cruds/edit/removeTrainerUpdate.json',
				type : 'get',
				dataType : 'json',
				data : {id:id},
				success : function(result){
					$('#modal-delete-edit-trainer-update').modal('hide');
					loadListEditTechnologyTrainerUpdate();
					alert("Trainer successfully deleted");
				}
			});
			return false;
		});
		
		//SAVE HASIL EDIT
		$('#modal-edit').on('submit','#form-technology-edit',function(){
			var name = $('#name').val();
			if (name != ""){
				$.ajax({
					url : 'technology/cruds/update.json',
					type : 'get',
					dataType : 'json',
					data : $(this).serialize(),
					success : function(result){
						alert("Data has been updated");
						$('#modal-edit').modal('hide');
						loadListTechnology();
					}
				});
				return false;
			}
			else{
				alert("Name field can't be empty");
				return false;
			}
		});
	});
	
</script>