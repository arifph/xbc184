<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal" align="center">
	<form action="#" method="get" id="form-technology-choose-trainer">
		<table>
			<col width="260px">
			<tr>
				<td>
					<select id="trainerId" name="trainerId" style="width:260px;">
							<option value="-">--Choose Trainer--</option>
						<c:forEach items="${trainerModelList}" var="trainerModelList">
							<option value="${trainerModelList.id}">
								${trainerModelList.name}
							</option>
						</c:forEach>
					</select>
				</td>
			</tr>
		</table>
		<br/>
		<table>
			<col width="130px">
			<col width="130px">
			<tr>
				<td align="left">
					<button type="button" class="btn btn-danger" id="btn-close">
						Cancel
					</button>
				</td>
				<td align="right"><button type="submit" class="btn btn-success">Save</button></td>	
			</tr>
		</table>
	</form>
</div>
<script>
/* $('#form-technology-choose-trainer').on('submit',function(){
	$.ajax({
		url:'technologytrainer/cruds/add.json',
		type:'get',
		dataType:'json',
		data: $(this).serialize(),
		success: function(result){
			alert("sudah memilih trainer");
			$('#modal-choose').modal('hide');	
		}
	});
}); */
</script>