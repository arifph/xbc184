<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal" align="center">
	<form action="#" method="get" id="form-technology-edit" >
	
		<input type="hidden" id="id" name="id" value="${technologyModel.id}"/>
		<input type="hidden" id="createdBy" name="createdBy" value="${technologyModel.createdBy}"/>
		<input type="hidden" id="createdOn" name="createdOn" value="${technologyModel.createdOn}"/>
		
		<input type="text" id="name" name="name"
					value="${technologyModel.name}" size="40" placeholder="Name"  
					style="font-style:italic">
		<br/>
		<br/>
		<textarea id="notes" name="notes" 
					style="height: 120px; font-style:italic; width: 68mm;"
					placeholder="Notes">${technologyModel.notes}</textarea>
					
		<br/>
		<br/>
		<table>
			<col width="130px">
			<col width="130px">
			<tr>
				<td></td>
				<td align="right"><button class="btn btn-primary" type="button" id="btn-choose-add-trainer">+ Tambah</button></td>
			</tr>
			<tr>
				<td>Nama</td>
				<td align="right">#</td>
			</tr>
			<tbody id="list-edit-trainer">
				<col width="130px">
				<col width="130px">
			</tbody>
		</table>
		<table>
			<col width="130px">
			<col width="130px">
			<tbody id="list-edit-trainer-update">
				<col width="130px">
				<col width="130px">
			</tbody>
			<tr>
				<td align="left">
					<button type="button" class="btn btn-danger" data-dismiss="modal">
						Cancel
					</button>
				</td>
				<td align="right"><button type="submit" class="btn btn-success">Update</button></td>	
			</tr>
		</table>
	</form>
</div>

<div id="modal-edit-add-trainer" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-green">
				<h2>Choose Trainer</h2>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popup -->
			</div>
		</div>
	</div>
</div>

<div id="modal-delete-edit-trainer" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-green">
				<h2>Delete</h2>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popup -->
			</div>
		</div>
	</div>
</div>

<div id="modal-delete-edit-trainer-update" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-green">
				<h2>Delete</h2>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popup -->
			</div>
		</div>
	</div>
</div>

<script>
/* $(document).ready(function(){ */
	//TOMBOL TAMBAH TRAINER
	/* $('#btn-choose-add-trainer').on('click',function(){
		$.ajax({
			url:'technology/cruds/trainer/edit.html',
			type:'get',
			dataType:'html',
			success: function(result){
				alert("sudah masuk");
				$('#modal-edit-add-trainer').find('.modal-body').html(result);
				$('#modal-edit-add-trainer').modal('show');	
			}
		});
		return false;
	});
	
	//tambah trainer baru ke list
	$('#modal-edit-add-trainer, #form-technology-choose-edit-trainer').on('submit',function(){
		var id = $('#trainerId').val();
		alert(id);
		$.ajax({
			url:'technologytrainer/cruds/edit/addTrainer.json',
			type:'get',
			dataType:'json',
			data: {id:id},
			success: function(result){
				alert("sudah memilih trainer");
				$('#modal-edit-add-trainer').modal('hide');
				loadListEditTechnologyTrainerUpdate();
			}
		});
		return false;
	});
	
	//memunculkan confirm delete trainer lama
	$('#list-edit-trainer').on('click','#btn-delete-trainer-old',function(){
		var id = $(this).attr('data-value');
		alert("masuk delete data lama");
		$.ajax({
			url:'technologytrainer/cruds/edit/deleteTrainer.html',
			type:'get',
			dataType:'html',
			data: {id:id},
			success: function(result){
				alert("sudah masuk");
				$('#modal-delete-edit-trainer').find('.modal-body').html(result);
				$('#modal-delete-edit-trainer').modal('show');	
			}
		});
		return false;
	});
	//eksekusi trainer lama
	$('#modal-delete-edit-trainer').on('submit','#form-technology-delete-edit-trainer',function(){
		var id = $(this).find('#id-technology-trainer').val();
		alert(id);
		$.ajax({
			url : 'technologytrainer/cruds/edit/removeTrainer.json',
			type : 'get',
			dataType : 'json',
			data : {id:id},
			success : function(result){
				loadListEditTechnologyTrainer();
				alert("data anda telah dihapus");
				$('#modal-delete-edit-trainer').modal('hide');
			}
		});
		return false;
	});
	
	//pop up konfirmasi delete trainer baru
	$('#list-edit-trainer-update').on('click','#btn-delete-trainer-update',function(){
		var id = $(this).attr('data-value');
		alert("masuk delete update");
		$.ajax({
			url:'technologytrainer/cruds/edit/deleteTrainerUpdate.html',
			type:'get',
			dataType:'html',
			data: {id:id},
			success: function(result){
				alert("sudah masuk");
				$('#modal-delete-edit-trainer-update').find('.modal-body').html(result);
				$('#modal-delete-edit-trainer-update').modal('show');	
			}
		});
		return false;
	});
	//eksekusi delete trainer baru
	$('#modal-delete-edit-trainer-update').on('submit','#form-technology-delete-edit-trainer-update',function(){
		var id = $(this).find('#id-technology-trainer').val();
		alert(id);
		$.ajax({
			url : 'technologytrainer/cruds/edit/removeTrainerUpdate.json',
			type : 'get',
			dataType : 'json',
			data : {id:id},
			success : function(result){
				alert("data anda telah dihapus");
				loadListEditTechnologyTrainerUpdate();
				$('#modal-delete-edit-trainer-update').modal('hide');
			}
		});
		return false;
	}); */
/* } */
</script>