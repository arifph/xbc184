<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal" align="center">
	<h3>Are you sure delete this data ?</h3>
	<form action="#" method="get" id="form-technology-delete-edit-trainer">
		<input type="hidden" id="id-technology-trainer" name="id-technology-trainer" value="${idTechnologyTrainer}"/>
		<br/>
		<table>
			<col width="130px">
			<col width="130px">
			<tr>
				<td align="left">
					<button type="button" class="btn btn-danger" data-dismiss="modal">
						No
					</button>
				</td>
				<td align="right"><button type="submit" class="btn btn-success" data-value="${idTechnologyTrainer}">Yes</button></td>	
			</tr>
		</table>
	</form>
</div>