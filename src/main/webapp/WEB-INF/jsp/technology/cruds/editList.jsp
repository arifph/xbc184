<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:forEach items="${technologyTrainerModelList}"
	var="trainerModelListSukaSuka" varStatus="number">
	<tr>
		<td>${trainerModelListSukaSuka.trainerModel.name}</td>
		<td>
			<div class="box-tools pull-right">
				<div class="btn-group">
					<button type="button" class="btn btn-box-tool dropdown-toggle"
						data-toggle="dropdown">
						<i class="fa  fa-list"></i>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#" id="btn-delete-trainer-old"
							data-value="${trainerModelListSukaSuka.id}">Delete</a></li>
					</ul>
				</div>
			</div>
		</td>
	</tr>
</c:forEach>