<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal" align="center">
	<form action="#" method="get" id="form-technology-add" >
		<table>
			<col width="260px">
			<tr>
				<td><input type="text" id="name" name="name" 
					size="40" placeholder="Name"  
					style="font-style:italic">
				</td>
			</tr>
			<tr>
				<td><br/></td>
			</tr>
			<tr>
				<td><textarea id="notes" name="notes" 
					style="height: 130px; font-style:italic; width:68mm;"
					placeholder="Notes"></textarea>
				</td>
			</tr>
			<tr>
				<td><br/></td>
			</tr>
		</table>
		<table>
			<col width="130px">
			<col width="130px">
			<tr>
				<td></td>
				<td align="right"><button class="btn btn-primary" type="button" id="btn-choose-add">+ Tambah</button></td>
			</tr>
			<tr>
				<td><br/></td>
				<td><br/></td>
			</tr>
			<tr>
				<td align="left">Nama</td>
				<td align="right">#</td>
			</tr>
			<tbody id="list-add-trainer">
				<col width="130">
				<col width="130">
			</tbody>
			<tr>
				<td align="left">
					<button type="button" class="btn btn-danger" id="btn-cancel">
						Cancel
					</button>
				</td>
				<td align="right"><button type="submit" class="btn btn-success" id="btn-add">Create<br/></button></td>	
			</tr>
		</table>
	</form>
</div>

<div id="modal-add-choose" class="modal" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-green">
				<h2>Choose Trainer</h2>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popup -->
			</div>
		</div>
	</div>
</div>

<div id="modal-add-delete" class="modal" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-green">
				<h2>Choose Trainer</h2>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popup -->
			</div>
		</div>
	</div>
</div>

<script>
/* $(document).ready(function(){ */
		/* $('#btn-choose-add').on('click',function(){
			$.ajax({
				url:'technology/cruds/trainer/add.html',
				type:'get',
				dataType:'html',
				success: function(result){
					alert("sudah masuk");
					$('#modal-add-choose').find('.modal-body').html(result);
					$('#modal-add-choose').modal('show');	
				}
			});
		});
		
		$('#modal-add-choose , #form-technology-choose-trainer').on('click','#btn-close',function(){
			$('#modal-add-choose').modal('hide');
		});
		
		$('#modal-add-choose').on('submit','#form-technology-choose-trainer',function(){
			$.ajax({
				url:'technologytrainer/cruds/add.json',
				type:'get',
				dataType:'json',
				data: $(this).serialize(),
				success: function(result){
					alert("sudah memilih trainer");
					$('#modal-add-choose').modal('hide');
					loadListTechnologyTrainer();
				}
			});
			return false;
		});
		
		$('#form-technology-add').on('click', '#btn-cancel',function(){
			$('#modal-input').modal('hide');
		});
		
		$('#list-add-trainer').on('click','#btn-delete',function(){
			var index = $(this).attr('data-value');
			alert(index);
			$.ajax({
				url:'technology/cruds/trainer/deleteAdd.html',
				type:'get',
				dataType:'html',
				data: {index:index},
				success: function(result){
					alert("sudah menghapus trainer");
					$('#modal-add-delete').find('.modal-body').html(result);
					$('#modal-add-delete').modal('show');
				}
			});
			return false;
		});
		
		$('#modal-add-delete').on('submit',function(){
			var index = $('#id').val();
			alert(index);
			$.ajax({
				url:'technology/cruds/trainer/removeAdd.json',
				type:'get',
				dataType:'json',
				data: {index:index},
				success: function(result){
					alert("sudah menghapus trainer");
					loadListTechnologyTrainer();
					$('#modal-add-delete').modal('hide');
				}
			});
			return false;
		});
		
		$('#modal-input').on('submit','#form-technology-add',function(){
			$.ajax({
				url:'technologytrainer/cruds/createTechnology.json',
				type:'get',
				dataType:'json',
				data: $(this).serialize(),
				success: function(result){
					alert("data sudah masuk technology");
					$('#modal-input').modal('hide');
					loadListTechnology();
				}
			});
			return false;
		}); */
/* }); */
</script>