<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<h1>Ubah Jurusan</h1>
	
	<form action="#" method="get" id="form-jurusan-delete">
		<input type="hidden" id="idJurusan" name="idJurusan" value ="${jurusanModel.idJurusan}">
		<table>
			<tr>
				<td>Kode Jurusan</td>
				<td><input type="text" id="kodeJurusan" name="kodeJurusan"  value="${jurusanModel.kodeJurusan}"> </td>
			</tr>
			
			<tr>
				<td>Nama Jurusan</td>
				<td><input type="text" id="namaJurusan" name="namaJurusan" value="${jurusanModel.namaJurusan}"> </td>
			</tr>
			<tr>
				<td>Lokasi Fakultas</td>
				<td>
					<select id="idLokasi" name="idLokasi">
						<c:forEach items="${lokasiModelList}" var="lokasiModel">
							<option value="${lokasiModel.idLokasi}" ${lokasiModel.idLokasi==jurusanModel.idLokasi ? 'selected="true"':''}>
								${lokasiModel.namaLokasi}
							</option>
						</c:forEach>						
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td><button type="submit">Delete</button></td>
			</tr>
		</table>
	
	</form>
</div>