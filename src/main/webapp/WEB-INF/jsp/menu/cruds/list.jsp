<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${menuModelList}" var="menuModel" varStatus="number">
	<tr>
		<td>${menuModel.code}</td>
		<td>${menuModel.title}</td>
		<td>${menuModel.menuParentBy.title}</td>
		<td>
        	<div class="btn-group-vertical">

                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-fw fa-database"></i>
                  </button>
                 	<ul class="dropdown-menu" role="menu">
                    	<li><a id="btnEdit" data-value="${menuModel.id}" >Edit</a></li>
                        <li><a id="btnDelete" data-value="${menuModel.id}" >Delete</a></li>
                    </ul>
                 </div>
             </div>
                  
			<%-- <button type="button" class="btn btn-warning" value="${jurusanModel.idJurusan}" id="btn-detail">Lihat</button>
			<button type="button" class="btn btn-success" value="${jurusanModel.idJurusan}" id="btn-edit">Ubah</button>
			<button type="button" class="btn btn-danger" value="${jurusanModel.idJurusan}" id="btn-delete">Hapus</button> --%>
		</td>
	</tr>
</c:forEach>