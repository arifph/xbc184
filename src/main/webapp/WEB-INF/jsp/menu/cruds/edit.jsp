<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<i class="fa fa-close"></i>
	</button>
	<h3>EDIT MENU</h3><br/>
</div>
	
	<form action="#" method="get" id="form-menu-edit"> 		
		<div class="box-body">
			<div class="col-md-6">
			<div class="form-group">
                <input type="text" class="form-control" placeholder="Kode Menu" name="code" value="${menuModel.code}" disabled="disabled">
                 <input type=hidden class="form-control" name="id" value="${menuModel.id}">
			</div>
        	<div class="form-group">
                <input type="text" class="form-control" placeholder="Title" name="title" required="required" value="${menuModel.title}">
			</div>
			<div class="form-group">
                <textarea rows="4" cols="5" style="width:100%; height: 100%" placeholder="Description" name="description">${menuModel.description}</textarea>
			</div>
			</div>
			<div class="col-md-6">
			<div class="form-group">
                <input type="text" class="form-control" placeholder="Image URL" name="imageUrl" id="imageUrl" value="${menuModel.imageUrl}">
			</div>
			<div class="form-group">
                <input type="text" class="form-control" placeholder="Menu Order" name="menuOrder" id="menuOrder" value="${menuModel.menuOrder}">
			</div>
			<div class="form-group">       
            	<select class="form-control" id="menuParent" name="menuParent" >
            		<option>--Choose Menu Parent--</option>  
            		<c:forEach items="${menuModelList}" var="menuModelCek">
                    	
                   		<option value="${menuModelCek.id}" ${menuModelCek.id == menuModel.menuParent ? 'selected="true"':'' } >${menuModelCek.title}</option>
                    </c:forEach>
                   
                 </select>
            </div>
			<div class="form-group">
                <input type="text" class="form-control" placeholder="Menu URL" name="menuUrl" id="menuUrl" value="${menuModel.menuUrl}">
			</div>
			
			<div class="box-footer">
                <button type="reset" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" class="btn btn-info pull-right">SAVE</button>
            </div>
		</div>
		</div>
		
	
	</form>

<!--  <script type="text/javascript">
function ceksama(){
	var password = document.getElementById("password");
	var repassword = document.getElementById("repassword");
	var divRepassword = document.getElementById("divRepassword");
	var spanRepassword = document.getElementById("spanRepassword");
	
	 if(password.value != repassword.value) {
		alert('Password beda');
		
	} 
	 return false;
	
	
}
</script>  -->