<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<i class="fa fa-close"></i>
	</button>
	<h3>RESET PASSWORD</h3><br/>
</div>
<div class="form-horizontal">
	
	
	<form action="#" method="get" id="form-reset-password"> 		
		<div class="box-body">
			
        	<div class="form-group">
                <input type="password" class="form-control" placeholder="Password" name="password" id="password" required pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$">
                <input type="hidden" class="form-control" placeholder="Email" name="id" value="${userModel.id}" >
			</div>
			<div class="form-group">
                <input type="password" class="form-control" placeholder="Re-type Password" name="repassword" id="val_confirm_password" required="required"/>
			</div>			
			
			<div class="box-footer">
                <button type="reset" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                <button type="submit" class="btn btn-info pull-right">SAVE</button>
            </div>
		</div>
		
	
	</form>
</div>
 <!-- <script>
            $(function () {
                /*  For advanced usage and examples please check out
                 *  Jquery Validation   -> https://github.com/jzaefferer/jquery-validation
                 */

                /* Initialize Form Validation */
                $('#form-reset-password').validate({
                    errorClass: 'help-block', // You can add help-inline instead of help-block if you like validation messages to the right of the inputs
                    errorElement: 'div',
                    errorPlacement: function (error, e) {
                        e.parents('.form-group > div').append(error);
                    },
                    highlight: function (e) {
                        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                        $(e).closest('.help-block').remove();
                    },
                    success: function (e) {
                        // You can remove the .addClass('has-success') part if you don't want the inputs to get green after success!
                        e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        e.closest('.help-block').remove();
                    },
                    rules: {
                        val_password: {
                            required: true,
                            minlength: 5
                        },
                        val_confirm_password: {
                            required: true,
                            minlength: 5,
                            equalTo: '#val_password'
                        }
                    },
                    messages: { 
                        val_password: {
                            required: 'Please provide a password',
                            minlength: 'Your password must be at least 5 characters long'
                        },
                        val_confirm_password: {
                            required: 'Please provide a password',
                            minlength: 'Your password must be at least 5 characters long',
                            equalTo: 'Please enter the same password as above'
                        }
                    }
                });
            });
        </script>
 -->