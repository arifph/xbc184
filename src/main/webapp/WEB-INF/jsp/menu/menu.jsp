<div class="panel" style="background: white; margin-top: 50px; min-height: 500px">
	<h1>MENU</h1>
	
	
	<div>
		<table class="table" id="tbl-menu">
			<tr>
				<td colspan="2"><form action="#" method="get" id="form-search">
						<input type="text" id="tittleSearch" name="tittleSearch" size="27" placeholder="Search by title">
						<button type="submit" class="btn btn-primary">Cari</button>
					</form>
				</td>
			
				<td></td>
				<td><button type="button" class="btn btn-primary" id="btn-add">Tambah</button>
				</td>
			</tr>
			<tr>
				<td><b>CODE</b></td>
				<td><b>TITLE</b></td>
				<td><b>MENU PARENT</b></td>
				<td><b>#</b></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tbody id="list-menu">
				<!-- isi Listnya -->
			</tbody>
		</table>
	</div>
	
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form PopUp User</h4>
			</div> -->
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script>

	loadListMenu(); /* untuk manggil otomatis krn tidak ada ready */
	
	function loadListMenu(){
		$.ajax({
			url:'menu/cruds/list.html',
			type:'get',
			dataType:'html',
			success: function(result){
				$('#list-menu').html(result);
			}
		});
	}
		
	//document ready itu setelah halaman dipanggil
	$(document).ready(function(){
		
		//fungsi ajax untuk popup tambah
		$('#btn-add').on('click',function(){
			$.ajax({
				url:'menu/cruds/add.html',
				type:'get',
				dataType:'html',
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup tambah
		
		
		//fungsi ajax untuk create tambah
		$('#modal-input').on('submit','#form-menu-add',function(){
			$.ajax({
				url:'menu/cruds/create.json',
				type:'get',
				dataType:'json',
				data: $(this).serialize(),
				success: function(result){
						$('#modal-input').modal('hide');
						alert("Data successfully saved");
						loadListMenu();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk create tambah
		
		//fungsi ajax untuk view (EDIT)
		$('#list-menu').on('click','#btnEdit',function(){
			var id = $(this).attr('data-value');
			console.log("id : " + id);
			$.ajax({
				url:'menu/cruds/edit.html',
				type:'get',
				dataType:'html',
				data:{id : id},
				success: function(result){ 
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk ubah
		
		//fungsi ajax untuk update
		$('#modal-input').on('submit','#form-menu-edit',function(){
			
			$.ajax({
				url:'menu/cruds/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data Successfully updated ! ");
					loadListMenu();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update
		
		
		//fungsi ajax untuk hapus
		$('#list-menu').on('click','#btnDelete',function(){
			 var cmt = confirm('Are you sure to delete this data?');
			 if (cmt == true) {
				var id = $(this).attr('data-value');
					console.log("id : " + id);
     			$.ajax({
     				url:'menu/cruds/delete.json',
     				type:'get',
     				dataType:'json',
     				data:{id : id},
     				success: function(result){
     					$('#modal-input').modal('hide');
    					alert("Data successfully deleted !");
    					loadListMenu();
     				}
     			});
             }
             else {
                
                 return false;
             }
			
		});
		//akhir fungsi ajax untuk hapus
		
		/* //fungsi ajax untuk delete
		$('#modal-input').on('submit','#form-jurusan-delete',function(){
			
			$.ajax({
				url:'jurusan/cruds/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah dihapus");
					loadListJurusan();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete
		
		//fungsi ajax untuk search namaJurusan
		$('#form-jurusan-search-nama').on('submit',function(){
			$.ajax({
				url:'jurusan/cruds/search/nama.html',
				type:'get',
				dataType:'html',
				data: $(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan nama");
					$('#list-jurusan').html(result); //reload list jurusan
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search namaJurusan */
		
		//fungsi ajax untuk search Username dan password
		$('#form-search').on('submit',function(){
			$.ajax({
				url:'menu/cruds/search/search.html',
				type:'get',
				dataType:'html',
				data: $(this).serialize(),
				success: function(result){
					
					$('#list-menu').html(result); //reload list jurusan
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search kodeJurusan
		
		/* //fungsi ajax untuk search kodeJurusan /namaJurusan
		$('#form-jurusan-search').on('submit',function(){
			$.ajax({
				url:'jurusan/cruds/search.html',
				type:'get',
				dataType:'html',
				data: $(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan kode/ nama jurusan");
					$('#list-jurusan').html(result); //reload list jurusan
				}
			});
			return false;
		}); */
		//akhir fungsi ajax untuk search kodeJurusan/namaJurusan
		
	});
	//akhir dokumen ready
</script>