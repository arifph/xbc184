<div class="panel" style="background : white; margin-top: 50px; min-height: 500px;">
	<div>
		<table class="table" id="tbl-header-bootcampTestType">
			<tr bgcolor="orange">
				<td><h3><b><font color="white">BOOTCAMP TEST TYPE</font></b></h3></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
		<table class="table" id ="tbl-bootcampTestType" style="width: 70%; margin-left: 15%; margin-right: 15%;">
			<tr>
				<td>
					<form action="#" method="get" id="form-bootcampTestType-search-name">
						<input placeholder="Search by name" type="text" id="nameBootcampTestTypeKey" name="nameBootcampTestTypeKey" size="20">
						<button class="btn btn-warning" type="submit">O</button>
					</form>
				</td>
				<td></td>
				<td >
					<button type="button" class="btn btn-warning" id="btn-add" >+</button>
				</td>
			</tr>
			<tr bgcolor="orange">
				<td>NAME</td>
				<td>CREATED BY</td>
				<td>#</td>
			</tr>
			<tbody id="list-bootcampTestType">
			<!-- isi listya -->
			</tbody>
		</table>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script>

	loadListBootcampTestType();
	
	function loadListBootcampTestType() {
		$.ajax({
			url: 'bootcampTestType/cruds/list.html',
			type: 'get',
			dataType:'html',
			success: function(result) {
				$('#list-bootcampTestType').html(result);
			}
		});
	}
	//document ready itu setelah halaman dipanggil
	$(document).ready(function(){
		
		//fungsi ajax untuk pop up tambah
		$('#btn-add').on('click', function(){
			$.ajax({
				url:'bootcampTestType/cruds/add.html',
				type:'get',
				dataType:'html',
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		}); // akhir fungsi ajax untuk popup tambah
		
		//fungsi ajax untuk tambah
		$('#modal-input').on('submit', '#form-bootcampTestType-add', function(){
			$.ajax({
				url:'bootcampTestType/cruds/create.json',
				type:'get',
				dataType:'json',
				data: $(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully saved");
					loadListBootcampTestType();
				}
			});
			return false;
		}); // akhir fungsi ajax  tambah
		
		//fungsi ajax untuk ubah
		$('#list-bootcampTestType').on('click','#btn-edit',function(){
			var idBootcampTestType = $(this).attr("data-value");
			/* console.log("idbootcampTestType: " + idbootcampTestType) */
			$.ajax({
				url:'bootcampTestType/cruds/edit.html',
				type:'get',
				dataType:'html',
				data:{idBootcampTestType : idBootcampTestType},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk ubah
		
		//fungsi ajax untuk update
		$('#modal-input').on('submit','#form-bootcampTestType-edit',function(){
			$.ajax({
				url:'bootcampTestType/cruds/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully updated");
					loadListBootcampTestType();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update
	
		//fungsi search nama Bootcamp Type
		$('#form-bootcampTestType-search-name').on('submit',function(){
			$.ajax({
				url:'bootcampTestType/cruds/search/name.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("Data successfully submitted");
					$('#list-bootcampTestType').html(result);
				}
			});
			return false;
		});
		//akhir fungsi search nama Bootcamp Type
		
		//fungsi ajax untuk hapus
		$('#list-bootcampTestType').on('click','#btn-delete',function(){
			var idBootcampTestType = $(this).attr("data-value");
			/* var idbootcampTestType = $(this).val(); */
			$.ajax({
				url:'bootcampTestType/cruds/remove.html',
				type:'get',
				dataType:'html',
				data:{idBootcampTestType:idBootcampTestType},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk hapus
		
		
		//fungsi ajax untuk delete
		$('#modal-input').on('submit','#form-bootcampTestType-delete',function(){
			$.ajax({
				url:'bootcampTestType/cruds/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully deleted");
					loadListBootcampTestType();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete
		
	}); 
	//akhir dokumen ready
</script>