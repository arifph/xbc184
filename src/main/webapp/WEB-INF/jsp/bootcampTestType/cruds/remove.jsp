<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div>
	<div>
		<table class="table" id="tbl-header-bootcampTestType">
			<tr bgcolor="orange">
				<td><h3><b><font color="white">DELETE</font></b></h3></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
	
	<form action="#" method="get" id="form-bootcampTestType-delete">
		<input type="hidden" id="idBootcampTestType" name="idBootcampTestType" value="${bootcampTestTypeModel.idBootcampTestType}" />
		<div style="margin: auto; width: 60%;">
			<h3><b>Are you sure delete this data ?</b></h3>
		</div>
		<button type="button" class="btn btn-danger" data-dismiss="modal" style="float: left; margin-left: 20%;">NO</button>
		<button type="submit" class="btn btn-success" style="float: right; margin-right: 20%;">YES</button>
	</form>
	<hr>
	<hr>
</div>