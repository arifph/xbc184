<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${bootcampTestTypeModelList}" var="bootcampTestTypeModel">
	<tr>
		<td>${bootcampTestTypeModel.nameBootcampTestType}</td>
		<td>${bootcampTestTypeModel.xCreatedBy.username}</td>
		<td>
			<div class="dropdown" align="right">
				<div class="button-group" align="left">
					<button type="button" class="btn-box-tool dropdown-toggler" data-toggle= "dropdown">
						<i class="fa fa-navicon"></i>
					</button>
					<ul id="menu" class="dropdown-menu" role="menu">
						<li><a id="btn-edit" data-value="${bootcampTestTypeModel.idBootcampTestType}">Edit</a></li>
						<li><a id="btn-delete" data-value="${bootcampTestTypeModel.idBootcampTestType}">Delete</a></li>
						
					</ul>
				</div>
			</div>
		</td>
	</tr>
	
</c:forEach>