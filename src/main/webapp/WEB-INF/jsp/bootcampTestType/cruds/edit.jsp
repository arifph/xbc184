<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<div>
		<table class="table" id="tbl-header-bootcampTestType">
			<tr bgcolor="orange">
				<td><h3><b><font color="white">EDIT BOOTCAMP TEST TYPE</font></b></h3></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
	
	<form action="#" method="get" id="form-bootcampTestType-edit">
	<input type="hidden" id="idBootcampTestType" name="idBootcampTestType" value ="${bootcampTestTypeModel.idBootcampTestType}"/>
		<table style="width: 80%; margin-left: 10%; margin-right: 10%; border-collapse: separate; border-spacing: 15px; ">
			<tr>
				<td><input type="text" id="nameBootcampTestType" name="nameBootcampTestType"  size="63%" value="${bootcampTestTypeModel.nameBootcampTestType}"> </td>
			</tr>
			<tr>
				<td>
					<textarea  rows="3" cols="65%" 
					 id="notesBootcampTestType" name="notesBootcampTestType" maxlength="250px">${bootcampTestTypeModel.notesBootcampTestType}
					</textarea>
				</td>
			</tr>
		</table>
	
		<button type="submit" style="float: right; margin-right: 10%; margin-bottom: 15%;">SAVE</button>
		<button type="button" data-dismiss="modal" style="float: right; margin-right: 5%; margin-bottom: 15%;">CANCEL</button>
	
	</form>
	
	<hr>
</div>