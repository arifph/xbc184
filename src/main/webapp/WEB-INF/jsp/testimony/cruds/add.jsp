<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<form action="#" method="get" id="form-addTestimony">
		<table class="table fixed">

			<!-- 		name untuk controller -->
			<tr>
				<td style="width: 100%"><input type="text" id="title" name="title"
					placeholder="Title" class="form-control"/></td>
			</tr>
						
			<tr>
				<td>
					<textarea class="form-control" rows="6" id="content" name="content" placeholder="Content" style="resize: none"></textarea>
				</td>
			</tr>
			
			
			 <tr>
				<td><button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button></td>
						
				<td><button type="submit" class="btn btn-primary">ADD</button></td>
			</tr>
		</table>
	</form>
</div>