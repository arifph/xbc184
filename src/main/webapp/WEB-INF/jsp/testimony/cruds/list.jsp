<%@ taglib uri = "http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<table class="table">
<c:forEach items="${testimonyModelList}" var="testimonyModel" varStatus="number">
	<tr>
		<td>${testimonyModel.title}</td>
		<td>
		<div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-list"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#" data-value="${testimonyModel.id}" id="btn-edit">Edit</a></li>
                    <li><a href="#" data-value="${testimonyModel.id}" id="btn-delete">Delete</a></li>
                  </ul>
                </div>
		</td>
	</tr>
</c:forEach>
</table>