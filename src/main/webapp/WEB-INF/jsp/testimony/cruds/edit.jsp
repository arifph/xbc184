<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<form action="#" method="get" id="form-editTestimony">
		<input type="hidden" id="id" name="id" value="${testimonyModel.id}"/>
		<table class="table fixed">

			<!-- 		name untuk controller -->
			<tr>
				<td style="width: 100%"><input type="text" id="title" name="title"
					value="${testimonyModel.title}" class="form-control"/></td>
			</tr>
						
			<tr>
				<td>
					<textarea class="form-control" rows="6" id="content" name="content" style="resize: none">${testimonyModel.content}</textarea>
				</td>
			</tr>
			
			
			 <tr>
				<td><button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button></td>
						
				<td><button type="submit" class="btn btn-primary">Save</button></td>
			</tr>
		</table>
	</form>
</div>