<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>TESTIMONY</h1>
	<div>
		<table class="table" id="tbl-testimony">

			<tr>	
				<td>
					<form action="#" method="get" id="form-testimony-search-title">
						<div class ="row">
							<div class="col-xs-4">
								<div class="input-group">
								<input type="text" id="titleKey" name="titleKey" class="form-control" placeholder="Search by title"/>
								<span class="input-group-btn">
								<button type="submit" class="btn btn-flat btn btn-primary" >
									<i class="fa fa-search"></i>
								</button>
								</span>
								</div>
							</div>
						</div>
					</form>
				<td>
					<button type="button" class="btn btn-primary" id="btn-add"><b>+</b></button>	
				</td>
				
			</tr>

			<tr>

			</tr>
			
			<tr>
				<td>TITILE</td>
				<td>#</td>
			</tr>

			<tbody id="list-testimony">

			</tbody>
		</table>
	</div>
</div>


<!-- Modal untuk add Testimony -->
<div id="modal-testimony" class="modal fade" tabindex="-1" data-focus-on="input:first" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-light-blue">
				<h4><b>ADD TESTIMONY</b></h4>
			</div>
			<div class="modal-body-testimony">
				tempat jsp popupnya
							
			</div>
			<div class="modal-footer">
  			</div>
		</div>
	</div>
</div>

<div id="modal-delete" class="modal fade" tabindex="-1" data-focus-on="input:first" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-light-blue">
				<h4><b>ADD TESTIMONY</b></h4>
			</div>
			<div class="modal-body-delete">
				tempat jsp popupnya
							
			</div>
			<div class="modal-footer">
  			</div>
		</div>
	</div>
</div>





<script>

loadListTestimony();

function loadListTestimony() {
	$.ajax({
		url : 'testimony/cruds/list.html',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#list-testimony').html(result);
		}
	});
}


$(document).ready(
		function() {
			//fungsi ajax untuk memunculkan popup tambah testimony
			$('#btn-add').on('click', function() {
				$.ajax({
					url : 'testimony/cruds/add.html',
					type : 'get',
					dataType : 'html',
					success : function(result) {
						$('#modal-testimony').find('.modal-body-testimony').html(result);
						$('#modal-testimony').modal('show');

					}
				});
			});

			//fungsi ajax untuk menyimpan popup tambah testimony
			$('#modal-testimony').on('submit','#form-addTestimony',function() {
							$.ajax({
								url : 'testimony/cruds/create.json',
								type : 'get',
								dataType : 'json',
								data : $(this).serialize(),
								success : function(result) {
										$('#modal-testimony').modal('hide');
										alert("data telah ditambah");
										loadListTestimony();
								}
							});
							return false; //supaya tidak merefresh ulang semua data, jadi data ditambah saja
						});			
		
			//edit popup btn
			$('#list-testimony').on('click', '#btn-edit', function() {
				var id = $(this).attr('data-value');
				$.ajax({
					url : 'testimony/cruds/edit.html',
					type : 'get',
					dataType : 'html',
					data : {
						id : id
					},
					success : function(result) {
						$('#modal-testimony').find('.modal-body-testimony').html(result);
						$('#modal-testimony').modal('show');
					}
				});
			});
		
		
			//fungsi ajax untuk update di dalam popup update
			$('#modal-testimony').on('submit', '#form-editTestimony',
					function() {
						$.ajax({
							url : 'testimony/cruds/update.json',
							type : 'get',
							dataType : 'json',
							data : $(this).serialize(),
							success : function(result) {
								$('#modal-testimony').modal('hide');
								alert("data telah diubah");
								loadListTestimony();
							}
						});
						return false;
					});
		
			//btn hapus
			$('#list-testimony').on('click', '#btn-delete', function() {
				var id = $(this).attr('data-value');
				$.ajax({
					url : 'testimony/cruds/remove.html',
					type : 'get',
					dataType : 'html',
					data : {
						id : id
					},
					success : function(result) {
						$('#modal-delete').find('.modal-body-delete').html(result);
						$('#modal-delete').modal('show');
					}
				});
			});
			
			$('#modal-delete').on('submit', '#form-deleteTestimony',
					function() {
						$.ajax({
							url : 'testimony/cruds/delete.json',
							type : 'get',
							dataType : 'json',
							data : $(this).serialize(),// ambil data dari backend secara kesuluruhan
							success : function(result) {
								$('#modal-delete').modal('hide');
								alert("data telah dihapus");
								loadListTestimony();
							}
						});
						return false;
					});
		
		
			//search nama testimony
			$('#form-testimony-search-title').on('submit', function() {
				$.ajax({
					url : 'testimony/cruds/search/name.html',
					type : 'get',
					dataType : 'html',
					data : $(this).serialize(),
					success : function(result) {
						alert("data telah dicari berdasarkan title!");
						$('#list-testimony').html(result);
					}
				});
				return false;
			});			
			
		
		
		});

			
			
</script>