<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="form-horizontal">
	<!-- <form action="#" method="get" id="form-batch-setup-test"> -->
	
		<div class="box-body">
			<c:forEach items="${testModelList}" var="testModel" varStatus="number">
               	<c:set var="btn" value="btn${testModel.id}"></c:set>
               	<c:set var="pilihan" value="pilihan${testModel.id}"></c:set>
               	
               	<c:set var="pilih" value="false"></c:set>
				<c:set var="teksTombol" value="Choose"></c:set>
				<c:set var="classTombol" value="btn btn-info btn-flat"></c:set>
               	
               	<!-- Mengecek apakah test sudah pernah dipilih untuk suatu batch -->
               	<<c:forEach items="${listOfChosenTest}" var="chosenTest" varStatus="number">
	               	<c:if test="${chosenTest.testId eq testModel.id}">
               			<c:set var="pilih" value="true"></c:set>
               			<c:set var="teksTombol" value="CANCEL"></c:set>
               			<c:set var="classTombol" value="btn btn-warning btn-flat"></c:set>
               		</c:if>
               	</c:forEach>
               	
				<form action="#" method="get" id="form-batch-setup-test">
					<input type="hidden" id="batchId" name="batchId" value="${batchModel.id}" />
					<input type="hidden" id="testId" name="testId" value="${testModel.id}" />
					<div class="box-body">
						<div class="row">
							<div class="col-xs-11">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" value="${testModel.name}" disabled />
								<input type="hidden" id="${pilihan}" value="${pilih}" />
								<%-- <input type="text" id="${pilihan}" class="form-control" value="${pilih} ${pilihan}" /> --%>
								
				                <span class="input-group-btn">
				                	<%-- <input onclick="changeChoice(${testModel.id})" type="submit" class="btn btn-info btn-flat" id="${btn}" value="Choose" /> --%>
				                	
				                	<input onclick="changeChoice(${testModel.id})" type="submit" class="${classTombol}" id="${btn}" value="${teksTombol}" />
				               	</span>
				            </div>
							</div>
						</div>
					</div>
            	</form>
			</c:forEach>
		</div>
		<div class="box-body">
		    <div class="row pull-right">
				<button type="button" class="btn btn-primary" data-dismiss="modal">CLOSE</button>
		    </div>
	    </div>
	<!-- </form> -->
</div>