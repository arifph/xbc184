<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<!-- <h1>Tambah Batch</h1> -->
	<form action="#" method="get" id="form-batch-edit">
	<input type="hidden" id="id" name="id" value="${batchModel.id}" />
		<div class="box-body">
		    <div class="row">
		    	<div class="col-xs-6">
			    	<input class="form-control" type="text" id="name" name="name" placeholder="Name" value="${batchModel.name}"/>
			    </div>
			    <div class="col-xs-6">
			    	<select class="form-control" id="roomId" name="roomId">
						<option value="">- Choose Room -</option>
						<c:forEach items="${roomModelList}" var="roomModel">
							<option value="${roomModel.id}" 
							${batchModel.roomId == roomModel.id ? 'selected="true"' : ''}>
								${roomModel.name}
							</option>
						</c:forEach>
					</select>
			    </div>
		    </div>
		    <br/>
		    <div class="row">
		    	<div class="col-xs-6">
			    	<select class="form-control" id="technologyId" name="technologyId">
						<option value="">- Choose Technology -</option>
						<c:forEach items="${technologyModelList}" var="technologyModel">
							<option value="${technologyModel.id}" 
							${batchModel.technologyId == technologyModel.id ? 'selected="true"' : ''}>
								${technologyModel.name}
							</option>
						</c:forEach>
					</select>
			    </div>
			    <div class="col-xs-6">
			    	<select class="form-control" id="trainerId" name="trainerId">
						<option value="">- Choose Trainer -</option>
						<c:forEach items="${trainerModelList}" var="trainerModel">
							<option value="${trainerModel.id}" 
							${batchModel.trainerId == trainerModel.id ? 'selected="true"' : ''}>
								${trainerModel.name}
							</option>
						</c:forEach>
					</select>
			    </div>
		    </div>
		    <br/>
		    <div class="row">
		    	<div class="col-xs-6">
			    	<div class="input-group date">
	                  <div class="input-group-addon">
	                    <i class="fa fa-calendar"></i>
	                  </div>
	                  <input type="text" class="form-control pull-right" id="periodTo" name="periodTo" placeholder="Period To" onfocus="(this.type='date')" value="${batchModel.periodTo}">
	                </div>
			    </div>
			    <div class="col-xs-6">
			    	<div class="input-group date">
	                  <div class="input-group-addon">
	                    <i class="fa fa-calendar"></i>
	                  </div>
	                  <input type="text" class="form-control pull-right" id="periodFrom" name="periodFrom" placeholder="Period From" onfocus="(this.type='date')" value="${batchModel.periodFrom}">
	                </div>
			    </div>
		    </div>
		    <br/>
		    <div class="row">
		    	<div class="col-xs-6">
			    	<select class="form-control" id="bootcampTypeId" name="bootcampTypeId">
						<option value="">- Choose Bootcamp Type -</option>
						<c:forEach items="${bootcampTypeModelList}" var="bootcampTypeModel">
							<option value="${bootcampTypeModel.id}" 
							${batchModel.bootcampTypeId == bootcampTypeModel.id ? 'selected="true"' : ''}>
								${bootcampTypeModel.name}
							</option>
						</c:forEach>
					</select>
			    </div>
			    <div class="col-xs-6">
			    	<textarea class="form-control" rows="3" id="notes" name="notes" placeholder="Notes">${batchModel.notes}</textarea>
			    </div>
		    </div>
	    </div>
		
		<div class="box-body">
		    <div class="row pull-right">
			    <button type="reset" class="btn btn-default" data-dismiss="modal">
					CANCEL
				</button>
				<button type="submit" class="btn btn-primary">SAVE</button>
		    </div>
	    </div>
	</form>
</div>