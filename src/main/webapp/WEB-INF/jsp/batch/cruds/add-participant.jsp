<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<!-- <h1>Tambah Batch</h1> -->
	<form action="#" method="get" id="form-batch-add-participant">
	<input type="hidden" id="batchId" name="batchId" value="${batchModel.id}" />
		<div class="box-body">
		    <div class="row">
			    <div class="col-xs-10">
			    	<select class="form-control" id="biodataId" name="biodataId">
						<option value="">- Choose Biodata Name -</option>
						<c:forEach items="${biodataModelList}" var="biodataModel">
							<option value="${biodataModel.id}">
								${biodataModel.name}
							</option>
						</c:forEach>
					</select>
			    </div>
		    </div>
		    <br/>
		</div>
		<div class="box-body">
		    <div class="row pull-right">
			    <button type="reset" class="btn btn-default" data-dismiss="modal">
					CANCEL
				</button>
				<button type="submit" class="btn btn-primary">SAVE</button>
		    </div>
	    </div>
	</form>
</div>