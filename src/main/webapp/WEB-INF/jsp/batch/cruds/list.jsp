<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${batchModelList}" var="batchModel" varStatus="number">
	<tr>
		
		<%-- <c:forEach items="${technologyModelList}" var="technologyModel" varStatus="number">
			<c:if test="${batchModel.technologyId == technologyModel.id}">
				<td>${technologyModel.name}</td>
			</c:if>
		</c:forEach> --%>
		
		<td>${batchModel.technologyModel.name}</td>
		<td>${batchModel.name}</td>
		<td>${batchModel.trainerModel.name}</td>
		
		<%-- <c:forEach items="${trainerModelList}" var="trainerModel" varStatus="number">
			<c:if test="${batchModel.trainerId == trainerModel.id}">
				<td>${trainerModel.name}</td>
			</c:if>
		</c:forEach> --%>
		
		
		<td>
			<%-- <button type="button" class="btn btn-warning" value="${batchModel.id}" id="btn-detail">Lihat</button>
			<button type="button" class="btn btn-success" value="${batchModel.id}" id="btn-edit">Ubah</button>
			<button type="button" class="btn btn-danger" value="${batchModel.id}" id="btn-delete">Hapus</button> --%>
			<div class="btn-group">
              <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-list-ul"></i></button>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#" id="menu-edit" data-value="${batchModel.id}">Edit</a></li>
                <li><a href="#" id="menu-add-participant" data-value="${batchModel.id}">Add Participant</a></li>
                <li><a href="#" id="menu-setup-test" data-value="${batchModel.id}">Setup Test</a></li>
                <%-- <li><a href="#" id="menu-delete" data-value="${batchModel.id}">Delete</a></li> --%>
                
                <%-- <li><button type="button" class="btn" value="${batchModel.id}" id="menu-edit">Edit</button></li>
                <li><button type="button" class="btn" value="${batchModel.id}" id="menu-delete">Delete</button></li> --%>
              </ul>
            </div>
		</td>
	</tr>
</c:forEach>