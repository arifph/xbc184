<div class="panel" style="margin-top: 50px; min-height:500px;">
	<section class="content-header">
		<h1>BATCH</h1>
	</section>
	
	<section class="content">
		<div class="box box-primary">
	<!-- <div class="panel" style="background: white; margin-top: 50px; min-height:500px;"> -->
		<!-- <h1>BATCH</h1> -->
		<div>
			<table class="table">
				<tr>
					<td>
						<!-- <div class="input-group input-group-sm col-xs-7"> -->
							<form action="#" method="get" id="form-batch-search">
								<!-- <div class="row">
									<input type="text" id="keyword" name="keyword" placeholder="Search by technology / majors "/>
									<div class="input-group-btn">
										<button class="btn btn-primary btn-flat fa fa-search" type="submit"></button>
									</div>
								</div> -->
								<div class="row">
									<div class="col-xs-4">
										<input class="form-control" type="text" id="keyword" name="keyword" placeholder="Search by technology / name"/>
									</div>
									<button class="btn btn-primary btn-flat fa fa-search" type="submit"></button>
								</div>
							</form>
						<!-- </div> -->
					</td>
					<td></td>
					<td></td>
					<td>
						<button type="button" class="btn btn-primary fa fa-plus" id="btn-add"></button>
					</td>
				</tr>	
			</table>
		</div>
		
		<div>
			<table class="table" id="tbl-batch">
				<tr>
					<td>TECHNOLOGY</td>
					<td>NAME</td>
					<td>TRAINER</td>
					<td>#</td>
				</tr>
				<tbody id="list-batch">
					
				</tbody>
			</table>
		</div>
	</div>
	
	<div id="modal-input" class="modal modal-primary fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<!-- tombol close di popupnya -->
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h4>BATCH</h4>
				</div>
				<div class="modal-body">
					<div class="modal fade">
						<!-- tempat jsp popupnya -->
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="modal-update" class="modal modal-primary fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<!-- tombol close di popupnya -->
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h4>EDIT BATCH</h4>
				</div>
				<div class="modal-body">
					<!-- tempat jsp popupnya -->
				</div>
			</div>
		</div>
	</div>
	
	<div id="modal-add-participant" class="modal modal-primary fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<!-- tombol close di popupnya -->
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h4>ADD PARTICIPANT</h4>
				</div>
				<div class="modal-body">
					<!-- tempat jsp popupnya -->
				</div>
			</div>
		</div>
	</div>
	
	<div id="modal-setup-test" class="modal modal-primary fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<!-- tombol close di popupnya -->
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h4>SETUP TEST</h4>
				</div>
				<div class="modal-body">
					<!-- tempat jsp popupnya -->
				</div>
			</div>
		</div>
	</div>
	
	<div id="modal-delete" class="modal modal-warning fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<!-- tombol close di popupnya -->
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h4>DELETE</h4>
				</div>
				<div class="modal-body">
					<!-- tempat jsp popupnya -->
				</div>
			</div>
		</div>
	</div>
	</section>
</div>

<script>

	loadListBatch(); // untuk memanggil otomatis

	function loadListBatch(){ // fungsi ini tidak bisa jalan otomatis tanpa line di atas, karena di luar document.ready
		$.ajax({
			url:'batch/cruds/list.html',
			type: 'get',
			dataType: 'html',
			success: function(result) {
				$('#list-batch').html(result); // fungsi ini memanggil id list-batch
			}
		});
	}
	
	/* function changeChoice(){
		$("#btnChoose").html('CANCEL');
	} */
	
	/* function changeChoice(){
		var elem = document.getElementById(btnChoose);
	    if (elem.value=="Choose") elem.value = "CANCEL";
	    else elem.value = "Choose";
	} */
	
	/* function changeChoice(){
		var elem;
		for(var i=1; i<=buttonId; i++){
			elem = document.getElementById(i);
			if (elem.value=="Choose") elem.value = "CANCEL";
		    else elem.value = "Choose";
		}
	} */
	
	/* function changeChoice(objButton){
		if (objButton.value=="Choose") objButton.value = "CANCEL";
	    else objButton.value = "Choose";
	} */
	
	/* var chosen = false;
	function changeChoice(nilai){
		// alert(nilai);
		if(chosen){
			$("#btn"+nilai).attr('value', 'Choose');
			$("#btn"+nilai).attr('class', 'btn btn-info btn-flat');
			chosen = false;
		}
		else {
			$("#btn"+nilai).attr('value', 'CANCEL');
			$("#btn"+nilai).attr('class', 'btn btn-warning btn-flat');
			chosen = true;
		}
	} */
	
	var chosen;
	function changeChoice(nilai){
		if($("#pilihan"+nilai).val() == "true") chosen = true;
		else chosen = false;
		
		if(chosen){
			$("#btn"+nilai).attr('value', 'Choose');
			$("#btn"+nilai).attr('class', 'btn btn-info btn-flat');
			$("#pilihan"+nilai).attr('value', 'false');
			//chosen = false;
		}
		else {
			$("#btn"+nilai).attr('value', 'CANCEL');
			$("#btn"+nilai).attr('class', 'btn btn-warning btn-flat');
			$("#pilihan"+nilai).attr('value', 'true');
			//chosen = true;
		}
	}

	//document ready itu setelah halamannya dipanggil
	$(document).ready(function(){
		
		// fungsi ajax untuk memunculkan POP UP tambah
		$('#btn-add').on('click',function(){
			$.ajax({
				url:'batch/cruds/add.html',
				type:'get',
				dataType:'html',
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		// akhir fungsi ajax untuk memunculkan POP UP tambah
		
		// fungsi ajax untuk menambah batch (tombol create pada popup tambah)
		$('#modal-input').on('submit', '#form-batch-add',function(){
			$.ajax({
				url:'batch/cruds/create.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully saved ! ");
					loadListBatch();
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk menambah batch
		
		// fungsi ajax untuk menambah batch (tombol create pada popup tambah)
	//	$('#modal-input').on('submit', '#form-batch-add',function(){
	//		$.ajax({
	//			url:'batch/cruds/create.json',
	//			type:'get',
	//			dataType:'json',
	//			data:$(this).serialize(),
	//			success: function(result){
	//				/* if (result.jumlahDataBatch > 0) {
	//					alert("ERROR: kode batch " + result.kodeBatch + " sudah ada di database!");
	//				}
	//				else */ /* if (result.jumlahDataBatch2 > 0) {
	//					alert("ERROR: nama batch " + result.namaBatch + " sudah ada di database!");
	//				} */
	//				//else {
	//					$('#modal-input').modal('hide');
	//					alert("data telah ditambahkan");
	//					loadListBatch();
	//				//}
	//			}
	//		});
	//		return false;
	//	});
		// akhir fungsi ajax untuk menambah batch
		
		// fungsi ajax untuk memunculkan POP UP lihat
	//	$('#list-batch').on('click', '#btn-detail', function(){
	//		var idBatch = $(this).val();
	//		$.ajax({
	//			url:'batch/cruds/detail.html',
	//			type:'get',
	//			dataType:'html',
	//			data:{idBatch:idBatch},
	//			success: function(result){
	//				$('#modal-input').find('.modal-body').html(result);
	//				$('#modal-input').modal('show');
	//			}
	//		});
	//	});
		// akhir fungsi ajax untuk lihat
		
		// fungsi ajax untuk memunculkan POP UP ubah
		$('#list-batch').on('click', '#menu-edit', function(){
			var id = $(this).attr('data-value');
			$.ajax({
				url:'batch/cruds/edit.html',
				type:'get',
				dataType:'html',
				data:{id:id},
				success: function(result){
					$('#modal-update').find('.modal-body').html(result);
					$('#modal-update').modal('show');
				}
			});
		});
		// akhir fungsi ajax untuk ubah
		
		// fungsi ajax untuk update batch
		$('#modal-update').on('submit', '#form-batch-edit', function(){
			
			$.ajax({
				url:'batch/cruds/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-update').modal('hide');
					alert("Data successfully updated ! ");
					loadListBatch();
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk update batch
		
		// fungsi ajax untuk memunculkan POP UP add participant
		$('#list-batch').on('click', '#menu-add-participant', function(){
			var id = $(this).attr('data-value');
			$.ajax({
				url:'batch/cruds/add-participant.html',
				type:'get',
				dataType:'html',
				data:{id:id},
				success: function(result){
					$('#modal-add-participant').find('.modal-body').html(result);
					$('#modal-add-participant').modal('show');
				}
			});
		});
		// akhir fungsi ajax untuk POP UP add participant
		
		// fungsi ajax untuk add participant
		$('#modal-add-participant').on('submit', '#form-batch-add-participant', function(){
			
			$.ajax({
				url:'batch/cruds/insert-participant.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-add-participant').modal('hide');
					alert("Data successfully saved ! ");
					loadListBatch();
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk add participant
		
		// fungsi ajax untuk memunculkan POP UP setup test
		$('#list-batch').on('click', '#menu-setup-test', function(){
			var id = $(this).attr('data-value');
			$.ajax({
				url:'batch/cruds/setup-test.html',
				type:'get',
				dataType:'html',
				data:{id:id},
				success: function(result){
					$('#modal-setup-test').find('.modal-body').html(result);
					$('#modal-setup-test').modal('show');
				}
			});
		});
		// akhir fungsi ajax untuk POP UP setup test
		
		/* $('#btnChoose').on('click', function(){
			$("#btnChoose").attr('value', 'CANCEL');
		}); */
		
		/* // fungsi ajax untuk setup test
		$('#modal-setup-test').on('submit', '#form-batch-setup-test', function(){
			
			$.ajax({
				url:'batch/cruds/modify-test.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-setup-test').modal('hide');
					alert("Data successfully saved ! ");
					loadListBatch();
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk setup test */
		
		// fungsi ajax untuk setup test
		/* var chosen = false; */
		/* var btn = $('#btn').val(); */
		$('#modal-setup-test').on('submit', '#form-batch-setup-test', function(){
			/* if(chosen){
				$("#btnChoose").attr('value', 'Choose');
				$("#btnChoose").attr('class', 'btn btn-info btn-flat');
				chosen = false;
			}
			else {
				$("#btnChoose").attr('value', 'CANCEL');
				$("#btnChoose").attr('class', 'btn btn-warning btn-flat');
				chosen = true;
			} */
			/* $.each(btnChoose, function(i, btnChoose[i]){
				if(chosen){
					$("#btnChoose[i]").attr('value', 'Choose');
					$("#btnChoose[i]").attr('class', 'btn btn-info btn-flat');
					chosen = false;
				}
				else {
					$("#btnChoose[i]").attr('value', 'CANCEL');
					$("#btnChoose[i]").attr('class', 'btn btn-warning btn-flat');
					chosen = true;
				}
			}
			); */
			
			/* if(chosen){
				$("#btn4").attr('value', 'Choose');
				$("#btn4").attr('class', 'btn btn-info btn-flat');
				chosen = false;
			}
			else {
				$("#btn4").attr('value', 'CANCEL');
				$("#btn4").attr('class', 'btn btn-warning btn-flat');
				chosen = true;
			} */
			
			/* int i = 2; */
			/* if(chosen){
				$("#".btn).attr('value', 'Choose');
				$("#".btn).attr('class', 'btn btn-info btn-flat');
				chosen = false;
			}
			else {
				$("#".btn).attr('value', 'CANCEL');
				$("#".btn).attr('class', 'btn btn-warning btn-flat');
				chosen = true;
			} */
			
			$.ajax({
				url:'batch/cruds/modify-test.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					alert("Data successfully saved ! ");
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk setup test
		
		/* // fungsi ajax untuk memunculkan POP UP hapus
		$('#list-batch').on('click', '#menu-delete', function(){
			// var idBatch = $(this).val();
			var id = $(this).attr('data-value');
			$.ajax({
				url:'batch/cruds/remove.html',
				type:'get',
				dataType:'html',
				data:{id:id},
				success: function(result){
					$('#modal-delete').find('.modal-body').html(result);
					$('#modal-delete').modal('show');
				}
			});
		});
		// akhir fungsi ajax untuk hapus
		
		// fungsi ajax untuk delete batch
		$('#modal-delete').on('submit', '#form-batch-delete', function(){
			// var id = $(this);
			$.ajax({
				url:'batch/cruds/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-delete').modal('hide');
					alert("Data successfully deleted ! ");
					loadListBatch();
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk delete batch */
		
		// fungsi ajax untuk mencari batch berdasarkan technology atau name
		$('#form-batch-search').on('submit',function(){
			$.ajax({
				url:'batch/cruds/search.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					/* alert("data telah dicari berdasarkan nama / technology"); */
					$('#list-batch').html(result);
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk mencari batch berdasarkan technology atau name
		
		// fungsi ajax untuk mencari batch berdasarkan kode
	//	$('#form-batch-search-kode').on('submit',function(){
	//		$.ajax({
	//			url:'batch/cruds/search/kode.html',
	//			type:'get',
	//			dataType:'html',
	//			data:$(this).serialize(),
	//			success: function(result){
	//				alert("data telah dicari berdasarkan kode");
	//				$('#list-batch').html(result);
	//			}
	//		});
	//		return false;
	//	});
		// akhir fungsi ajax untuk mencari batch berdasarkan kode
		
		// fungsi ajax untuk mencari berdasarkan kode / nama batch
	//	$('#form-batch-search').on('submit',function(){
	//		$.ajax({
	//			url:'batch/cruds/search.html',
	//			type:'get',
	//			dataType:'html',
	//			data:$(this).serialize(),
	//			success: function(result){
	//				// alert("data telah dicari berdasarkan keyword");
	//				$('#list-batch').html(result);
	//			}
	//		});
	//		return false;
	//	});
		// akhir fungsi ajax untuk mencari berdasarkan kode / nama batch
	});
	// akhir dari dokumen ready
</script>