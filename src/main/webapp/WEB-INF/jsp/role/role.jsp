<div class= "panel" style = "background: white; margin-top: 50px; min-height: 500px">
	<div>
		<table class= "table" id= "tbl-header-role">
			<tr bgcolor= "#0066ff">
				<td><h4><b><font color="white">ROLE</font></b></h4></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
	
	<div>
		<table class= "table" id ="tbl-role" style= "width: 70%; margin-left: 15%; margin-right: 15%; table-layout:fixed">
			<tr>
				<td>
					<form action= "#" method= "get" id= "form-role-search-nama">
						<input type= "text" id= "namaRoleKey" name= "namaRoleKey" size= "15" placeholder="Search by name"/>
						<button class= "btn btn-primary" type= "submit">Search</button>
					</form>
				</td>
				<td></td>
				<td><button type = "button" class= "btn btn-primary" id= "btn-add">Add</button></td>
			</tr>
			<tr></tr>
			<tr bgcolor="#3399ff">
				<td>CODE</td>
				<td>NAME</td>
				<td align= "center">#</td>
			</tr>
			<tbody id= "list-role">
			</tbody>
		</table>
	</div>
</div>

<div id= "modal-input" class= "modal fade">
	<div class= "modal-dialog">
		<div class= "modal-content">
			<div class= "modal-header">
			</div>
			<div class= "modal-body">
				<!-- Tempat jsp PopUp -->
			</div>
		</div>
	</div>
</div>

<script>

	loadListRole();
	
	function loadListRole() {
		$.ajax({
			url: 'role/cruds/list.html',
			type: 'get',
			dataType: 'html',
			success: function(result) {
				$('#list-role').html(result);
			}
		})
	}
	
	//Document ready setelah halaman dipanggil
	$(document).ready(function() {
		
		//Fungsi ajax untuk popup tambah
		$('#btn-add').on('click', function() {
			$.ajax({
				url: 'role/cruds/add.html',
				type: 'get',
				dataType: 'html',
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		})
		//Akhir fungsi ajax untuk popup tambah
		
		//Fungsi ajax untuk create tambah
		$('#modal-input').on('submit', '#form-role-add', function() {
			$.ajax({
				url: 'role/cruds/create.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(result){
					
					if (result.jumlahDataRole > 0) {
						alert("Role Name "+result.namaRole+" Already Exist !");
					} else {
						$('#modal-input').modal('hide');
						alert("Data successfully saved !");
						loadListRole();
					}
				}
			});
			return false;
		});
		//Akhir fungsi ajax untuk create tambah
		
		//Fungsi ajax untuk ubah
		$('#list-role').on('click', '#btn-edit', function() {
			var idRole = $(this).attr("data-value");
			$.ajax({
				url: 'role/cruds/edit.html',
				type: 'get',
				dataType: 'html',
				data: {idRole: idRole},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		})
		//Akhir fungsi ajax untuk ubah
		
		//Fungsi ajax untuk update
		$('#modal-input').on('submit', '#form-role-edit', function() {
			$.ajax({
				url: 'role/cruds/update.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("Data successfully Updated !");
					loadListRole();
				}
			});
			return false;
		})
		//Akhir fungsi ajax untuk update
		
		//Fungsi ajax untuk hapus
		$('#list-role').on('click', '#btn-delete', function() {
			//var idRole = $(this).val();
			var idRole = $(this).attr("data-value");
			$.ajax({
				url: 'role/cruds/remove.html',
				type: 'get',
				dataType: 'html',
				data: {idRole: idRole},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		})
		//Akhir fungsi ajax untuk hapus
		
		//Fungsi ajax untuk delete
		$('#modal-input').on('submit', '#form-role-delete', function() {
			$.ajax({
				url: 'role/cruds/delete.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data successfully deleted !");
					loadListRole();
				}
			});
			return false;
		})
		//Akhir fungsi ajax untuk delete
		
		//Fungsi ajax untuk search nama
		$('#form-role-search-nama').on('submit', function() {
			$.ajax({
				url: 'role/cruds/search/nama.html',
				type: 'get',
				dataType: 'html',
				data: $(this).serialize(),
				success: function(result){
					$('#list-role').html(result);
				}
			});
			return false;
		})
		//Akhir fungsi ajax untuk search nama
		
	});
</script>	