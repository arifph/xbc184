<%@ taglib uri= "http://java.sun.com/jstl/core_rt" prefix= "c" %>
<%@ taglib uri= "http://java.sun.com/jsp/jstl/fmt" prefix= "fmt" %>

<c:forEach items= "${roleModelList}" var= "roleModel" varStatus= "number">
	<tr>
		<td>${roleModel.kodeRole}</td>
		<td>${roleModel.namaRole}</td>
		<td>
			<div class= "dropdown" align= "center">
				<div class= "button-group" align= "center">
					<button type= "button" class= "btn-box-tool dropdown-toggler" data-toggle= "dropdown">
						<i class= "fa fa-navicon"></i>
					</button>
					<ul id= "menu" class= "dropdown-menu" role= "menu">
						<li><a id= "btn-edit" data-value= "${roleModel.idRole}">Edit</a></li>
						<li><a id= "btn-delete" data-value= "${roleModel.idRole}">Delete</a></li>
					</ul>
				</div>
			</div>
		</td>
	</tr>
</c:forEach>