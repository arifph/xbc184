<%@ taglib uri= "http://java.sun.com/jstl/core_rt" prefix= "c" %>
<div class= "form-horizontal">
	<div>
		<table class= "table" id= "tbl-header-role">
			<tr bgcolor= "#0066ff">
				<td><h4><b><font color="white">EDIT ROLE</font></b></h4></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
	
	<form action= "#" method= "get" id= "form-role-edit">
	<input type= "hidden" id= "idRole" name= "idRole" value= "${roleModel.idRole}" />
	<input type= "hidden" id= "kodeRole" name= "kodeRole" value= "${roleModel.kodeRole}" />
		<table style= "width: 80%; margin-left: 10%; margin-right: 10%; border-collapse: separate; border-spacing: 15px;">
			<tr>
				<td><input type= "text" placeholder= "${roleModel.kodeRole}" size= 53% disabled/></td>
			</tr>
			
			<tr>
				<td><input type= "text" id= "namaRole" name= "namaRole" value= "${roleModel.namaRole}" size= 53%/></td>
			</tr>

			<tr>
				<td><textarea rows= "4" cols= "55%" name= "deskripsi" style= "resize: none">${roleModel.deskripsi}</textarea>
			</tr>
			
			<%-- <tr>
				<td><input type= "text" id= "deskripsi" name= "deskripsi" value= "${roleModel.deskripsi}"/></td>
			</tr> --%>
		</table>
		
		<button type= "submit" style="float: right; margin-right: 10%; margin-bottom: 15%;">SAVE</button>
		<button type= "button" data-dismiss= "modal" style="float: right; margin-right: 5%; margin-bottom: 15%;">CANCEL</button>

	</form>
	
	<hr>
	
</div>