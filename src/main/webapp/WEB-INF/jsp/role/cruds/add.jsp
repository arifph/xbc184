<%@ taglib uri= "http://java.sun.com/jstl/core_rt" prefix= "c" %>
<div class= "form-horizontal">
	<div>
		<table class= "table" id= "tbl-header-role">
			<tr bgcolor= "#0066ff">
				<td><h4><b><font color="white">ADD ROLE</font></b></h4></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
	
	<form action= "#" method= "get" id= "form-role-add">
		<table style= "width: 80%; margin-left: 10%; margin-right: 10%; border-collapse: separate; border-spacing: 15px;">
			<tr>
				<td><input type= "text" id= "kodeRole" name= "kodeRole" placeholder= "Code" size= 53%/></td>
			</tr>
			
			<tr>
				<td><input type= "text" id= "namaRole" name= "namaRole" placeholder= "Name" size= 53%/></td>
			</tr>
			
			<tr>
				<td><textarea rows= "4" cols= "55" name= "deskripsi" placeholder= "Description" style= "resize: none"></textarea>
			</tr>
			
			<!-- tr>
				<td><input type= "text" id= "deskripsi" name= "deskripsi" placeholder= "Description" size= 60%/></td>
			</tr> -->
		</table>
		
		<button type= "submit" style="float: right; margin-right: 10%; margin-bottom: 15%;">SAVE</button>
		<button type= "button" data-dismiss= "modal" style="float: right; margin-right: 5%; margin-bottom: 15%;">CANCEL</button>

	</form>
	
	<hr>
	
</div>